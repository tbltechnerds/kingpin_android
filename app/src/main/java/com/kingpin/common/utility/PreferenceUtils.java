package com.kingpin.common.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.ArrayList;

public class PreferenceUtils {

    public static void putPreference(Context context, String key, String value) {
        if(context != null) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            preferences.edit().putString(key, value).apply();
        }
    }

    public static void putPreference(Context context, String key, long value) {
        if(context != null) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            preferences.edit().putLong(key, value).apply();
        }
    }

    public static void putPreference(Context context, String key, int value) {
        if(context != null) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            preferences.edit().putInt(key, value).apply();
        }
    }

    public static void putPreference(Context context, String key, float value) {
        if(context != null) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            preferences.edit().putFloat(key, value).apply();
        }
    }

    public static void putPreference(Context context, String key, boolean value) {
        if(context != null) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            preferences.edit().putBoolean(key, value).apply();
        }
    }

    public static void putIntArrayPreference(Context context, String key, ArrayList<Integer> value) {
        if(context != null) {
            StringBuilder builder = new StringBuilder();
            if(value != null) {
                for(int item : value) {
                    builder.append(item);
                    builder.append(",");
                }
            }
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            preferences.edit().putString(key, builder.toString()).apply();
        }
    }

    public static String getStringPreference(Context context, String key) {
        if(context != null) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            return preferences.getString(key, "");
        }
        return "";
    }

    public static long getLongPreference(Context context, String key) {
        if(context != null) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            return preferences.getLong(key, 0);
        }
        return 0;
    }

    public static int getIntPreference(Context context, String key) {
        if(context != null) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            return preferences.getInt(key, 0);
        }
        return 0;
    }

    public static float getFloatPreference(Context context, String key) {
        if(context != null) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            return preferences.getFloat(key, 0.0f);
        }
        return 0.0f;
    }

    public static boolean getBooleanPreference(Context context, String key) {
        if(context != null) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            return preferences.getBoolean(key, false);
        }
        return false;
    }

    public static ArrayList<Integer> getIntArrayPreference(Context context, String key) {
        ArrayList<Integer> result = new ArrayList<>();
        if(context != null) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            String savedString = preferences.getString(key, "");
            if(savedString != null && !savedString.isEmpty()) {
                String[] items = savedString.split(",");
                for(String item: items)
                    result.add(Integer.parseInt(item));
            }
        }
        return result;
    }
}
