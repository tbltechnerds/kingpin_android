package com.kingpin.common.utility;

import android.support.annotation.NonNull;

import com.kingpin.common.base.APIService;
import com.kingpin.listener.ApiListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class APIUtils {

    private static APIUtils mInstance = null;
    private APIClient mAPIClient;

    public static APIUtils getInstance() {
        if(mInstance == null)
            mInstance = new APIUtils();
        return mInstance;
    }

    private APIUtils() {
        mAPIClient = new APIClient();
    }

    public void signup(JSONObject data, final ApiListener callback, final int api_kind) {
        APIService service = mAPIClient.getApiService();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), String.valueOf(data));
        Call<ResponseBody> call = service.signup(requestBody);
        callAPI(call, callback, api_kind);
    }

    public void signin(JSONObject data, final ApiListener callback, final int api_kind) {
        APIService service = mAPIClient.getApiService();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), String.valueOf(data));
        Call<ResponseBody> call = service.signin(requestBody);
        callAPI(call, callback, api_kind);
    }

    public void getMonthlyPot(JSONObject data, final ApiListener callback, final int api_kind) {
        APIService service = mAPIClient.getApiService();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), String.valueOf(data));
        Call<ResponseBody> call = service.getMonthlyPot(requestBody);
        callAPI(call, callback, api_kind);
    }

    public void getBalance(JSONObject data, final ApiListener callback, final int api_kind) {
        APIService service = mAPIClient.getApiService();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), String.valueOf(data));
        Call<ResponseBody> call = service.getBalance(requestBody);
        callAPI(call, callback, api_kind);
    }

    public void signout(JSONObject data, final ApiListener callback, final int api_kind) {
        APIService service = mAPIClient.getApiService();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), String.valueOf(data));
        Call<ResponseBody> call = service.signout(requestBody);
        callAPI(call, callback, api_kind);
    }

    public void requestCode(JSONObject data, final ApiListener callback, final int api_kind) {
        APIService service = mAPIClient.getApiService();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), String.valueOf(data));
        Call<ResponseBody> call = service.forgotPassword(requestBody);
        callAPI(call, callback, api_kind);
    }

    public void resetPassword(JSONObject data, final ApiListener callback, final int api_kind) {
        APIService service = mAPIClient.getApiService();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), String.valueOf(data));
        Call<ResponseBody> call = service.resetPassword(requestBody);
        callAPI(call, callback, api_kind);
    }

    public void signin_fb(JSONObject data, final ApiListener callback, final int api_kind) {
        APIService service = mAPIClient.getApiService();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), String.valueOf(data));
        Call<ResponseBody> call = service.signin_fb(requestBody);
        callAPI(call, callback, api_kind);
    }

    public void getUpcomeGames(JSONObject data, final ApiListener callback, final int api_kind) {
        APIService service = mAPIClient.getApiService();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), String.valueOf(data));
        Call<ResponseBody> call = service.getUpcomeGames(requestBody);
        callAPI(call, callback, api_kind);
    }

    public void getTopKingpins(JSONObject data, final ApiListener callback, final int api_kind) {
        APIService service = mAPIClient.getApiService();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), String.valueOf(data));
        Call<ResponseBody> call = service.getTopKingpins(requestBody);
        callAPI(call, callback, api_kind);
    }

    public void getKingpinDetail(JSONObject data, final ApiListener callback, final int api_kind) {
        APIService service = mAPIClient.getApiService();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), String.valueOf(data));
        Call<ResponseBody> call = service.getKingpinDetail(requestBody);
        callAPI(call, callback, api_kind);
    }

    public void getKingpinHistory(JSONObject data, final ApiListener callback, final int api_kind) {
        APIService service = mAPIClient.getApiService();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), String.valueOf(data));
        Call<ResponseBody> call = service.getKingpinHistory(requestBody);
        callAPI(call, callback, api_kind);
    }

    public void getKingpinUpcoming(JSONObject data, final ApiListener callback, final int api_kind) {
        APIService service = mAPIClient.getApiService();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), String.valueOf(data));
        Call<ResponseBody> call = service.getKingpinUpcoming(requestBody);
        callAPI(call, callback, api_kind);
    }

    public void getRealBalance(JSONObject data, final ApiListener callback, final int api_kind) {
        APIService service = mAPIClient.getApiService();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), String.valueOf(data));
        Call<ResponseBody> call = service.getRealBalance(requestBody);
        callAPI(call, callback, api_kind);
    }

    public void clearBetHistory(JSONObject data, final ApiListener callback, final int api_kind) {
        APIService service = mAPIClient.getApiService();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), String.valueOf(data));
        Call<ResponseBody> call = service.clearBetHistory(requestBody);
        callAPI(call, callback, api_kind);
    }

    public void payoutWithPaypal(JSONObject data, final ApiListener callback, final int api_kind) {
        APIService service = mAPIClient.getApiService();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), String.valueOf(data));
        Call<ResponseBody> call = service.payoutWithPaypal(requestBody);
        callAPI(call, callback, api_kind);
    }

    public void payoutWithBank(JSONObject data, final ApiListener callback, final int api_kind) {
        APIService service = mAPIClient.getApiService();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), String.valueOf(data));
        Call<ResponseBody> call = service.payoutWithBank(requestBody);
        callAPI(call, callback, api_kind);
    }

    public void updateProfile(JSONObject data, String filePath, final ApiListener callback, final int api_kind) {
        APIService service = mAPIClient.getApiService();
        Map<String, RequestBody> map = new HashMap<>();

        if(!filePath.isEmpty()) {
            File file = new File(filePath);
            RequestBody bitmapBody = RequestBody.create(MediaType.parse("image/jpg"), file);
            map.put("image\"; filename=\"profile.jpg\"", bitmapBody);
        }

        Iterator<String> iter = data.keys();
        while(iter.hasNext()) {
            String key = iter.next();
            try {
                Object value = data.get(key);
                RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(value));
                map.put(key, requestBody);
            }
            catch(JSONException e) {
                e.printStackTrace();
            }
        }

        Call<ResponseBody> call = service.updateProfile(map);
        callAPI(call, callback, api_kind);
    }

    public void getPaymentHistory(JSONObject data, final ApiListener callback, final int api_kind) {
        APIService service = mAPIClient.getApiService();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), String.valueOf(data));
        Call<ResponseBody> call = service.getPaymentHistory(requestBody);
        callAPI(call, callback, api_kind);
    }

    public void getAffiliateHistory(JSONObject data, final ApiListener callback, final int api_kind) {
        APIService service = mAPIClient.getApiService();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), String.valueOf(data));
        Call<ResponseBody> call = service.getAffiliateHistory(requestBody);
        callAPI(call, callback, api_kind);
    }

    public void purchasedSubscribe(JSONObject data, final ApiListener callback, final int api_kind) {
        APIService service = mAPIClient.getApiService();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), String.valueOf(data));
        Call<ResponseBody> call = service.purchasedSubscribe(requestBody);
        callAPI(call, callback, api_kind);
    }

    private void callAPI(Call<ResponseBody> call, final ApiListener callback, final int api_kind) {
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if(response.code() == 200) {
                    try {
                        ResponseBody body = response.body();
                        if(body != null) {
                            String bodyString = new String(body.bytes());
                            if(callback != null)
                                callback.onAPISuccess(api_kind, bodyString);
                        }
                        else {
                            if(callback != null)
                                callback.onAPIError(api_kind, response);
                        }
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                        if(callback != null)
                            callback.onAPIError(api_kind, response);
                    }
                }
                else {
                    if(callback != null)
                        callback.onAPIError(api_kind, response);
                }

            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if(callback != null)
                    callback.onAPIFailure(api_kind);
            }
        });
    }

    public void getGameList(JSONObject data, final ApiListener callback, final int api_kind) {
        APIService service = mAPIClient.getApiService();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), String.valueOf(data));
        Call<ResponseBody> call = service.getGameList(requestBody);
        callAPI(call, callback, api_kind);
    }

    public void saveBets(JSONObject data, final ApiListener callback, final int api_kind) {
        APIService service = mAPIClient.getApiService();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), String.valueOf(data));
        Call<ResponseBody> call = service.saveBets(requestBody);
        callAPI(call, callback, api_kind);
    }

    public void follow(JSONObject data, final ApiListener callback, final int api_kind) {
        APIService service = mAPIClient.getApiService();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), String.valueOf(data));
        Call<ResponseBody> call = service.follow(requestBody);
        callAPI(call, callback, api_kind);
    }

    public void unfollow(JSONObject data, final ApiListener callback, final int api_kind) {
        APIService service = mAPIClient.getApiService();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), String.valueOf(data));
        Call<ResponseBody> call = service.unfollow(requestBody);
        callAPI(call, callback, api_kind);
    }

    public void getMyKingpins(JSONObject data, final ApiListener callback, final int api_kind) {
        APIService service = mAPIClient.getApiService();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), String.valueOf(data));
        Call<ResponseBody> call = service.getMyKingpins(requestBody);
        callAPI(call, callback, api_kind);
    }

    public void sendFeedback(JSONObject data, final ApiListener callback, final int api_kind) {
        APIService service = mAPIClient.getApiService();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), String.valueOf(data));
        Call<ResponseBody> call = service.sendFeedback(requestBody);
        callAPI(call, callback, api_kind);
    }
}
