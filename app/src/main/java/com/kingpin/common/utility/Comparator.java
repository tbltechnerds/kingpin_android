package com.kingpin.common.utility;

import com.kingpin.mvp.model.Bet;
import com.kingpin.mvp.model.UpcomingGameDetail;

import java.util.Date;

public class Comparator {
    public static class DateComparator1 implements java.util.Comparator<Bet> {
        public int compare(Bet first, Bet second) {
            Date first_date = first.getMatchDate();
            Date second_date = second.getMatchDate();
            return second_date.compareTo(first_date);
        }
    }

    public static class DateComparator2 implements java.util.Comparator<Bet> {
        public int compare(Bet first, Bet second) {
            Date first_date = first.getMatchDate();
            Date second_date = second.getMatchDate();
            return first_date.compareTo(second_date);
        }
    }

    public static class BetAmountComparator1 implements java.util.Comparator<Bet> {
        public int compare(Bet first, Bet second) {
            int first_amount = first.getBetAmount();
            int second_amount = second.getBetAmount();

            return Integer.compare(second_amount, first_amount);
        }
    }

    public static class BetAmountComparator2 implements java.util.Comparator<Bet> {
        public int compare(Bet first, Bet second) {
            int first_amount = first.getBetAmount();
            int second_amount = second.getBetAmount();

            return Integer.compare(first_amount, second_amount);
        }
    }

    public static class WonAmountComparator1 implements java.util.Comparator<Bet> {
        public int compare(Bet first, Bet second) {
            int first_amount = first.getWinLossAmount();
            int second_amount = second.getWinLossAmount();

            return Integer.compare(second_amount, first_amount);
        }
    }

    public static class WonAmountComparator2 implements java.util.Comparator<Bet> {
        public int compare(Bet first, Bet second) {
            int first_amount = first.getWinLossAmount();
            int second_amount = second.getWinLossAmount();

            return Integer.compare(first_amount, second_amount);
        }
    }

    public static class UpcomingGameDateComparator implements java.util.Comparator<UpcomingGameDetail> {
        public int compare(UpcomingGameDetail first, UpcomingGameDetail second) {
            Date first_date = first.getMatchDateTime();
            Date second_date = second.getMatchDateTime();
            return first_date.compareTo(second_date);
        }
    }
}
