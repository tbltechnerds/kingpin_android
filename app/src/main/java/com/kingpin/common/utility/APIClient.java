package com.kingpin.common.utility;

import android.support.annotation.NonNull;

import com.kingpin.common.base.APIService;
import com.kingpin.common.base.Constant;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {

    APIClient() {

    }

    public APIService getApiService() {
        return getRetroClient().create(APIService.class);
    }

    private Retrofit getRetroClient() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(@NonNull Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder requestBuilder = original.newBuilder();
                requestBuilder.addHeader("Content-Type", "application/json");

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        return new Retrofit.Builder()
                .baseUrl(Constant.SERVER_HOST)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}
