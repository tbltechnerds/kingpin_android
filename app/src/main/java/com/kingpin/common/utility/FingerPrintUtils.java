package com.kingpin.common.utility;

import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.support.annotation.RequiresApi;

import com.kingpin.R;
import com.kingpin.listener.AuthListener;
import com.kingpin.ui.activity.AuthActivity;

@RequiresApi(api = Build.VERSION_CODES.M)
@SuppressWarnings("deprecation")
public class FingerPrintUtils  extends FingerprintManager.AuthenticationCallback {

    private final FingerprintManager mFingerprintManager;
    private CancellationSignal mCancellationSignal;
    private AuthListener mAuthListener;

    public FingerPrintUtils(FingerprintManager fingerprintManager, AuthListener authListener) {
        this.mFingerprintManager = fingerprintManager;
        this.mAuthListener = authListener;
    }

    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
        mAuthListener.onAuthError(errString.toString());
    }

    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
        mAuthListener.onAuthFailed(helpString.toString());
    }

    @Override
    public void onAuthenticationFailed() {
        if(mAuthListener instanceof AuthActivity) {
            AuthActivity activity = (AuthActivity) mAuthListener;
            mAuthListener.onAuthFailed(activity.getResources().getString(R.string.toast_fingerprint_not_recognized));
        }
    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        if(mAuthListener instanceof AuthActivity) {
            AuthActivity activity = (AuthActivity) mAuthListener;
            mAuthListener.onAuthenticated(activity.getResources().getString(R.string.toast_fingerprint_success));
        }
    }

    public void startListening(FingerprintManager.CryptoObject cryptoObject) {

        mCancellationSignal = new CancellationSignal();
        mFingerprintManager.authenticate(cryptoObject, mCancellationSignal, 0, this, null);
    }

    public void stopListening() {
        if (mCancellationSignal != null) {
            mCancellationSignal.cancel();
            mCancellationSignal = null;
        }
    }
}
