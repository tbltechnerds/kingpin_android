package com.kingpin.common.utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtils {

    private static final String DATE_FORMAT_PATTERN1 = "yyyy-MM-dd HH:mm:ss";
    private static final String DATE_FORMAT_PATTERN2 = "dd-MMM-yy";
    private static final String DATE_FORMAT_PATTERN3 = "hh:mm a";
    private static final String DATE_FORMAT_PATTERN4 = "dd-MMM-yy hh:mm a";
    private static final String DATE_FORMAT_PATTERN5 = "yyyy-MM-dd'T'HH:mm:ss";
    private static final String DATE_FORMAT_PATTERN6 = "MMM dd yy";

    public static String convertToLocalDateWithFormat(String format, String original_date) {
        SimpleDateFormat server_format = new SimpleDateFormat(DATE_FORMAT_PATTERN1, Locale.US);
        server_format.setTimeZone(TimeZone.getTimeZone("EST"));
        Date date;
        try {
            date = server_format.parse(original_date);
        }
        catch (ParseException e) {
            e.printStackTrace();
            return "";
        }

        SimpleDateFormat local_format = new SimpleDateFormat(format, Locale.US);
        local_format.setTimeZone(TimeZone.getDefault());
        return local_format.format(date);
    }

    public static String convertToLocalDate(String original_date) {
        SimpleDateFormat server_format = new SimpleDateFormat(DATE_FORMAT_PATTERN1, Locale.US);
        server_format.setTimeZone(TimeZone.getTimeZone("EST"));
        Date date;
        try {
            date = server_format.parse(original_date);
        }
        catch (ParseException e) {
            e.printStackTrace();
            return "";
        }

        SimpleDateFormat local_format = new SimpleDateFormat(DATE_FORMAT_PATTERN2, Locale.US);
        local_format.setTimeZone(TimeZone.getDefault());
        return local_format.format(date);
    }

    public static String convertUTCStringToLocalDateString(String original_date) {
        SimpleDateFormat server_format = new SimpleDateFormat(DATE_FORMAT_PATTERN5, Locale.US);
        server_format.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date;
        try {
            date = server_format.parse(original_date);
        }
        catch (ParseException e) {
            e.printStackTrace();
            return "";
        }

        SimpleDateFormat local_format = new SimpleDateFormat(DATE_FORMAT_PATTERN2, Locale.US);
        local_format.setTimeZone(TimeZone.getDefault());
        return local_format.format(date);
    }

    public static String convertToLocalTime(String original_date) {
        SimpleDateFormat server_format = new SimpleDateFormat(DATE_FORMAT_PATTERN1, Locale.US);
        server_format.setTimeZone(TimeZone.getTimeZone("America/New_York"));
        Date date;
        try {
            date = server_format.parse(original_date);
        }
        catch (ParseException e) {
            e.printStackTrace();
            return "";
        }

        SimpleDateFormat local_format = new SimpleDateFormat(DATE_FORMAT_PATTERN3, Locale.US);
        local_format.setTimeZone(TimeZone.getDefault());
        return local_format.format(date);
    }

    public static String convertUTCStringToLocalTimeString(String original_date) {
        SimpleDateFormat server_format = new SimpleDateFormat(DATE_FORMAT_PATTERN5, Locale.US);
        server_format.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date;
        try {
            date = server_format.parse(original_date);
        }
        catch (ParseException e) {
            e.printStackTrace();
            return "";
        }

        SimpleDateFormat local_format = new SimpleDateFormat(DATE_FORMAT_PATTERN3, Locale.US);
        local_format.setTimeZone(TimeZone.getDefault());
        return local_format.format(date);
    }

    public static String convertToLocalDateAndTime(String original_date) {
        SimpleDateFormat server_format = new SimpleDateFormat(DATE_FORMAT_PATTERN1, Locale.US);
        server_format.setTimeZone(TimeZone.getTimeZone("EST"));
        Date date;
        try {
            date = server_format.parse(original_date);
        }
        catch (ParseException e) {
            e.printStackTrace();
            return "";
        }

        SimpleDateFormat local_format = new SimpleDateFormat(DATE_FORMAT_PATTERN4, Locale.US);
        local_format.setTimeZone(TimeZone.getDefault());
        return local_format.format(date);
    }

    public static Date getCurrentDate() {
        return new Date();
    }

    public static Date convertToLocal(String original_date) {
        SimpleDateFormat server_format = new SimpleDateFormat(DATE_FORMAT_PATTERN1, Locale.US);
        server_format.setTimeZone(TimeZone.getTimeZone("EST"));

        try {
            return server_format.parse(original_date);
        }
        catch (ParseException e) {
            e.printStackTrace();
            return new Date();
        }
    }

    public static Date convertUTCStringToLocalDate(String original_date) {
        SimpleDateFormat server_format = new SimpleDateFormat(DATE_FORMAT_PATTERN5, Locale.US);
        server_format.setTimeZone(TimeZone.getTimeZone("UTC"));

        try {
            return server_format.parse(original_date);
        }
        catch (ParseException e) {
            e.printStackTrace();
            return new Date();
        }
    }

    public static String convertLocalDateToServerDateString(Date localDate) {
        SimpleDateFormat server_format = new SimpleDateFormat(DATE_FORMAT_PATTERN5, Locale.US);
        server_format.setTimeZone(TimeZone.getTimeZone("EST"));
        return server_format.format(localDate);
    }

    public static String getCurrentDateWithServerTimezone() {
        Date date = getCurrentDate();
        SimpleDateFormat server_format = new SimpleDateFormat(DATE_FORMAT_PATTERN1, Locale.US);
        server_format.setTimeZone(TimeZone.getTimeZone("EST"));
        return server_format.format(date);
    }

    public static Date getDateWithServerTimezone(String date_string) {
        SimpleDateFormat server_format = new SimpleDateFormat(DATE_FORMAT_PATTERN1, Locale.US);
        server_format.setTimeZone(TimeZone.getTimeZone("EST"));

        try {
            return server_format.parse(date_string);
        }
        catch (ParseException e) {
            e.printStackTrace();
            return new Date();
        }
    }

    public static String getCurrentDateWithLocalTimezone() {
        Date date = getCurrentDate();
        SimpleDateFormat local_format = new SimpleDateFormat(DATE_FORMAT_PATTERN1, Locale.US);
        return local_format.format(date);
    }
}
