package com.kingpin.common.utility;

import android.app.KeyguardManager;
import android.content.Context;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Environment;

public class CommonUtils {
    public static boolean isValidEmail(String email) {
        String emailPattern = "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        return (email.matches(emailPattern));
    }

    public static String getFilePath(Context context) {
        return context.getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "";
    }

    public static boolean enabledFingerPrint(Context context) {

        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            return false;

        KeyguardManager keyguardManager = context.getSystemService(KeyguardManager.class);
        FingerprintManager fingerprintManager = context.getSystemService(FingerprintManager.class);

        return keyguardManager != null && keyguardManager.isKeyguardSecure() &&
                fingerprintManager != null && fingerprintManager.hasEnrolledFingerprints();
    }
}
