package com.kingpin.common.task;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import com.kingpin.common.manager.FileManager;
import com.kingpin.listener.DownloadAvatarListener;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadAvatarTask extends AsyncTask<String, Void, Bitmap> {

    private DownloadAvatarListener mListener;
    private String file_path;
    private String download_link = "";

    public DownloadAvatarTask(String file_path, DownloadAvatarListener listener) {
        this.file_path = file_path;
        this.mListener = listener;
    }

    @Override
    protected Bitmap doInBackground(String... urls) {

        try {
            this.download_link = urls[0];
            if(this.download_link.isEmpty())
                return null;

            URL url = new URL(this.download_link);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);
        }
        catch (IOException e) {
            return null;
        }
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if(bitmap != null) {
            String[] url_peaces = this.download_link.split("/");
            String file_name = url_peaces[url_peaces.length - 1];
            FileManager.saveBitmap(bitmap, this.file_path, file_name);
        }

        if(this.mListener != null)
            this.mListener.DownloadAvatarCompleted(bitmap);
    }
}