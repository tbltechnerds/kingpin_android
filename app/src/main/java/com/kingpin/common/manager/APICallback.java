package com.kingpin.common.manager;

import com.kingpin.mvp.presenter.BasePresenter;

import java.util.ArrayList;

public class APICallback {

    private static APICallback mInstance = null;

    private ArrayList<BasePresenter> mPresenterList;

    public static APICallback getInstance() {
        if(mInstance == null)
            mInstance = new APICallback();
        return mInstance;
    }

    private APICallback() {
        mPresenterList = new ArrayList<>();
    }

    public void registerPresenter(BasePresenter presenter) {
        if(!this.mPresenterList.contains(presenter))
            this.mPresenterList.add(presenter);
    }

    public void unregisterPresenter(BasePresenter presenter) {
        this.mPresenterList.remove(presenter);
    }
}
