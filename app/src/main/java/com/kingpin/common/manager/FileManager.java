package com.kingpin.common.manager;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileManager {

    public static void saveBitmap(Bitmap bitmap, String file_directory, String file_name) {
        if(file_directory == null || file_directory.isEmpty())
            return;

        if(file_name == null || file_name.isEmpty())
            return;

        String filePath = file_directory + "/" + file_name;

        FileOutputStream out = null;

        try {
            out = new FileOutputStream(filePath);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            try {
                if(out != null)
                    out.close();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static Bitmap getBitmap(String file_path) {
        if(file_path == null || file_path.isEmpty())
            return null;

        File file = new File(file_path);
        if(file.exists()) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            return BitmapFactory.decodeFile(file_path, options);
        }

        return null;
    }
}
