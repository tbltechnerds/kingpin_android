package com.kingpin.common.manager;

import android.graphics.Bitmap;

import com.kingpin.common.base.Constant;
import com.kingpin.listener.FilterListener;
import com.kingpin.mvp.model.BovadaGame;
import com.kingpin.mvp.model.Game;
import com.kingpin.mvp.model.Kingpin;
import com.kingpin.mvp.model.RapidGame;
import com.kingpin.mvp.model.UpcomingGame;
import com.kingpin.mvp.model.UpcomingGameDetail;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

public class TempDataManager {

    private static TempDataManager mInstance = null;

    private ArrayList<UpcomingGame> mUpcomingGames;
    private ArrayList<Kingpin> mKingpins;
    private ArrayList<Kingpin> mMyKingpins;
    private UpcomingGameDetail mSeletedMatch;
    private Constant.FILTER_SPORT mSelectedSport;
    private int mSelectedFilter;
    private Kingpin mSelectedKingpin;
    private Integer mSelectedKingpinId;
    private boolean mShowUpcoming;
    private boolean mFilterRevert;
    private boolean mDisableTimeperiodFilter;
    private boolean mDisableActiveFilter;
    private boolean mDisableSearchUserNameFilter;
    private boolean mDisableBetTypeFilter;
    private FilterListener mFilterListener;
    private Bitmap mTempBitmap;
    private boolean mIsRegisteringToken;

    // place pick module
    private ArrayList<Game> mGames;
    private Game mSelectedGame;

    public boolean isFirstLoading; // first time of loading of upcoming game fragment

    // functions

    public static TempDataManager getInstance() {
        if(mInstance == null)
            mInstance = new TempDataManager();
        return mInstance;
    }

    private TempDataManager() {
        init();
    }

    private void init() {
        mUpcomingGames = new ArrayList<>();
        mKingpins = new ArrayList<>();
        mMyKingpins = new ArrayList<>();
        mSeletedMatch = null;
        mSelectedSport = DataManager.getInstance().getDefaultSelectedSportOnExpertBets();
        mSelectedFilter = 0;
        mSelectedKingpin = null;
        mSelectedKingpinId = null;
        mShowUpcoming = false;
        mFilterRevert = false;
        mDisableActiveFilter = false;
        mDisableSearchUserNameFilter = false;
        mDisableTimeperiodFilter = false;
        mFilterListener = null;
        mTempBitmap = null;
        mIsRegisteringToken = false;
        mGames = new ArrayList<>();

        isFirstLoading = true;
    }

    public void setUpcomingGames(ArrayList<UpcomingGame> games) {
        this.mUpcomingGames = games;
    }

    public ArrayList<UpcomingGame> getUpcomingGames() {
        return this.mUpcomingGames;
    }

    public void setKingpins(ArrayList<Kingpin> kingpins) {
        this.mKingpins = kingpins;
    }

    public void addKingpins(ArrayList<Kingpin> kingpins) {
        this.mKingpins.addAll(kingpins);
    }

    public ArrayList<Kingpin> getKingpins() {
        return this.mKingpins;
    }

    public void setMyKingpins(ArrayList<Kingpin> myKingpins) {
        mMyKingpins = myKingpins;
    }

    public ArrayList<Kingpin> getMyKingpins() {
        return mMyKingpins;
    }

    public UpcomingGameDetail getSelectedMatch() {
        return this.mSeletedMatch;
    }

    public void setSelectedMatch(UpcomingGameDetail match) {
        this.mSeletedMatch = match;
    }

    public Constant.FILTER_SPORT getSelectedSport() {
        return this.mSelectedSport;
    }

    public void setSelectedSport(Constant.FILTER_SPORT sport) {
        this.mSelectedSport = sport;
    }

    public int getSelectedFilter() {
        return this.mSelectedFilter;
    }

    public void setSelectedFilter(int filter) {
        this.mSelectedFilter = filter;
    }

    public boolean isFilterRevert() {
        return this.mFilterRevert;
    }

    public void setFilterRevert(boolean value) {
        this.mFilterRevert = value;
    }

    public void setDisableActiveFilter(boolean value) {
        this.mDisableActiveFilter = value;
    }

    public boolean isDisableActiveFilter() {
        return this.mDisableActiveFilter;
    }

    public void setDisableSearchFilter(boolean value) {
        this.mDisableSearchUserNameFilter = value;
    }

    public boolean isDisableSearchFilter() {
        return this.mDisableSearchUserNameFilter;
    }

    public void setDisableTimePeriodFilter(boolean value) {
        this.mDisableTimeperiodFilter = value;
    }

    public boolean isDisableTimePeriodFilter() {
        return this.mDisableTimeperiodFilter;
    }

    public void setDisableBetTypeFilter(boolean value) {
        this.mDisableBetTypeFilter = value;
    }

    public boolean isDisableBetTypeFilter() {
        return this.mDisableBetTypeFilter;
    }

    public void setSelectedKingpin(Kingpin kingpin) {
        this.mSelectedKingpin = kingpin;
    }

    public Kingpin getSelectedKingpin() {
        return this.mSelectedKingpin;
    }

    public void setSelectedKingpinId(Integer kingpinId) {
        this.mSelectedKingpinId = kingpinId;
    }

    public Integer getSelectedKingpinId() {
        return this.mSelectedKingpinId;
    }

    public void setShowUpcoming(boolean bShowUpcoming) {
        this.mShowUpcoming = bShowUpcoming;
    }

    public boolean isShowUpcoming() {
        return this.mShowUpcoming;
    }

    public void setFilterListener(FilterListener listener) {
        this.mFilterListener = listener;
    }

    public FilterListener getmFilterListener() {
        return this.mFilterListener;
    }

    public void setTempBitmap(Bitmap bitmap) {
        mTempBitmap = bitmap;
    }

    public Bitmap getTempBitmap() {
        return mTempBitmap;
    }

    public void setIsRegisteringToken(boolean isRegisteringToken) {
        this.mIsRegisteringToken = isRegisteringToken;
    }

    public boolean isRegisteringToken() {
        return this.mIsRegisteringToken;
    }

    // place pick module

    public boolean hasGames() {
        return mGames.size() != 0;
    }

    public void clearGames() {
        mGames.clear();
    }

    public void setGames(ArrayList<Game> games) {
        mGames = games;
        if (mGames != null && mGames.size() > 1) {
            Collections.sort(mGames, new Comparator<Game>() {
                @Override
                public int compare(Game game1, Game game2) {
                    Date date1 = new Date();
                    if (game1.isBovada()) {
                        date1 = ((BovadaGame)game1).getGameStartDate();
                    } else {
                        date1 = ((RapidGame)game1).getGameDate();
                    }
                    if (date1 == null) {
                        date1 = new Date();
                    }
                    Date date2 = new Date();
                    if (game2.isBovada()) {
                        date2 = ((BovadaGame)game2).getGameStartDate();
                    } else {
                        date2 = ((RapidGame) game2).getGameDate();
                    }
                    if (date2 == null) {
                        date2 = new Date();
                    }
                    return date1.compareTo(date2);
                }
            });
        }
    }

    public ArrayList<Game> getGames() {
        return mGames;
    }

    public void setSelectedGame(Game game) {
        mSelectedGame = game;
    }

    public Game getSelectedGame() {
        return mSelectedGame;
    }
}
