package com.kingpin.common.manager;

import android.content.Context;

import com.google.gson.JsonObject;
import com.kingpin.common.base.Constant;
import com.kingpin.common.utility.CommonUtils;
import com.kingpin.common.utility.PreferenceUtils;
import com.kingpin.mvp.model.FilterOptions;
import com.kingpin.mvp.model.PlaceBetCart;
import com.kingpin.mvp.model.Profile;
import com.onesignal.OneSignal;
import com.revenuecat.purchases.Purchases;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class DataManager {

    private static DataManager mInstance = null;

    private Profile mUser;
    private int mSubScriberCount;
    private String mMonthlyPot;
    private boolean mIsFreeTrialAvailable;
    private String mAffiliateTerms;
    private boolean mShowPromoCodeOnRegister;
    private boolean mShowFacebookLogin;
    private double mBalance;
    private double mRealBalance;
    private double mAffiliateEarnings;
    private double mAffiliateAvailable;
    private Constant.FILTER_SPORT mPlaceBetFilterOption;
    private FilterOptions mFilterOptions;
    private boolean mNeedToUpdateBalance;
    private ArrayList<PlaceBetCart> betCarts;

    private String deviceToken;
    private boolean needToRegisterToken;

    public static DataManager getInstance() {
        if(mInstance == null)
            mInstance = new DataManager();
        return mInstance;
    }

    private DataManager() {
        init();
    }

    private void init() {
        mUser = null;
        mSubScriberCount = 0;
        mMonthlyPot = "";
        mIsFreeTrialAvailable = false;
        mAffiliateTerms = "";
        mShowPromoCodeOnRegister = false;
        mShowFacebookLogin = true;
        mBalance = 0.0f;
        mRealBalance = 0.0f;
        mPlaceBetFilterOption = Constant.FILTER_SPORT.FOOTBALLNFL;
        mFilterOptions = new FilterOptions();
        mNeedToUpdateBalance = true;
        betCarts = new ArrayList<>();

        deviceToken = null;
        needToRegisterToken = false;
    }

    public boolean storeAccount(Context context, Profile profile) {
        boolean bChangedMembership = false;
        if(this.mUser != null)
            bChangedMembership = (this.mUser.getMembership() != profile.getMembership());

        this.mUser = profile;
        this.mUser.save(context);
        initPurchaseSDK(context);

        OneSignal.setExternalUserId(String.format("%d",profile.getId()));
        OneSignal.setEmail(profile.getEmail());

        return bChangedMembership;
    }

    public Profile getAccount() {
        return this.mUser;
    }

    public boolean isStoredAccount(Context context) {
        if(Profile.isStoredProfile(context)) {
            this.mUser = Profile.retrieveProfile(context);
            initPurchaseSDK(context);

            this.mFilterOptions.loadFilterOption(context);
            return true;
        }
        return false;
    }

    public void initPurchaseSDK(Context context) {
        if (mUser != null) {
            Purchases.setDebugLogsEnabled(true);
            Purchases.configure(context, "bTAxKoihvPkteaDVjwijkMNIujwHWsgV", String.format("%d", mUser.getId()));
        }
    }

    public boolean isLoggedInStatus() {
        return this.mUser != null;
    }

    public void saveLoggedInStatus(Context context, boolean status) {
        PreferenceUtils.putPreference(context, Constant.PREF_SAVE_LOGGEDIN_STATUS, status);
    }

    public boolean isSavedLoggedInStatus(Context context) {
        return PreferenceUtils.getBooleanPreference(context, Constant.PREF_SAVE_LOGGEDIN_STATUS);
    }

    public void setSubScriberCount(int subscriberCount) {
        this.mSubScriberCount = subscriberCount;
    }

    public void setMonthlyPot(String monthlyPot) {
        this.mMonthlyPot = monthlyPot;
    }

    public String getMonthlyPot() {
        return this.mMonthlyPot;
    }

    public void setTrialAvailableStatus(boolean available) {
        mIsFreeTrialAvailable = available;
    }

    public boolean isTrialAvailable() {
        return mIsFreeTrialAvailable;
    }

    public void setAffiliateTerms(String affiliateTerms) {
        mAffiliateTerms = affiliateTerms;
    }

    public String getAffiliateTerms() {
        return mAffiliateTerms;
    }

    public void setShowPromoCodeOnRegister(boolean showPromoCodeOnRegister) {
        mShowPromoCodeOnRegister = showPromoCodeOnRegister;
    }

    public boolean getShowPromoCodeOnRegister() {
        return mShowPromoCodeOnRegister;
    }

    public void setShowFacebookLogin(boolean showFacebookLogin) {
        mShowFacebookLogin = showFacebookLogin;
    }

    public boolean getShowFacebookLogin() {
        return mShowFacebookLogin;
    }

    public void setBalance(double balance) {
        this.mBalance = balance;
    }

    public double getBalance() {
        return this.mBalance;
    }

    public void setRealBalance(double realBalance) {
        this.mRealBalance = realBalance;
    }

    public double getRealBalance() {
        return this.mRealBalance;
    }

    public void setAffiliateEarnings(double affiliateEarnings) {
        mAffiliateEarnings = affiliateEarnings;
    }

    public double getAffiliateEarnings() {
        return mAffiliateEarnings;
    }

    public void setAffiliateAvailable(double affiliateAvailable) {
        mAffiliateAvailable = affiliateAvailable;
    }

    public double getAffiliateAvailable() {
        return mAffiliateAvailable;
    }


    public void setDeviceToken(String token) {
        deviceToken = token;
        needToRegisterToken = true;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public boolean isNeedToRegisterToken() {
        return needToRegisterToken;
    }

    public void setNeedToRegisterTokenStatus(boolean needToRegister) {
        needToRegisterToken = needToRegister;
    }

    public void setDefaultPlaceBetFilter(String sport) {
        switch (sport.toLowerCase()) {
            case "football":
            case "nfl":
                this.mPlaceBetFilterOption = Constant.FILTER_SPORT.FOOTBALLNFL;
                break;
            case "ncf":
                this.mPlaceBetFilterOption = Constant.FILTER_SPORT.FOOTBALLNCAAF;
                break;
            case "cfl":
                this.mPlaceBetFilterOption = Constant.FILTER_SPORT.FOOTBALLCFL;
                break;
            case "basketball":
            case "nba":
                this.mPlaceBetFilterOption = Constant.FILTER_SPORT.BASKETBALLNBA;
                break;
            case "ncb":
                this.mPlaceBetFilterOption = Constant.FILTER_SPORT.BASKETBALLNCAAB;
                break;
            case "wnba":
                this.mPlaceBetFilterOption = Constant.FILTER_SPORT.BASKETBALLWNBA;
                break;
            case "mlb":
                this.mPlaceBetFilterOption = Constant.FILTER_SPORT.BASEBALLMLB;
                break;
            case "nhl":
                this.mPlaceBetFilterOption = Constant.FILTER_SPORT.HOCKEYNHL;
                break;
            case "soccer":
            case "american football":
            case "english premier league":
                this.mPlaceBetFilterOption = Constant.FILTER_SPORT.SOCCEREnglishPremierLeague;
                break;
            case "english championship":
                this.mPlaceBetFilterOption = Constant.FILTER_SPORT.SOCCEREnglishChampionship;
                break;
            case "english fa cup":
                this.mPlaceBetFilterOption = Constant.FILTER_SPORT.SOCCEREnglishFACup;
                break;
            case "spanish la liga":
                this.mPlaceBetFilterOption = Constant.FILTER_SPORT.SOCCERSpanishLaLiga;
                break;
            case "german bundesliga 1":
                this.mPlaceBetFilterOption = Constant.FILTER_SPORT.SOCCERGermanBundesliga1;
                break;
            case "french ligue 1":
                this.mPlaceBetFilterOption = Constant.FILTER_SPORT.SOCCERFrenchLigue;
                break;
            case "italian serie A":
                this.mPlaceBetFilterOption = Constant.FILTER_SPORT.SOCCERItalianSerieA;
                break;
            case "uefa europa league":
                this.mPlaceBetFilterOption = Constant.FILTER_SPORT.SOCCERUEFAEuropaLeague;
                break;
            case "uefa champions league":
                this.mPlaceBetFilterOption = Constant.FILTER_SPORT.SOCCERUEFAChampionLeague;
                break;
            case "fifa world cup 2018":
                this.mPlaceBetFilterOption = Constant.FILTER_SPORT.SOCCERFIFAWorldcup;
                break;
            case "mls":
                this.mPlaceBetFilterOption = Constant.FILTER_SPORT.SOCCERMLS;
                break;
            case "tennis":
                this.mPlaceBetFilterOption = Constant.FILTER_SPORT.TENNIS;
                break;
            case "mma":
                this.mPlaceBetFilterOption = Constant.FILTER_SPORT.MMA;
                break;
            case "boxing":
                this.mPlaceBetFilterOption = Constant.FILTER_SPORT.BOXING;
                break;
        }
    }

    public void setPlaceBetFilterOption(Constant.FILTER_SPORT option) {
        mPlaceBetFilterOption = option;
    }

    public Constant.FILTER_SPORT getPlaceBetFilterOption() {
        return this.mPlaceBetFilterOption;
    }

    public Constant.FILTER_SPORT getDefaultSelectedSportOnExpertBets() {
        Constant.FILTER_SPORT sport = Constant.FILTER_SPORT.FOOTBALLNFL;
        switch (mPlaceBetFilterOption) {
            case FOOTBALL:
            case FOOTBALLNFL:
                sport = Constant.FILTER_SPORT.FOOTBALLNFL;
                break;
            case FOOTBALLNCAAF:
                sport = Constant.FILTER_SPORT.FOOTBALLNCAAF;
                break;
            case BASKETBALL:
            case BASKETBALLNBA:
                sport = Constant.FILTER_SPORT.BASKETBALLNBA;
                break;
            case BASKETBALLNCAAB:
                sport = Constant.FILTER_SPORT.BASKETBALLNCAAB;
                break;
            case BASEBALL:
            case BASEBALLMLB:
                sport = Constant.FILTER_SPORT.BASEBALL;
                break;
            case HOCKEY:
            case HOCKEYNHL:
                sport = Constant.FILTER_SPORT.HOCKEY;
                break;
            case SOCCER:
            case SOCCEREnglishPremierLeague:
                sport = Constant.FILTER_SPORT.SOCCER;
                break;
            case TENNIS:
                sport = Constant.FILTER_SPORT.TENNIS;
                break;
            case BOXING:
                sport = Constant.FILTER_SPORT.BOXING;
                break;
            case MMA:
                sport = Constant.FILTER_SPORT.MMA;
                break;
            default:
                break;
        }
        return sport;
    }

    public ArrayList<String> getCurrentPlacebetFilterOption() {
        ArrayList<String> selectedSports = new ArrayList<String>();

        if (getPlaceBetFilterOption() == Constant.FILTER_SPORT.FOOTBALLNFL) {
            selectedSports.add("NFL");
        }

        if (getPlaceBetFilterOption() == Constant.FILTER_SPORT.FOOTBALLNFL1stHalf) {
            selectedSports.add("NFL 1st Half");
        }

        if (getPlaceBetFilterOption() == Constant.FILTER_SPORT.FOOTBALLNFL1stQuarter) {
            selectedSports.add("NFL 1st Quarter");
        }

        if (getPlaceBetFilterOption() == Constant.FILTER_SPORT.FOOTBALLNCAAF) {
            selectedSports.add("NCF");
        }

        if (getPlaceBetFilterOption() == Constant.FILTER_SPORT.FOOTBALLNCAAF1stHalf) {
            selectedSports.add("NCF 1st Half");
        }

        if (getPlaceBetFilterOption() == Constant.FILTER_SPORT.FOOTBALLCFL) {
            selectedSports.add("CFL");
        }

        if (getPlaceBetFilterOption() == Constant.FILTER_SPORT.BASKETBALLNBA) {
            selectedSports.add("NBA");
        }

        if (getPlaceBetFilterOption() == Constant.FILTER_SPORT.BASKETBALLNBA1stHalf) {
            selectedSports.add("NBA 1st Half");
        }

        if (getPlaceBetFilterOption() == Constant.FILTER_SPORT.BASKETBALLNCAAB) {
            selectedSports.add("NCB");
        }

        if (getPlaceBetFilterOption() == Constant.FILTER_SPORT.BASKETBALLNCAAB1stHalf) {
            selectedSports.add("NCB 1st Half");
        }

        if (getPlaceBetFilterOption() == Constant.FILTER_SPORT.BASKETBALLWNBA) {
            selectedSports.add("WNBA");
        }

        if (getPlaceBetFilterOption() == Constant.FILTER_SPORT.BASEBALLMLB) {
            selectedSports.add("MLB");
        }

        if (getPlaceBetFilterOption() == Constant.FILTER_SPORT.BASEBALLMLB1stFive) {
            selectedSports.add("MLB 1st 5");
        }

        if (getPlaceBetFilterOption() == Constant.FILTER_SPORT.HOCKEYNHL) {
            selectedSports.add("NHL");
        }

        if (getPlaceBetFilterOption() == Constant.FILTER_SPORT.HOCKEYNHL1stPeriod) {
            selectedSports.add("NHL 1st Period");
        }

        if (getPlaceBetFilterOption() == Constant.FILTER_SPORT.SOCCEREnglishPremierLeague) {
            selectedSports.add("English Premier League");
        }

        if (getPlaceBetFilterOption() == Constant.FILTER_SPORT.SOCCEREnglishChampionship) {
            selectedSports.add("English Championship");
        }

        if (getPlaceBetFilterOption() == Constant.FILTER_SPORT.SOCCEREnglishFACup) {
            selectedSports.add("English FA Cup");
        }

        if (getPlaceBetFilterOption() == Constant.FILTER_SPORT.SOCCERSpanishLaLiga) {
            selectedSports.add("Spanish La Liga");
        }

        if (getPlaceBetFilterOption() == Constant.FILTER_SPORT.SOCCERGermanBundesliga1) {
            selectedSports.add("German Bundesliga 1");
        }

        if (getPlaceBetFilterOption() == Constant.FILTER_SPORT.SOCCERFrenchLigue) {
            selectedSports.add("French Ligue 1");
        }

        if (getPlaceBetFilterOption() == Constant.FILTER_SPORT.SOCCERItalianSerieA) {
            selectedSports.add("Italian Serie A");
        }

        if (getPlaceBetFilterOption() == Constant.FILTER_SPORT.SOCCERUEFAEuropaLeague) {
            selectedSports.add("UEFA Europa League");
        }

        if (getPlaceBetFilterOption() == Constant.FILTER_SPORT.SOCCERUEFAChampionLeague) {
            selectedSports.add("UEFA Champions League");
        }

        if (getPlaceBetFilterOption() == Constant.FILTER_SPORT.SOCCERMLS) {
            selectedSports.add("MLS");
        }

        if (getPlaceBetFilterOption() == Constant.FILTER_SPORT.SOCCERFIFAWorldcup) {
            selectedSports.add("FIFA World Cup 2018");
        }

        if (getPlaceBetFilterOption() == Constant.FILTER_SPORT.TENNIS) {
            selectedSports.add("Tennis");
        }

        if (getPlaceBetFilterOption() == Constant.FILTER_SPORT.MMA) {
            selectedSports.add("MMA");
        }

        if (getPlaceBetFilterOption() == Constant.FILTER_SPORT.BOXING) {
            selectedSports.add("Boxing");
        }

        return selectedSports;
    }

    public String getCurrentPlacebetFilterSportName() {

        Constant.FILTER_SPORT filterOption = getPlaceBetFilterOption();
        if (filterOption == null) {
            return "";
        }

        if (filterOption == Constant.FILTER_SPORT.FOOTBALL) {
            return "Football";
        }

        if (filterOption == Constant.FILTER_SPORT.BASKETBALL) {
            return "Basketball";
        }

        if (filterOption == Constant.FILTER_SPORT.BASEBALL) {
            return "Baseball";
        }

        if (filterOption == Constant.FILTER_SPORT.HOCKEY) {
            return "Hockey";
        }

        if (filterOption == Constant.FILTER_SPORT.SOCCER) {
            return "Soccer";
        }

        if (filterOption == Constant.FILTER_SPORT.TENNIS) {
            return "Tennis";
        }

        if (filterOption == Constant.FILTER_SPORT.MMA) {
            return "MMA";
        }

        if (filterOption == Constant.FILTER_SPORT.BOXING) {
            return "Boxing";
        }

        return "";
    }

    public void saveFingerprintID(Context context, String userId, String password) {
        PreferenceUtils.putPreference(context, Constant.PREF_FINGERPRINT_USERID, userId);
        PreferenceUtils.putPreference(context, Constant.PREF_FINGERPRINT_PASSWORD, password);
    }

    public boolean enableFingerprintID(Context context) {

        String userId = PreferenceUtils.getStringPreference(context, Constant.PREF_FINGERPRINT_USERID);
        String password = PreferenceUtils.getStringPreference(context, Constant.PREF_FINGERPRINT_PASSWORD);

        if(userId == null || userId.isEmpty() || password == null || password.isEmpty())
            return false;

        return CommonUtils.enabledFingerPrint(context);
    }

    public void clearFingerprintID(Context context) {
        PreferenceUtils.putPreference(context, Constant.PREF_FINGERPRINT_USERID, "");
        PreferenceUtils.putPreference(context, Constant.PREF_FINGERPRINT_PASSWORD, "");
    }

    public void signout(Context context) {

        PreferenceUtils.putPreference(context, Constant.PREF_MEMBERSHIP_CHECKEDDATE, "");
        mUser.clear(context);
        mUser = null;
        mBalance = 0.0f;
        mNeedToUpdateBalance = true;
        mFilterOptions.clearFilterOption(context);
        saveLoggedInStatus(context, false);
    }

    public boolean isSubscribed() {
        return mUser != null && mUser.getMembership() == 1;
    }

    public FilterOptions getFilterOptions() {
        return this.mFilterOptions;
    }

    public boolean isSelectedSport(Constant.FILTER_SPORT sport) {
        if(mFilterOptions.getSports().contains(Constant.FILTER_SPORT.ALL))
            return true;

        return mFilterOptions.getSports().contains(sport);
    }

    public boolean isSelectedBetType(Constant.FILTER_BETTYPE bettype) {
        if(mFilterOptions.getBetTypes().contains(Constant.FILTER_BETTYPE.ALL))
            return true;

        return mFilterOptions.getBetTypes().contains(bettype);
    }

    // cart
    private void saveCarts() {
        // save carts to preference
    }

    private void loadMyCarts() {
        // load carts from preference

        clearCart();
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                clearCart();
            }
        }, 10 * 60 * 10000, 10 * 60 * 1000);
    }

    public  int getCartBadge() {
        return betCarts.size();
    }

    public ArrayList<PlaceBetCart> getCarts() {
        return betCarts;
    }

    public boolean addCart(PlaceBetCart cart) {
        if (existCart(cart)) {
            return false;
        } else {
            betCarts.add(cart);
            saveCarts();
            return true;
        }
    }

    public boolean existCart(PlaceBetCart cart) {
        for (int index = 0 ; index < betCarts.size() ; index ++) {
            PlaceBetCart cart_ = betCarts.get(index);
            if (cart.marketid.equals(cart_.marketid)) {
                return true;
            }

            //Only able to bet on a single bet once unless the line changes, or it says the same thing as the website does
            if (cart.sport.equals(cart_.sport) &&
                    cart.teams.equals(cart_.teams) &&
                    cart.bet_type.equals(cart_.bet_type) &&
                    cart.match_date.equals(cart_.match_date)) {
                return true;
            }
        }
        return false;
    }

    public void clearCart() {
        betCarts.clear();
        saveCarts();
    }

    public void removeCart(PlaceBetCart cart) {
        for (int index = 0 ; index < betCarts.size() ; index ++) {
            PlaceBetCart cart_ = betCarts.get(index);
            if (cart.marketid.equals(cart_.marketid)) {
                betCarts.remove(index);
                break;
            }
        }
        saveCarts();
    }

    public void updateCartWithAmount(int amount, PlaceBetCart cart) {
        for (int index = 0 ; index < betCarts.size() ; index ++) {
            PlaceBetCart cart_ = betCarts.get(index);
            if (cart.marketid.equals(cart_.marketid)) {
                cart.bet_amount = amount;
            }
            saveCarts();
        }
    }

    public void updateAllCartWithAmount(int amount) {
        for (int index = 0 ; index < betCarts.size() ; index ++) {
            PlaceBetCart cart = betCarts.get(index);
            cart.bet_amount = amount;
            saveCarts();
        }
    }

    // static functions
    public static boolean hasValidValue(JsonObject json, String key) {
        return (json.has(key) && !json.get(key).isJsonNull());
    }

    // manage day purchase item
    public void purchasedNonRenewSubscribeInApp(Context context) {
        Date expire = getNonRenewSubscribeInAppExpireDate(context);
        if (isAvailableNonRenewSubscribeInApp(context) && expire != null) {
            Calendar calendar = Calendar.getInstance(); // gets a calendar using the default time zone and locale.
            calendar.setTime(expire);
            calendar.add(Calendar.SECOND, Constant.NonRenewableSubscribeDuration);
            Date newExpire = calendar.getTime();
            setNonRenewSubscribeInAppExpireDate(context, newExpire);
        } else {
            Calendar calendar = Calendar.getInstance(); // gets a calendar using the default time zone and locale.
            calendar.add(Calendar.SECOND, Constant.NonRenewableSubscribeDuration);
            Date newExpire = calendar.getTime();
            setNonRenewSubscribeInAppExpireDate(context, newExpire);
        }
    }

    public boolean isAvailableNonRenewSubscribeInApp(Context context) {
        Date expire = getNonRenewSubscribeInAppExpireDate(context);
        if (expire != null) {
            Date now = new Date();
            return expire.after(now);
        } else {
            return false;
        }
    }

    public Date getNonRenewSubscribeInAppExpireDate(Context context) {
        if (mUser == null) {
            return null;
        }

        String expireDateString = PreferenceUtils.getStringPreference(context, "renew_subscribe_expire_userid" + mUser.getEmail());
        if (expireDateString != null) {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd:HH:mm:ss", Locale.US);
            try {
                return df.parse(expireDateString);
            } catch (ParseException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    public void setNonRenewSubscribeInAppExpireDate(Context context, Date date) {
        if (mUser == null) {
            return;
        }

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd:HH:mm:ss", Locale.US);
        PreferenceUtils.putPreference(context, "renew_subscribe_expire_userid" + mUser.getEmail(), df.format(date));
    }
}
