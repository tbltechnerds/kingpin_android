package com.kingpin.common.manager;

import android.util.Log;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.kingpin.billingmodule.billing.BillingConstants;
import com.kingpin.billingmodule.billing.BillingManager;
import com.kingpin.billingmodule.billing.BillingProvider;
import com.kingpin.common.base.Constant;
import com.kingpin.mvp.model.FilterOptions;
import com.kingpin.ui.activity.BaseActivity;
import com.kingpin.ui.activity.HomeActivity;

import java.util.ArrayList;
import java.util.List;

public class InAppManager {
    private static InAppManager mInstance = null;
    private static final String TAG = "InAppManager";

    private BillingManager billingManager;

    private UpdateListener mUpdateListener;
    private BaseActivity mActivity;

    private BillingProvider provider;

    public static InAppManager getInstance() {
        if(mInstance == null)
            mInstance = new InAppManager();
        return mInstance;
    }

    private InAppManager() {
        init();
    }

    private void init() {
        mUpdateListener = new UpdateListener();
        mActivity = null;
        provider = null;
    }

    public void startBillingClient(HomeActivity activity) {
        mActivity = activity;
        billingManager = new BillingManager(activity, getUpdateListener());
    }

    public UpdateListener getUpdateListener() {
        return mUpdateListener;
    }

    public boolean getInAppItems(final BillingProvider provider) {
        if (billingManager != null && billingManager.getBillingClientResponseCode() == BillingClient.BillingResponse.OK) {
            List<String> subscriptionsSkus = BillingConstants.getSkuList(BillingClient.SkuType.SUBS);
            billingManager.querySkuDetailsAsync(BillingClient.SkuType.SUBS, subscriptionsSkus, new SkuDetailsResponseListener(){
                @Override
                public void onSkuDetailsResponse(int responseCode, List<SkuDetails> skuDetailsList) {
                    if (responseCode != BillingClient.BillingResponse.OK) {
                        Log.w("InAppPurchaseFragment", "Unsuccessful query for type: sub"
                                + ". Error code: " + responseCode);
                        provider.failedGetInAppItems();
                    } else if (skuDetailsList != null && skuDetailsList.size() > 0) {
                        provider.completedGetInAppItems(skuDetailsList);
                    } else {
                        provider.failedGetInAppItems();
                    }

                }
            });
            return true;
        } else {
            return false;
        }
    }

    public void purchaseInAppItem(String skuId, final BillingProvider provider) {
        this.provider = provider;
        if (billingManager != null && billingManager.getBillingClientResponseCode() == BillingClient.BillingResponse.OK) {
            billingManager.initiatePurchaseFlow(skuId, BillingClient.SkuType.SUBS);
        } else {
            provider.failedPurchaseItem();
            this.provider = null;
        }
    }

    public void restorePurchases(final BillingProvider provider) {
        this.provider = provider;
        if (billingManager != null && billingManager.getBillingClientResponseCode() == BillingClient.BillingResponse.OK) {
            billingManager.queryPurchases();
        } else {
            provider.failedPurchaseItem();
            this.provider = null;
        }
    }

    /**
     * Handler to billing updates
     */
    private class UpdateListener implements BillingManager.BillingUpdatesListener {
        @Override
        public void onBillingClientSetupFinished() {
            Log.d(TAG, "billing client setup finished");
        }

        @Override
        public void onConsumeFinished(String token, @BillingClient.BillingResponse int result) {
            Log.d(TAG, "Consumption finished. Purchase token: " + token + ", result: " + result);

            // Note: We know this is the SKU_GAS, because it's the only one we consume, so we don't
            // check if token corresponding to the expected sku was consumed.
            // If you have more than one sku, you probably need to validate that the token matches
            // the SKU you expect.
            // It could be done by maintaining a map (updating it every time you call consumeAsync)
            // of all tokens into SKUs which were scheduled to be consumed and then looking through
            // it here to check which SKU corresponds to a consumed token.
            if (result == BillingClient.BillingResponse.OK) {
                // Successfully consumed, so we apply the effects of the item in our
                // game world's logic, which in our case means filling the gas tank a bit
                Log.d(TAG, "Consumption successful. Provisioning.");

            } else {
                Log.d(TAG, "Consumption failed.");

            }

            Log.d(TAG, "End consumption flow.");
        }

        @Override
        public void onPurchasesUpdated(List<Purchase> purchaseList) {

            if (provider == null) {
                return;
            }

            boolean foundPurchase = false;
            for (Purchase purchase : purchaseList) {
                switch (purchase.getSku()) {
                    case BillingConstants.SKU_MONTHLY_WITH_TRIAL:
                        Log.d(TAG, "is free trial");
                        foundPurchase = true;
                        provider.completedPurchaseItem(BillingConstants.SKU_MONTHLY_WITH_TRIAL);
                        break;
                    case BillingConstants.SKU_MONTHLY:
                        Log.d(TAG, "is monthly");
                        foundPurchase = true;
                        provider.completedPurchaseItem(BillingConstants.SKU_MONTHLY);
                        break;
                    case BillingConstants.SKU_QUARTER:
                        Log.d(TAG, "is quarter");
                        foundPurchase = true;
                        provider.completedPurchaseItem(BillingConstants.SKU_QUARTER);
                        break;
                    case BillingConstants.SKU_YEAR:
                        Log.d(TAG, "is year");
                        foundPurchase = true;
                        provider.completedPurchaseItem(BillingConstants.SKU_YEAR);
                        break;
                        default:
                            break;
                }
            }

            if (!foundPurchase) {
                provider.completedPurchaseItem("");
            }
            provider = null;
        }
    }
}
