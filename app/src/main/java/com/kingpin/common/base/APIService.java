package com.kingpin.common.base;

import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;

public interface APIService {

    int API_SIGNUP                  = 0;
    int API_SIGNIN                  = 1;
    int API_FORGOT_PASSWORD         = 2;
    int API_GET_MONTHLY_POT         = 3;
    int API_GET_BALANCE             = 4;
    int API_SIGNOUT                 = 5;
    int API_RESET_PASS              = 6;
    int API_SIGNIN_FB               = 7;
    int API_GET_UPCOME_GAMES        = 8;
    int API_GET_TOP_KINGPINS        = 9;
    int API_GET_KINGPIN_DETAIL      = 10;
    int API_GET_KINGPIN_HISTORY     = 11;
    int API_GET_KINGPIN_UPCOMING    = 12;
    int API_GET_REAL_BALANCE        = 13;
    int API_CLEAR_BET_HISTORY       = 14;
    int API_PAYOUT_PAYPAL           = 15;
    int API_PAYOUT_BANK             = 16;
    int API_UPDATE_PROFILE          = 17;
    int API_GET_PAYMENT_HISTORY     = 18;
    int API_GET_GAMES               = 19;
    int API_SAVE_BETS               = 20;
    int API_FOLLOW                  = 21;
    int API_UNFOLLOW                = 22;
    int API_GET_MY_KINGPINS         = 23;
    int API_SEND_FEEDBACK           = 24;
    int API_GET_AFFILIATE_HISTORY   = 25;
    int API_PURCHASED_SUBSCRIBE     = 26;

    @POST("/api/signup")
    Call<ResponseBody> signup(@Body RequestBody requestBody);

    @POST("/api/login")
    Call<ResponseBody> signin(@Body RequestBody requestBody);

    @POST("/api/forgot_password")
    Call<ResponseBody> forgotPassword(@Body RequestBody requestBody);

    @POST("/api/get_monthly_pot")
    Call<ResponseBody> getMonthlyPot(@Body RequestBody requestBody);

    @POST("/api/get_balance")
    Call<ResponseBody> getBalance(@Body RequestBody requestBody);

    @POST("/api/logout")
    Call<ResponseBody> signout(@Body RequestBody requestBody);

    @POST("/api/reset_password")
    Call<ResponseBody> resetPassword(@Body RequestBody requestBody);

    @POST("/api/social_login")
    Call<ResponseBody> signin_fb(@Body RequestBody requestBody);

    @POST("/api/upcoming_general")
    Call<ResponseBody> getUpcomeGames(@Body RequestBody requestBody);

    @POST("/api/top_kingpins")
    Call<ResponseBody> getTopKingpins(@Body RequestBody requestBody);

    @POST("/api/get_kingpin")
    Call<ResponseBody> getKingpinDetail(@Body RequestBody requestBody);

    @POST("/api/get_history")
    Call<ResponseBody> getKingpinHistory(@Body RequestBody requestBody);

    @POST("/api/get_upcoming")
    Call<ResponseBody> getKingpinUpcoming(@Body RequestBody requestBody);

    @POST("/api/get_payout")
    Call<ResponseBody> getRealBalance(@Body RequestBody requestBody);

    @POST("/api/clear_bet_history")
    Call<ResponseBody> clearBetHistory(@Body RequestBody requestBody);

    @POST("/api/paypal_payout")
    Call<ResponseBody> payoutWithPaypal(@Body RequestBody requestBody);

    @POST("/api/bank_payout")
    Call<ResponseBody> payoutWithBank(@Body RequestBody requestBody);

    @Multipart
    @POST("/api/update_profile")
    Call<ResponseBody> updateProfile(@PartMap Map<String, RequestBody> params);

    @POST("/api/payment_history")
    Call<ResponseBody> getPaymentHistory(@Body RequestBody requestBody);

    @POST("/api/get_bet_list")
    Call<ResponseBody> getGameList(@Body RequestBody requestBody);

    @POST("/api/save_bets")
    Call<ResponseBody> saveBets(@Body RequestBody requestBody);

    @POST("/api/only_follow")
    Call<ResponseBody> follow(@Body RequestBody requestBody);

    @POST("/api/unfollow")
    Call<ResponseBody> unfollow(@Body RequestBody requestBody);

    @POST("/api/my_kingpins")
    Call<ResponseBody> getMyKingpins(@Body RequestBody requestBody);

    @POST("/api/contact_us")
    Call<ResponseBody> sendFeedback(@Body RequestBody requestBody);

    @POST("/api/affiliate_history")
    Call<ResponseBody> getAffiliateHistory(@Body RequestBody requestBody);

    @POST("/api/purchase")
    Call<ResponseBody> purchasedSubscribe(@Body RequestBody requestBody);
}
