package com.kingpin.common.base;

public interface Constant {
    String SERVER_HOST                  = "https://kingpin.pro";

    String INTENT_EXTRA_FRAGMENT        = "extra_fragment";

    String PREF_USER_LOGGEDIN           = "pref_user_loggedin";
    String PREF_USER_ID                 = "pref_user_id";
    String PREF_USER_NAME               = "pref_user_name";
    String PREF_USER_FIRSTNAME          =" pref_user_firstname";
    String PREF_USER_LASTNAME           = "pref_user_lastname";
    String PREF_USER_EMAIL              = "pref_user_email";
    String PREF_USER_TWITTER            = "pref_user_twitter";
    String PREF_USER_INSTAGRAM          = "pref_user_instagram";
    String PREF_USER_COUNTRY            = "pref_user_country";
    String PREF_USER_STATE              = "pref_user_state";
    String PREF_USER_CITY               = "pref_user_city";
    String PREF_USER_PROMO              = "pref_user_promo";
    String PREF_USER_AVATAR             = "pref_user_avatar";
    String PREF_USER_MEMBERSHIP         = "pref_user_membership";
    String PREF_USER_MODE               = "pref_user_mode";
    String PREF_USER_PAYMENT_TYPE       = "pref_user_payment_type";
    String PREF_USER_TRIAL              = "pref_user_trial";
    String PREF_USER_NOTIFICATIONS      = "pref_user_notifications";
    String PREF_USER_PUSH_NOTIFY        = "pref_user_push_notify";
    String PREF_USER_SHOW_RATEDATE      = "pref_user_show_ratedate";
    String PREF_USER_RATE_STATUS        = "pref_user_rate_status";
    String PREF_USER_RANKED             = "pref_user_ranked";
    String PREF_USER_BETTING_STRATEGY   = "pref_user_betting_strategy";
    String PREF_USER_REFERRAL_AFFILIATE_CODE = "pref_user_referral_affiliate_code";
    String PREF_USER_IS_VALID_AFFILIATE_CODE = "pref_user_is_valid_affiliate_code";

    String PREF_SAVE_LOGGEDIN_STATUS    = "pref_save_loggedin_status";

    String PREF_FILTER_ACTIVE           = "pref_filter_active";
    String PREF_FILTER_KEYWORD          = "pref_filter_keyword";
    String PREF_FILTER_TIME             = "pref_filter_time";
    String PREF_FILTER_SPORT            = "pref_filter_sport";
    String PREF_FILTER_BETTYPE          = "pref_filter_bettype";

    String PREF_FINGERPRINT_USERID      = "pref_fingerprint_userid";
    String PREF_FINGERPRINT_PASSWORD    = "pref_fingerprint_password";

    String PREF_MEMBERSHIP_CHECKEDDATE  = "pref_membership_checkeddate";

    String PREF_DO_NOT_SHOW_BET_HELP    = "pref_do_not_show_bet_help";

    String PREF_ASKRATE_DATE            = "pref_askrate_date";

    int NonRenewableSubscribeDuration = 24 * 3600;

    String APP_SHARE_LINK = "https://www.KingPin.pro/android";

    enum FILTER_SPORT {
        ALL,

        FOOTBALL,
        FOOTBALLNFL,
        FOOTBALLNFL1stHalf,
        FOOTBALLNFL1stQuarter,
        FOOTBALLNCAAF,
        FOOTBALLNCAAF1stHalf,
        FOOTBALLCFL,

        BASKETBALL,
        BASKETBALLNBA,
        BASKETBALLNBA1stHalf,
        BASKETBALLNCAAB,
        BASKETBALLNCAAB1stHalf,
        BASKETBALLWNBA,

        BASEBALL,
        BASEBALLMLB,
        BASEBALLMLB1stFive,

        HOCKEY,
        HOCKEYNHL,
        HOCKEYNHL1stPeriod,

        SOCCER,
        SOCCEREnglishPremierLeague,
        SOCCEREnglishChampionship,
        SOCCEREnglishFACup,
        SOCCERSpanishLaLiga,
        SOCCERGermanBundesliga1,
        SOCCERFrenchLigue,
        SOCCERItalianSerieA,
        SOCCERUEFAEuropaLeague,
        SOCCERUEFAChampionLeague,
        SOCCERMLS,
        SOCCERFIFAWorldcup,

        TENNIS,
        BOXING,
        MMA
    }

    enum TIME_PERIOD {
        ALL,
        YESTERDAY,
        ONEWEEK,
        ONEMONTH,
        THREEMONTH,
        SIXMONTH,
        ONEYEAR
    }

    enum FILTER_BETTYPE {
        ALL,
        MONEYLINE,
        SPREAD,
        OVER_UNDER,
        PARLAY
    }
}