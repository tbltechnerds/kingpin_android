package com.kingpin.mvp.view;

import com.kingpin.mvp.model.Bet;
import com.kingpin.mvp.model.Kingpin;

import java.util.ArrayList;

public interface UserProfileView extends BaseView {
    void OnLoadKingpinDetail(Kingpin kingpin);
    void OnLoadBetHistories(ArrayList<Bet> bets);
    void OnLoadUpcomingBets(ArrayList<Bet> bets);
    void OnChooseUpcomingBet();
    void OnCompletedFollow();
    void OnCompletedUnfollow();
}