package com.kingpin.mvp.view;

import android.content.Intent;
import android.support.annotation.StringRes;

import com.hannesdorfmann.mosby.mvp.MvpView;
import com.kingpin.ui.activity.BaseActivity;

public interface BaseView extends MvpView {
    void showLoading();
    void hideLoading();

    void showToast(@StringRes int messageId);
    void showToast(String message);
    void hideToast();

    void onShareText(String text);

    void updateFragment(int fragment_id);
    void addFragment(int fragment_id);
    void removeFragment(int fragment_id);

    void startIntent(Class<?> activity, int fragment, boolean bFinish);
    void startIntent(Intent intent, int fragment, boolean bFinish);

    BaseActivity getCurrentActivity();
}
