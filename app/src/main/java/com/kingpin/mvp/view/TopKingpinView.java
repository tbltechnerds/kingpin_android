package com.kingpin.mvp.view;

import com.kingpin.mvp.model.Kingpin;

import java.util.ArrayList;

public interface TopKingpinView extends BaseView {
    void onSort(int filter_type);
    void onViewUserProfile(Kingpin kingpin);
    void onUpcomingBets(Kingpin kingpin);
    void onLoadKingpins(ArrayList<Kingpin> kingpins, int current_page);
    void onLoadFailedKingpins();
}

