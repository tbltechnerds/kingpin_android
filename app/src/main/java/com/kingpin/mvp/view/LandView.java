package com.kingpin.mvp.view;

public interface LandView extends BaseView {
    void completedToGetBalance();
    void gotoHome();
    boolean isChoosenRemember();
    void askSupportFingerprint(String userId, String password);
}