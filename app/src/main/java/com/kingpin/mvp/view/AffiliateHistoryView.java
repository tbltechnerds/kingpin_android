package com.kingpin.mvp.view;

import com.kingpin.mvp.model.AffiliateCriedit;
import com.kingpin.mvp.model.Withdraw;

import java.util.ArrayList;

public interface AffiliateHistoryView extends BaseView {
    void OnLoadAffiliateHistories(ArrayList<AffiliateCriedit> criedits);
}

