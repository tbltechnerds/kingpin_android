package com.kingpin.mvp.view;

import com.kingpin.mvp.model.BovadaGame;
import com.kingpin.mvp.model.Game;
import com.kingpin.mvp.model.RapidGame;

public interface PlacePickView extends BaseView {
    void onLoadGames();
    void onLoadFailedToGetGames();
    void onViewGame(Game game);
}

