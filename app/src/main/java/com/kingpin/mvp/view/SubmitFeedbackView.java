package com.kingpin.mvp.view;

public interface SubmitFeedbackView extends BaseView {
    void onSuccessToSubmit();
    void onFailedToSubmit();
}