package com.kingpin.mvp.view;

import com.kingpin.mvp.model.Withdraw;

import java.util.ArrayList;

public interface PaymentHistoryView extends BaseView {
    void OnLoadPaymentHistories(ArrayList<Withdraw> withdraws);
    void OnChooseHistory(Withdraw withdrawd);
}

