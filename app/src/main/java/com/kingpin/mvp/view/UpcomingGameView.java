package com.kingpin.mvp.view;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.kingpin.common.base.Constant;
import com.kingpin.mvp.model.TopUsers;
import com.kingpin.mvp.model.UpcomingGame;
import com.kingpin.mvp.model.UpcomingGameDetail;

import java.util.ArrayList;

public interface UpcomingGameView extends BaseView {
    void onTab(Constant.FILTER_SPORT tabType);
    void onLoadGames(ArrayList<UpcomingGame> games, JsonArray allTopUsers);
    void onChooseGame(UpcomingGameDetail match);
    void OnUpdatedProfile();
}
