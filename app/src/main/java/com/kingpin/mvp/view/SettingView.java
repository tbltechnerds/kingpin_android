package com.kingpin.mvp.view;

public interface SettingView extends BaseView {
    void hide();
    void onLoggedOut();
}
