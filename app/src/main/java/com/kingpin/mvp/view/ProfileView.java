package com.kingpin.mvp.view;

public interface ProfileView extends BaseView {
    void OnLoadRealBalance();
    void OnUpdatedProfile();
}

