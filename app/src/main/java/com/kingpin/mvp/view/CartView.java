package com.kingpin.mvp.view;

import com.kingpin.mvp.model.PlaceBetCart;

public interface CartView extends BaseView {
    boolean isParlay();
    void onRemoveCart(PlaceBetCart cart);
    void updateCartWithAmount(int amount, PlaceBetCart cart);
    void completedSaveBets();
    void failedSaveBets();
}