package com.kingpin.mvp.view;

public interface UpcomingGameDetailView extends BaseView {
    void onKingpinProfile(int user_id);
    void onSubscribe();
}

