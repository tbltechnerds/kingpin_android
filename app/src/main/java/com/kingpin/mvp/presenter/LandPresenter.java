package com.kingpin.mvp.presenter;

import com.google.gson.Gson;
import com.kingpin.app.KingpinApplication;
import com.kingpin.common.base.APIService;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.utility.APIUtils;
import com.kingpin.mvp.model.response.MonthlyPotResponse;
import com.kingpin.mvp.model.response.SigninFBResponse;
import com.kingpin.mvp.model.response.SigninResponse;
import com.kingpin.mvp.model.response.SignupResponse;
import com.kingpin.mvp.view.LandView;

import org.json.JSONException;
import org.json.JSONObject;

public class LandPresenter extends BasePresenter<LandView> {

    private String userId = "";
    private String password = "";

    @Override
    public void onAPISuccess(int api_kind, String result) {

        super.onAPISuccess(api_kind, result);

        switch (api_kind) {
            case APIService.API_SIGNUP: {
                getView().hideLoading();

                Gson gson = new Gson();
                SignupResponse response = gson.fromJson(result, SignupResponse.class);
                if (response.isSuccess()) {
                    DataManager.getInstance().storeAccount(getView().getCurrentActivity(), response.getProfile());
                    DataManager.getInstance().saveLoggedInStatus(getView().getCurrentActivity(), true);
                    DataManager.getInstance().setNeedToRegisterTokenStatus(true);
                    getView().gotoHome();
                }
                else {
                    getView().showToast(response.getMessage());
                }
                break;
            }
            case APIService.API_SIGNIN: {
                getView().hideLoading();

                Gson gson = new Gson();
                SigninResponse response = gson.fromJson(result, SigninResponse.class);
                if(response.isSuccess()) {
                    DataManager.getInstance().storeAccount(getView().getCurrentActivity(), response.getProfile());
                    DataManager.getInstance().saveLoggedInStatus(getView().getCurrentActivity(), getView().isChoosenRemember());
                    DataManager.getInstance().setNeedToRegisterTokenStatus(true);
                    getView().askSupportFingerprint(this.userId, this.password);
                }
                else {
                    getView().showToast(response.getMessage());
                }
                break;
            }
            case APIService.API_GET_MONTHLY_POT: {
                getView().hideLoading();

                Gson gson = new Gson();
                MonthlyPotResponse response = gson.fromJson(result, MonthlyPotResponse.class);
                if(response.isSuccess()) {
                    DataManager.getInstance().setMonthlyPot(response.getData().getMonthlyPot());
                    DataManager.getInstance().setSubScriberCount(response.getData().getSubScriberCount());
                    DataManager.getInstance().setTrialAvailableStatus(response.getData().getTrialAvailableStatus());
                    DataManager.getInstance().setAffiliateTerms(response.getData().getAffiliateTerms());
                    DataManager.getInstance().setShowPromoCodeOnRegister(response.getData().isShowPromoCodeOnRegister());
                    DataManager.getInstance().setShowFacebookLogin(response.getData().isShowFacebookLogin());
                    if(response.getData().getDefaultSport() != null)
                        DataManager.getInstance().setDefaultPlaceBetFilter(response.getData().getDefaultSport());

                    getView().completedToGetBalance();

                    if(DataManager.getInstance().isSavedLoggedInStatus(getView().getCurrentActivity())) {
                        if(DataManager.getInstance().isStoredAccount(getView().getCurrentActivity()))
                            getView().gotoHome();
                    }
                }
                else {
                    getView().showToast(response.getMessage());
                }
                break;
            }
            case APIService.API_SIGNIN_FB: {
                getView().hideLoading();

                Gson gson = new Gson();
                SigninFBResponse response = gson.fromJson(result, SigninFBResponse.class);
                if(response.isSuccess()) {
                    DataManager.getInstance().storeAccount(getView().getCurrentActivity(), response.getProfile());
                    DataManager.getInstance().saveLoggedInStatus(getView().getCurrentActivity(), true);
                    DataManager.getInstance().setNeedToRegisterTokenStatus(true);
                    getView().gotoHome();
                }
                else {
                    getView().showToast(response.getMessage());
                }
                break;
            }
        }
    }

    public void signup(String name, String city, String email, String password, String promoCode) {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name", name);
            jsonObject.put("city", city);
            jsonObject.put("email", email);
            jsonObject.put("password", password);
            jsonObject.put("sign_up_used_affiliate_code", promoCode);

            getView().showLoading();
            APIUtils.getInstance().signup(jsonObject, this, APIService.API_SIGNUP);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void signin(String email, String password) {
        this.userId = email;
        this.password = password;

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("email", email);
            jsonObject.put("password", password);

            getView().showLoading();
            APIUtils.getInstance().signin(jsonObject, this, APIService.API_SIGNIN);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getMonthlyPot() {
        getView().showLoading();
        JSONObject jsonObject = new JSONObject();
        APIUtils.getInstance().getMonthlyPot(jsonObject, this, APIService.API_GET_MONTHLY_POT);
    }

    public void signin_fb(String fb_id, String email, String first_name, String last_name) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", fb_id);
            jsonObject.put("email", email);
            jsonObject.put("name", first_name + " " + last_name);

            getView().showLoading();
            APIUtils.getInstance().signin_fb(jsonObject, this, APIService.API_SIGNIN_FB);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
