package com.kingpin.mvp.presenter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kingpin.common.base.APIService;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.utility.APIUtils;
import com.kingpin.mvp.model.Profile;
import com.kingpin.mvp.model.Withdraw;
import com.kingpin.mvp.model.response.AffiliateHistoryResponse;
import com.kingpin.mvp.model.response.PaymentHistoryResponse;
import com.kingpin.mvp.view.AffiliateHistoryView;
import com.kingpin.mvp.view.PaymentHistoryView;

import org.json.JSONException;
import org.json.JSONObject;

public class AffiliateHistoryPresenter extends BasePresenter<AffiliateHistoryView> {

    @Override
    public void onAPISuccess(int api_kind, String result) {

        super.onAPISuccess(api_kind, result);

        switch (api_kind) {
            case APIService.API_GET_AFFILIATE_HISTORY: {
                getView().hideLoading();

                GsonBuilder gsonBuilder = new GsonBuilder();
                gsonBuilder.registerTypeAdapter(Withdraw.class, Withdraw.deserializer);
                Gson gson = gsonBuilder.create();
                final AffiliateHistoryResponse response = gson.fromJson(result, AffiliateHistoryResponse.class);
                if(response.isSuccess())
                    getView().OnLoadAffiliateHistories(response.getCriedits());
                else
                    getView().showToast(response.getMessage());
                break;
            }
        }
    }

    public void getAffiliateHistory() {
        Profile me = DataManager.getInstance().getAccount();
        if (me == null) {
            return;
        }
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("user_id", me.getId());
            //jsonObject.put("user_id", 6151);

            getView().showLoading();
            APIUtils.getInstance().getAffiliateHistory(jsonObject, this, APIService.API_GET_AFFILIATE_HISTORY);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
