package com.kingpin.mvp.presenter;

import android.graphics.Bitmap;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kingpin.common.base.APIService;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.manager.FileManager;
import com.kingpin.common.manager.TempDataManager;
import com.kingpin.common.utility.APIUtils;
import com.kingpin.common.utility.CommonUtils;
import com.kingpin.mvp.model.Profile;
import com.kingpin.mvp.model.TopUsers;
import com.kingpin.mvp.model.UpcomingGame;
import com.kingpin.mvp.model.response.UpcomeGameResponse;
import com.kingpin.mvp.view.UpcomingGameView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class UpcomingGamePresenter extends BasePresenter<UpcomingGameView> {

    @Override
    public void onAPISuccess(int api_kind, final String result) {

        super.onAPISuccess(api_kind, result);

        switch (api_kind) {
            case APIService.API_GET_UPCOME_GAMES: {
                new Thread() {
                    @Override
                    public void run() {
                        getView().hideLoading();
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        gsonBuilder.registerTypeAdapter(UpcomingGame.class, UpcomingGame.deserializer);
                        Gson gson = gsonBuilder.create();
                        final UpcomeGameResponse response = gson.fromJson(result, UpcomeGameResponse.class);
                        if(response.isSuccess())
                            TempDataManager.getInstance().setUpcomingGames(response.getGames());

                        if(getView().toString() == null)
                            return;

                        getView().getCurrentActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (response.isSuccess())
                                    getView().onLoadGames(response.getGames(), response.getAllTopUsers());
                                else
                                    getView().showToast(response.getMessage());
                            }
                        });
                    }
                }.start();
                break;
            }
        }
    }

    public void getUpcomeGames() {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("activeBettors", 1);
            if (TempDataManager.getInstance().isFirstLoading) {
                getView().showLoading();
            }
            APIUtils.getInstance().getUpcomeGames(jsonObject, this, APIService.API_GET_UPCOME_GAMES);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void saveDeviceToken() {
        try {

            String token = DataManager.getInstance().getDeviceToken();
            if (token == null) {
                return;
            }

            Profile me = DataManager.getInstance().getAccount();
            if (me == null) {
                return;
            }

            if (!DataManager.getInstance().isNeedToRegisterToken()) {
                return;
            }

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("user_id", me.getId());
            jsonObject.put("device_token", token);
            jsonObject.put("device_type", 1); // 0: iphone, 1: android

            updateProfile(jsonObject, "", true);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
