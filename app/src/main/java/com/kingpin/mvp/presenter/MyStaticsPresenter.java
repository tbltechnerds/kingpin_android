package com.kingpin.mvp.presenter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kingpin.common.base.APIService;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.utility.APIUtils;
import com.kingpin.mvp.model.Bet;
import com.kingpin.mvp.model.Kingpin;
import com.kingpin.mvp.model.Profile;
import com.kingpin.mvp.model.response.BetHistoryResponse;
import com.kingpin.mvp.model.response.KingpinDetailResponse;
import com.kingpin.mvp.view.MyStaticsView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MyStaticsPresenter extends BasePresenter<MyStaticsView> {

    @Override
    public void onAPISuccess(int api_kind, final String result) {

        super.onAPISuccess(api_kind, result);

        switch (api_kind) {
            case APIService.API_GET_KINGPIN_DETAIL: {
                new Thread() {
                    @Override
                    public void run() {
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        gsonBuilder.registerTypeAdapter(Kingpin.class, Kingpin.deserializer);
                        Gson gson = gsonBuilder.create();
                        final KingpinDetailResponse response = gson.fromJson(result, KingpinDetailResponse.class);

                        if (getView().toString() == null)
                            return;

                        getView().getCurrentActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                getView().hideLoading();
                                if (response.isSuccess())
                                    getView().OnLoadKingpinDetail(response.getKingpin());
                                else
                                    getView().showToast(response.getMessage());
                            }
                        });
                    }
                }.start();
                break;
            }
            case APIService.API_GET_KINGPIN_HISTORY: {
                new Thread() {
                    @Override
                    public void run() {
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        gsonBuilder.registerTypeAdapter(Bet.class, Bet.deserializer);
                        Gson gson = gsonBuilder.create();
                        final BetHistoryResponse response = gson.fromJson(result, BetHistoryResponse.class);

                        if(getView().toString() == null)
                            return;

                        getView().getCurrentActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                getView().hideLoading();
                                if (response.isSuccess())
                                    getView().OnLoadBetHistories(response.getBets());
                                else
                                    getView().showToast(response.getMessage());
                            }
                        });
                    }
                }.start();
                break;
            }
            case APIService.API_GET_KINGPIN_UPCOMING: {
                new Thread() {
                    @Override
                    public void run() {
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        gsonBuilder.registerTypeAdapter(Bet.class, Bet.deserializer);
                        Gson gson = gsonBuilder.create();
                        final BetHistoryResponse response = gson.fromJson(result, BetHistoryResponse.class);

                        if(getView().toString() == null)
                            return;

                        getView().getCurrentActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                getView().hideLoading();
                                if (response.isSuccess())
                                    getView().OnLoadUpcomingBets(response.getBets());
                                else
                                    getView().showToast(response.getMessage());
                            }
                        });
                    }
                }.start();
                break;
            }
        }
    }

    public void getKingpinDetail() {

        Profile me = DataManager.getInstance().getAccount();
        if (me == null) {
            return;
        }

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("user_id", me.getId());
            jsonObject.put("kingpin_id", me.getId());
            jsonObject.put("times", new JSONArray(DataManager.getInstance().getFilterOptions().getTimePeriodString()));
            jsonObject.put("sports", new JSONArray(DataManager.getInstance().getFilterOptions().getSportsString()));
            jsonObject.put("bettypes", new JSONArray(DataManager.getInstance().getFilterOptions().getBetTypesString()));

            getView().showLoading();
            APIUtils.getInstance().getKingpinDetail(jsonObject, this, APIService.API_GET_KINGPIN_DETAIL);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getBettingHistory() {
        Profile me = DataManager.getInstance().getAccount();
        if (me == null) {
            return;
        }
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("kingpin_id", me.getId());
            jsonObject.put("times", new JSONArray(DataManager.getInstance().getFilterOptions().getTimePeriodString()));
            jsonObject.put("sports", new JSONArray(DataManager.getInstance().getFilterOptions().getSportsString()));
            jsonObject.put("bettypes", new JSONArray(DataManager.getInstance().getFilterOptions().getBetTypesString()));

            getView().showLoading();
            APIUtils.getInstance().getKingpinHistory(jsonObject, this, APIService.API_GET_KINGPIN_HISTORY);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getUpcomingBets() {
        Profile me = DataManager.getInstance().getAccount();
        if (me == null) {
            return;
        }
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("kingpin_id", me.getId());
            jsonObject.put("sports", new JSONArray(DataManager.getInstance().getFilterOptions().getSportsString()));
            jsonObject.put("bettypes", new JSONArray(DataManager.getInstance().getFilterOptions().getBetTypesString()));

            getView().showLoading();
            APIUtils.getInstance().getKingpinUpcoming(jsonObject, this, APIService.API_GET_KINGPIN_UPCOMING);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }
}