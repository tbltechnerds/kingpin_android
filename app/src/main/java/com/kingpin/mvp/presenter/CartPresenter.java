package com.kingpin.mvp.presenter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kingpin.common.base.APIService;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.manager.TempDataManager;
import com.kingpin.common.utility.APIUtils;
import com.kingpin.mvp.model.PlaceBetCart;
import com.kingpin.mvp.model.Profile;
import com.kingpin.mvp.model.response.PlacePickListResponse;
import com.kingpin.mvp.model.response.SaveBetsResponse;
import com.kingpin.mvp.view.CartView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Response;

public class CartPresenter extends BasePresenter<CartView> {
    @Override
    public void onAPISuccess(int api_kind, final String result) {

        super.onAPISuccess(api_kind, result);

        switch (api_kind) {
            case APIService.API_SAVE_BETS: {
                new Thread() {
                    @Override
                    public void run() {
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        //gsonBuilder.registerTypeAdapter(Kingpin.class, Kingpin.deserializer);
                        Gson gson = gsonBuilder.create();
                        final SaveBetsResponse response = gson.fromJson(result, SaveBetsResponse.class);

                        getView().getCurrentActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                getView().hideLoading();
                                if (response.isSuccess())
                                    getView().completedSaveBets();
                                else
                                    getView().showToast(response.getMessage());
                            }
                        });
                    }
                }.start();
                break;
            }
        }
    }

    @Override
    public void onAPIError(int api_kind, Response<ResponseBody> response) {
        super.onAPIError(api_kind, response);

        getView().getCurrentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getView().hideLoading();
                getView().failedSaveBets();
            }
        });
    }

    @Override
    public void onAPIFailure(int api_kind) {
        super.onAPIFailure(api_kind);

        getView().getCurrentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getView().hideLoading();
                getView().failedSaveBets();
            }
        });
    }

    public void saveBet(boolean isParlay, int totalStake, double totalWin) {
        try {
            Profile me = DataManager.getInstance().getAccount();
            if (me == null) {
                getView().showToast("You are not logged in yet. Please try again after log in.");
                return;
            }

            getView().showLoading();

            JSONArray bets = new JSONArray();
            for (int index = 0 ; index < DataManager.getInstance().getCarts().size() ; index ++) {
                PlaceBetCart cart = DataManager.getInstance().getCarts().get(index);
                bets.put(cart.getDictionary());
            }

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("user_id", me.getId());
            jsonObject.put("bets", bets);

            if (isParlay) {
                jsonObject.put("parlay", 1);
                jsonObject.put("parlay_potential_win", totalWin);
                jsonObject.put("stake_input_parlay", totalStake);
            } else {
                jsonObject.put("parlay", 0);
            }

            APIUtils.getInstance().saveBets(jsonObject, this, APIService.API_SAVE_BETS);
        }
        catch (JSONException e) {
            e.printStackTrace();
            getView().hideLoading();
            getView().failedSaveBets();
        }
    }
}