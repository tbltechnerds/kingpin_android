package com.kingpin.mvp.presenter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hannesdorfmann.mosby.mvp.MvpNullObjectBasePresenter;
import com.kingpin.R;
import com.kingpin.app.KingpinApplication;
import com.kingpin.common.base.APIService;
import com.kingpin.common.base.Constant;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.manager.TempDataManager;
import com.kingpin.common.utility.APIUtils;
import com.kingpin.common.utility.DateUtils;
import com.kingpin.listener.ApiListener;
import com.kingpin.listener.UIListener;
import com.kingpin.mvp.model.Kingpin;
import com.kingpin.mvp.model.Profile;
import com.kingpin.mvp.model.response.BalanceResponse;
import com.kingpin.mvp.model.response.FollowResponse;
import com.kingpin.mvp.model.response.SaveBetsResponse;
import com.kingpin.mvp.model.response.UpdateProfileResponse;
import com.kingpin.mvp.view.BaseView;
import com.kingpin.mvp.view.InAppPurchaseView;
import com.kingpin.mvp.view.UpcomingGameView;
import com.kingpin.ui.fragment.UpcomingGameFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Response;

public abstract class BasePresenter<V extends BaseView> extends MvpNullObjectBasePresenter<V> implements ApiListener {

    private int isTrial;

    @Override
    public void onAPISuccess(int api_kind, String result) {
        switch (api_kind) {
            case APIService.API_GET_BALANCE: {
                getView().hideLoading();

                Gson gson = new Gson();
                BalanceResponse response = gson.fromJson(result, BalanceResponse.class);
                if(response.isSuccess()) {
                    DataManager.getInstance().setBalance(response.getData().getBalance());
                    DataManager.getInstance().storeAccount(getView().getCurrentActivity(), response.getData().getUser());
                    getView().addFragment(UIListener.HomeActivityListener.FRAGMENT_SETTING);
                }
                else {
                    getView().showToast(response.getMessage());
                }
                break;
            }
            case APIService.API_UPDATE_PROFILE: {
                getView().hideLoading();

                Gson gson = new Gson();
                UpdateProfileResponse response = gson.fromJson(result, UpdateProfileResponse.class);
                if(response.isSuccess()) {
                    DataManager.getInstance().storeAccount(getView().getCurrentActivity(), response.getProfile());
                    if(response.getMessage().isEmpty())
                        getView().showToast(R.string.toast_update_profile);
                    else
                        getView().showToast(response.getMessage());

                    if(TempDataManager.getInstance().isRegisteringToken()) {
                        TempDataManager.getInstance().setIsRegisteringToken(false);
                        DataManager.getInstance().setNeedToRegisterTokenStatus(false);
                    }

                    OnUpdatedProfile();
                }
                else {
                    getView().showToast(response.getMessage());
                }
                break;
            }
            case APIService.API_PURCHASED_SUBSCRIBE: {
                getView().hideLoading();

                Gson gson = new Gson();
                UpdateProfileResponse response = gson.fromJson(result, UpdateProfileResponse.class);
                if(response.isSuccess()) {

                }
                else {
                    //getView().showToast(response.getMessage());
                }
                if (isTrial == 1) {
                    updateProfileWithSubscribedStatus(true, true, false);
                } else {
                    updateProfileWithSubscribedStatus(true, false, false);
                }
                break;
            }
        }
    }

    @Override
    public void onAPIError(int api_kind, Response<ResponseBody> response) {
        getView().hideLoading();
        if (api_kind == APIService.API_PURCHASED_SUBSCRIBE) {
            if (isTrial == 1) {
                updateProfileWithSubscribedStatus(true, true, false);
            } else {
                updateProfileWithSubscribedStatus(true, false, false);
            }
        } else {
            getView().showToast(R.string.toast_unknown_error);
        }
    }

    @Override
    public void onAPIFailure(int api_kind) {
        getView().hideLoading();
        getView().showToast(R.string.toast_not_valid_server);
    }

    public void OnUpdatedProfile() {
        if(this instanceof InAppPurchasePresenter) {
            ((InAppPurchasePresenter)this).getView().OnUpdatedProfile();
        } else if (this instanceof UpcomingGamePresenter) {
            ((UpcomingGamePresenter)this).getView().OnUpdatedProfile();
        }
    }

    public void getBalance() {
        try {

            Profile me = DataManager.getInstance().getAccount();
            if (me == null) {
                return;
            }

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("user_id", me.getId());

            getView().showLoading();
            APIUtils.getInstance().getBalance(jsonObject, this, APIService.API_GET_BALANCE);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void updateProfile(JSONObject params, String filePath, boolean isRegisteringToken) { //bitmpa is null, isRegsiteringToken = false
        TempDataManager.getInstance().setIsRegisteringToken(isRegisteringToken);
        getView().showLoading();
        APIUtils.getInstance().updateProfile(params, filePath, this, APIService.API_UPDATE_PROFILE);
    }

    public void updateProfileWithSubscribedStatus(boolean subscribed, boolean isTrial, boolean isRegisteringToken) {

        Profile me = DataManager.getInstance().getAccount();
        if (me == null) {
            return;
        }

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("user_id", me.getId());
            if (subscribed) {
                jsonObject.put("membership", "1");
                jsonObject.put("payment_type", "inapp_android");
            } else {
                //disable to downgrade membership
//                jsonObject.put("membership", "0");
//                jsonObject.put("payment_type", "");
            }

            if (isTrial) {
                jsonObject.put("isTrial", "1");
            } else {
                jsonObject.put("isTrial", "0");
            }

            String token = DataManager.getInstance().getDeviceToken();
            if (isRegisteringToken && token != null) {
                jsonObject.put("device_token", token);
                jsonObject.put("device_type", 1); // 0: iphone, 1: android
            }

            updateProfile(jsonObject, "", isRegisteringToken);
        } catch (Exception e) {

        }
    }

    public void purchasedSubscribe(String interval, int trial, double price) {
        Profile me = DataManager.getInstance().getAccount();
        if (me == null) {
            return;
        }

        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("user_id", me.getId());
            jsonObject.put("interval", interval);
            jsonObject.put("trial", trial);
            jsonObject.put("price", price);
            if (interval.equals("day")) {
                Calendar calendar = Calendar.getInstance(); // gets a calendar using the default time zone and locale.
                calendar.add(Calendar.SECOND, Constant.NonRenewableSubscribeDuration);
                Date expire = calendar.getTime();
                jsonObject.put("rc_expires_date", DateUtils.convertLocalDateToServerDateString(expire));
            }

            isTrial = trial;

            getView().showLoading();
            APIUtils.getInstance().purchasedSubscribe(jsonObject, this, APIService.API_PURCHASED_SUBSCRIBE);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }
}