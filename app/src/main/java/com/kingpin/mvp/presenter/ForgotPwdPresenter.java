package com.kingpin.mvp.presenter;

import com.google.gson.Gson;
import com.kingpin.common.base.APIService;
import com.kingpin.common.utility.APIUtils;
import com.kingpin.listener.UIListener;
import com.kingpin.mvp.model.response.RequestCodeResponse;
import com.kingpin.mvp.model.response.ResetPassResponse;
import com.kingpin.mvp.view.ForgotPwdView;

import org.json.JSONException;
import org.json.JSONObject;

public class ForgotPwdPresenter extends BasePresenter<ForgotPwdView> {

    @Override
    public void onAPISuccess(int api_kind, String result) {

        super.onAPISuccess(api_kind, result);

        switch (api_kind) {
            case APIService.API_FORGOT_PASSWORD: {
                Gson gson = new Gson();
                RequestCodeResponse response = gson.fromJson(result, RequestCodeResponse.class);
                getView().hideLoading();
                getView().showToast(response.getMessage());
                break;
            }
            case APIService.API_RESET_PASS: {
                Gson gson = new Gson();
                ResetPassResponse response = gson.fromJson(result, ResetPassResponse.class);
                getView().hideLoading();
                getView().showToast(response.getMessage());
                getView().updateFragment(UIListener.StartActivityListener.FRAGMENT_LAND);
                break;
            }
        }
    }

    public void requestCode(String email) {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("email", email);

            getView().showLoading();
            APIUtils.getInstance().requestCode(jsonObject, this, APIService.API_FORGOT_PASSWORD);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void changePassword(String email, String code, String password) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("email", email);
            jsonObject.put("verification_code", code);
            jsonObject.put("password", password);

            getView().showLoading();
            APIUtils.getInstance().resetPassword(jsonObject, this, APIService.API_RESET_PASS);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }
}