package com.kingpin.mvp.presenter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kingpin.common.base.APIService;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.manager.TempDataManager;
import com.kingpin.common.utility.APIUtils;
import com.kingpin.mvp.model.Profile;
import com.kingpin.mvp.model.response.PlacePickListResponse;
import com.kingpin.mvp.model.response.SendFeedbackResponse;
import com.kingpin.mvp.view.SubmitFeedbackView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Response;

public class SubmitFeedbackPresenter extends BasePresenter<SubmitFeedbackView> {

    @Override
    public void onAPISuccess(int api_kind, final String result) {

        super.onAPISuccess(api_kind, result);

        switch (api_kind) {
            case APIService.API_SEND_FEEDBACK: {
                new Thread() {
                    @Override
                    public void run() {
                        getView().hideLoading();
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        //gsonBuilder.registerTypeAdapter(Kingpin.class, Kingpin.deserializer);
                        Gson gson = gsonBuilder.create();
                        final SendFeedbackResponse response = gson.fromJson(result, SendFeedbackResponse.class);

                        if (getView().toString() == null)
                            return;

                        getView().getCurrentActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (response.isSuccess())
                                    getView().onSuccessToSubmit();
                                else
                                    getView().showToast(response.getMessage());
                            }
                        });
                    }
                }.start();
                break;
            }
        }
    }

    @Override
    public void onAPIError(int api_kind, Response<ResponseBody> response) {
        super.onAPIError(api_kind, response);
        getView().onFailedToSubmit();
    }

    @Override
    public void onAPIFailure(int api_kind) {
        super.onAPIFailure(api_kind);
        getView().onFailedToSubmit();
    }

    public void sendFeedback(String feedback) {
        Profile me = DataManager.getInstance().getAccount();
        if (me == null) {
            getView().onFailedToSubmit();
            return;
        }

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("user_id", me.getId());
            jsonObject.put("message", feedback);

            getView().showLoading();
            APIUtils.getInstance().sendFeedback(jsonObject, this, APIService.API_SEND_FEEDBACK);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
