package com.kingpin.mvp.presenter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kingpin.common.base.APIService;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.manager.TempDataManager;
import com.kingpin.common.utility.APIUtils;
import com.kingpin.mvp.model.Bet;
import com.kingpin.mvp.model.Kingpin;
import com.kingpin.mvp.model.Profile;
import com.kingpin.mvp.model.response.BetHistoryResponse;
import com.kingpin.mvp.model.response.FollowResponse;
import com.kingpin.mvp.model.response.KingpinDetailResponse;
import com.kingpin.mvp.view.UserProfileView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class UserProfilePresenter extends BasePresenter<UserProfileView> {

    @Override
    public void onAPISuccess(int api_kind, final String result) {

        super.onAPISuccess(api_kind, result);

        switch (api_kind) {
            case APIService.API_GET_KINGPIN_DETAIL: {
                new Thread() {
                    @Override
                    public void run() {
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        gsonBuilder.registerTypeAdapter(Kingpin.class, Kingpin.deserializer);
                        Gson gson = gsonBuilder.create();
                        final KingpinDetailResponse response = gson.fromJson(result, KingpinDetailResponse.class);
                        if (response.isSuccess())
                            TempDataManager.getInstance().setSelectedKingpin(response.getKingpin());

                        if (getView().toString() == null)
                            return;

                        getView().getCurrentActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                getView().hideLoading();
                                if (response.isSuccess())
                                    getView().OnLoadKingpinDetail(response.getKingpin());
                                else
                                    getView().showToast(response.getMessage());
                            }
                        });
                    }
                }.start();
                break;
            }
            case APIService.API_GET_KINGPIN_HISTORY: {
                new Thread() {
                    @Override
                    public void run() {
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        gsonBuilder.registerTypeAdapter(Bet.class, Bet.deserializer);
                        Gson gson = gsonBuilder.create();
                        final BetHistoryResponse response = gson.fromJson(result, BetHistoryResponse.class);

                        if(getView().toString() == null)
                            return;

                        getView().getCurrentActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                getView().hideLoading();
                                if (response.isSuccess())
                                    getView().OnLoadBetHistories(response.getBets());
                                else
                                    getView().showToast(response.getMessage());
                            }
                        });
                    }
                }.start();
                break;
            }
            case APIService.API_GET_KINGPIN_UPCOMING: {
                new Thread() {
                    @Override
                    public void run() {
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        gsonBuilder.registerTypeAdapter(Bet.class, Bet.deserializer);
                        Gson gson = gsonBuilder.create();
                        final BetHistoryResponse response = gson.fromJson(result, BetHistoryResponse.class);

                        if(getView().toString() == null)
                            return;

                        getView().getCurrentActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                getView().hideLoading();
                                if (response.isSuccess())
                                    getView().OnLoadUpcomingBets(response.getBets());
                                else
                                    getView().showToast(response.getMessage());
                            }
                        });
                    }
                }.start();
                break;
            }
            case APIService.API_FOLLOW: {
                Gson gson = new Gson();
                final FollowResponse response = gson.fromJson(result, FollowResponse.class);
                getView().getCurrentActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getView().hideLoading();
                        if (response.isSuccess())
                            getView().OnCompletedFollow();
                        else
                            getView().showToast(response.getMessage());
                    }
                });
                break;
            }
            case APIService.API_UNFOLLOW: {
                Gson gson = new Gson();
                final FollowResponse response = gson.fromJson(result, FollowResponse.class);
                getView().getCurrentActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getView().hideLoading();
                        if (response.isSuccess())
                            getView().OnCompletedUnfollow();
                        else
                            getView().showToast(response.getMessage());
                    }
                });
                break;
            }
        }
    }

    public void getKingpinDetail() {

        int id;
        Kingpin kingpin = TempDataManager.getInstance().getSelectedKingpin();
        Integer kingpinId = TempDataManager.getInstance().getSelectedKingpinId();
        if(kingpin != null)
            id = kingpin.getId();
        else if(kingpinId != null)
            id = kingpinId;
        else
            return;

        try {
            JSONObject jsonObject = new JSONObject();
            Profile me = DataManager.getInstance().getAccount();
            if (me != null) {
                jsonObject.put("user_id", me.getId());
            }
            jsonObject.put("kingpin_id", id);
            jsonObject.put("times", new JSONArray(DataManager.getInstance().getFilterOptions().getTimePeriodString()));
            jsonObject.put("sports", new JSONArray(DataManager.getInstance().getFilterOptions().getSportsString()));
            jsonObject.put("bettypes", new JSONArray(DataManager.getInstance().getFilterOptions().getBetTypesString()));

            getView().showLoading();
            APIUtils.getInstance().getKingpinDetail(jsonObject, this, APIService.API_GET_KINGPIN_DETAIL);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getBettingHistory() {

        int id;
        Kingpin kingpin = TempDataManager.getInstance().getSelectedKingpin();
        Integer kingpinId = TempDataManager.getInstance().getSelectedKingpinId();
        if(kingpin != null)
            id = kingpin.getId();
        else if(kingpinId != null)
            id = kingpinId;
        else
            return;

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("kingpin_id", id);
            jsonObject.put("times", new JSONArray(DataManager.getInstance().getFilterOptions().getTimePeriodString()));
            jsonObject.put("sports", new JSONArray(DataManager.getInstance().getFilterOptions().getSportsString()));
            jsonObject.put("bettypes", new JSONArray(DataManager.getInstance().getFilterOptions().getBetTypesString()));

            getView().showLoading();
            APIUtils.getInstance().getKingpinHistory(jsonObject, this, APIService.API_GET_KINGPIN_HISTORY);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getUpcomingBets() {

        int id;
        Kingpin kingpin = TempDataManager.getInstance().getSelectedKingpin();
        Integer kingpinId = TempDataManager.getInstance().getSelectedKingpinId();
        if(kingpin != null)
            id = kingpin.getId();
        else if(kingpinId != null)
            id = kingpinId;
        else
            return;

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("kingpin_id", id);
            jsonObject.put("times", new JSONArray(DataManager.getInstance().getFilterOptions().getTimePeriodString()));
            jsonObject.put("sports", new JSONArray(DataManager.getInstance().getFilterOptions().getSportsString()));
            jsonObject.put("bettypes", new JSONArray(DataManager.getInstance().getFilterOptions().getBetTypesString()));

            getView().showLoading();
            APIUtils.getInstance().getKingpinUpcoming(jsonObject, this, APIService.API_GET_KINGPIN_UPCOMING);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void followKingping(Kingpin kingpin) {
        try {
            Profile me = DataManager.getInstance().getAccount();
            if (me == null) {
                return;
            }

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("user_id", me.getId());
            jsonObject.put("kingpin_id", kingpin.getId());

            getView().showLoading();
            APIUtils.getInstance().follow(jsonObject, this, APIService.API_FOLLOW);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void unfollowKingping(Kingpin kingpin) {
        try {
            Profile me = DataManager.getInstance().getAccount();
            if (me == null) {
                return;
            }

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("user_id", me.getId());
            jsonObject.put("kingpin_id", kingpin.getId());

            getView().showLoading();
            APIUtils.getInstance().unfollow(jsonObject, this, APIService.API_UNFOLLOW);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }
}