package com.kingpin.mvp.presenter;

import com.google.gson.Gson;
import com.kingpin.common.base.APIService;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.utility.APIUtils;
import com.kingpin.mvp.model.Profile;
import com.kingpin.mvp.model.response.PayoutBankResponse;
import com.kingpin.mvp.model.response.PayoutPaypalResponse;
import com.kingpin.mvp.view.RequestPayoutView;

import org.json.JSONException;
import org.json.JSONObject;

public class RequestPayoutPresenter extends BasePresenter<RequestPayoutView> {

    @Override
    public void onAPISuccess(int api_kind, String result) {

        super.onAPISuccess(api_kind, result);

        switch (api_kind) {
            case APIService.API_PAYOUT_PAYPAL: {
                getView().hideLoading();

                Gson gson = new Gson();
                PayoutPaypalResponse response = gson.fromJson(result, PayoutPaypalResponse.class);
                if (response.isSuccess()) {
                    getView().showToast("Successfully requested");
                }
                else {
                    getView().showToast(response.getMessage());
                }
                break;
            }
            case APIService.API_PAYOUT_BANK: {
                getView().hideLoading();

                Gson gson = new Gson();
                PayoutBankResponse response = gson.fromJson(result, PayoutBankResponse.class);
                if (response.isSuccess()) {
                    getView().showToast("Successfully requested");
                }
                else {
                    getView().showToast(response.getMessage());
                }
                break;
            }
        }
    }

    public void requestPayoutWithPaypal(double amount, String email) {
        Profile me = DataManager.getInstance().getAccount();
        if (me == null) {
            return;
        }
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("user_id", me.getId());
            jsonObject.put("method", "paypal");
            jsonObject.put("amount", amount);
            jsonObject.put("pp_email", email);

            getView().showLoading();
            APIUtils.getInstance().payoutWithPaypal(jsonObject, this, APIService.API_PAYOUT_PAYPAL);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void requestPayoutWithBank(double amount, String holderName, String iBanName, String swiftCode, String bankName, String bankCity, String bankAddress) {
        Profile me = DataManager.getInstance().getAccount();
        if (me == null) {
            return;
        }
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("user_id", me.getId());
            jsonObject.put("method", "bank");
            jsonObject.put("amount", amount);
            jsonObject.put("holder_name", holderName);
            jsonObject.put("iban", iBanName);
            jsonObject.put("swift", swiftCode);
            jsonObject.put("bank_name", bankName);
            jsonObject.put("bank_branch", bankCity);
            jsonObject.put("bank_address", bankAddress);

            getView().showLoading();
            APIUtils.getInstance().payoutWithBank(jsonObject, this, APIService.API_PAYOUT_BANK);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
