package com.kingpin.mvp.presenter;

import android.graphics.Bitmap;

import com.kingpin.common.manager.DataManager;
import com.kingpin.common.manager.FileManager;
import com.kingpin.common.manager.TempDataManager;
import com.kingpin.common.utility.CommonUtils;
import com.kingpin.mvp.model.Profile;
import com.kingpin.mvp.view.EditProfileView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class EditProfilePresenter extends BasePresenter<EditProfileView> {

    @Override
    public void OnUpdatedProfile() {
        super.OnUpdatedProfile();
        getView().OnUpdatedProfile();
    }

    public void saveProfile(String city, String state, String country, String twitter, String instagram, boolean bChangePassword, String oldPassword, String newPassword) {
        Profile me = DataManager.getInstance().getAccount();
        if (me == null) {
            return;
        }
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("user_id", me.getId());
            jsonObject.put("city", city);
            jsonObject.put("state", state);
            jsonObject.put("country", country);
            jsonObject.put("twitter", twitter);
            jsonObject.put("instagram", instagram);

            if(bChangePassword) {
                jsonObject.put("old_password", oldPassword);
                jsonObject.put("new_password", newPassword);
            }

            String file = "";
            Bitmap bitmap = TempDataManager.getInstance().getTempBitmap();
            if(bitmap != null) {
                String filePath = CommonUtils.getFilePath(getView().getCurrentActivity());
                String fileName = "me-" +  new SimpleDateFormat("HHmmss", Locale.US).format(new Date()) + ".jpg";
                file = filePath + "/" + fileName;
                FileManager.saveBitmap(bitmap, filePath, fileName);
            }

            updateProfile(jsonObject, file, false);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }
}