package com.kingpin.mvp.presenter;

import com.google.gson.Gson;
import com.kingpin.common.base.APIService;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.utility.APIUtils;
import com.kingpin.mvp.model.Profile;
import com.kingpin.mvp.model.response.SignoutResponse;
import com.kingpin.mvp.view.SettingView;

import org.json.JSONException;
import org.json.JSONObject;

public class SettingPresenter extends BasePresenter<SettingView> {

    @Override
    public void onAPISuccess(int api_kind, String result) {

        super.onAPISuccess(api_kind, result);

        switch (api_kind) {
            case APIService.API_SIGNOUT: {
                getView().hideLoading();

                Gson gson = new Gson();
                SignoutResponse response = gson.fromJson(result, SignoutResponse.class);
                if(response.isSuccess()) {
                    getView().onLoggedOut();
                }
                else {
                    getView().showToast(response.getMessage());
                }
                break;
            }
        }
    }

    public void signout() {
        try {

            Profile me = DataManager.getInstance().getAccount();
            if (me == null) {
                getView().onLoggedOut();
                return;
            }

            String token = DataManager.getInstance().getDeviceToken();
            if (token == null) {
                getView().onLoggedOut();
                return;
            }

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("user_id", me.getId());
            jsonObject.put("device_token", token);
            jsonObject.put("device_type", "1"); // 0: iphone, 1: android

            getView().showLoading();
            APIUtils.getInstance().signout(jsonObject, this, APIService.API_SIGNOUT);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }
}