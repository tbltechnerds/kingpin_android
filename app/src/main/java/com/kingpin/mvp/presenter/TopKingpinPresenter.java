package com.kingpin.mvp.presenter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kingpin.common.base.APIService;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.manager.TempDataManager;
import com.kingpin.common.utility.APIUtils;
import com.kingpin.mvp.model.Kingpin;
import com.kingpin.mvp.model.Profile;
import com.kingpin.mvp.model.response.KingpinListResponse;
import com.kingpin.mvp.view.TopKingpinView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Response;

public class TopKingpinPresenter extends BasePresenter<TopKingpinView> {

    @Override
    public void onAPISuccess(int api_kind, final String result) {

        super.onAPISuccess(api_kind, result);

        switch (api_kind) {
            case APIService.API_GET_TOP_KINGPINS: {
                new Thread() {
                    @Override
                    public void run() {
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        gsonBuilder.registerTypeAdapter(Kingpin.class, Kingpin.deserializer);
                        Gson gson = gsonBuilder.create();
                        final KingpinListResponse response = gson.fromJson(result, KingpinListResponse.class);
                        if (response.isSuccess())
                            TempDataManager.getInstance().addKingpins(response.getKingpins());

                        if (getView().toString() == null)
                            return;

                        getView().getCurrentActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (response.isSuccess())
                                    getView().onLoadKingpins(response.getKingpins(), response.getCurrentPage());
                                else
                                    getView().showToast(response.getMessage());
                            }
                        });
                    }
                }.start();
                break;
            }
        }
    }

    @Override
    public void onAPIError(int api_kind, Response<ResponseBody> response) {
        super.onAPIError(api_kind, response);
        getView().onLoadFailedKingpins();
    }

    @Override
    public void onAPIFailure(int api_kind) {
        super.onAPIFailure(api_kind);
        getView().onLoadFailedKingpins();
    }

    public void getTopKingpins(int page_num) {
        try {
            JSONObject jsonObject = new JSONObject();
            Profile me = DataManager.getInstance().getAccount();
            if (me != null) {
                jsonObject.put("user_id", me.getId());
            }
            jsonObject.put("times", new JSONArray(DataManager.getInstance().getFilterOptions().getTimePeriodString()));
            jsonObject.put("sports", new JSONArray(DataManager.getInstance().getFilterOptions().getSportsString()));
            jsonObject.put("activeBettors", DataManager.getInstance().getFilterOptions().isOnlyActiveKingpin() ? 1 : 0);
            jsonObject.put("keyword", DataManager.getInstance().getFilterOptions().getKeyword());
            jsonObject.put("bettypes", new JSONArray(DataManager.getInstance().getFilterOptions().getBetTypesString()));

            String sortTypString = "";
            switch (TempDataManager.getInstance().getSelectedFilter()) {
                case 0:
                    sortTypString = "rankings";
                    break;
                case 1:
                    sortTypString = "winnings";
                    break;
                case 2:
                    sortTypString = "bet_number";
                    break;
                case 3:
                    sortTypString = "last30_winnings";
                    break;
                case 4:
                    sortTypString = "wins";
                    break;
                case 5:
                    sortTypString = "win_percent";
                    break;
            }

            if(!sortTypString.isEmpty()) {
                if(TempDataManager.getInstance().getSelectedFilter() == 0) {
                    if(TempDataManager.getInstance().isFilterRevert())
                        sortTypString += "DESC";
                    else
                        sortTypString += "ASC";
                }
                else {
                    if(TempDataManager.getInstance().isFilterRevert())
                        sortTypString += "ASC";
                    else
                        sortTypString += "DESC";
                }
                jsonObject.put("sortType", sortTypString);
            }

            jsonObject.put("pagenum", page_num + 1);

            APIUtils.getInstance().getTopKingpins(jsonObject, this, APIService.API_GET_TOP_KINGPINS);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

