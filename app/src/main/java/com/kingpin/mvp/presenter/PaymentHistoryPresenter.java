package com.kingpin.mvp.presenter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kingpin.common.base.APIService;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.utility.APIUtils;
import com.kingpin.mvp.model.Profile;
import com.kingpin.mvp.model.Withdraw;
import com.kingpin.mvp.model.response.PaymentHistoryResponse;
import com.kingpin.mvp.view.PaymentHistoryView;

import org.json.JSONException;
import org.json.JSONObject;

public class PaymentHistoryPresenter extends BasePresenter<PaymentHistoryView> {

    @Override
    public void onAPISuccess(int api_kind, String result) {

        super.onAPISuccess(api_kind, result);

        switch (api_kind) {
            case APIService.API_GET_PAYMENT_HISTORY: {
                getView().hideLoading();

                GsonBuilder gsonBuilder = new GsonBuilder();
                gsonBuilder.registerTypeAdapter(Withdraw.class, Withdraw.deserializer);
                Gson gson = gsonBuilder.create();
                final PaymentHistoryResponse response = gson.fromJson(result, PaymentHistoryResponse.class);
                if(response.isSuccess())
                    getView().OnLoadPaymentHistories(response.getData().getWithdraws());
                else
                    getView().showToast(response.getMessage());
                break;
            }
        }
    }

    public void getPaymentHistory() {
        Profile me = DataManager.getInstance().getAccount();
        if (me == null) {
            return;
        }
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("user_id", me.getId());

            getView().showLoading();
            APIUtils.getInstance().getPaymentHistory(jsonObject, this, APIService.API_GET_PAYMENT_HISTORY);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
