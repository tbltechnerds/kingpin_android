package com.kingpin.mvp.presenter;

import com.google.gson.Gson;
import com.kingpin.common.base.APIService;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.utility.APIUtils;
import com.kingpin.mvp.model.Profile;
import com.kingpin.mvp.model.response.ClearBetHistoryResponse;
import com.kingpin.mvp.model.response.PayoutResponse;
import com.kingpin.mvp.view.ProfileView;

import org.json.JSONException;
import org.json.JSONObject;

public class ProfilePresenter extends BasePresenter<ProfileView> {

    @Override
    public void onAPISuccess(int api_kind, String result) {

        super.onAPISuccess(api_kind, result);

        switch (api_kind) {
            case APIService.API_GET_REAL_BALANCE: {
                getView().hideLoading();

                Gson gson = new Gson();
                PayoutResponse response = gson.fromJson(result, PayoutResponse.class);
                if (response.isSuccess()) {
                    DataManager.getInstance().setRealBalance(response.getData().getPayout());
                    DataManager.getInstance().setAffiliateEarnings(response.getData().getAffiliateEarnings());
                    DataManager.getInstance().setAffiliateAvailable(response.getData().getAffiliateAvailable());
                    getView().OnLoadRealBalance();
                }
                else {
                    getView().showToast(response.getMessage());
                }
                break;
            }
            case APIService.API_CLEAR_BET_HISTORY: {
                getView().hideLoading();

                Gson gson = new Gson();
                ClearBetHistoryResponse response = gson.fromJson(result, ClearBetHistoryResponse.class);
                if (response.isSuccess())
                    getView().showToast("Betting history cleared successfully");
                else
                    getView().showToast(response.getMessage());
                break;
            }
        }
    }

    @Override
    public void OnUpdatedProfile() {
        super.OnUpdatedProfile();
        getView().OnUpdatedProfile();
    }

    public void getAvailableBalance() {
        Profile me = DataManager.getInstance().getAccount();
        if (me == null) {
            return;
        }

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("user_id", me.getId());

            getView().showLoading();
            APIUtils.getInstance().getRealBalance(jsonObject, this, APIService.API_GET_REAL_BALANCE);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void clearBetHistory(String sport) {
        Profile me = DataManager.getInstance().getAccount();
        if (me == null) {
            return;
        }

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("user_id", me.getId());
            jsonObject.put("sport", sport);

            getView().showLoading();
            APIUtils.getInstance().clearBetHistory(jsonObject, this, APIService.API_CLEAR_BET_HISTORY);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void updateNotificationSetting(int notification) {
        Profile me = DataManager.getInstance().getAccount();
        if (me == null) {
            return;
        }

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("user_id", String.valueOf(me.getId()));
            jsonObject.put("notifications", String.valueOf(notification));
            updateProfile(jsonObject, "", false);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void updateBettingStrategy(String bettingStrategy) {
        Profile me = DataManager.getInstance().getAccount();
        if (me == null) {
            return;
        }

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("user_id", String.valueOf(me.getId()));
            jsonObject.put("betting_strategy", bettingStrategy);
            updateProfile(jsonObject, "", false);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
