package com.kingpin.mvp.presenter;

import android.app.Activity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kingpin.common.base.APIService;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.manager.TempDataManager;
import com.kingpin.common.utility.APIUtils;
import com.kingpin.mvp.model.Kingpin;
import com.kingpin.mvp.model.Profile;
import com.kingpin.mvp.model.response.KingpinListResponse;
import com.kingpin.mvp.view.MyKingpinView;
import com.kingpin.ui.activity.BaseActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Response;

public class MyKingpinPresenter extends BasePresenter<MyKingpinView> {
    @Override
    public void onAPISuccess(int api_kind, final String result) {

        super.onAPISuccess(api_kind, result);

        switch (api_kind) {
            case APIService.API_GET_MY_KINGPINS: {
                new Thread() {
                    @Override
                    public void run() {
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        gsonBuilder.registerTypeAdapter(Kingpin.class, Kingpin.deserializer);
                        Gson gson = gsonBuilder.create();
                        final KingpinListResponse response = gson.fromJson(result, KingpinListResponse.class);
                        if (response.isSuccess()) {
                            TempDataManager.getInstance().setMyKingpins(response.getKingpins());
                        }

                        BaseActivity activity = getView().getCurrentActivity();
                        if (activity != null) {
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (response.isSuccess())
                                        getView().onLoadKingpins(response.getKingpins());
                                    else
                                        getView().showToast(response.getMessage());
                                }
                            });
                        }
                    }
                }.start();
                break;
            }
        }
    }

    @Override
    public void onAPIError(int api_kind, Response<ResponseBody> response) {
        super.onAPIError(api_kind, response);
        getView().onLoadFailedKingpins();
    }

    @Override
    public void onAPIFailure(int api_kind) {
        super.onAPIFailure(api_kind);
        getView().onLoadFailedKingpins();
    }

    public void getMyKingpins() {
        try {
            JSONObject jsonObject = new JSONObject();
            Profile me = DataManager.getInstance().getAccount();
            if (me != null) {
                jsonObject.put("user_id", me.getId());
            }
            jsonObject.put("times", new JSONArray(DataManager.getInstance().getFilterOptions().getTimePeriodString()));
            jsonObject.put("sports", new JSONArray(DataManager.getInstance().getFilterOptions().getSportsString()));
            jsonObject.put("activeBettors", DataManager.getInstance().getFilterOptions().isOnlyActiveKingpin() ? 1 : 0);
            jsonObject.put("keyword", DataManager.getInstance().getFilterOptions().getKeyword());
            jsonObject.put("bettypes", new JSONArray(DataManager.getInstance().getFilterOptions().getBetTypesString()));

            APIUtils.getInstance().getMyKingpins(jsonObject, this, APIService.API_GET_MY_KINGPINS);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }
}