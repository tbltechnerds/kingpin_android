package com.kingpin.mvp.presenter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kingpin.common.base.APIService;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.manager.TempDataManager;
import com.kingpin.common.utility.APIUtils;
import com.kingpin.mvp.model.Kingpin;
import com.kingpin.mvp.model.response.PlacePickListResponse;
import com.kingpin.mvp.view.PlacePickView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Response;

public class PlacePickPresenter extends BasePresenter<PlacePickView> {

    @Override
    public void onAPISuccess(int api_kind, final String result) {

        super.onAPISuccess(api_kind, result);

        switch (api_kind) {
            case APIService.API_GET_GAMES: {
                new Thread() {
                    @Override
                    public void run() {
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        //gsonBuilder.registerTypeAdapter(Kingpin.class, Kingpin.deserializer);
                        Gson gson = gsonBuilder.create();
                        final PlacePickListResponse response = gson.fromJson(result, PlacePickListResponse.class);
                        if (response.isSuccess()) {
                            TempDataManager.getInstance().setGames(response.getGames());
                        }

                        if (getView().toString() == null)
                            return;

                        getView().getCurrentActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (response.isSuccess())
                                    getView().onLoadGames();
                                else
                                    getView().showToast(response.getMessage());
                            }
                        });
                    }
                }.start();
                break;
            }
        }
    }

    @Override
    public void onAPIError(int api_kind, Response<ResponseBody> response) {
        super.onAPIError(api_kind, response);
        getView().onLoadGames();
    }

    @Override
    public void onAPIFailure(int api_kind) {
        super.onAPIFailure(api_kind);
        getView().onLoadFailedToGetGames();
    }

    public void getGames() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("sports", new JSONArray(DataManager.getInstance().getCurrentPlacebetFilterOption()));

            APIUtils.getInstance().getGameList(jsonObject, this, APIService.API_GET_GAMES);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

}