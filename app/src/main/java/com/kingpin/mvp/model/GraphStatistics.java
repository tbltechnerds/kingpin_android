package com.kingpin.mvp.model;

import java.util.ArrayList;

public class GraphStatistics extends BaseStatistics{
    private String title;
    private String detail;
    private ArrayList<String> labelList;
    private ArrayList<Float> valueList;

    public GraphStatistics(int type, String title, String detail, ArrayList<String> labelList, ArrayList<Float> valueList) {
        super(type);
        this.title = title;
        this.detail = detail;
        this.labelList = labelList;
        this.valueList = valueList;
    }

    public String getTitle() {
        return this.title;
    }

    public String getDetail() {
        return this.detail;
    }

    public ArrayList<String> getLabelList() {
        return this.labelList;
    }

    public ArrayList<Float> getValueList() {
        return this.valueList;
    }
}
