package com.kingpin.mvp.model.response;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kingpin.common.manager.DataManager;
import com.kingpin.mvp.model.Bet;
import com.kingpin.mvp.model.BovadaGame;
import com.kingpin.mvp.model.Game;
import com.kingpin.mvp.model.RapidGame;

import java.util.ArrayList;

public class PlacePickListResponse {
    @SerializedName("status")
    @Expose
    private boolean status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private JsonArray data;

    public PlacePickListResponse() {
        this.status = false;
        this.message = "";
        this.data = new JsonArray();
    }

    public boolean isSuccess() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    private JsonArray getEvents(JsonObject gameData) {
        if (gameData == null) {
            return null;
        } else {
            if (gameData.has("events") && !gameData.get("events").isJsonNull()) {
                return gameData.get("events").getAsJsonArray();
            } else {
                return null;
            }
        }
    }

    private boolean isRapidGame(JsonObject gameData) {
        if (gameData == null) {
            return false;
        } else {
            if (gameData.has("rundown") && !gameData.get("rundown").isJsonNull()) {
                String rundown = gameData.get("rundown").getAsString();
                return rundown.equals("1");
            } else {
                return false;
            }
        }
    }

    private String getSportName(JsonObject gameData) {
        if (gameData == null) {
            return "";
        } else {
            if (gameData.has("sport_name") && !gameData.get("sport_name").isJsonNull()) {
                return gameData.get("sport_name").getAsString();
            } else {
                return "";
            }
        }
    }

    public Boolean isSoccer(JsonObject gameData) {
        if (gameData == null) {
            return false;
        } else {
            if (gameData.has("is_soccer") && !gameData.get("is_soccer").isJsonNull()) {
                return gameData.get("is_soccer").getAsBoolean();
            } else {
                return false;
            }
        }
    }

    public ArrayList<Game> getGames() {
        ArrayList<Game> games = new ArrayList<>();
        for (int gIndex = 0 ; gIndex < data.size() ; gIndex ++) {
            JsonObject gameData = data.get(gIndex).getAsJsonObject();
            String sport = getSportName(gameData);
            if (isRapidGame(gameData)) {
                JsonArray events = getEvents(gameData);
                for (int index = 0 ; index < events.size() ; index ++) {
                    JsonObject event = events.get(index).getAsJsonObject();
                    RapidGame game = RapidGame.initWithJsonObject(event, sport);
                    games.add(game);
                }
            } else {
                boolean isSoccer = isSoccer(gameData);
                JsonArray events = getEvents(gameData);
                for (int eIndex = 0 ; eIndex < events.size() ; eIndex ++) {
                    JsonObject event = events.get(eIndex).getAsJsonObject();
                    BovadaGame game = BovadaGame.initWithJsonObject(event, isSoccer, sport, DataManager.getInstance().getPlaceBetFilterOption());
                    games.add(game);
                }
            }
        }
        return games;
    }

}
