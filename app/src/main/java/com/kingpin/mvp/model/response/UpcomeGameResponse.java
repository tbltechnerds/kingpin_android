package com.kingpin.mvp.model.response;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kingpin.mvp.model.TopUsers;
import com.kingpin.mvp.model.UpcomingGame;

import java.util.ArrayList;

public class UpcomeGameResponse {
    @SerializedName("status")
    @Expose
    private boolean status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private ArrayList<UpcomingGame> games;

    @SerializedName("top_users")
    @Expose
    private JsonArray allTopUsers;

    public UpcomeGameResponse() {
        this.status = false;
        this.message = "";
        this.games = new ArrayList<>();
        this.allTopUsers = new JsonArray();
    }

    public boolean isSuccess() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public ArrayList<UpcomingGame> getGames() {
        return this.games;
    }

    public JsonArray getAllTopUsers() {
        return this.allTopUsers;
    }
}
