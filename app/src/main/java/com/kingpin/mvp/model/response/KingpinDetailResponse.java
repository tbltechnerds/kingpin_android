package com.kingpin.mvp.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kingpin.mvp.model.Kingpin;

public class KingpinDetailResponse {
    @SerializedName("status")
    @Expose
    private boolean status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private Kingpin kingpin;

    public KingpinDetailResponse() {
        this.status = false;
        this.message = "";
        this.kingpin = new Kingpin();
    }

    public boolean isSuccess() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public Kingpin getKingpin() {
        return this.kingpin;
    }
}
