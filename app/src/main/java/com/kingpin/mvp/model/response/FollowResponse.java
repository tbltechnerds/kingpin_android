package com.kingpin.mvp.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FollowResponse {
    @SerializedName("status")
    @Expose
    private boolean status;

    @SerializedName("message")
    @Expose
    private String message;

    public FollowResponse() {
        this.status = false;
        this.message = "";
    }

    public boolean isSuccess() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
