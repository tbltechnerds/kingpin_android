package com.kingpin.mvp.model;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BovadaGamePrice {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("fractional")
    @Expose
    private String fractional;

    @SerializedName("decimal")
    @Expose
    private String decimal;

    @SerializedName("handicap")
    @Expose
    private String handicap;

    @SerializedName("american")
    @Expose
    private String american;

    public BovadaGamePrice() {
        id = "";
        fractional = "N/A";
        decimal = "N/A";
        handicap = "N/A";
        american = "N/A";
    }

    public static BovadaGamePrice initWithJsonObject(JsonObject json) {

        BovadaGamePrice price = new BovadaGamePrice();

        if (json.has("id") && !json.get("id").isJsonNull()) {
            price.id = json.get("id").getAsString();
        }

        if (json.has("fractional") && !json.get("fractional").isJsonNull()) {
            price.fractional = json.get("fractional").getAsString();
        }

        if (json.has("decimal") && !json.get("decimal").isJsonNull()) {
            price.decimal = json.get("decimal").getAsString();
        }

        if (json.has("handicap") && !json.get("handicap").isJsonNull()) {
            price.handicap = json.get("handicap").getAsString();
        }

        if (json.has("american") && !json.get("american").isJsonNull()) {
            price.american = json.get("american").getAsString();
        }

        return price;
    }

    public String getId() {
        return id;
    }

    public String getFractional() {
        return fractional;
    }

    public String getDecimal() {
        return decimal;
    }

    public String getHandicap() {
        return handicap;
    }

    public String getAmerican() {
        return american;
    }
}
