package com.kingpin.mvp.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kingpin.mvp.model.Profile;

public class UpdateProfileResponse {
    @SerializedName("status")
    @Expose
    private boolean status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private Profile profile;

    public UpdateProfileResponse() {
        this.status = false;
        this.message = "";
        this.profile = null;
    }

    public boolean isSuccess() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public Profile getProfile() {
        return profile;
    }
}
