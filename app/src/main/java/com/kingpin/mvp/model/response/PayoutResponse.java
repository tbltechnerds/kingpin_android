package com.kingpin.mvp.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PayoutResponse {
    @SerializedName("status")
    @Expose
    private boolean status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private Data data;

    public PayoutResponse() {
        this.status = false;
        this.message = "";
        this.data = new Data();
    }

    public boolean isSuccess() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public Data getData() {
        return data;
    }

    public class Data {
        @SerializedName("payout")
        @Expose
        private double payout;

        @SerializedName("affiliate_earnings")
        @Expose
        private double affiliate_earnings;

        @SerializedName("commision")
        @Expose
        private double commision;

        Data() {
            this.payout = 0.0f;
        }

        public double getPayout() {
            return this.payout;
        }

        public double getAffiliateEarnings() {
            return this.affiliate_earnings;
        }

        public double getAffiliateAvailable() {
            return this.commision;
        }
    }
}
