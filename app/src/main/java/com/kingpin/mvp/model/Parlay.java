package com.kingpin.mvp.model;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kingpin.common.utility.DateUtils;

import java.lang.reflect.Type;
import java.util.Date;

public class Parlay {

    @SerializedName("parlay_id")
    @Expose
    private int parlay_id;

    @SerializedName("match_date")
    @Expose
    private String match_date;

    @SerializedName("teams")
    @Expose
    private String teams;

    @SerializedName("team_bet")
    @Expose
    private String team_bet;

    @SerializedName("result")
    @Expose
    private String result;

    @SerializedName("sport")
    @Expose
    private String sport;

    @SerializedName("line")
    @Expose
    private String line;

    @SerializedName("bet_amount")
    @Expose
    private int bet_amount;

    @SerializedName("win_loss_amount")
    @Expose
    private int win_loss_amount;

    @SerializedName("potential_win")
    @Expose
    private int potential_win;

    private Parlay() {
        this.parlay_id = -1;
        this.match_date = "";
        this.teams = "";
        this.team_bet = "";
        this.result = "";
        this.sport = "";
        this.line = "";
        this.bet_amount = 0;
        this.win_loss_amount = 0;
        this.potential_win = 0;
    }

    String getTeamBet() {
        return this.team_bet;
    }

    String getLine() {
        return this.line;
    }

    private void setParlayId(int id) {
        this.parlay_id = id;
    }

    private void setMatchDate(String date) {
        this.match_date = date;
    }

    public Date getMatchDate() {
        return DateUtils.getDateWithServerTimezone(this.match_date);
    }

    private void setTeams(String teams) {
        this.teams = teams;
    }

    public String getTeams() {
        return this.teams;
    }

    private void setTeamBet(String team_bet) {
        this.team_bet = team_bet;
    }

    private void setResult(String result) {
        this.result = result;
    }

    private void setSports(String sport) {
        this.sport = sport;
    }

    private void setLine(String line) {
        this.line = line;
    }

    private void setBetAmount(int amount) {
        this.bet_amount = amount;
    }

    private void setWinLoseAmount(int amount) {
        this.win_loss_amount = amount;
    }

    private void setPotentialWin(int amount) {
        this.potential_win = amount;
    }

    static JsonDeserializer<Parlay> deserializer = new JsonDeserializer<Parlay>() {
        @Override
        public Parlay deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject parlay_json = json.getAsJsonObject();
            Parlay parlay = new Parlay();

            if(parlay_json.has("parlay_id") && !parlay_json.get("parlay_id").isJsonNull())
                parlay.setParlayId(parlay_json.get("parlay_id").getAsInt());

            if(parlay_json.has("match_date") && !parlay_json.get("match_date").isJsonNull())
                parlay.setMatchDate(parlay_json.get("match_date").getAsString());

            if(parlay_json.has("teams") && !parlay_json.get("teams").isJsonNull())
                parlay.setTeams(parlay_json.get("teams").getAsString());

            if(parlay_json.has("team_bet") && !parlay_json.get("team_bet").isJsonNull())
                parlay.setTeamBet(parlay_json.get("team_bet").getAsString());

            if(parlay_json.has("result") && !parlay_json.get("result").isJsonNull())
                parlay.setResult(parlay_json.get("result").getAsString());

            if(parlay_json.has("sport") && !parlay_json.get("sport").isJsonNull())
                parlay.setSports(parlay_json.get("sport").getAsString());

            if(parlay_json.has("line") && !parlay_json.get("line").isJsonNull())
                parlay.setLine(parlay_json.get("line").getAsString());

            if(parlay_json.has("bet_amount") && !parlay_json.get("bet_amount").isJsonNull()) {
                int bet_amount;
                try {
                    bet_amount = parlay_json.get("bet_amount").getAsInt();
                }
                catch(Exception e) {
                    double double_value = parlay_json.get("bet_amount").getAsDouble();
                    bet_amount = (int) Math.round(double_value);
                }
                parlay.setBetAmount(bet_amount);
            }

            if(parlay_json.has("win_loss_amount") && !parlay_json.get("win_loss_amount").isJsonNull()) {
                int win_loss_amount;
                try {
                    win_loss_amount = parlay_json.get("win_loss_amount").getAsInt();
                }
                catch(Exception e) {
                    double double_value = parlay_json.get("win_loss_amount").getAsDouble();
                    win_loss_amount = (int) Math.round(double_value);
                }
                parlay.setWinLoseAmount(win_loss_amount);
            }

            if(parlay_json.has("potential_win") && !parlay_json.get("potential_win").isJsonNull()) {
                int potential_win;
                try {
                    potential_win = parlay_json.get("potential_win").getAsInt();
                }
                catch(Exception e) {
                    double double_value = parlay_json.get("potential_win").getAsDouble();
                    potential_win = (int) Math.round(double_value);
                }
                parlay.setPotentialWin(potential_win);
            }

            return parlay;
        }
    };
}
