package com.kingpin.mvp.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kingpin.mvp.model.Withdraw;

import java.util.ArrayList;

public class PaymentHistoryResponse {
    @SerializedName("status")
    @Expose
    private boolean status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private Data data;

    public PaymentHistoryResponse() {
        this.status = false;
        this.message = "";
        this.data = new Data();
    }

    public boolean isSuccess() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public Data getData() {
        return data;
    }

    public class Data {
        @SerializedName("withdrawls")
        @Expose
        private ArrayList<Withdraw> withdrawls;

        Data() {
            this.withdrawls = new ArrayList<>();
        }

        public ArrayList<Withdraw> getWithdraws() {
            return this.withdrawls;
        }
    }
}
