package com.kingpin.mvp.model;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kingpin.common.utility.DateUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Set;

public class TopUsers {

    @SerializedName("sport")
    @Expose
    private String sport;

    @SerializedName("user_info")
    @Expose
    private ArrayList<Kingpin> kingins;

    private TopUsers() {
        this.sport = "";
        this.kingins = new ArrayList<>();
    }

    public String getSportName() {
        return sport;
    }

    public void setSportName(String sport) {
        this.sport = sport;
    }

    private void addKingpin(Kingpin kingpin) {
        this.kingins.add(kingpin);
    }

    public ArrayList<Kingpin> getKingpins() {
        return kingins;
    }

    public static JsonDeserializer<TopUsers> deserializer = new JsonDeserializer<TopUsers>() {
        @Override
        public TopUsers deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject detail_json = json.getAsJsonObject();
            TopUsers topUsers = new TopUsers();
            topUsers.setSportName(detail_json.get("sport").getAsString());

            JsonElement kingpins_element = detail_json.get("user_info");
            if(kingpins_element.isJsonArray()) {
                JsonArray kingpins_array = kingpins_element.getAsJsonArray();
                for(JsonElement kingpinDic : kingpins_array) {
                    Gson gson = new Gson();
                    Kingpin kingpin = gson.fromJson(kingpinDic, Kingpin.class);
                    topUsers.addKingpin(kingpin);
                }
            }

            return topUsers;
        }
    };
}
