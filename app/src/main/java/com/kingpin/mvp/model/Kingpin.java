package com.kingpin.mvp.model;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class Kingpin {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("user_avatar")
    @Expose
    private String user_avatar;

    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("betting_strategy")
    @Expose
    private String bettingStrategy;

    @SerializedName("twitter")
    @Expose
    private String twitter;

    @SerializedName("instagram")
    @Expose
    private String instagram;

    @SerializedName("DaysBetting")
    @Expose
    private int daysBetting;

    @SerializedName("daysFollowing")
    @Expose
    private int daysFollowing;

    @SerializedName("countBets")
    @Expose
    private int countBets;

    @SerializedName("countWin")
    @Expose
    private int countWin;

    @SerializedName("countLoss")
    @Expose
    private int countLoss;

    @SerializedName("countDraw")
    @Expose
    private int countDraw;

    @SerializedName("topSport")
    @Expose
    private String topSport;

    @SerializedName("total_money_paid")
    @Expose
    private int total_money_paid;

    @SerializedName("countFollowers")
    @Expose
    private int countFollowers;

    @SerializedName("percentWin")
    @Expose
    private int percentWin;

    @SerializedName("avgBetRisk")
    @Expose
    private int avgBetRisk;

    @SerializedName("price_per_follower")
    @Expose
    private int price_per_follower;

    @SerializedName("drawdown")
    @Expose
    private int drawdown;

    @SerializedName("avgBetSize")
    @Expose
    private int avgBetSize;

    @SerializedName("avgWinSize")
    @Expose
    private int avgWinSize;

    @SerializedName("count30daysDraw")
    @Expose
    private int count30daysDraw;

    @SerializedName("count30daysLoss")
    @Expose
    private int count30daysLoss;

    @SerializedName("count30daysWin")
    @Expose
    private int count30daysWin;

    @SerializedName("count30daysBets")
    @Expose
    private int count30daysBets;

    @SerializedName("count1daysDraw")
    @Expose
    private int count1daysDraw;

    @SerializedName("count1daysLoss")
    @Expose
    private int count1daysLoss;

    @SerializedName("count1daysWin")
    @Expose
    private int count1daysWin;

    @SerializedName("count1daysBets")
    @Expose
    private int count1daysBets;

    @SerializedName("count7daysDraw")
    @Expose
    private int count7daysDraw;

    @SerializedName("count7daysLoss")
    @Expose
    private int count7daysLoss;

    @SerializedName("count7daysWin")
    @Expose
    private int count7daysWin;

    @SerializedName("count7daysBets")
    @Expose
    private int count7daysBets;

    @SerializedName("betsPer30Days")
    @Expose
    private int betsPer30Days;

    @SerializedName("winningLosses")
    @Expose
    private int winningLosses;

    @SerializedName("last30daysPercentWin")
    @Expose
    private int last30daysPercentWin;

    @SerializedName("last30daysWinnings")
    @Expose
    private int last30daysWinnings;

    @SerializedName("last1daysPercentWin")
    @Expose
    private int last1daysPercentWin;

    @SerializedName("last1daysWinnings")
    @Expose
    private int last1daysWinnings;

    @SerializedName("last7daysPercentWin")
    @Expose
    private int last7daysPercentWin;

    @SerializedName("last7daysWinnings")
    @Expose
    private int last7daysWinnings;

    @SerializedName("rank")
    @Expose
    private int rank;

    @SerializedName("manualRank")
    @Expose
    private int manualRank;

    @SerializedName("is_followed")
    @Expose
    private boolean is_followed;

    @SerializedName("is_upcoming")
    @Expose
    private boolean is_upcoming;

    @SerializedName("dates_for_upper_graph")
    @Expose
    private ArrayList<String> dates_for_upper_graph;

    @SerializedName("values_for_upper_graph")
    @Expose
    private ArrayList<Float> values_for_upper_graph;

    @SerializedName("dates_for_bottom_graph")
    @Expose
    private ArrayList<String> dates_for_bottom_graph;

    @SerializedName("values_for_bottom_graph")
    @Expose
    private ArrayList<Float> values_for_bottom_graph;

    public Kingpin() {
        this.id = -1;
        this.name = "";
        this.user_avatar = "";
        this.city = "";
        this.bettingStrategy = "";
        this.twitter = "";
        this.instagram = "";
        this.daysBetting = 0;
        this.daysFollowing = 0;
        this.countBets = 0;
        this.countWin = 0;
        this.countLoss = 0;
        this.countDraw = 0;
        this.topSport = "";
        this.total_money_paid = 0;
        this.countFollowers = 0;
        this.percentWin = 0;
        this.avgBetRisk = 0;
        this.drawdown = 0;
        this.avgBetSize = 0;
        this.avgWinSize = 0;
        this.count30daysDraw = 0;
        this.count30daysLoss = 0;
        this.count30daysWin = 0;
        this.count30daysBets = 0;
        this.count1daysDraw = 0;
        this.count1daysLoss = 0;
        this.count1daysWin = 0;
        this.count1daysBets = 0;
        this.count7daysDraw = 0;
        this.count7daysLoss = 0;
        this.count7daysWin = 0;
        this.count7daysBets = 0;
        this.betsPer30Days = 0;
        this.winningLosses = 0;
        this.last30daysPercentWin = 0;
        this.last30daysWinnings = 0;
        this.last1daysPercentWin = 0;
        this.last1daysWinnings = 0;
        this.last7daysPercentWin = 0;
        this.last7daysWinnings = 0;
        this.rank = 1000000;
        this.manualRank = 1000000;
        this.is_followed = false;
        this.is_upcoming = false;
        this.dates_for_upper_graph = new ArrayList<>();
        this.values_for_upper_graph = new ArrayList<>();
        this.dates_for_bottom_graph = new ArrayList<>();
        this.values_for_bottom_graph = new ArrayList<>();
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    private void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return this.city;
    }

    public void setBettingStrategy(String bettingStrategy) {
        this.bettingStrategy = bettingStrategy;
    }

    public String getBettingStrategy() { return this.bettingStrategy; }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getTwitter() { return twitter; }

    public  void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getInstagram() { return instagram; }

    private void setRank(int rank) {
        this.rank = rank;
    }

    public int getRank() {
        if (manualRank < 1000000) {
            return manualRank;
        } else {
            return rank;
        }
    }

    private void setManualRank(int rank) {
        this.manualRank = rank;
    }

    public String getRankLabel(String sport) {
        int realRank = getRank();
        if (realRank == 1) {
            return "#1 " + sport;
        } else if (realRank == 2) {
            return "#2 " + sport;
        } else if (realRank == 1000000) {
            return "NR";
        } else {
            return "#" + String.valueOf(realRank);
        }
    }

    private void setDaysFollowing(int daysFollowing) {
        this.daysFollowing = daysFollowing;
    }

    public int getDaysFollowing() {
        return this.daysFollowing;
    }

    private void setDaysBetting(int daysBetting) {
        this.daysBetting = daysBetting;
    }

    public int getDaysBetting() {
        return this.daysBetting;
    }

    private void setCountBets(int countBets) {
        this.countBets = countBets;
    }

    public int getCountBets() {
        return this.countBets;
    }

    private void setWinningLosses(int winningLosses) {
        this.winningLosses = winningLosses;
    }

    public int getWinningLosses() {
        return this.winningLosses;
    }

    private void setTotalMoneyPaid(int total_money_paid) {
        this.total_money_paid = total_money_paid;
    }

    public int getTotalMoneyPaid() {
        return this.total_money_paid;
    }

    private void setTopSport(String topSport) {
        this.topSport = topSport;
    }

    public String getTopSport() {
        return this.topSport;
    }

    private void setCountWin(int countWin) {
        this.countWin = countWin;
    }

    public int getCountWin() {
        return this.countWin;
    }

    private void setCountLoss(int countLoss) {
        this.countLoss = countLoss;
    }

    public int getCountLoss() {
        return this.countLoss;
    }

    private void setCountDraw(int countDraw) {
        this.countDraw = countDraw;
    }

    public int getCountDraw() {
        return this.countDraw;
    }

    private void setPercentWin(int percentWin) {
        this.percentWin = percentWin;
    }

    public int getPercentWin() {
        return this.percentWin;
    }

    private void setUpcoming(boolean is_upcoming) {
        this.is_upcoming = is_upcoming;
    }

    public boolean isUpcoming() {
        return this.is_upcoming;
    }

    private void setAvatar(String avatar) {
        this.user_avatar = avatar;
    }

    public String getAvatar() {
        return this.user_avatar;
    }

    private void setCountFollowers(int countFollowers) {
        this.countFollowers = countFollowers;
    }

    public int getCountFollowers() {
        return this.countFollowers;
    }

    private void setAvgBetRisk(int avgBetRisk) {
        this.avgBetRisk = avgBetRisk;
    }

    public int getAvgBetRisk() {
        return this.avgBetRisk;
    }

    private void setAvgBetSize(int avgBetSize) {
        this.avgBetSize = avgBetSize;
    }

    public int getAvgBetSize() {
        return this.avgBetSize;
    }

    private void setAvgWinSize(int avgWinSize) {
        this.avgWinSize = avgWinSize;
    }

    public int getAvgWinSize() {
        return this.avgWinSize;
    }

    private void setPricePerFollower(int price_per_follower) {
        this.price_per_follower = price_per_follower;
    }

    private void setDrawdown(int drawdown) {
        this.drawdown = drawdown;
    }

    private void setCount30daysBets(int count30daysBets) {
        this.count30daysBets = count30daysBets;
    }

    public int getCount30daysBets() {
        return this.count30daysBets;
    }

    private void setCount30daysWin(int count30daysWin) {
        this.count30daysWin = count30daysWin;
    }

    public int getCount30daysWin() {
        return this.count30daysWin;
    }

    private void setCount30daysDraw(int count30daysDraw) {
        this.count30daysDraw = count30daysDraw;
    }

    public int getCount30daysDraw() {
        return this.count30daysDraw;
    }

    private void setCount30daysLoss(int count30daysLoss) {
        this.count30daysLoss = count30daysLoss;
    }

    public int getCount30daysLoss() {
        return this.count30daysLoss;
    }

    // yesterday
    private void setCount1daysBets(int count1daysBets) {
        this.count1daysBets = count1daysBets;
    }

    public int getCount1daysBets() {
        return this.count1daysBets;
    }

    private void setCount1daysWin(int count1daysWin) {
        this.count1daysWin = count1daysWin;
    }

    public int getCount1daysWin() {
        return this.count1daysWin;
    }

    private void setCount1daysDraw(int count1daysDraw) {
        this.count1daysDraw = count1daysDraw;
    }

    public int getCount1daysDraw() {
        return this.count1daysDraw;
    }

    private void setCount1daysLoss(int count1daysLoss) {
        this.count1daysLoss = count1daysLoss;
    }

    public int getCount1daysLoss() {
        return this.count1daysLoss;
    }

    // week
    private void setCount7daysBets(int count7daysBets) {
        this.count7daysBets = count7daysBets;
    }

    public int getCount7daysBets() {
        return this.count7daysBets;
    }

    private void setCount7daysWin(int count7daysWin) {
        this.count7daysWin = count7daysWin;
    }

    public int getCount7daysWin() {
        return this.count7daysWin;
    }

    private void setCount7daysDraw(int count7daysDraw) {
        this.count7daysDraw = count7daysDraw;
    }

    public int getCount7daysDraw() {
        return this.count7daysDraw;
    }

    private void setCount7daysLoss(int count7daysLoss) {
        this.count7daysLoss = count7daysLoss;
    }

    public int getCount7daysLoss() {
        return this.count7daysLoss;
    }
    /////////

    private void setBetsPer30Days(int betsPer30Days) {
        this.betsPer30Days = betsPer30Days;
    }

    public int getBetsPer30Days() {
        return this.betsPer30Days;
    }

    // 30 days ago
    private void setLast30daysPercentWin(int last30daysPercentWin) {
        this.last30daysPercentWin = last30daysPercentWin;
    }

    public int getLast30daysPercentWin() {
        return this.last30daysPercentWin;
    }

    private void setLast30daysWinnings(int last30daysWinnings) {
        this.last30daysWinnings = last30daysWinnings;
    }

    public int getLast30daysWinnings() {
        return this.last30daysWinnings;
    }

    // 30 days ago
    private void setLast7daysPercentWin(int last7daysPercentWin) {
        this.last7daysPercentWin = last7daysPercentWin;
    }

    public int getLast7daysPercentWin() {
        return this.last7daysPercentWin;
    }

    private void setLast7daysWinnings(int last7daysWinnings) {
        this.last7daysWinnings = last7daysWinnings;
    }

    public int getLast7daysWinnings() {
        return this.last7daysWinnings;
    }

    // yesterday ago
    private void setLast1daysPercentWin(int last1daysPercentWin) {
        this.last1daysPercentWin = last1daysPercentWin;
    }

    public int getLast1daysPercentWin() {
        return this.last1daysPercentWin;
    }

    private void setLast1daysWinnings(int last1daysWinnings) {
        this.last1daysWinnings = last1daysWinnings;
    }

    public int getLast1daysWinnings() {
        return this.last1daysWinnings;
    }
    ///////////

    private void setFollowed(boolean is_followed) {
        this.is_followed = is_followed;
    }

    public boolean isFollowed() {
        return this.is_followed;
    }

    private void addDateForUpperGraph(String value) {
        this.dates_for_upper_graph.add(value);
    }

    private void addDateForBottomGraph(String value) {
        this.dates_for_bottom_graph.add(value);
    }

    private void addValueForUpperGraph(float value) {
        this.values_for_upper_graph.add(value);
    }

    private void addValueForBottomGraph(float value) {
        this.values_for_bottom_graph.add(value);
    }

    public ArrayList<Float> getValuesForUpperGraph() {
        return this.values_for_upper_graph;
    }

    public ArrayList<String> getDatesForUpperGraph() {
        return this.dates_for_upper_graph;
    }

    public static JsonDeserializer<Kingpin> deserializer = new JsonDeserializer<Kingpin>() {
        @Override
        public Kingpin deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject kingpin_json = json.getAsJsonObject();
            Kingpin kingpin = new Kingpin();

            if(kingpin_json.has("id") && !kingpin_json.get("id").isJsonNull())
                kingpin.setId(kingpin_json.get("id").getAsInt());

            if(kingpin_json.has("name") && !kingpin_json.get("name").isJsonNull())
                kingpin.setName(kingpin_json.get("name").getAsString());

            if(kingpin_json.has("user_avatar") && !kingpin_json.get("user_avatar").isJsonNull())
                kingpin.setAvatar(kingpin_json.get("user_avatar").getAsString());

            if(kingpin_json.has("city") && !kingpin_json.get("city").isJsonNull())
                kingpin.setCity(kingpin_json.get("city").getAsString());

            if(kingpin_json.has("betting_strategy") && !kingpin_json.get("betting_strategy").isJsonNull())
                kingpin.setBettingStrategy(kingpin_json.get("betting_strategy").getAsString());

            if(kingpin_json.has("twitter") && !kingpin_json.get("twitter").isJsonNull())
                kingpin.setTwitter(kingpin_json.get("twitter").getAsString());

            if(kingpin_json.has("instagram") && !kingpin_json.get("instagram").isJsonNull())
                kingpin.setInstagram(kingpin_json.get("instagram").getAsString());

            if(kingpin_json.has("daysBetting") && !kingpin_json.get("daysBetting").isJsonNull()) {
                int daysBetting = 0;
                try {
                    //daysBetting = kingpin_json.get("daysBetting").getAsInt();
                    double double_value = kingpin_json.get("daysBetting").getAsDouble();
                    daysBetting = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setDaysBetting(daysBetting);
            }

            if(kingpin_json.has("daysFollowing") && !kingpin_json.get("daysFollowing").isJsonNull()) {
                int daysFollowing = 0;
                try {
                    //daysFollowing = kingpin_json.get("daysFollowing").getAsInt();
                    double double_value = kingpin_json.get("daysFollowing").getAsDouble();
                    daysFollowing = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setDaysFollowing(daysFollowing);
            }

            if(kingpin_json.has("countBets") && !kingpin_json.get("countBets").isJsonNull()) {
                int countBets = 0;
                try {
                    //countBets = kingpin_json.get("countBets").getAsInt();
                    double double_value = kingpin_json.get("countBets").getAsDouble();
                    countBets = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setCountBets(countBets);
            }

            if(kingpin_json.has("winningLosses") && !kingpin_json.get("winningLosses").isJsonNull()) {
                int winningLosses = 0;
                try {
                    //winningLosses = kingpin_json.get("winningLosses").getAsInt();
                    double double_value = kingpin_json.get("winningLosses").getAsDouble();
                    winningLosses = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setWinningLosses(winningLosses);
            }

            if(kingpin_json.has("countWin") && !kingpin_json.get("countWin").isJsonNull()) {
                int countWin = 0;
                try {
                    //countWin = kingpin_json.get("countWin").getAsInt();
                    double double_value = kingpin_json.get("countWin").getAsDouble();
                    countWin = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setCountWin(countWin);
            }

            if(kingpin_json.has("countLoss") && !kingpin_json.get("countLoss").isJsonNull()) {
                int countLoss = 0;
                try {
                    //countLoss = kingpin_json.get("countLoss").getAsInt();
                    double double_value = kingpin_json.get("countLoss").getAsDouble();
                    countLoss = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setCountLoss(countLoss);
            }

            if(kingpin_json.has("countDraw") && !kingpin_json.get("countDraw").isJsonNull()) {
                int countDraw = 0;
                try {
                    //countDraw = kingpin_json.get("countDraw").getAsInt();
                    double double_value = kingpin_json.get("countDraw").getAsDouble();
                    countDraw = (int) Math.round(double_value);
                } catch (Exception e) {

                }
                kingpin.setCountDraw(countDraw);
            }

            if(kingpin_json.has("topSport") && !kingpin_json.get("topSport").isJsonNull())
                kingpin.setTopSport(kingpin_json.get("topSport").getAsString());

            if(kingpin_json.has("total_money_paid") && !kingpin_json.get("total_money_paid").isJsonNull()) {
                int total_money_paid = 0;
                try {
                    //total_money_paid = kingpin_json.get("total_money_paid").getAsInt();
                    double double_value = kingpin_json.get("total_money_paid").getAsDouble();
                    total_money_paid = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setTotalMoneyPaid(total_money_paid);
            }

            if(kingpin_json.has("countFollowers") && !kingpin_json.get("countFollowers").isJsonNull()) {
                int countFollowers = 0;
                try {
                    //countFollowers = kingpin_json.get("countFollowers").getAsInt();
                    double double_value = kingpin_json.get("countFollowers").getAsDouble();
                    countFollowers = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setCountFollowers(countFollowers);
            }

            if(kingpin_json.has("percentWin") && !kingpin_json.get("percentWin").isJsonNull()) {
                int percentWin = 0;
                try {
                    //percentWin = kingpin_json.get("percentWin").getAsInt();
                    double double_value = kingpin_json.get("percentWin").getAsDouble();
                    percentWin = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setPercentWin(percentWin);
            }

            if(kingpin_json.has("avgBetRisk") && !kingpin_json.get("avgBetRisk").isJsonNull()) {
                int avgBetRisk = 0;
                try {
                    //avgBetRisk = kingpin_json.get("avgBetRisk").getAsInt();
                    double double_value = kingpin_json.get("avgBetRisk").getAsDouble();
                    avgBetRisk = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setAvgBetRisk(avgBetRisk);
            }

            if(kingpin_json.has("price_per_follower") && !kingpin_json.get("price_per_follower").isJsonNull()) {
                int pricePerFollower = 0;
                try {
                    //pricePerFollower = kingpin_json.get("price_per_follower").getAsInt();
                    double double_value = kingpin_json.get("price_per_follower").getAsDouble();
                    pricePerFollower = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setPricePerFollower(pricePerFollower);
            }

            if(kingpin_json.has("drawdown") && !kingpin_json.get("drawdown").isJsonNull()) {
                int drawdown = 0;
                try {
                    //drawdown = kingpin_json.get("drawdown").getAsInt();
                    double double_value = kingpin_json.get("drawdown").getAsDouble();
                    drawdown = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setDrawdown(drawdown);
            }

            if(kingpin_json.has("avgBetSize") && !kingpin_json.get("avgBetSize").isJsonNull()) {
                int avgBetSize = 0;
                try {
                    //avgBetSize = kingpin_json.get("avgBetSize").getAsInt();
                    double double_value = kingpin_json.get("avgBetSize").getAsDouble();
                    avgBetSize = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setAvgBetSize(avgBetSize);
            }

            if(kingpin_json.has("avgWinSize") && !kingpin_json.get("avgWinSize").isJsonNull()) {
                int avgWinSize = 0;
                try {
                    avgWinSize = kingpin_json.get("avgWinSize").getAsInt();
                    double double_value = kingpin_json.get("avgWinSize").getAsDouble();
                    avgWinSize = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setAvgWinSize(avgWinSize);
            }

            if(kingpin_json.has("count30daysBets") && !kingpin_json.get("count30daysBets").isJsonNull()) {
                int count30daysBets = 0;
                try {
                    //count30daysBets = kingpin_json.get("count30daysBets").getAsInt();
                    double double_value = kingpin_json.get("count30daysBets").getAsDouble();
                    count30daysBets = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setCount30daysBets(count30daysBets);
            }

            if(kingpin_json.has("count30daysWin") && !kingpin_json.get("count30daysWin").isJsonNull()) {
                int count30daysWin = 0;
                try {
                    //count30daysWin = kingpin_json.get("count30daysWin").getAsInt();
                    double double_value = kingpin_json.get("count30daysWin").getAsDouble();
                    count30daysWin = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setCount30daysWin(count30daysWin);
            }

            if(kingpin_json.has("count30daysDraw") && !kingpin_json.get("count30daysDraw").isJsonNull()) {
                int count30daysDraw = 0;
                try {
                    //count30daysDraw = kingpin_json.get("count30daysDraw").getAsInt();
                    double double_value = kingpin_json.get("count30daysDraw").getAsDouble();
                    count30daysDraw = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setCount30daysDraw(count30daysDraw);
            }

            if(kingpin_json.has("count30daysLoss") && !kingpin_json.get("count30daysLoss").isJsonNull()) {
                int count30daysLoss = 0;
                try {
                    //count30daysLoss = kingpin_json.get("count30daysLoss").getAsInt();
                    double double_value = kingpin_json.get("count30daysLoss").getAsDouble();
                    count30daysLoss = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setCount30daysLoss(count30daysLoss);
            }

            // yesterday
            if(kingpin_json.has("count1daysBets") && !kingpin_json.get("count1daysBets").isJsonNull()) {
                int count1daysBets = 0;
                try {
                    //count1daysBets = kingpin_json.get("count1daysBets").getAsInt();
                    double double_value = kingpin_json.get("count1daysBets").getAsDouble();
                    count1daysBets = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setCount1daysBets(count1daysBets);
            }

            if(kingpin_json.has("count1daysWin") && !kingpin_json.get("count1daysWin").isJsonNull()) {
                int count1daysWin = 0;
                try {
                    //count1daysWin = kingpin_json.get("count1daysWin").getAsInt();
                    double double_value = kingpin_json.get("count1daysWin").getAsDouble();
                    count1daysWin = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setCount1daysWin(count1daysWin);
            }

            if(kingpin_json.has("count1daysDraw") && !kingpin_json.get("count1daysDraw").isJsonNull()) {
                int count1daysDraw = 0;
                try {
                    //count1daysDraw = kingpin_json.get("count1daysDraw").getAsInt();
                    double double_value = kingpin_json.get("count1daysDraw").getAsDouble();
                    count1daysDraw = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setCount1daysDraw(count1daysDraw);
            }

            if(kingpin_json.has("count1daysLoss") && !kingpin_json.get("count1daysLoss").isJsonNull()) {
                int count1daysLoss = 0;
                try {
                    //count1daysLoss = kingpin_json.get("count1daysLoss").getAsInt();
                    double double_value = kingpin_json.get("count1daysLoss").getAsDouble();
                    count1daysLoss = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setCount1daysLoss(count1daysLoss);
            }

            // week ago
            if(kingpin_json.has("count7daysBets") && !kingpin_json.get("count7daysBets").isJsonNull()) {
                int count7daysBets = 0;
                try {
                    //count7daysBets = kingpin_json.get("count7daysBets").getAsInt();
                    double double_value = kingpin_json.get("count7daysBets").getAsDouble();
                    count7daysBets = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setCount7daysBets(count7daysBets);
            }

            if(kingpin_json.has("count7daysWin") && !kingpin_json.get("count7daysWin").isJsonNull()) {
                int count7daysWin = 0;
                try {
                    //count7daysWin = kingpin_json.get("count7daysWin").getAsInt();
                    double double_value = kingpin_json.get("count7daysWin").getAsDouble();
                    count7daysWin = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setCount7daysWin(count7daysWin);
            }

            if(kingpin_json.has("count7daysDraw") && !kingpin_json.get("count7daysDraw").isJsonNull()) {
                int count7daysDraw = 0;
                try {
                    //count7daysDraw = kingpin_json.get("count7daysDraw").getAsInt();
                    double double_value = kingpin_json.get("count7daysDraw").getAsDouble();
                    count7daysDraw = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setCount7daysDraw(count7daysDraw);
            }

            if(kingpin_json.has("count7daysLoss") && !kingpin_json.get("count7daysLoss").isJsonNull()) {
                int count7daysLoss = 0;
                try {
                    //count7daysLoss = kingpin_json.get("count7daysLoss").getAsInt();
                    double double_value = kingpin_json.get("count7daysLoss").getAsDouble();
                    count7daysLoss = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setCount7daysLoss(count7daysLoss);
            }
            /////////////////

            if(kingpin_json.has("betsPer30Days") && !kingpin_json.get("betsPer30Days").isJsonNull()) {
                int betsPer30Days = 0;
                try {
                    //betsPer30Days = kingpin_json.get("betsPer30Days").getAsInt();
                    double double_value = kingpin_json.get("betsPer30Days").getAsDouble();
                    betsPer30Days = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setBetsPer30Days(betsPer30Days);
            }

            // 30 days ago
            if(kingpin_json.has("last30daysPercentWin") && !kingpin_json.get("last30daysPercentWin").isJsonNull()) {
                int last30daysPercentWin = 0;
                try {
                    //last30daysPercentWin = kingpin_json.get("last30daysPercentWin").getAsInt();
                    double double_value = kingpin_json.get("last30daysPercentWin").getAsDouble();
                    last30daysPercentWin = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setLast30daysPercentWin(last30daysPercentWin);
            }

            if(kingpin_json.has("last30daysWinnings") && !kingpin_json.get("last30daysWinnings").isJsonNull()) {
                int last30daysWinnings = 0;
                try {
                    //last30daysWinnings = kingpin_json.get("last30daysWinnings").getAsInt();
                    double double_value = kingpin_json.get("last30daysWinnings").getAsDouble();
                    last30daysWinnings = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setLast30daysWinnings(last30daysWinnings);
            }

            // yesterday ago
            if(kingpin_json.has("last1daysPercentWin") && !kingpin_json.get("last1daysPercentWin").isJsonNull()) {
                int last1daysPercentWin = 0;
                try {
                    //last1daysPercentWin = kingpin_json.get("last1daysPercentWin").getAsInt();
                    double double_value = kingpin_json.get("last1daysPercentWin").getAsDouble();
                    last1daysPercentWin = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setLast1daysPercentWin(last1daysPercentWin);
            }

            if(kingpin_json.has("last1daysWinnings") && !kingpin_json.get("last1daysWinnings").isJsonNull()) {
                int last1daysWinnings = 0;
                try {
                    //last1daysWinnings = kingpin_json.get("last1daysWinnings").getAsInt();
                    double double_value = kingpin_json.get("last1daysWinnings").getAsDouble();
                    last1daysWinnings = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setLast1daysWinnings(last1daysWinnings);
            }

            // 7 days ago
            if(kingpin_json.has("last7daysPercentWin") && !kingpin_json.get("last7daysPercentWin").isJsonNull()) {
                int last7daysPercentWin = 0;
                try {
                    //last7daysPercentWin = kingpin_json.get("last7daysPercentWin").getAsInt();
                    double double_value = kingpin_json.get("last7daysPercentWin").getAsDouble();
                    last7daysPercentWin = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setLast7daysPercentWin(last7daysPercentWin);
            }

            if(kingpin_json.has("last7daysWinnings") && !kingpin_json.get("last7daysWinnings").isJsonNull()) {
                int last7daysWinnings = 0;
                try {
                    //last7daysWinnings = kingpin_json.get("last7daysWinnings").getAsInt();
                    double double_value = kingpin_json.get("last7daysWinnings").getAsDouble();
                    last7daysWinnings = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setLast7daysWinnings(last7daysWinnings);
            }
            ///////////////

            if(kingpin_json.has("rank") && !kingpin_json.get("rank").isJsonNull()) {
                int rank = 0;
                try {
                    //rank = kingpin_json.get("rank").getAsInt();
                    double double_value = kingpin_json.get("rank").getAsDouble();
                    rank = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setRank(rank);
            }

            if(kingpin_json.has("manualRank") && !kingpin_json.get("manualRank").isJsonNull()) {
                int rank = 0;
                try {
                    //rank = kingpin_json.get("rank").getAsInt();
                    double double_value = kingpin_json.get("manualRank").getAsDouble();
                    rank = (int) Math.round(double_value);
                }
                catch(Exception e) {

                }
                kingpin.setManualRank(rank);
            }

            if(kingpin_json.has("is_followed") && !kingpin_json.get("is_followed").isJsonNull())
                kingpin.setFollowed(kingpin_json.get("is_followed").getAsBoolean());

            if(kingpin_json.has("is_upcoming") && !kingpin_json.get("is_upcoming").isJsonNull())
                kingpin.setUpcoming(kingpin_json.get("is_upcoming").getAsBoolean());

            if(kingpin_json.has("dates_for_upper_graph")
                    && !kingpin_json.get("dates_for_upper_graph").isJsonNull()
                    && kingpin_json.get("dates_for_upper_graph").isJsonArray()
            ) {
                JsonArray array = kingpin_json.get("dates_for_upper_graph").getAsJsonArray();
                for(JsonElement item : array) {
                    kingpin.addDateForUpperGraph(item.getAsString());
                }
            }

            if(kingpin_json.has("dates_for_bottom_graph")
                    && !kingpin_json.get("dates_for_bottom_graph").isJsonNull()
                    && kingpin_json.get("dates_for_bottom_graph").isJsonArray()
            ) {
                JsonArray array = kingpin_json.get("dates_for_bottom_graph").getAsJsonArray();
                for(JsonElement item : array) {
                    kingpin.addDateForBottomGraph(item.getAsString());
                }
            }

            if(kingpin_json.has("values_for_upper_graph")
                    && !kingpin_json.get("values_for_upper_graph").isJsonNull()
                    && kingpin_json.get("values_for_upper_graph").isJsonArray()
            ) {
                JsonArray array = kingpin_json.get("values_for_upper_graph").getAsJsonArray();
                for(JsonElement item : array) {
                    kingpin.addValueForUpperGraph(item.getAsFloat());
                }
            }

            if(kingpin_json.has("values_for_bottom_graph")
                    && !kingpin_json.get("values_for_bottom_graph").isJsonNull()
                    && kingpin_json.get("values_for_bottom_graph").isJsonArray()
            ) {
                JsonArray array = kingpin_json.get("values_for_bottom_graph").getAsJsonArray();
                for(JsonElement item : array) {
                    kingpin.addValueForBottomGraph(item.getAsFloat());
                }
            }

            return kingpin;
        }
    };
}
