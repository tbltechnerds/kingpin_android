package com.kingpin.mvp.model;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.kingpin.common.base.Constant;
import com.kingpin.common.utility.DateUtils;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;

public class BovadaGame extends Game {

    private boolean isSoccerType;
    private String desc;

    private String startTime;

    private BovadaGameOutcome moneyline1;
    private BovadaGameOutcome moneyline2;
    private BovadaGameOutcome moneylineX;

    private BovadaGameOutcome spread1;
    private BovadaGameOutcome spread2;

    private BovadaGameOutcome total1;
    private BovadaGameOutcome total2;

    public BovadaGame() {
        super();

        isSoccerType = false;
        desc = "";
        startTime = "";

        moneyline1 = new BovadaGameOutcome();
        moneyline2 = new BovadaGameOutcome();
        moneylineX = new BovadaGameOutcome();

        spread1 = new BovadaGameOutcome();
        spread2 = new BovadaGameOutcome();

        total1 = new BovadaGameOutcome();
        total2 = new BovadaGameOutcome();
    }

    public static ArrayList<Constant.FILTER_SPORT> getExceptionSports() {
        ArrayList<Constant.FILTER_SPORT> sports = new ArrayList<>();
        sports.add(Constant.FILTER_SPORT.MMA);
        sports.add(Constant.FILTER_SPORT.TENNIS);
        sports.add(Constant.FILTER_SPORT.BOXING);
        return sports;
    }

    public static BovadaGame initWithJsonObject(JsonObject json, boolean isSoccer, String sport, Constant.FILTER_SPORT filterOption)  {

        BovadaGame game = new BovadaGame();
        game.setIsBovada(true);
        game.setSport(sport);
        game.isSoccerType = isSoccer;

        if (json.has("description") && !json.get("description").isJsonNull()) {
            game.desc = json.get("description").getAsString();
        }

        if (json.has("startTime") && !json.get("startTime").isJsonNull()) {
            game.startTime = json.get("startTime").getAsString();
        }

        if (isSoccer) {
            if (json.has("markets") && !json.get("markets").isJsonNull()) {
                JsonArray markets = json.get("markets").getAsJsonArray();
                for (int index = 0 ; index < markets.size() ; index ++) {
                    JsonObject market = markets.get(index).getAsJsonObject();
                    if (!market.has("code") || market.get("code").isJsonNull()) {
                        continue;
                    }
                    String code = market.get("code").getAsString();

                    if (!market.has("period") || market.get("period").isJsonNull()) {
                        continue;
                    }
                    JsonObject period = market.get("period").getAsJsonObject();

                    if (!period.has("code") || period.get("code").isJsonNull()) {
                        continue;
                    }
                    String periodCode = period.get("code").getAsString();

                    if (!market.has("outcomes") || market.get("outcomes").isJsonNull()) {
                        continue;
                    }
                    JsonArray outcomes = market.get("outcomes").getAsJsonArray();

                    if (outcomes.size() < 2) {
                        continue;
                    }

                    if (outcomes.size() > 2) {
                        if (code.equals("3W-1X2") && periodCode.equals("REG")) {
                            JsonObject outcome0 = outcomes.get(0).getAsJsonObject();
                            game.moneyline1 = BovadaGameOutcome.initWithJsonObject(outcome0);

                            JsonObject outcome1= outcomes.get(1).getAsJsonObject();
                            game.moneyline2 = BovadaGameOutcome.initWithJsonObject(outcome1);

                            JsonObject outcome2 = outcomes.get(2).getAsJsonObject();
                            game.moneylineX = BovadaGameOutcome.initWithJsonObject(outcome2);
                        }
                    }

                    if (code.equals("2W-HCAP") && periodCode.equals("REG")) {
                        JsonObject outcome0 = outcomes.get(0).getAsJsonObject();
                        game.spread1 = BovadaGameOutcome.initWithJsonObject(outcome0);

                        JsonObject outcome1 = outcomes.get(1).getAsJsonObject();
                        game.spread2 = BovadaGameOutcome.initWithJsonObject(outcome1);
                    }

                    if (code.equals("2W-OU") && periodCode.equals("REG")) {
                        JsonObject outcome0 = outcomes.get(0).getAsJsonObject();
                        game.total1 = BovadaGameOutcome.initWithJsonObject(outcome0);

                        JsonObject outcome1 = outcomes.get(1).getAsJsonObject();
                        game.total2 = BovadaGameOutcome.initWithJsonObject(outcome1);
                    }
                }
            }
        } else {
            if (json.has("markets") && !json.get("markets").isJsonNull()) {
                JsonArray markets = json.get("markets").getAsJsonArray();
                for (int index = 0 ; index < markets.size() ; index ++) {
                    JsonObject market = markets.get(index).getAsJsonObject();

                    if (!market.has("code") || market.get("code").isJsonNull()) {
                        continue;
                    }
                    String code = market.get("code").getAsString();

                    if (!market.has("period") || market.get("period").isJsonNull()) {
                        continue;
                    }
                    JsonObject period = market.get("period").getAsJsonObject();

                    if (!period.has("code") || period.get("code").isJsonNull()) {
                        continue;
                    }
                    String periodCode = period.get("code").getAsString();

                    if (!market.has("outcomes") || market.get("outcomes").isJsonNull()) {
                        continue;
                    }
                    JsonArray outcomes = market.get("outcomes").getAsJsonArray();

                    if (outcomes.size() < 2) {
                        continue;
                    }

                    if (code.equals("2W-12") && periodCode.equals("MATCH")) {
                        if (BovadaGame.getExceptionSports().contains(filterOption)) {
                            JsonObject outcome0 = outcomes.get(0).getAsJsonObject();
                            game.moneyline1 = BovadaGameOutcome.initWithJsonObject(outcome0);

                            JsonObject outcome1 = outcomes.get(1).getAsJsonObject();
                            game.moneyline2 = BovadaGameOutcome.initWithJsonObject(outcome1);
                        } else {
                            JsonObject outcome0 = outcomes.get(1).getAsJsonObject();
                            game.moneyline1 = BovadaGameOutcome.initWithJsonObject(outcome0);

                            JsonObject outcome1 = outcomes.get(0).getAsJsonObject();
                            game.moneyline2 = BovadaGameOutcome.initWithJsonObject(outcome1);
                        }
                    }

                    if (code.equals("2W-HCAP") && periodCode.equals("MATCH")) {
                        if (BovadaGame.getExceptionSports().contains(filterOption)) {
                            JsonObject outcome0 = outcomes.get(0).getAsJsonObject();
                            game.spread1 = BovadaGameOutcome.initWithJsonObject(outcome0);

                            JsonObject outcome1 = outcomes.get(1).getAsJsonObject();
                            game.spread2 = BovadaGameOutcome.initWithJsonObject(outcome1);
                        } else {
                            JsonObject outcome0 = outcomes.get(1).getAsJsonObject();
                            game.spread1 = BovadaGameOutcome.initWithJsonObject(outcome0);

                            JsonObject outcome1 = outcomes.get(0).getAsJsonObject();
                            game.spread2 = BovadaGameOutcome.initWithJsonObject(outcome1);
                        }
                    }

                    if (code.equals("2W-OU") && periodCode.equals("MATCH")) {
                        JsonObject outcome0 = outcomes.get(0).getAsJsonObject();
                        game.total1 = BovadaGameOutcome.initWithJsonObject(outcome0);

                        JsonObject outcome1 = outcomes.get(1).getAsJsonObject();
                        game.total2 = BovadaGameOutcome.initWithJsonObject(outcome1);
                    }
                }
            }
        }

        return game;
    }

    public boolean isPassedGame() {
        Date gameDate = getGameStartDate();
        Date now = new Date();
        return now.after(gameDate);
    }

    public Date getGameStartDate() {
        return DateUtils.convertUTCStringToLocalDate(startTime);
    }

    public String getGameDate() {
        String date = DateUtils.convertUTCStringToLocalDateString(startTime);
        String time = DateUtils.convertUTCStringToLocalTimeString(startTime);
        return String.format("%s\n(%s)", date, time);
    }

    public String getGameTitle() {
        return desc;
    }

    public boolean isSoccer() {
        return isSoccerType;
    }

    public BovadaGameOutcome getMoneyline1() {
        return moneyline1;
    }

    public BovadaGameOutcome getMoneylineX() {
        return moneylineX;
    }

    public BovadaGameOutcome getMoneyline2() {
        return moneyline2;
    }

    public BovadaGameOutcome getSpread1() {
        return spread1;
    }

    public BovadaGameOutcome getSpread2() {
        return spread2;
    }

    public BovadaGameOutcome getTotal1() {
        return total1;
    }

    public BovadaGameOutcome getTotal2() {
        return total2;
    }
}
