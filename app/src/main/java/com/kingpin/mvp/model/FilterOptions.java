package com.kingpin.mvp.model;

import android.content.Context;

import com.kingpin.common.base.Constant;
import com.kingpin.common.utility.PreferenceUtils;

import java.util.ArrayList;

public class FilterOptions {

    private boolean mOnlyActiveKingpin;
    private String mKeyword;
    private Constant.TIME_PERIOD mTimePeriod;
    private ArrayList<Constant.FILTER_SPORT> mSports = new ArrayList<>();
    private ArrayList<Constant.FILTER_BETTYPE> mBetTypes = new ArrayList<>();

    private void init() {
        this.mOnlyActiveKingpin = false;
        this.mKeyword = "";
        this.mTimePeriod = Constant.TIME_PERIOD.ALL;
        this.mSports.clear();
        this.mSports.add(Constant.FILTER_SPORT.ALL);
    }

    public static String getMainFilterOptionString(String subOption) {
        switch (subOption.toLowerCase()) {
            case "american football":
                return "Football";
            case "nfl":
                return "Football";
            case "basketball":
                return "Basketball";
            case "baseball":
                return "Baseball";
            case "Hockey":
                return "Hockey";
        }

        String[] soccerSubOptions = {"english premier", "english championship", "english cup", "la liga", "bundesliga", "french soccer", "italian soccer", "europa league", "champions league", "mls", "fifa"};
        for (String soccer : soccerSubOptions) {
            if (soccer.equals(subOption.toLowerCase()))
                return "Soccer";
        }

        if(subOption.isEmpty())
            return subOption;

        return subOption.substring(0, 1).toUpperCase() + subOption.substring(1);
    }

    public FilterOptions() {
       init();
    }

    public void loadFilterOption(Context context) {
        this.mOnlyActiveKingpin = PreferenceUtils.getBooleanPreference(context, Constant.PREF_FILTER_ACTIVE);
        this.mKeyword = PreferenceUtils.getStringPreference(context, Constant.PREF_FILTER_KEYWORD);
        this.mTimePeriod = Constant.TIME_PERIOD.values()[PreferenceUtils.getIntPreference(context, Constant.PREF_FILTER_TIME)];

        ArrayList<Integer> sports = PreferenceUtils.getIntArrayPreference(context, Constant.PREF_FILTER_SPORT);
        this.mSports.clear();
        if(sports == null || sports.size() == 0) {
            this.mSports.add(Constant.FILTER_SPORT.ALL);
        }
        else {
            for(int sport : sports)
                this.mSports.add(Constant.FILTER_SPORT.values()[sport]);
        }

        ArrayList<Integer> bettypes = PreferenceUtils.getIntArrayPreference(context, Constant.PREF_FILTER_BETTYPE);
        this.mBetTypes.clear();
        if(bettypes == null || bettypes.size() == 0) {
            this.mBetTypes.add(Constant.FILTER_BETTYPE.ALL);
        }
        else {
            for(int bettype : bettypes)
                this.mBetTypes.add(Constant.FILTER_BETTYPE.values()[bettype]);
        }
    }

    public void saveFilterOption(Context context) {
        PreferenceUtils.putPreference(context, Constant.PREF_FILTER_ACTIVE, mOnlyActiveKingpin);
        PreferenceUtils.putPreference(context, Constant.PREF_FILTER_KEYWORD, mKeyword);
        PreferenceUtils.putPreference(context, Constant.PREF_FILTER_TIME, mTimePeriod.ordinal());

        ArrayList<Integer> sports = new ArrayList<>();
        for(Constant.FILTER_SPORT sport : mSports)
            sports.add(sport.ordinal());
        PreferenceUtils.putIntArrayPreference(context, Constant.PREF_FILTER_SPORT, sports);

        ArrayList<Integer> bettypes = new ArrayList<>();
        for(Constant.FILTER_BETTYPE bettype : mBetTypes)
            bettypes.add(bettype.ordinal());
        PreferenceUtils.putIntArrayPreference(context, Constant.PREF_FILTER_BETTYPE, bettypes);
    }

    public void clearFilterOption(Context context) {
        init();
        PreferenceUtils.putPreference(context, Constant.PREF_FILTER_ACTIVE, this.mOnlyActiveKingpin);
        PreferenceUtils.putPreference(context, Constant.PREF_FILTER_KEYWORD, this.mKeyword);
        PreferenceUtils.putPreference(context, Constant.PREF_FILTER_TIME, this.mTimePeriod.ordinal());
        PreferenceUtils.putIntArrayPreference(context, Constant.PREF_FILTER_SPORT, null);
        PreferenceUtils.putIntArrayPreference(context, Constant.PREF_FILTER_BETTYPE, null);
    }

    public void setInitializeFilterOption() {
        init();
    }

    public Constant.TIME_PERIOD getTimePeriod() {
        return this.mTimePeriod;
    }

    public ArrayList<String> getTimePeriodString() {
        ArrayList<String> time = new ArrayList<>();

        switch (this.mTimePeriod) {
            case YESTERDAY:
                time.add("Yesterday");
                break;
            case ONEWEEK:
                time.add("One week");
                break;
            case ONEMONTH:
                time.add("One month");
                break;
            case THREEMONTH:
                time.add("Three months");
                break;
            case SIXMONTH:
                time.add("Six months");
                break;
            case ONEYEAR:
                time.add("One year");
                break;
        }

        return time;
    }

    public void setTimePeriod(Constant.TIME_PERIOD timePeriod) {
        this.mTimePeriod = timePeriod;
    }

    public ArrayList<Constant.FILTER_SPORT> getSports() {
        return mSports;
    }

    public ArrayList<String> getSportsString() {
        ArrayList<String> selectedSports = new ArrayList<>();

        if (haveAllSubSports(mSports, Constant.FILTER_SPORT.FOOTBALL)) {
            selectedSports.add("NFL");
            selectedSports.add("NFL 1st Half");
            selectedSports.add("NFL 1st Quarter");
            selectedSports.add("NCF");
            selectedSports.add("NCF 1st Half");
            selectedSports.add("CFL");
        } else {
            if  (mSports.contains(Constant.FILTER_SPORT.FOOTBALLNFL)) {
                selectedSports.add("NFL");
            }

            if  (mSports.contains(Constant.FILTER_SPORT.FOOTBALLNFL1stHalf)) {
                selectedSports.add("NFL 1st Half");
            }

            if  (mSports.contains(Constant.FILTER_SPORT.FOOTBALLNFL1stQuarter)) {
                selectedSports.add("NFL 1st Quarter");
            }

            if  (mSports.contains(Constant.FILTER_SPORT.FOOTBALLNCAAF)) {
                selectedSports.add("NCF");
            }

            if  (mSports.contains(Constant.FILTER_SPORT.FOOTBALLNCAAF1stHalf)) {
                selectedSports.add("NCF 1st Half");
            }

            if  (mSports.contains(Constant.FILTER_SPORT.FOOTBALLCFL)) {
                selectedSports.add("CFL");
            }
        }

        if (haveAllSubSports(mSports, Constant.FILTER_SPORT.BASKETBALL)) {
            selectedSports.add("NBA");
            selectedSports.add("NBA 1st Half");
            selectedSports.add("NCB");
            selectedSports.add("NCB 1st Half");
            selectedSports.add("WNBA");
        } else {
            if  (mSports.contains(Constant.FILTER_SPORT.BASKETBALLNBA)) {
                selectedSports.add("NBA");
            }

            if  (mSports.contains(Constant.FILTER_SPORT.BASKETBALLNBA1stHalf)) {
                selectedSports.add("NBA 1st Half");
            }

            if  (mSports.contains(Constant.FILTER_SPORT.BASKETBALLNCAAB)) {
                selectedSports.add("NCB");
            }

            if  (mSports.contains(Constant.FILTER_SPORT.BASKETBALLNCAAB1stHalf)) {
                selectedSports.add("NCB 1st Half");
            }

            if  (mSports.contains(Constant.FILTER_SPORT.BASKETBALLWNBA)) {
                selectedSports.add("WNBA");
            }
        }

        if (haveAllSubSports(mSports, Constant.FILTER_SPORT.BASEBALL)) {
            selectedSports.add("MLB");
            selectedSports.add("MLB 1st 5");
        } else {
            if  (mSports.contains(Constant.FILTER_SPORT.BASEBALLMLB)) {
                selectedSports.add("MLB");
            }

            if  (mSports.contains(Constant.FILTER_SPORT.BASEBALLMLB1stFive)) {
                selectedSports.add("MLB 1st 5");
            }
        }

        if (haveAllSubSports(mSports, Constant.FILTER_SPORT.HOCKEY)) {
            selectedSports.add("NHL");
            selectedSports.add("NHL 1st Period");
        } else {
            if  (mSports.contains(Constant.FILTER_SPORT.HOCKEYNHL)) {
                selectedSports.add("NHL");
            }

            if  (mSports.contains(Constant.FILTER_SPORT.HOCKEYNHL1stPeriod)) {
                selectedSports.add("NHL 1st Period");
            }
        }

        if (haveAllSubSports(mSports, Constant.FILTER_SPORT.SOCCER)) {
            selectedSports.add("Soccer");
            selectedSports.add("English Premier League");
            selectedSports.add("English Championship");
            selectedSports.add("English FA Cup");
            selectedSports.add("Spanish La Liga");
            selectedSports.add("German Bundesliga 1");
            selectedSports.add("French Ligue 1");
            selectedSports.add("Italian Serie A");
            selectedSports.add("UEFA Europa League");
            selectedSports.add("UEFA Champions League");
            selectedSports.add("MLS");
            selectedSports.add("FIFA World Cup 2018");

        } else {
            if  (mSports.contains(Constant.FILTER_SPORT.SOCCEREnglishPremierLeague)) {
                selectedSports.add("English Premier League");
            }

            if  (mSports.contains(Constant.FILTER_SPORT.SOCCEREnglishChampionship)) {
                selectedSports.add("English Championship");
            }

            if  (mSports.contains(Constant.FILTER_SPORT.SOCCEREnglishFACup)) {
                selectedSports.add("English FA Cup");
            }

            if  (mSports.contains(Constant.FILTER_SPORT.SOCCERSpanishLaLiga)) {
                selectedSports.add("Spanish La Liga");
            }

            if  (mSports.contains(Constant.FILTER_SPORT.SOCCERGermanBundesliga1)) {
                selectedSports.add("German Bundesliga 1");
            }

            if  (mSports.contains(Constant.FILTER_SPORT.SOCCERFrenchLigue)) {
                selectedSports.add("French Ligue 1");
            }

            if  (mSports.contains(Constant.FILTER_SPORT.SOCCERItalianSerieA)) {
                selectedSports.add("Italian Serie A");
            }

            if  (mSports.contains(Constant.FILTER_SPORT.SOCCERUEFAEuropaLeague)) {
                selectedSports.add("UEFA Europa League");
            }

            if  (mSports.contains(Constant.FILTER_SPORT.SOCCERUEFAChampionLeague)) {
                selectedSports.add("UEFA Champions League");
            }

            if  (mSports.contains(Constant.FILTER_SPORT.SOCCERMLS)) {
                selectedSports.add("MLS");
            }

            if  (mSports.contains(Constant.FILTER_SPORT.SOCCERFIFAWorldcup)) {
                selectedSports.add("FIFA World Cup 2018");
            }
        }

        if  (mSports.contains(Constant.FILTER_SPORT.TENNIS)) {
            selectedSports.add("Tennis");
        }

        if  (mSports.contains(Constant.FILTER_SPORT.MMA)) {
            selectedSports.add("MMA");
        }

        if  (mSports.contains(Constant.FILTER_SPORT.BOXING)) {
            selectedSports.add("Boxing");
        }

        return selectedSports;
    }

    public void setSport(Constant.FILTER_SPORT sport) {
        if(sport == Constant.FILTER_SPORT.ALL) {
            if(mSports.contains(Constant.FILTER_SPORT.ALL)) {
                mSports.clear();
            }
            else {
                mSports.clear();
                mSports.add(Constant.FILTER_SPORT.ALL);
            }
        }
        else {
            if (mSports.contains(Constant.FILTER_SPORT.ALL)) {
                mSports.clear();
            }

            if (sport == Constant.FILTER_SPORT.FOOTBALL) {
                if (mSports.contains(Constant.FILTER_SPORT.FOOTBALL)) {
                    mSports = removeSubSports(mSports, Constant.FILTER_SPORT.FOOTBALL);
                } else {
                    mSports = removeSubSports(mSports, Constant.FILTER_SPORT.FOOTBALL);
                    mSports.add(Constant.FILTER_SPORT.FOOTBALL);
                    mSports.add(Constant.FILTER_SPORT.FOOTBALLNFL);
                    mSports.add(Constant.FILTER_SPORT.FOOTBALLNFL1stHalf);
                    mSports.add(Constant.FILTER_SPORT.FOOTBALLNFL1stQuarter);
                    mSports.add(Constant.FILTER_SPORT.FOOTBALLNCAAF);
                    mSports.add(Constant.FILTER_SPORT.FOOTBALLNCAAF1stHalf);
                    mSports.add(Constant.FILTER_SPORT.FOOTBALLCFL);
                }
            } else if (sport == Constant.FILTER_SPORT.BASKETBALL) {
                if (mSports.contains(Constant.FILTER_SPORT.BASKETBALL)) {
                    mSports = removeSubSports(mSports, Constant.FILTER_SPORT.BASKETBALL);
                } else {
                    mSports = removeSubSports(mSports, Constant.FILTER_SPORT.BASKETBALL);
                    mSports.add(Constant.FILTER_SPORT.BASKETBALL);
                    mSports.add(Constant.FILTER_SPORT.BASKETBALLNBA);
                    mSports.add(Constant.FILTER_SPORT.BASKETBALLNBA1stHalf);
                    mSports.add(Constant.FILTER_SPORT.BASKETBALLNCAAB);
                    mSports.add(Constant.FILTER_SPORT.BASKETBALLNCAAB1stHalf);
                    mSports.add(Constant.FILTER_SPORT.BASKETBALLWNBA);
                }
            } else if (sport == Constant.FILTER_SPORT.BASEBALL) {
                if (mSports.contains(Constant.FILTER_SPORT.BASEBALL)) {
                    mSports = removeSubSports(mSports, Constant.FILTER_SPORT.BASEBALL);
                } else {
                    mSports = removeSubSports(mSports, Constant.FILTER_SPORT.BASEBALL);
                    mSports.add(Constant.FILTER_SPORT.BASEBALL);
                    mSports.add(Constant.FILTER_SPORT.BASEBALLMLB);
                    mSports.add(Constant.FILTER_SPORT.BASEBALLMLB1stFive);
                }
            } else if (sport == Constant.FILTER_SPORT.HOCKEY) {
                if (mSports.contains(Constant.FILTER_SPORT.HOCKEY)) {
                    mSports = removeSubSports(mSports, Constant.FILTER_SPORT.HOCKEY);
                } else {
                    mSports = removeSubSports(mSports, Constant.FILTER_SPORT.HOCKEY);
                    mSports.add(Constant.FILTER_SPORT.HOCKEY);
                    mSports.add(Constant.FILTER_SPORT.HOCKEYNHL);
                    mSports.add(Constant.FILTER_SPORT.HOCKEYNHL1stPeriod);
                }
            } else if (sport == Constant.FILTER_SPORT.SOCCER) {
                if (mSports.contains(Constant.FILTER_SPORT.SOCCER)) {
                    mSports = removeSubSports(mSports, Constant.FILTER_SPORT.SOCCER);
                } else {
                    mSports = removeSubSports(mSports, Constant.FILTER_SPORT.SOCCER);
                    mSports.add(Constant.FILTER_SPORT.SOCCER);
                    mSports.add(Constant.FILTER_SPORT.SOCCEREnglishPremierLeague);
                    mSports.add(Constant.FILTER_SPORT.SOCCEREnglishChampionship);
                    mSports.add(Constant.FILTER_SPORT.SOCCEREnglishFACup);
                    mSports.add(Constant.FILTER_SPORT.SOCCERSpanishLaLiga);
                    mSports.add(Constant.FILTER_SPORT.SOCCERGermanBundesliga1);
                    mSports.add(Constant.FILTER_SPORT.SOCCERFrenchLigue);
                    mSports.add(Constant.FILTER_SPORT.SOCCERItalianSerieA);
                    mSports.add(Constant.FILTER_SPORT.SOCCERUEFAEuropaLeague);
                    mSports.add(Constant.FILTER_SPORT.SOCCERUEFAChampionLeague);
                    mSports.add(Constant.FILTER_SPORT.SOCCERMLS);
                    mSports.add(Constant.FILTER_SPORT.SOCCERFIFAWorldcup);
                }
            } else {
                if (mSports.contains(sport)) {
                    mSports.remove(sport);
                } else {
                    mSports.add(sport);
                }

                if (haveAllSubSports(mSports, Constant.FILTER_SPORT.FOOTBALL)) {
                    if (!mSports.contains(Constant.FILTER_SPORT.FOOTBALL)) {
                        mSports.add(Constant.FILTER_SPORT.FOOTBALL);
                    }
                } else {
                    mSports.remove(Constant.FILTER_SPORT.FOOTBALL);
                }

                if (haveAllSubSports(mSports, Constant.FILTER_SPORT.BASKETBALL)) {
                    if (!mSports.contains(Constant.FILTER_SPORT.BASKETBALL)) {
                        mSports.add(Constant.FILTER_SPORT.BASKETBALL);
                    }
                } else {
                    mSports.remove(Constant.FILTER_SPORT.BASKETBALL);
                }

                if (haveAllSubSports(mSports, Constant.FILTER_SPORT.BASEBALL)) {
                    if (!mSports.contains(Constant.FILTER_SPORT.BASEBALL)) {
                        mSports.add(Constant.FILTER_SPORT.BASEBALL);
                    }
                } else {
                    mSports.remove(Constant.FILTER_SPORT.BASEBALL);
                }

                if (haveAllSubSports(mSports, Constant.FILTER_SPORT.HOCKEY)) {
                    if (!mSports.contains(Constant.FILTER_SPORT.HOCKEY)) {
                        mSports.add(Constant.FILTER_SPORT.HOCKEY);
                    }
                } else {
                    mSports.remove(Constant.FILTER_SPORT.HOCKEY);
                }

                if (haveAllSubSports(mSports, Constant.FILTER_SPORT.SOCCER)) {
                    if (!mSports.contains(Constant.FILTER_SPORT.SOCCER)) {
                        mSports.add(Constant.FILTER_SPORT.SOCCER);
                    }
                } else {
                    mSports.remove(Constant.FILTER_SPORT.SOCCER);
                }
            }

            if (mSports.size() == 34) {
                mSports.clear();
                mSports.add(Constant.FILTER_SPORT.ALL);
            }
        }
    }

    private ArrayList<Constant.FILTER_SPORT>  removeSubSports(ArrayList<Constant.FILTER_SPORT> sports, Constant.FILTER_SPORT mainSport) {
        if (mainSport == Constant.FILTER_SPORT.FOOTBALL) {
            sports.remove(Constant.FILTER_SPORT.FOOTBALL);
            sports.remove(Constant.FILTER_SPORT.FOOTBALLNFL);
            sports.remove(Constant.FILTER_SPORT.FOOTBALLNFL1stHalf);
            sports.remove(Constant.FILTER_SPORT.FOOTBALLNFL1stQuarter);
            sports.remove(Constant.FILTER_SPORT.FOOTBALLNCAAF);
            sports.remove(Constant.FILTER_SPORT.FOOTBALLNCAAF1stHalf);
            sports.remove(Constant.FILTER_SPORT.FOOTBALLCFL);
        } else if (mainSport == Constant.FILTER_SPORT.BASKETBALL) {
            sports.remove(Constant.FILTER_SPORT.BASKETBALL);
            sports.remove(Constant.FILTER_SPORT.BASKETBALLNBA);
            sports.remove(Constant.FILTER_SPORT.BASKETBALLNBA1stHalf);
            sports.remove(Constant.FILTER_SPORT.BASKETBALLNCAAB);
            sports.remove(Constant.FILTER_SPORT.BASKETBALLNCAAB1stHalf);
            sports.remove(Constant.FILTER_SPORT.BASKETBALLWNBA);
        } else if (mainSport == Constant.FILTER_SPORT.BASEBALL) {
            sports.remove(Constant.FILTER_SPORT.BASEBALL);
            sports.remove(Constant.FILTER_SPORT.BASEBALLMLB);
            sports.remove(Constant.FILTER_SPORT.BASEBALLMLB1stFive);
        } else if (mainSport == Constant.FILTER_SPORT.HOCKEY) {
            sports.remove(Constant.FILTER_SPORT.HOCKEY);
            sports.remove(Constant.FILTER_SPORT.HOCKEYNHL);
            sports.remove(Constant.FILTER_SPORT.HOCKEYNHL1stPeriod);
        } else if (mainSport == Constant.FILTER_SPORT.SOCCER) {
            sports.remove(Constant.FILTER_SPORT.SOCCER);
            sports.remove(Constant.FILTER_SPORT.SOCCEREnglishPremierLeague);
            sports.remove(Constant.FILTER_SPORT.SOCCEREnglishChampionship);
            sports.remove(Constant.FILTER_SPORT.SOCCEREnglishFACup);
            sports.remove(Constant.FILTER_SPORT.SOCCERSpanishLaLiga);
            sports.remove(Constant.FILTER_SPORT.SOCCERGermanBundesliga1);
            sports.remove(Constant.FILTER_SPORT.SOCCERFrenchLigue);
            sports.remove(Constant.FILTER_SPORT.SOCCERItalianSerieA);
            sports.remove(Constant.FILTER_SPORT.SOCCERUEFAEuropaLeague);
            sports.remove(Constant.FILTER_SPORT.SOCCERUEFAChampionLeague);
            sports.remove(Constant.FILTER_SPORT.SOCCERMLS);
            sports.remove(Constant.FILTER_SPORT.SOCCERFIFAWorldcup);
        }
        return sports;
    }

    private boolean haveAllSubSports(ArrayList<Constant.FILTER_SPORT> sports, Constant.FILTER_SPORT mainSport) {
        if (mainSport == Constant.FILTER_SPORT.FOOTBALL) {
            return sports.contains(Constant.FILTER_SPORT.FOOTBALLNFL) &&
                    sports.contains(Constant.FILTER_SPORT.FOOTBALLNFL1stHalf) &&
                    sports.contains(Constant.FILTER_SPORT.FOOTBALLNFL1stQuarter) &&
                    sports.contains(Constant.FILTER_SPORT.FOOTBALLNCAAF) &&
                    sports.contains(Constant.FILTER_SPORT.FOOTBALLNCAAF1stHalf) &&
                    sports.contains(Constant.FILTER_SPORT.FOOTBALLCFL);
        } else if (mainSport == Constant.FILTER_SPORT.BASKETBALL) {
            return sports.contains(Constant.FILTER_SPORT.BASKETBALLNBA) &&
                    sports.contains(Constant.FILTER_SPORT.BASKETBALLNBA1stHalf) &&
                    sports.contains(Constant.FILTER_SPORT.BASKETBALLNCAAB) &&
                    sports.contains(Constant.FILTER_SPORT.BASKETBALLNCAAB1stHalf) &&
                    sports.contains(Constant.FILTER_SPORT.BASKETBALLWNBA);
        } else if (mainSport == Constant.FILTER_SPORT.BASEBALL) {
            return sports.contains(Constant.FILTER_SPORT.BASEBALLMLB) &&
                    sports.contains(Constant.FILTER_SPORT.BASEBALLMLB1stFive);
        } else if (mainSport == Constant.FILTER_SPORT.HOCKEY) {
            return sports.contains(Constant.FILTER_SPORT.HOCKEYNHL) &&
                    sports.contains(Constant.FILTER_SPORT.HOCKEYNHL1stPeriod);
        } else if (mainSport == Constant.FILTER_SPORT.SOCCER) {
            return sports.contains(Constant.FILTER_SPORT.SOCCEREnglishPremierLeague) &&
                    sports.contains(Constant.FILTER_SPORT.SOCCEREnglishChampionship) &&
                    sports.contains(Constant.FILTER_SPORT.SOCCEREnglishFACup) &&
                    sports.contains(Constant.FILTER_SPORT.SOCCERSpanishLaLiga) &&
                    sports.contains(Constant.FILTER_SPORT.SOCCERGermanBundesliga1) &&
                    sports.contains(Constant.FILTER_SPORT.SOCCERFrenchLigue) &&
                    sports.contains(Constant.FILTER_SPORT.SOCCERItalianSerieA) &&
                    sports.contains(Constant.FILTER_SPORT.SOCCERUEFAEuropaLeague) &&
                    sports.contains(Constant.FILTER_SPORT.SOCCERUEFAChampionLeague) &&
                    sports.contains(Constant.FILTER_SPORT.SOCCERMLS) &&
                    sports.contains(Constant.FILTER_SPORT.SOCCERFIFAWorldcup);
        }
        return false;
    }

    public ArrayList<Constant.FILTER_BETTYPE> getBetTypes() {
        return mBetTypes;
    }

    public ArrayList<String> getBetTypesString() {
        ArrayList<String> selectedBetTypes = new ArrayList<>();
        if(this.mBetTypes.contains(Constant.FILTER_BETTYPE.ALL)) {
            selectedBetTypes.add("moneyline");
            selectedBetTypes.add("handicap");
            selectedBetTypes.add("total");
            selectedBetTypes.add("parlay");
            return selectedBetTypes;
        }

        if(this.mBetTypes.contains(Constant.FILTER_BETTYPE.MONEYLINE)) {
            selectedBetTypes.add("moneyline");
        }

        if(this.mBetTypes.contains(Constant.FILTER_BETTYPE.SPREAD)) {
            selectedBetTypes.add("handicap");
        }

        if(this.mBetTypes.contains(Constant.FILTER_BETTYPE.OVER_UNDER)) {
            selectedBetTypes.add("total");
        }

        if(this.mBetTypes.contains(Constant.FILTER_BETTYPE.PARLAY)) {
            selectedBetTypes.add("parlay");
        }

        return selectedBetTypes;
    }

    public void setBetType(Constant.FILTER_BETTYPE betType) {
        if(betType == Constant.FILTER_BETTYPE.ALL) {
            if(mBetTypes.contains(Constant.FILTER_BETTYPE.ALL)) {
                mBetTypes.clear();
            }
            else {
                mBetTypes.clear();
                mBetTypes.add(Constant.FILTER_BETTYPE.ALL);
            }
        }
        else {
            if(mBetTypes.contains(betType)) {
                mBetTypes.remove(betType);
            }
            else {
                if(mBetTypes.contains(Constant.FILTER_BETTYPE.ALL)) {
                    mBetTypes.clear();
                    mBetTypes.add(betType);
                }
                else {
                    mBetTypes.add(betType);
                    if(mBetTypes.size() == 4) {
                        mBetTypes.clear();
                        mBetTypes.add(Constant.FILTER_BETTYPE.ALL);
                    }
                }
            }
        }
    }

    public boolean isOnlyActiveKingpin() {
        return this.mOnlyActiveKingpin;
    }

    public void setOnlyActiveKingpin(boolean value) {
        this.mOnlyActiveKingpin = value;
    }

    public String getKeyword() {
        return this.mKeyword;
    }

    public void setKeyword(String keyword) {
        this.mKeyword = keyword;
    }
}
