package com.kingpin.mvp.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kingpin.mvp.model.AffiliateCriedit;
import com.kingpin.mvp.model.Withdraw;

import java.util.ArrayList;

public class AffiliateHistoryResponse {
    @SerializedName("status")
    @Expose
    private boolean status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private ArrayList<AffiliateCriedit> criedits;

    public AffiliateHistoryResponse() {
        this.status = false;
        this.message = "";
        this.criedits = new ArrayList<>();
    }

    public boolean isSuccess() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public ArrayList<AffiliateCriedit> getCriedits() {
        return criedits;
    }
}
