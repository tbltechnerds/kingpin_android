package com.kingpin.mvp.model;

public class Statistics extends BaseStatistics{

    private String label;
    private String value;

    public Statistics(int type, String label, String value) {
        super(type);
        this.label = label;
        this.value = value;
    }

    public String getLabel() {
        return this.label;
    }

    public String getValue() {
        return this.value;
    }
}
