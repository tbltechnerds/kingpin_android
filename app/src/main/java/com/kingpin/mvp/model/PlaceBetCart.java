package com.kingpin.mvp.model;

import org.json.JSONObject;

public class PlaceBetCart {
    public String odd;
    public String line;
    public String opposite_lines;
    public String teams;
    public String event_id;
    public String marketid;
    public String opposite_market_ids;
    public String team_bet;
    public String sport;
    public String match_date;
    public String bet_type;
    public int code;
    public int bet_amount;
    public String bet_param;

    public PlaceBetCart() {
        odd = "0.0";
        line = "0";
        opposite_lines = "0";
        teams = "";
        event_id = "";
        marketid = "";
        opposite_market_ids = "";
        team_bet = "";
        sport = "";
        match_date = "";
        bet_type = "";
        code = 0;
        bet_amount = 0;
        bet_param = "1";
    }

    public JSONObject getDictionary() {

        JSONObject jsonObject = new JSONObject();
        try {
            if (this.sport != null) jsonObject.put("sport", this.sport);
            if (this.odd != null) jsonObject.put("odd", this.odd);
            if (this.line != null) jsonObject.put("line", this.line);
            if (this.opposite_lines != null) jsonObject.put("opposite_lines", this.opposite_lines);
            if (this.teams != null) jsonObject.put("teams", this.teams);
            if (this.event_id != null) jsonObject.put("event_id", this.event_id);
            if (this.marketid != null) jsonObject.put("marketid", this.marketid);
            if (this.opposite_market_ids != null) jsonObject.put("opposite_market_ids", this.opposite_market_ids);
            if (this.team_bet != null) jsonObject.put("team_bet", this.team_bet);
            if (this.sport != null) jsonObject.put("sport", this.sport);
            if (this.match_date != null) jsonObject.put("match_date", this.match_date);
            if (this.bet_type != null) jsonObject.put("bet_type", this.bet_type);
            jsonObject.put("code", this.code);
            jsonObject.put("bet_amount", this.bet_amount);
            if (this.bet_param != null) jsonObject.put("bet_param", this.bet_param);
        } catch (Exception e) {

        }

        return jsonObject;
    }

    public String getPeriodCode() {
        if (sport == null) {
            return "";
        }

        String type = "";
        if (sport.equals("NFL 1st Half")) {
            type = "1H";
        } else if (sport.equals("NFL 2nd Half")) {
            type = "2H";
        } else if (sport.equals("NFL 1st Quarter")) {
            type = "1Q";
        } else if (sport.equals("NFL 2nd Quarter")) {
            type = "2Q";
        } else if (sport.equals("NFL 3rd Quarter")) {
            type = "3Q";
        } else if (sport.equals("NFL 4th Quarter")) {
            type = "4Q";
        } else if (sport.equals("NHL 1st Period")) {
            type = "1P";
        } else if (sport.equals("MLB 1st 5")) {
            type = "F5";
        } else if (sport.equals("NCF 1st Half")) {
            type = "1H";
        } else if (sport.equals("NCB 1st Half")) {
            type = "1H";
        } else if (sport.equals("NBA 1st Half")) {
            type = "1H";
        }

        return type;
    }
}
