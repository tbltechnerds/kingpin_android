package com.kingpin.mvp.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kingpin.common.utility.DateUtils;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class RapidGame extends Game {

    @SerializedName("event_id")
    @Expose
    private String event_id;

    @SerializedName("event_date")
    @Expose
    private String event_date;

    @SerializedName("rotation_number_home")
    @Expose
    private int rotation_number_home;

    @SerializedName("rotation_number_away")
    @Expose
    private int rotation_number_away;

    @SerializedName("lines")
    @Expose
    private  JsonObject lines;

    @SerializedName("teams")
    @Expose
    private JsonArray teams;

    public RapidGame() {
        super();
        event_id = "";
        event_date = "";
        rotation_number_home = 0;
        rotation_number_away = 0;
        lines = new JsonObject();
        teams = new JsonArray();
    }

    public String getEventId() {
        return event_id;
    }

    public void setEventId(String event_id) {
        this.event_id = event_id;
    }

    public String getEventDate() {
        return event_date;
    }

    public void setEventDate(String event_date) {
        this.event_date = event_date;
    }

    public int getRotationNumberHome() {
        return rotation_number_home;
    }

    public void setRotationNumberHome(int rotation_number_home) {
        this.rotation_number_home = rotation_number_home;
    }

    public int getRotationNumberAway() {
        return rotation_number_away;
    }

    public void setRotationNumberAway(int rotation_number_away) {
        this.rotation_number_away = rotation_number_away;
    }

    public JsonObject getLines() {
        return lines;
    }

    public void setLines(JsonObject lines) {
        this.lines = lines;
    }

    public JsonArray getTeams() {
        return teams;
    }

    public String getHomeTeamName() {
        String homeTeam = "";
        if (teams.size() > 0) {
            JsonElement element = teams.get(0);
            JsonObject team = element.getAsJsonObject();
            if (team.has("is_home") &&
                    !team.get("is_home").isJsonNull() &&
                    team.has("name") &&
                    !team.get("name").isJsonNull()
            ) {
                if (team.get("is_home").getAsBoolean()) {
                    homeTeam = team.get("name").getAsString();
                    return homeTeam;
                }

            }
        }

        if (teams.size() > 1) {
            JsonElement element = teams.get(1);
            JsonObject team = element.getAsJsonObject();
            if (team.has("is_home") &&
                    !team.get("is_home").isJsonNull() &&
                    team.has("name") &&
                    !team.get("name").isJsonNull()
            ) {
                if (team.get("is_home").getAsBoolean()) {
                    homeTeam = team.get("name").getAsString();
                    return homeTeam;
                }

            }
        }

        return homeTeam;
    }

    public String getAwayTeamName() {
        String awayTeam = "";
        if (teams.size() > 0) {
            JsonElement element = teams.get(0);
            JsonObject team = element.getAsJsonObject();
            if (team.has("is_home") &&
                    !team.get("is_home").isJsonNull() &&
                    team.has("name") &&
                    !team.get("name").isJsonNull()
            ) {
                if (!team.get("is_home").getAsBoolean()) {
                    awayTeam = team.get("name").getAsString();
                    return awayTeam;
                }

            }
        }

        if (teams.size() > 1) {
            JsonElement element = teams.get(1);
            JsonObject team = element.getAsJsonObject();
            if (team.has("is_home") &&
                    !team.get("is_home").isJsonNull() &&
                    team.has("name") &&
                    !team.get("name").isJsonNull()
            ) {
                if (!team.get("is_home").getAsBoolean()) {
                    awayTeam = team.get("name").getAsString();
                    return awayTeam;
                }

            }
        }

        return awayTeam;
    }

    public String getGameName() {
        String homeTeam = getHomeTeamName();
        String awayTeam = getAwayTeamName();
        return homeTeam + " vs " + awayTeam;
    }

    public String getGameDateString() {
        String date = DateUtils.convertUTCStringToLocalDateString(event_date);
        String time = DateUtils.convertUTCStringToLocalTimeString(event_date);
        return String.format("%s\n(%s)", date, time);
    }

    public Date getGameDate() {
        return DateUtils.convertUTCStringToLocalDate(event_date);
    }

    public boolean isPassedGame() {
        Date gameDate = getGameDate();
        Date now = new Date();
        return now.after(gameDate);
    }

    public void setTeams(JsonArray teams) {
        this.teams = teams;
    }

    public JsonObject getGameDetails() {
        String keyword = "";
        if (getSport().equals("NFL 1st Half")) {
            keyword = "period_first_half";
        } else if (getSport().equals("NFL 2nd Half")) {
            keyword = "period_second_half";
        } else if (getSport().equals("NFL 1st Quarter")) {
            keyword = "period_first_period";
        } else if (getSport().equals("NFL 2nd Quarter")) {
            keyword = "period_second_period";
        } else if (getSport().equals("NFL 3rd Quarter")) {
            keyword = "period_third_period";
        } else if (getSport().equals("NFL 4th Quarter")) {
            keyword = "period_fourth_period";
        } else if (getSport().equals("NHL 1st Period")) {
            keyword = "period_first_period";
        } else if (getSport().equals("MLB 1st 5")) {
            keyword = "period_first_half";
        } else if (getSport().equals("NCF 1st Half")) {
            keyword = "period_first_half";
        } else if (getSport().equals("NCB 1st Half")) {
            keyword = "period_first_half";
        } else if (getSport().equals("NBA 1st Half")) {
            keyword = "period_first_half";
        }

        List<String> keys = new ArrayList<>(lines.keySet());
        List<Integer> intKeys = new ArrayList<>();
        for (String key : keys) {
            intKeys.add(Integer.parseInt(key));
        }
        Collections.sort(intKeys);

        if (intKeys.size() > 0) {
            Integer firstIntKey = intKeys.get(0);
            String firstKey = firstIntKey.toString();

            if (lines.has(firstKey) && !lines.get(firstKey).isJsonNull()) {
                JsonObject details = lines.get(firstKey).getAsJsonObject();
                if (keyword.equals("")) {
                    return details;
                } else {
                    if (details.has(keyword) && !details.get(keyword).isJsonNull()) {
                        return details.get(keyword).getAsJsonObject();
                    } else {
                        return null;
                    }
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public JsonObject getMoneylineData() {
        JsonObject details = getGameDetails();
        if (details != null) {
            if (details.has("moneyline") && !details.get("moneyline").isJsonNull()) {
                return details.get("moneyline").getAsJsonObject();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public JsonObject getSpreadData() {
        JsonObject details = getGameDetails();
        if (details != null) {
            if (details.has("spread") && !details.get("spread").isJsonNull()) {
                return details.get("spread").getAsJsonObject();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public JsonObject getTotalData() {
        JsonObject details = getGameDetails();
        if (details != null) {
            if (details.has("total") && !details.get("total").isJsonNull()) {
                return details.get("total").getAsJsonObject();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public static RapidGame initWithJsonObject(JsonObject json, String sport)  {

        RapidGame game = new RapidGame();
        game.setIsBovada(false);
        game.setSport(sport);

        if(json.has("event_id") && !json.get("event_id").isJsonNull())
            game.setEventId(json.get("event_id").getAsString());

        if(json.has("event_date") && !json.get("event_date").isJsonNull())
            game.setEventDate(json.get("event_date").getAsString());

        if(json.has("rotation_number_home") && !json.get("rotation_number_home").isJsonNull())
            game.setRotationNumberHome(json.get("rotation_number_home").getAsInt());

        if(json.has("rotation_number_away") && !json.get("rotation_number_away").isJsonNull())
            game.setRotationNumberAway(json.get("rotation_number_away").getAsInt());

        if(json.has("lines") && !json.get("lines").isJsonNull())
            game.setLines(json.get("lines").getAsJsonObject());
        else if(json.has("line_periods") && !json.get("line_periods").isJsonNull())
            game.setLines(json.get("line_periods").getAsJsonObject());

        if(json.has("teams") && !json.get("teams").isJsonNull())
            game.setTeams(json.get("teams").getAsJsonArray());

        return game;
    }
}
