package com.kingpin.mvp.model;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Type;

public class AffiliateCriedit {

    @SerializedName("client_name")
    @Expose
    private String client_name;

    @SerializedName("client_purchased_membership_date")
    @Expose
    private String client_purchased_membership_date;

    @SerializedName("client_membership_type")
    @Expose
    private String client_membership_type;

    @SerializedName("commission")
    @Expose
    private double commission;

    @SerializedName("withdraw_availability_date")
    @Expose
    private String withdraw_availability_date;

    private AffiliateCriedit() {
        this.client_name = "";
        this.client_purchased_membership_date = "";
        this.client_membership_type = "";
        this.commission = 0.0;
        this.withdraw_availability_date = "";
    }

    public void setClientName(String clientName) {
        client_name = clientName;
    }

    public String getClientName() {
        return client_name;
    }

    public void setPurchasedMembershipDate(String purchasedMembershipDate) {
        client_purchased_membership_date = purchasedMembershipDate;
    }

    public String getPurchasedMembershipDate() {
        return client_purchased_membership_date;
    }

    public void setClientMembershipType(String membershipType) {
        client_membership_type = membershipType;
    }

    public String getClientMembershipType() {
        return client_membership_type;
    }

    public void setCommission(double commission) {
        this.commission = commission;
    }

    public double getCommission() {
        return commission;
    }

    public void setWithdrawAvailableDate(String availableDate) {
        withdraw_availability_date = availableDate;
    }

    public String getWithdrawAvailableDate() {
        return withdraw_availability_date;
    }

    public static JsonDeserializer<AffiliateCriedit> deserializer = new JsonDeserializer<AffiliateCriedit>() {
        @Override
        public AffiliateCriedit deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject withdraw_json = json.getAsJsonObject();
            AffiliateCriedit withdraw = new AffiliateCriedit();

            if(withdraw_json.has("client_name") && !withdraw_json.get("client_name").isJsonNull())
                withdraw.setClientName(withdraw_json.get("client_name").getAsString());

            if(withdraw_json.has("client_purchased_membership_date") && !withdraw_json.get("client_purchased_membership_date").isJsonNull())
                withdraw.setPurchasedMembershipDate(withdraw_json.get("client_purchased_membership_date").getAsString());

            if(withdraw_json.has("client_membership_type") && !withdraw_json.get("client_membership_type").isJsonNull())
                withdraw.setClientMembershipType(withdraw_json.get("client_membership_type").getAsString());

            if(withdraw_json.has("commission") && !withdraw_json.get("commission").isJsonNull()) {
                double commission;
                try {
                    commission = withdraw_json.get("commission").getAsDouble();
                }
                catch(Exception e) {
                    int int_value = withdraw_json.get("commission").getAsInt();
                    commission = (double) int_value;
                }
                withdraw.setCommission(commission);
            }

            if(withdraw_json.has("withdraw_availability_date") && !withdraw_json.get("withdraw_availability_date").isJsonNull())
                withdraw.setWithdrawAvailableDate(withdraw_json.get("withdraw_availability_date").getAsString());

            return withdraw;
        }
    };
}
