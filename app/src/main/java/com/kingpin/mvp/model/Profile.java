package com.kingpin.mvp.model;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kingpin.common.base.Constant;
import com.kingpin.common.utility.PreferenceUtils;

public class Profile {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("first_name")
    @Expose
    private String first_name; //can be null

    @SerializedName("last_name")
    @Expose
    private String last_name; //can be null

    @SerializedName("country")
    @Expose
    private String country; //can be null

    @SerializedName("state")
    @Expose
    private String state; //can be null

    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("twitter")
    @Expose
    private String twitter;

    @SerializedName("instagram")
    @Expose
    private String instagram;

    @SerializedName("promocode")
    @Expose
    private String promocode;

    @SerializedName("user_avatar")
    @Expose
    private String user_avatar;

    @SerializedName("verified")
    @Expose
    private int verified;

    @SerializedName("updated_at")
    @Expose
    private String updatedTime;

    @SerializedName("created_at")
    @Expose
    private String createdTime;

    @SerializedName("mode")
    @Expose
    private int mode;

    @SerializedName("membership")
    @Expose
    private int membership;

    @SerializedName("isTrial")
    @Expose
    private int isTrial;

    @SerializedName("notifications")
    @Expose
    private int notifications;

    @SerializedName("push_notify")
    @Expose
    private int push_notify;

    @SerializedName("rate_status")
    @Expose
    private int rate_status;

    @SerializedName("ranked")
    @Expose
    private int ranked;

    @SerializedName("payment_type")
    @Expose
    private String payment_type;

    @SerializedName("show_rate_date")
    @Expose
    private String show_rate_date;

    @SerializedName("betting_strategy")
    @Expose
    private String bettingStrategy;

    @SerializedName("referral_affiliate_code")
    @Expose
    private String referral_affiliate_code;

    @SerializedName("is_valid_affiliate_code")
    @Expose
    private boolean is_valid_affiliate_code;

    public static boolean isStoredProfile(Context context) {
        return PreferenceUtils.getBooleanPreference(context, Constant.PREF_USER_LOGGEDIN);
    }

    public static Profile retrieveProfile(Context context) {
        Profile profile = new Profile();
        profile.load(context);
        return profile;
    }

    public Profile() {
        this.id = -1;
        this.name = "";
        this.first_name = "";
        this.last_name = "";
        this.country = "";
        this.state = "";
        this.city = "";
        this.email = "";
        this.twitter = "";
        this.instagram = "";
        this.promocode = "";
        this.user_avatar = "";
        this.verified = 0;
        this.updatedTime = "";
        this.createdTime = "";
        this.mode = 0;
        this.membership = 0;
        this.isTrial = 0;
        this.notifications = 1;
        this.push_notify = 1;
        this.rate_status = 0;
        this.ranked = 0;
        this.payment_type = "";
        this.show_rate_date = "";
        this.bettingStrategy = "";
        this.referral_affiliate_code = "";
        this.is_valid_affiliate_code = false;
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getAvatar() {
        return this.user_avatar;
    }

    public int getMembership() {
        return this.membership;
    }

    public int getTrial() {
        if (getMembership() == 0) {
            return 0;
        } else {
            return this.isTrial;
        }
    }

    public String getPaymentType() {
        return payment_type;
    }

    public String getFullAddress() {
        if(country != null && !country.isEmpty()) {
            if(state != null && !state.isEmpty())
                return city + ", " + state + ", " + country;

            return city + ", " + country;
        }
        else {
            return city;
        }
    }

    public String getEmail() {
        return email;
    }

    public String getTwitter() {
        return twitter;
    }

    public String getInstagram() {
        return instagram;
    }

    public int getNotifications() {
        return notifications;
    }

    public int getPushNotification() {
        return push_notify;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getCountry() {
        return country;
    }

    public String getBettingStrategy() { return bettingStrategy; }

    public boolean isRanked() {
        return ranked == 1;
    }

    public String getReferralAffiliateCode() { return referral_affiliate_code; }

    public boolean isVaildAffiliateCode() { return is_valid_affiliate_code; }

    public void clear(Context context) {
        PreferenceUtils.putPreference(context, Constant.PREF_USER_LOGGEDIN, false);
    }

    public void save(Context context) {
        PreferenceUtils.putPreference(context, Constant.PREF_USER_LOGGEDIN, true);
        PreferenceUtils.putPreference(context, Constant.PREF_USER_ID, this.id);
        PreferenceUtils.putPreference(context, Constant.PREF_USER_NAME, this.name);
        PreferenceUtils.putPreference(context, Constant.PREF_USER_FIRSTNAME, this.first_name);
        PreferenceUtils.putPreference(context, Constant.PREF_USER_LASTNAME, this.last_name);
        PreferenceUtils.putPreference(context, Constant.PREF_USER_EMAIL, this.email);
        PreferenceUtils.putPreference(context, Constant.PREF_USER_TWITTER, this.twitter);
        PreferenceUtils.putPreference(context, Constant.PREF_USER_INSTAGRAM, this.instagram);
        PreferenceUtils.putPreference(context, Constant.PREF_USER_COUNTRY, this.country);
        PreferenceUtils.putPreference(context, Constant.PREF_USER_STATE, this.state);
        PreferenceUtils.putPreference(context, Constant.PREF_USER_CITY, this.city);
        PreferenceUtils.putPreference(context, Constant.PREF_USER_PROMO, this.promocode);
        PreferenceUtils.putPreference(context, Constant.PREF_USER_AVATAR, this.user_avatar);
        PreferenceUtils.putPreference(context, Constant.PREF_USER_MEMBERSHIP, this.membership);
        PreferenceUtils.putPreference(context, Constant.PREF_USER_MODE, this.mode);
        PreferenceUtils.putPreference(context, Constant.PREF_USER_PAYMENT_TYPE, this.payment_type);
        PreferenceUtils.putPreference(context, Constant.PREF_USER_TRIAL, this.isTrial);
        PreferenceUtils.putPreference(context, Constant.PREF_USER_NOTIFICATIONS, this.notifications);
        PreferenceUtils.putPreference(context, Constant.PREF_USER_PUSH_NOTIFY, this.push_notify);
        PreferenceUtils.putPreference(context, Constant.PREF_USER_SHOW_RATEDATE, this.show_rate_date);
        PreferenceUtils.putPreference(context, Constant.PREF_USER_RATE_STATUS, this.rate_status);
        PreferenceUtils.putPreference(context, Constant.PREF_USER_RANKED, this.ranked);
        PreferenceUtils.putPreference(context, Constant.PREF_USER_BETTING_STRATEGY, this.bettingStrategy);
        PreferenceUtils.putPreference(context, Constant.PREF_USER_REFERRAL_AFFILIATE_CODE, this.referral_affiliate_code);
        PreferenceUtils.putPreference(context, Constant.PREF_USER_IS_VALID_AFFILIATE_CODE, this.is_valid_affiliate_code);
    }

    private void load(Context context) {
        this.id = PreferenceUtils.getIntPreference(context, Constant.PREF_USER_ID);
        this.name = PreferenceUtils.getStringPreference(context, Constant.PREF_USER_NAME);
        this.first_name = PreferenceUtils.getStringPreference(context, Constant.PREF_USER_FIRSTNAME);
        this.last_name = PreferenceUtils.getStringPreference(context, Constant.PREF_USER_LASTNAME);
        this.email = PreferenceUtils.getStringPreference(context, Constant.PREF_USER_EMAIL);
        this.twitter = PreferenceUtils.getStringPreference(context, Constant.PREF_USER_TWITTER);
        this.instagram = PreferenceUtils.getStringPreference(context, Constant.PREF_USER_INSTAGRAM);
        this.country = PreferenceUtils.getStringPreference(context, Constant.PREF_USER_COUNTRY);
        this.state = PreferenceUtils.getStringPreference(context, Constant.PREF_USER_STATE);
        this.city = PreferenceUtils.getStringPreference(context, Constant.PREF_USER_CITY);
        this.promocode = PreferenceUtils.getStringPreference(context, Constant.PREF_USER_PROMO);
        this.user_avatar = PreferenceUtils.getStringPreference(context, Constant.PREF_USER_AVATAR);
        this.membership = PreferenceUtils.getIntPreference(context, Constant.PREF_USER_MEMBERSHIP);
        this.mode = PreferenceUtils.getIntPreference(context, Constant.PREF_USER_MODE);
        this.payment_type = PreferenceUtils.getStringPreference(context, Constant.PREF_USER_PAYMENT_TYPE);
        this.isTrial = PreferenceUtils.getIntPreference(context, Constant.PREF_USER_TRIAL);
        this.notifications = PreferenceUtils.getIntPreference(context, Constant.PREF_USER_NOTIFICATIONS);
        this.push_notify = PreferenceUtils.getIntPreference(context, Constant.PREF_USER_PUSH_NOTIFY);
        this.show_rate_date = PreferenceUtils.getStringPreference(context, Constant.PREF_USER_SHOW_RATEDATE);
        this.rate_status = PreferenceUtils.getIntPreference(context, Constant.PREF_USER_RATE_STATUS);
        this.ranked = PreferenceUtils.getIntPreference(context, Constant.PREF_USER_RANKED);
        this.bettingStrategy = PreferenceUtils.getStringPreference(context, Constant.PREF_USER_BETTING_STRATEGY);
        this.referral_affiliate_code = PreferenceUtils.getStringPreference(context, Constant.PREF_USER_REFERRAL_AFFILIATE_CODE);
        this.is_valid_affiliate_code = PreferenceUtils.getBooleanPreference(context, Constant.PREF_USER_IS_VALID_AFFILIATE_CODE);
    }
}
