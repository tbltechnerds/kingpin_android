package com.kingpin.mvp.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kingpin.common.utility.DateUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

public class UpcomingGame {

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("games")
    @Expose
    private ArrayList<UpcomingGameDetail> games;

    private UpcomingGame() {
        this.name = "";
        this.games = new ArrayList<>();
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<UpcomingGameDetail> getGames() {
        return this.games;
    }

    private void addGame(UpcomingGameDetail game) {
        this.games.add(game);
    }

    public static JsonDeserializer<UpcomingGame> deserializer = new JsonDeserializer<UpcomingGame>() {
        @Override
        public UpcomingGame deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject game_json = json.getAsJsonObject();
            UpcomingGame game = new UpcomingGame();
            game.setName(game_json.get("name").getAsString());

            JsonElement matches_element = game_json.get("games");
            if(matches_element.isJsonArray()) {
                JsonArray match_array = matches_element.getAsJsonArray();
                for(JsonElement match : match_array) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    gsonBuilder.registerTypeAdapter(UpcomingGameDetail.class, UpcomingGameDetail.deserializer);
                    Gson gson = gsonBuilder.create();
                    UpcomingGameDetail detail = gson.fromJson(match, UpcomingGameDetail.class);
                    if(detail.isPast())
                        continue;

                    boolean exist_match = false;
                    for(UpcomingGameDetail old_game : game.getGames()) {
                        if(old_game.getName().equals(detail.getName()) && old_game.getMatchDate().equals(detail.getMatchDate())) {
                            exist_match = true;
                            old_game.addBets(detail.getBets());
                            break;
                        }
                    }

                    if(!exist_match)
                        game.addGame(detail);
                }
            }

            Collections.sort(game.getGames(), new GameComparator());

            for(UpcomingGameDetail match : game.getGames()) {
                match.sort();
            }
            return game;
        }
    };

    private static class GameComparator implements Comparator<UpcomingGameDetail> {
        public int compare(UpcomingGameDetail first, UpcomingGameDetail second) {
            Date first_date = DateUtils.convertToLocal(first.getMatchDate());
            Date second_date = DateUtils.convertToLocal(second.getMatchDate());
            return first_date.compareTo(second_date);
        }
    }
}
