package com.kingpin.mvp.model;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kingpin.common.utility.DateUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Set;

public class UpcomingGameDetail {

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("match_date")
    @Expose
    private String match_date;

    @SerializedName("bets")
    @Expose
    private ArrayList<UpcomingGamePicker> bets;

    private UpcomingGameDetail() {
        this.name = "";
        this.match_date = "";
        this.bets = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMatchDate() {
        return match_date;
    }

    public Date getMatchDateTime() {
        return DateUtils.convertToLocal(this.match_date);
    }

    void setMatchDate(String date) {
        this.match_date = date;
    }

    private void addBet(UpcomingGamePicker bet) {
        this.bets.add(bet);
    }

    void addBets(ArrayList<UpcomingGamePicker> new_bets) {
        this.bets.addAll(new_bets);
    }

    public ArrayList<UpcomingGamePicker> getBets() {
        return bets;
    }

    boolean isPast() {
        Date matchDate = DateUtils.convertToLocal(this.match_date);
        Date curDate = DateUtils.getCurrentDate();
        return curDate.getTime() > matchDate.getTime();
    }

    void sort() {
        Collections.sort(this.bets, new BetComparator());
    }

    static JsonDeserializer<UpcomingGameDetail> deserializer = new JsonDeserializer<UpcomingGameDetail>() {
        @Override
        public UpcomingGameDetail deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject detail_json = json.getAsJsonObject();
            UpcomingGameDetail gameDetail = new UpcomingGameDetail();
            gameDetail.setName(detail_json.get("name").getAsString());
            gameDetail.setMatchDate(detail_json.get("match_date").getAsString());

            JsonElement bets_element = detail_json.get("bets");
            if(bets_element.isJsonArray()) {
                JsonArray bet_array = bets_element.getAsJsonArray();
                for(JsonElement bet : bet_array) {
                    Gson gson = new Gson();
                    UpcomingGamePicker picker = gson.fromJson(bet, UpcomingGamePicker.class);
                    gameDetail.addBet(picker);
                }
            }
            else {
                JsonObject bet_object = bets_element.getAsJsonObject();
                Set<String> keys = bet_object.keySet();
                for(String key : keys) {
                    JsonElement bet = bet_object.get(key);
                    Gson gson = new Gson();
                    UpcomingGamePicker picker = gson.fromJson(bet, UpcomingGamePicker.class);
                    gameDetail.addBet(picker);
                }
            }

            return gameDetail;
        }
    };

    private class BetComparator implements Comparator<UpcomingGamePicker> {
        public int compare(UpcomingGamePicker first, UpcomingGamePicker second) {
            return Integer.compare(first.getKingpinRank(), second.getKingpinRank());
        }
    }
}
