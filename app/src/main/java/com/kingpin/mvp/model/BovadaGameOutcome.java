package com.kingpin.mvp.model;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kingpin.common.base.Constant;

public class BovadaGameOutcome {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("desc")
    @Expose
    private String desc;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("competitorId")
    @Expose
    private int competitorId;

    @SerializedName("price")
    @Expose
    private BovadaGamePrice price;

    public BovadaGameOutcome() {
        id = "";
        desc = "";
        status = "";
        competitorId = 0;
        price = new BovadaGamePrice();
    }

    public static BovadaGameOutcome initWithJsonObject(JsonObject json) {

        BovadaGameOutcome outcome = new BovadaGameOutcome();

        if (json.has("id") && !json.get("id").isJsonNull()) {
            outcome.id = json.get("id").getAsString();
        }

        if (json.has("description") && !json.get("description").isJsonNull()) {
            outcome.desc = json.get("description").getAsString();
        }

        if (json.has("status") && !json.get("status").isJsonNull()) {
            outcome.status = json.get("status").getAsString();
        }

        if (json.has("competitorId") && !json.get("competitorId").isJsonNull()) {
            outcome.competitorId = json.get("competitorId").getAsInt();
        }

        if (json.has("price") && !json.get("price").isJsonNull()) {
            JsonObject price = json.get("price").getAsJsonObject();
            outcome.price = BovadaGamePrice.initWithJsonObject(price);
        }

        return outcome;
    }

    public String getId() {
        return id;
    }

    public String getDesc() {
        return desc;
    }

    public String getStatus() {
        return status;
    }

    public int getCompetitorId() {
        return competitorId;
    }

    public BovadaGamePrice getPrice() {
        return price;
    }
}
