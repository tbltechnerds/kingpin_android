package com.kingpin.mvp.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kingpin.mvp.model.Profile;

public class BalanceResponse {
    @SerializedName("status")
    @Expose
    private boolean status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private Data data;

    public BalanceResponse() {
        this.status = false;
        this.message = "";
        this.data = new Data();
    }

    public boolean isSuccess() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public Data getData() {
        return data;
    }

    public class Data {
        @SerializedName("account_balance")
        @Expose
        private double account_balance;

        @SerializedName("user")
        @Expose
        private Profile user;

        public Data() {
            this.account_balance = 0.0f;
            this.user = null;
        }

        public double getBalance() {
            return this.account_balance;
        }

        public Profile getUser() {
            return this.user;
        }
    }
}
