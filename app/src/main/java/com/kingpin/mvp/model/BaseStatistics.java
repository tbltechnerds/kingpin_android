package com.kingpin.mvp.model;

public class BaseStatistics {

    public static final int TYPE_NORMAL = 0;
    public static final int TYPE_GRAPH = 1;

    private int type;

    BaseStatistics(int type) {
        this.type = type;
    }

    public int getType() {
        return this.type;
    }
}
