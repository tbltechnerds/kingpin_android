package com.kingpin.mvp.model;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Type;

public class Withdraw {

    @SerializedName("created_at")
    @Expose
    private String created_at;

    @SerializedName("amount")
    @Expose
    private int amount;

    @SerializedName("total_pot")
    @Expose
    private int total_pot;

    @SerializedName("month")
    @Expose
    private String month;

    @SerializedName("ranking")
    @Expose
    private int ranking;

    @SerializedName("description")
    @Expose
    private String description;

    private boolean isExpanded;

    private Withdraw() {
        this.created_at = "";
        this.amount = 0;
        this.total_pot = 0;
        this.month = "";
        this.ranking = 0;
        this.description = "";
        this.isExpanded = false;
    }

    private void setCreatedDate(String date) {
        this.created_at = date;
    }

    public String getCreatedDate() {
        return this.created_at;
    }

    private void setAmount(int amount) {
        this.amount = amount;
    }

    public int getAmount() {
        return this.amount;
    }

    private void setTotalPot(int totalPot) {
        this.total_pot = totalPot;
    }

    public int getTotalPot() {
        return this.total_pot;
    }

    private void setMonth(String month) {
        this.month = month;
    }

    public String getMonth() {
        return this.month;
    }

    private void setRanking(int ranking) {
        this.ranking = ranking;
    }

    public int getRanking() {
        return this.ranking;
    }

    private void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    public void setExpanded(boolean value) {
        this.isExpanded = value;
    }

    public boolean isExpanded() {
        return this.isExpanded;
    }

    public static JsonDeserializer<Withdraw> deserializer = new JsonDeserializer<Withdraw>() {
        @Override
        public Withdraw deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject withdraw_json = json.getAsJsonObject();
            Withdraw withdraw = new Withdraw();

            if(withdraw_json.has("created_at") && !withdraw_json.get("created_at").isJsonNull())
                withdraw.setCreatedDate(withdraw_json.get("created_at").getAsString());

            if(withdraw_json.has("amount") && !withdraw_json.get("amount").isJsonNull()) {
                int amount;
                try {
                    amount = withdraw_json.get("amount").getAsInt();
                }
                catch(Exception e) {
                    double double_value = withdraw_json.get("amount").getAsDouble();
                    amount = (int) Math.round(double_value);
                }
                withdraw.setAmount(amount);
            }

            if(withdraw_json.has("total_pot") && !withdraw_json.get("total_pot").isJsonNull()) {
                int total_pot;
                try {
                    total_pot = withdraw_json.get("total_pot").getAsInt();
                }
                catch(Exception e) {
                    double double_value = withdraw_json.get("total_pot").getAsDouble();
                    total_pot = (int) Math.round(double_value);
                }
                withdraw.setTotalPot(total_pot);
            }

            if(withdraw_json.has("month") && !withdraw_json.get("month").isJsonNull())
                withdraw.setMonth(withdraw_json.get("month").getAsString());

            if(withdraw_json.has("ranking") && !withdraw_json.get("ranking").isJsonNull())
                withdraw.setRanking(withdraw_json.get("ranking").getAsInt());

            if(withdraw_json.has("description") && !withdraw_json.get("description").isJsonNull())
                withdraw.setDescription(withdraw_json.get("description").getAsString());

            return withdraw;
        }
    };
}
