package com.kingpin.mvp.model;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kingpin.common.utility.DateUtils;

import java.util.Date;

public class Game {

    private String sport;
    private boolean isBovada;

    public Game() {
        sport = "";
        isBovada = true;
    }

    public String getSport() {
        return sport;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }

    public Boolean isBovada() { return isBovada; }

    public void setIsBovada(boolean isBovada) {
        this.isBovada = isBovada;
    }
}
