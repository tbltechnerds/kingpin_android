package com.kingpin.mvp.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kingpin.mvp.model.Bet;

import java.util.ArrayList;

public class BetHistoryResponse {
    @SerializedName("status")
    @Expose
    private boolean status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private ArrayList<Bet> bets;

    public BetHistoryResponse() {
        this.status = false;
        this.message = "";
        this.bets = new ArrayList<>();
    }

    public boolean isSuccess() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public ArrayList<Bet> getBets() {
        return this.bets;
    }
}
