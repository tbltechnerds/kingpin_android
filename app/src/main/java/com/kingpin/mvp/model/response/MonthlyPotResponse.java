package com.kingpin.mvp.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MonthlyPotResponse {

    @SerializedName("status")
    @Expose
    private boolean status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private Data data;

    public MonthlyPotResponse() {
        this.status = false;
        this.message = "";
        this.data = new Data();
    }

    public boolean isSuccess() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public Data getData() {
        return data;
    }

    public class Data {
        @SerializedName("subscriber_count")
        @Expose
        private int subscriber_count;

        @SerializedName("monthly_pot")
        @Expose
        private String monthly_pot;

        @SerializedName("default_sport")
        @Expose
        private String default_sport;

        @SerializedName("free_trial_android")
        @Expose
        private String free_trial_android;

        @SerializedName("affiliate_terms")
        @Expose
        private String affiliate_terms;

        @SerializedName("show_promo_ios")
        @Expose
        private String show_promo_ios;

        @SerializedName("show_facebook_login")
        @Expose
        private String show_facebook_login;

        Data() {
            this.subscriber_count = 0;
            this.monthly_pot = "";
            this.default_sport = "";
            this.free_trial_android = "0";
            this.affiliate_terms = "";
            this.show_promo_ios = "0";
            this.show_facebook_login = "1";
        }

        public int getSubScriberCount() {
            return this.subscriber_count;
        }

        public String getMonthlyPot() {
            return this.monthly_pot;
        }

        public String getDefaultSport() {
            return this.default_sport;
        }

        public boolean getTrialAvailableStatus() {
            return this.free_trial_android.equals("1");
        }

        public String getAffiliateTerms() {return this.affiliate_terms;}

        public boolean isShowPromoCodeOnRegister() {
            return this.show_promo_ios.equals("1");
        }

        public boolean isShowFacebookLogin() {
            return this.show_facebook_login.equals("1");
        }
    }
}
