package com.kingpin.mvp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpcomingGamePicker {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("sport")
    @Expose
    private String sport;

    @SerializedName("match_date")
    @Expose
    private String match_date;

    @SerializedName("marketId")
    @Expose
    private String marketId;

    @SerializedName("user_id")
    @Expose
    private int userId;

    @SerializedName("user_name")
    @Expose
    private String user_name;

    @SerializedName("user_avatar")
    @Expose
    private String user_avatar;

    @SerializedName("kingpin_rank")
    @Expose
    private Integer kingpin_rank;

    @SerializedName("team_bet")
    @Expose
    private String team_bet;

    @SerializedName("team_1")
    @Expose
    private String team1;

    @SerializedName("team_2")
    @Expose
    private String team2;

    @SerializedName("teams")
    @Expose
    private String teams;

    @SerializedName("line")
    @Expose
    private String line;

    @SerializedName("parlay_id")
    @Expose
    private int parlay_id;

    @SerializedName("summed_parlay_id")
    @Expose
    private int summed_parlay_id;

    @SerializedName("bet_param")
    @Expose
    private float bet_param;

    @SerializedName("bet_amount")
    @Expose
    private float bet_amount;

    @SerializedName("bet_type")
    @Expose
    private String bet_type;

    @SerializedName("win_loss_amount")
    @Expose
    private float win_loss_amount;

    @SerializedName("user_winningLosses")
    @Expose
    private float user_winningLosses;

    @SerializedName("potential_win")
    @Expose
    private float potential_win;

    @SerializedName("result")
    @Expose
    private float result;

    @SerializedName("odd")
    @Expose
    private float odd;

    @SerializedName("code")
    @Expose
    private int code;

    @SerializedName("created_at")
    @Expose
    private String created_at;

    @SerializedName("updated_at")
    @Expose
    private String updated_at;

    public UpcomingGamePicker() {
        this.id = 0;
        this.sport = "";
        this.match_date = "";
        this.marketId = "";
        this.userId = 0;
        this.user_name = "";
        this.user_avatar = "";
        this.kingpin_rank = 0;
        this.team_bet = "";
        this.team1 = "";
        this.team2 = "";
        this.teams = "";
        this.line = "";
        this.parlay_id = 0;
        this.summed_parlay_id = 0;
        this.bet_param = 0.0f;
        this.bet_amount = 0.0f;
        this.bet_type = "";
        this.win_loss_amount = 0.0f;
        this.user_winningLosses = 0.0f;
        this.potential_win = 0.0f;
        this.result = 0.0f;
        this.odd = 0.0f;
        this.code = 0;
        this.created_at = "";
        this.updated_at = "";
    }

    int getKingpinRank() {
        return (this.kingpin_rank == null) ? 10000 : this.kingpin_rank;
    }

    public String getKinpinRankString() {
        return (this.kingpin_rank == null) ? "" : String.valueOf(this.kingpin_rank);
    }

    public String getAvatar() {
        return this.user_avatar;
    }

    public String getName() {
        return this.user_name;
    }

    public float getWinloss() {
        return this.user_winningLosses;
    }

    public String getTeamBet() {
        return this.team_bet;
    }

    public String getLine() {
        return this.line;
    }

    public float getBetAmount() {
        return this.bet_amount;
    }

    public int getUserId() {
        return this.userId;
    }
}