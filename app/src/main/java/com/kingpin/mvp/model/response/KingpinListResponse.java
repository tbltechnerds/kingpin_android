package com.kingpin.mvp.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kingpin.mvp.model.Kingpin;

import java.util.ArrayList;

public class KingpinListResponse {
    @SerializedName("status")
    @Expose
    private boolean status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private ArrayList<Kingpin> kingpins;

    @SerializedName("current_page")
    @Expose
    private int current_page;

    public KingpinListResponse() {
        this.status = false;
        this.message = "";
        this.current_page = 0;
        this.kingpins = new ArrayList<>();
    }

    public boolean isSuccess() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public ArrayList<Kingpin> getKingpins() {
        return this.kingpins;
    }

    public int getCurrentPage() {
        return this.current_page;
    }
}
