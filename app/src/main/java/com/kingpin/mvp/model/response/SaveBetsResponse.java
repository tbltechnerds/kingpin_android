package com.kingpin.mvp.model.response;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kingpin.common.manager.DataManager;
import com.kingpin.mvp.model.BovadaGame;
import com.kingpin.mvp.model.RapidGame;

import java.util.ArrayList;

public class SaveBetsResponse {
    @SerializedName("status")
    @Expose
    private boolean status;

    @SerializedName("message")
    @Expose
    private String message;

    public SaveBetsResponse() {
        this.status = false;
        this.message = "";
    }

    public boolean isSuccess() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
