package com.kingpin.mvp.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kingpin.common.utility.DateUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;

public class Bet {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("match_date")
    @Expose
    private String match_date;

    @SerializedName("teams")
    @Expose
    private String teams;

    @SerializedName("team_bet")
    @Expose
    private String team_bet;

    @SerializedName("result")
    @Expose
    private String result;

    @SerializedName("sport")
    @Expose
    private String sport;

    @SerializedName("line")
    @Expose
    private String line;

    @SerializedName("bet_amount")
    @Expose
    private int bet_amount;

    @SerializedName("win_loss_amount")
    @Expose
    private int win_loss_amount;

    @SerializedName("potential_win")
    @Expose
    private int potential_win;

    @SerializedName("period_code")
    @Expose
    private String period_code;

    @SerializedName("parlay_id")
    @Expose
    private Integer parlay_id;

    @SerializedName("parlays")
    @Expose
    private ArrayList<Parlay> parlays;

    private Bet() {
        this.id = -1;
        this.match_date = "";
        this.teams = "";
        this.team_bet = "";
        this.result = "";
        this.sport = "";
        this.line = "";
        this.bet_amount = 0;
        this.win_loss_amount = 0;
        this.potential_win = 0;
        this.period_code = "";
        this.parlay_id = null;
        this.parlays = new ArrayList<>();
    }

    public Date getMatchDate() {
        return DateUtils.getDateWithServerTimezone(this.match_date);
    }

    public String getBetDate() {
        if(this.match_date == null)
            return "";

        String date = DateUtils.convertToLocalDate(this.match_date);
        String time = DateUtils.convertToLocalTime(this.match_date);
        return String.format("%s\n(%s)", date, time);
    }

    public String getBetDateOnLine() {
        if(this.match_date == null)
            return "";

        String date = DateUtils.convertToLocalDateWithFormat("MMM dd yy", this.match_date);
        String time = DateUtils.convertToLocalTime(this.match_date);
        return String.format("%s (%s)", date, time);
    }

    public String getBetOnlyDate() {
        if(this.match_date == null)
            return "";

        String date = DateUtils.convertToLocalDateWithFormat("MMM dd yy", this.match_date);
        return date;
    }

    public String getTeams() {
        return this.teams;
    }

    public String getTeamBet() {
        return this.team_bet;
    }

    public String getLine() {
        return this.line;
    }

    public boolean isParlayBet() {
        return this.parlay_id != null;
    }

    public int getBetAmount() {
        return this.bet_amount;
    }

    public int getPotentialWin() {
        return this.potential_win;
    }

    public String getResult() {
        return this.result;
    }

    public int getWinLossAmount() {
        return this.win_loss_amount;
    }

    public String getParlayBetTeamString() {
        StringBuilder team = new StringBuilder();
        if(this.parlays != null) {
            for(Parlay parlay : this.parlays) {
                if(team.length() == 0) {
                    team = new StringBuilder(parlay.getTeams());
                    team.append(" (" + parlay.getLine() + ")");
                } else {
                    team.append("\n").append(parlay.getTeamBet()).append(" (").append(parlay.getLine()).append(")");
                }
            }
        }
        return team.toString();
    }

    public int getParlayBetTeamCounts() {
        if(this.parlays == null)
            return 0;

        return this.parlays.size();
    }

    public String getPeriodCode() {
        return period_code;
    }

    private void setId(int id) {
        this.id = id;
    }

    private void setMatchDate(String date) {
        this.match_date = date;
    }

    private void setTeams(String teams) {
        this.teams = teams;
    }

    private void setTeamBet(String team_bet) {
        this.team_bet = team_bet;
    }

    private void setResult(String result) {
        this.result = result;
    }

    private void setSports(String sport) {
        this.sport = sport;
    }

    private void setLine(String line) {
        this.line = line;
    }

    private void setBetAmount(int amount) {
        this.bet_amount = amount;
    }

    private void setWinLoseAmount(int amount) {
        this.win_loss_amount = amount;
    }

    private void setPotentialWin(int amount) {
        this.potential_win = amount;
    }

    private void setPeriodCode(String periodCode) {
        this.period_code = periodCode;
    }

    private void setParlayId(int id) {
        this.parlay_id = id;
    }

    private void addParlay(Parlay parlay) {
        this.parlays.add(parlay);
    }

    public static JsonDeserializer<Bet> deserializer = new JsonDeserializer<Bet>() {
        @Override
        public Bet deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject bet_json = json.getAsJsonObject();
            Bet bet = new Bet();

            if(bet_json.has("id") && !bet_json.get("id").isJsonNull())
                bet.setId(bet_json.get("id").getAsInt());

            if(bet_json.has("match_date") && !bet_json.get("match_date").isJsonNull())
                bet.setMatchDate(bet_json.get("match_date").getAsString());

            if(bet_json.has("teams") && !bet_json.get("teams").isJsonNull())
                bet.setTeams(bet_json.get("teams").getAsString());

            if(bet_json.has("team_bet") && !bet_json.get("team_bet").isJsonNull())
                bet.setTeamBet(bet_json.get("team_bet").getAsString());

            if(bet_json.has("result") && !bet_json.get("result").isJsonNull())
                bet.setResult(bet_json.get("result").getAsString());

            if(bet_json.has("sport") && !bet_json.get("sport").isJsonNull())
                bet.setSports(bet_json.get("sport").getAsString());

            if(bet_json.has("line") && !bet_json.get("line").isJsonNull())
                bet.setLine(bet_json.get("line").getAsString());

            if(bet_json.has("bet_amount") && !bet_json.get("bet_amount").isJsonNull()) {
                int bet_amount;
                try {
                    bet_amount = bet_json.get("bet_amount").getAsInt();
                }
                catch(Exception e) {
                    double double_value = bet_json.get("bet_amount").getAsDouble();
                    bet_amount = (int) Math.round(double_value);
                }
                bet.setBetAmount(bet_amount);
            }

            if(bet_json.has("win_loss_amount") && !bet_json.get("win_loss_amount").isJsonNull()) {
                int win_loss_amount;
                try {
                    win_loss_amount = bet_json.get("win_loss_amount").getAsInt();
                }
                catch(Exception e) {
                    double double_value = bet_json.get("win_loss_amount").getAsDouble();
                    win_loss_amount = (int) Math.round(double_value);
                }
                bet.setWinLoseAmount(win_loss_amount);
            }

            if(bet_json.has("period_code") && !bet_json.get("period_code").isJsonNull())
                bet.setPeriodCode(bet_json.get("period_code").getAsString());

            if(bet_json.has("potential_win") && !bet_json.get("potential_win").isJsonNull()) {
                int potential_win;
                try {
                    potential_win = bet_json.get("potential_win").getAsInt();
                }
                catch(Exception e) {
                    double double_value = bet_json.get("potential_win").getAsDouble();
                    potential_win = (int) Math.round(double_value);
                }
                bet.setPotentialWin(potential_win);
            }

            if(bet_json.has("parlay_id") && !bet_json.get("parlay_id").isJsonNull()) {
                bet.setParlayId(bet_json.get("parlay_id").getAsInt());

                if(bet_json.has("parlays") && !bet_json.get("parlays").isJsonNull()) {
                    JsonElement parlays_element = bet_json.get("parlays");
                    if(parlays_element.isJsonArray()) {
                        JsonArray parlays_array = parlays_element.getAsJsonArray();
                        for(JsonElement item : parlays_array) {
                            GsonBuilder gsonBuilder = new GsonBuilder();
                            gsonBuilder.registerTypeAdapter(Parlay.class, Parlay.deserializer);
                            Gson gson = gsonBuilder.create();
                            Parlay parlay = gson.fromJson(item, Parlay.class);
                            bet.addParlay(parlay);
                        }
                    }
                }
            }

            return bet;
        }
    };
}
