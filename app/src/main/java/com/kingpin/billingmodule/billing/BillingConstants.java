/*
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kingpin.billingmodule.billing;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClient.SkuType;
import java.util.Arrays;
import java.util.List;

/**
 * Static fields and methods useful for billing
 */
public final class BillingConstants {
    // SKUs for our products: (non-consumable in-apps)

    // SKU for our subscription: (consumable in-apps)
    public static final String SKU_MONTHLY_WITH_TRIAL = "kingpinpro.monthly_trial";
    //public static final String SKU_MONTHLY = "android.test.purchased";
    public static final String SKU_MONTHLY = "kingpinpro.monthly";
    public static final String SKU_QUARTER = "kingpinpro.quarter";
    public static final String SKU_YEAR = "kingpinpro.year";

    //7-Day Trial to Sports Betting Tips by KingPin
    // Valid Promo code on registration + FREE trial OFF/ON
    public static final String SKU_DAY_PROMO = "kingpinpro.day.promo";                       // 7.99 / day : free trial off
    public static final String SKU_FREETRIAL_MONTHLY_PROMO = "kingpinpro.free.monthly.promo"; // 31.99 / monthly + 7 free days : free trial on
    public static final String SKU_MONTHLY_PROMO = "kingpinpro.monthly.promo";               // 31.99 / monthly,
    public static final String SKU_QUARTERLY_PROMO = "kingpinpro.quarterly.promo";           // 55.99 / quarterly
    public static final String SKU_YEARLY_PROMO = "kingpinpro.yearly.promo";                 // 79.99 / yearly

    // No Promo code on registration + FREE trial OFF/ON
    public static final String SKU_DAY_NOPROMO = "kingpinpro.day.nopromo";                       // 9.99 / day : free trial off
    public static final String SKU_FREETRIAL_MONTHLY_NOPROMO = "kingpinpro.free.monthly.nopromo"; // 39.99 / monthly + 7 free days : free trial on
    public static final String SKU_MONTHLY_NOPROMO = "kingpinpro.monthly.nopromo";               // 39.99 / monthly,
    public static final String SKU_QUARTERLY_NOPROMO = "kingpinpro.quarterly.nopromo";           // 69.99 / quarterly
    public static final String SKU_YEARLY_NOPROMO = "kingpinpro.yearly.nopromo";                 // 99.99 / yearly

    // revenuecart
    public static final String ENTITLEMENT_PRO = "Pro";
    public static final String OFFERING_DAY = "day";
    public static final String OFFERING_DAY_PROMO = "day.promo";
    public static final String OFFERING_MONTHLY = "monthly";
    public static final String OFFERING_MONTHLY_PROMO = "monthly.promo";
    public static final String OFFERING_QUARTERLY = "quarterly";
    public static final String OFFERING_QUARTERLY_PROMO = "quarterly.promo";
    public static final String OFFERING_YEARLY = "yearly";
    public static final String OFFERING_YEARLY_PROMO = "yearly.promo";
    public static final String OFFERING_TRIAL_MONTHLY = "trial.monthly";
    public static final String OFFERING_TRIAL_MONTHLY_PROMO = "trial.monthly.promo";

    private static final String[] IN_APP_SKUS = {};
    private static final String[] SUBSCRIPTIONS_SKUS = {
            SKU_MONTHLY_WITH_TRIAL,
            SKU_MONTHLY,
            SKU_QUARTER,
            SKU_YEAR,

            SKU_DAY_PROMO,
            SKU_FREETRIAL_MONTHLY_PROMO,
            SKU_MONTHLY_PROMO,
            SKU_QUARTERLY_PROMO,
            SKU_YEARLY_PROMO,

            SKU_DAY_NOPROMO,
            SKU_FREETRIAL_MONTHLY_NOPROMO,
            SKU_MONTHLY_NOPROMO,
            SKU_QUARTERLY_NOPROMO,
            SKU_YEARLY_NOPROMO,
    };

    private BillingConstants(){}

    /**
     * Returns the list of all SKUs for the billing type specified
     */
    public static List<String> getSkuList(@BillingClient.SkuType String billingType) {
        return (billingType == SkuType.INAPP) ? Arrays.asList(IN_APP_SKUS) : Arrays.asList(SUBSCRIPTIONS_SKUS);
    }
}

