package com.kingpin.app;

import android.app.Application;
import android.support.multidex.MultiDexApplication;

import com.onesignal.OneSignal;

public class KingpinApplication extends MultiDexApplication {

    private boolean bNeedToRegister = true;

    @Override
    public void onCreate() {
        super.onCreate();

        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

}
