package com.kingpin.listener;

import okhttp3.ResponseBody;
import retrofit2.Response;

public interface ApiListener {
    void onAPISuccess(int api_kind, String result);
    void onAPIError(int api_kind, Response<ResponseBody> response);
    void onAPIFailure(int api_kind);
}