package com.kingpin.listener;

import android.graphics.Bitmap;

public interface DownloadAvatarListener {
    void DownloadAvatarCompleted(Bitmap bitmap);
}
