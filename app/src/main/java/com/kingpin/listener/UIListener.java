package com.kingpin.listener;

import android.content.Intent;
import android.support.annotation.StringRes;

import com.kingpin.ui.activity.BaseActivity;

public class UIListener {

    public interface BaseActivityListener {
        void updateFragment(int fragment);
        void addFragment(int fragment);
        void removeFragment(int fragment);

        void showLoading();
        void hideLoading();

        void showToast(@StringRes int messageId);
        void showToast(String message);
        void hideToast();

        void startIntent(Class<?> activity, int fragment, boolean bFinish);
        void startIntent(Intent intent, int fragment, boolean bFinish);

        BaseActivity getActivity();

        int FRAGMENT_NULL               = 0;
    }

    public interface StartActivityListener extends BaseActivityListener {
        int FRAGMENT_SPLASH             = 1;
        int FRAGMENT_LAND               = 2;
        int FRAGMENT_FORGOTPWD          = 3;
    }

    public interface HomeActivityListener extends BaseActivityListener {
        int FRAGMENT_UPCOMING_GAME      = 1;
        int FRAGMENT_TOP_KINGPIN        = 2;
        int FRAGMENT_MY_STATICS         = 3;
        int FRAGMENT_PLACE_PICK         = 4;
        int FRAGMENT_FREE_TRIAL         = 5;
        int FRAGMENT_MY_KINGPIN         = 6;
        int FRAGMENT_SETTING            = 7;
        int FRAGMENT_UPCOMING_DETAIL    = 8;
        int FRAGMENT_INAPP_PURCHASE     = 9;
        int FRAGMENT_FILTER             = 10;
        int FRAGMENT_USER_PROFILE       = 11;
        int FRAGMENT_PLACEPICK_FILTER   = 12;
        int FRAGMENT_BETHELP            = 13;
        int FRAGMENT_ASK_RATE           = 14;
        int FRAGMENT_SUBMIT_FEEDBACK    = 15;
    }

    public interface ProfileActivityListener extends BaseActivityListener {
        int FRAGMENT_PROFILE            = 1;
        int FRAGMENT_INAPP_PURCHASE     = 2;
        int FRAGMENT_REQUEST_PAYOUT     = 3;
        int FRAGMENT_PAYMENT_HISTORY    = 4;
        int FRAGMENT_EDIT_PROFILE       = 5;
        int FRAGMENT_AFFILIATE_HISTORY  = 6;
    }

    public interface  GameDetailActivityListener extends BaseActivityListener {
        int FRAGMENT_RAPID              = 1;
        int FRAGMENT_BOVADA             = 2;
    }

    public interface CartActivityListener extends BaseActivityListener {
        int FRAGMENT_CART               = 1;
    }
}
