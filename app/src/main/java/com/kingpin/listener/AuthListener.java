package com.kingpin.listener;

public interface AuthListener {
    void onAuthenticated(String strSuccess);
    void onAuthError(String strError);
    void onAuthFailed(String strError);
}
