package com.kingpin.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kingpin.R;
import com.kingpin.common.manager.TempDataManager;
import com.kingpin.mvp.model.BovadaGame;
import com.kingpin.mvp.model.Game;
import com.kingpin.mvp.model.RapidGame;
import com.kingpin.mvp.view.BaseView;
import com.kingpin.ui.adapter.holder.GameHolder;

import java.util.ArrayList;

public class GameAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Game> mGames;

    private BaseView mListenerView;

    public GameAdapter(BaseView listenerView) {
        super();
        this.mListenerView = listenerView;
        this.mGames = TempDataManager.getInstance().getGames();
    }

    @NonNull
    @Override
    public GameHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_bet_game, parent, false);
        return new GameHolder(v, mListenerView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Game game = mGames.get(position);
        GameHolder gameHolder = (GameHolder) holder;
        gameHolder.bind( game);
    }

    @Override
    public int getItemCount() {
        return this.mGames.size();
    }

    public void updateGames() {
        this.mGames = TempDataManager.getInstance().getGames();
        notifyDataSetChanged();
    }
}
