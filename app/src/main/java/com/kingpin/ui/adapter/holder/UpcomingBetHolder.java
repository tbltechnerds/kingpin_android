package com.kingpin.ui.adapter.holder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.kingpin.R;
import com.kingpin.common.base.Constant;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.manager.TempDataManager;
import com.kingpin.mvp.model.Bet;
import com.kingpin.mvp.model.Kingpin;
import com.kingpin.mvp.model.Profile;
import com.kingpin.mvp.view.BaseView;
import com.kingpin.mvp.view.UserProfileView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UpcomingBetHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.txt_game) TextView mGameView;
    @BindView(R.id.txt_pick) TextView mPickView;
    @BindView(R.id.txt_amount) TextView mAmountView;
    @BindView(R.id.txt_amount_to_won) TextView mAmountWonView;
    @BindView(R.id.btn_share) ImageView btnShare;

    private BaseView mListenerView;
    private Bet mBet;

    public UpcomingBetHolder(@NonNull View itemView, BaseView listenerView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.mListenerView = listenerView;
        mGameView.setOnClickListener(this);
        mPickView.setOnClickListener(this);
        mAmountView.setOnClickListener(this);
        mAmountWonView.setOnClickListener(this);
        btnShare.setOnClickListener(this);
    }

    public void bind(Bet bet, boolean isFollowing, boolean isMine) {
        mBet = bet;

        if(bet.isParlayBet()) {
            mPickView.setText("Parlay");
            String pickedTeam = bet.getParlayBetTeamString() + "\n" + bet.getBetDateOnLine();
            mGameView.setText(pickedTeam);
        } else {
            if (bet.getPeriodCode().equals("")) {
                mPickView.setText(bet.getTeamBet() + " (" + bet.getLine() + ")");
            } else {
                mPickView.setText(bet.getTeamBet() + "(" + bet.getPeriodCode() + ")" + " (" + bet.getLine() + ")");
            }
            String pickedTeam = bet.getTeams() + "\n" + bet.getBetDateOnLine();
            mGameView.setText(pickedTeam);
        }


        if(DataManager.getInstance().isSubscribed() || isMine) {
//            if(isFollowing) {
//                mAmountView.setText(String.valueOf(bet.getBetAmount()));
//                mAmountWonView.setText(String.valueOf(bet.getPotentialWin()));
//            } else {
//                mAmountView.setText("Not following");
//                mAmountWonView.setText("Not following");
//            }

            mAmountView.setText(String.valueOf(bet.getBetAmount()));
            mAmountWonView.setText(String.valueOf(bet.getPotentialWin()));
        }
        else {
            mAmountView.setText("Subscribe\nto see");
            mAmountWonView.setText("Subscribe\nto see");

            if(bet.isParlayBet()) {
                mPickView.setText("Parlay");
                mGameView.setText("Subscribe\nto see");
            } else {
                mPickView.setText("Subscribe\nto see");
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_game:
            case R.id.txt_pick:
            case R.id.txt_amount:
            case R.id.txt_amount_to_won:
                if(mListenerView instanceof UserProfileView)
                    ((UserProfileView) mListenerView).OnChooseUpcomingBet();
                break;
            case R.id.btn_share:
                onShare();
                break;
        }
    }

    private void onShare() {
        Profile me = DataManager.getInstance().getAccount();
        Kingpin kingpin = TempDataManager.getInstance().getSelectedKingpin();
        if (me != null && kingpin != null) {
            String team = "";
            if (mBet.isParlayBet()) {
                team = mBet.getParlayBetTeamString();
            } else {
                team = mBet.getTeams() + " " + mBet.getTeamBet() + " (" + mBet.getLine() + ")";
            }


            String text = "Check out " + kingpin.getName() + "'s pick on the " + team + " " + Constant.APP_SHARE_LINK + " Get 10% off with promo code: " + me.getReferralAffiliateCode();
            mListenerView.onShareText(text);
        }
    }
}
