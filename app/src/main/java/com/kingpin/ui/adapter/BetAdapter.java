package com.kingpin.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kingpin.R;
import com.kingpin.mvp.model.UpcomingGamePicker;
import com.kingpin.mvp.view.BaseView;
import com.kingpin.ui.adapter.holder.BetHolder;

import java.util.ArrayList;

public class BetAdapter extends RecyclerView.Adapter<BetHolder> {

    private ArrayList<UpcomingGamePicker> mBets;
    private BaseView mListenerView;

    public BetAdapter(ArrayList<UpcomingGamePicker> bets, BaseView listenerView) {
        super();
        this.mListenerView = listenerView;
        this.mBets = bets;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public BetHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_bet, parent, false);
        return new BetHolder(v, mListenerView);
    }

    @Override
    public void onBindViewHolder(@NonNull BetHolder holder, int position) {
        holder.bind(this.mBets.get(position));
    }

    @Override
    public int getItemCount() {
        return this.mBets.size();
    }
}

