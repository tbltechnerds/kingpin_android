package com.kingpin.ui.adapter.holder;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IFillFormatter;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.kingpin.R;
import com.kingpin.common.base.Constant;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.manager.FileManager;
import com.kingpin.common.task.DownloadAvatarTask;
import com.kingpin.common.utility.CommonUtils;
import com.kingpin.listener.DownloadAvatarListener;
import com.kingpin.mvp.model.Bet;
import com.kingpin.mvp.model.BovadaGame;
import com.kingpin.mvp.model.FilterOptions;
import com.kingpin.mvp.model.Game;
import com.kingpin.mvp.model.Kingpin;
import com.kingpin.mvp.model.Profile;
import com.kingpin.mvp.model.RapidGame;
import com.kingpin.mvp.view.BaseView;
import com.kingpin.mvp.view.PlacePickView;
import com.kingpin.mvp.view.TopKingpinView;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GameHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.txt_date) TextView mGameDateView;
    @BindView(R.id.txt_title) TextView mGameTitleView;

    private BaseView mListenerView;
    private Game mGame;

    public GameHolder(@NonNull View itemView, BaseView listenerView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.mGame = null;
        this.mListenerView = listenerView;

        mGameTitleView.setOnClickListener(this);
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    public void bind(Game game) {
        mGame = game;
        if (game.isBovada()) {
            BovadaGame bovadaGame = (BovadaGame) game;
            mGameDateView.setText(bovadaGame.getGameDate());
            mGameTitleView.setText(bovadaGame.getGameTitle());
        } else {
            RapidGame rapidGame = (RapidGame) game;
            mGameDateView.setText(rapidGame.getGameDateString());
            mGameTitleView.setText(rapidGame.getGameName());
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txt_title:
                ((PlacePickView)mListenerView).onViewGame(mGame);
                break;
        }
    }
}
