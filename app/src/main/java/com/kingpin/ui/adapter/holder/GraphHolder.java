package com.kingpin.ui.adapter.holder;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IFillFormatter;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.kingpin.R;
import com.kingpin.mvp.model.GraphStatistics;
import com.kingpin.mvp.view.BaseView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GraphHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.txt_title) TextView mTitleView;
    @BindView(R.id.txt_detail) TextView mDetailView;
    @BindView(R.id.line_chart) LineChart mLineChart;

    private BaseView mListenerView;

    public GraphHolder(@NonNull View itemView, BaseView listenerView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        mListenerView = listenerView;
    }

    public void bind(GraphStatistics statistics) {
        mTitleView.setText(statistics.getTitle());
        mDetailView.setText(statistics.getDetail());
        loadChart(statistics.getLabelList(), statistics.getValueList());
    }

    private void loadChart(ArrayList<String> labelList, ArrayList<Float> valueList) {

        int line_width = mListenerView.getCurrentActivity().getResources().getDimensionPixelSize(R.dimen.dimen_2dp);
        float density = mListenerView.getCurrentActivity().getResources().getDisplayMetrics().density;

        mLineChart.setBackgroundColor(Color.TRANSPARENT);
        mLineChart.getDescription().setEnabled(false);
        mLineChart.setTouchEnabled(false);
        mLineChart.setDrawGridBackground(false);
        mLineChart.setDragEnabled(false);
        mLineChart.setScaleEnabled(false);
        mLineChart.setPinchZoom(false);

        YAxis leftAxis = mLineChart.getAxisLeft();
        leftAxis.setDrawGridLines(false);
        leftAxis.setDrawZeroLine(true);
        leftAxis.setZeroLineColor(mListenerView.getCurrentActivity().getResources().getColor(R.color.color_white));
        leftAxis.setZeroLineWidth(line_width / density);
        leftAxis.setGranularityEnabled(false);
        leftAxis.setAxisLineColor(mListenerView.getCurrentActivity().getResources().getColor(R.color.color_white));
        leftAxis.setAxisLineWidth(line_width / density);
        leftAxis.setTextColor(mListenerView.getCurrentActivity().getResources().getColor(R.color.color_white));

        mLineChart.getAxisLeft().setSpaceTop(0);
        mLineChart.getAxisLeft().setSpaceBottom(0);
        mLineChart.getXAxis().setEnabled(false);
        mLineChart.getAxisRight().setEnabled(false);

        setData(valueList);

        mLineChart.animateX(1500);

        Legend legend = mLineChart.getLegend();
        legend.setEnabled(false);
    }

    private void setData(ArrayList<Float> data) {

        ArrayList<Entry> values = new ArrayList<>();
        for(int index = 0; index < data.size(); index++)
            values.add(new Entry(index, data.get(index)));

        if(data.size() == 1)
            values.add(new Entry(1, data.get(0)));

        LineDataSet set1;

        if (mLineChart.getData() != null && mLineChart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) mLineChart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            set1.notifyDataSetChanged();
            mLineChart.getData().notifyDataChanged();
            mLineChart.notifyDataSetChanged();
        }
        else {
            set1 = new LineDataSet(values, null);

            set1.setDrawIcons(false);
            set1.setDrawCircles(false);
            set1.setDrawValues(false);
            set1.setHighlightEnabled(false);

            set1.setColor(mListenerView.getCurrentActivity().getResources().getColor(R.color.color_blue6));
            set1.setLineWidth(mListenerView.getCurrentActivity().getResources().getDimensionPixelSize(R.dimen.dimen_2dp) / mListenerView.getCurrentActivity().getResources().getDisplayMetrics().density);

            set1.setDrawFilled(true);
            set1.setFillFormatter(new IFillFormatter() {
                @Override
                public float getFillLinePosition(ILineDataSet dataSet, LineDataProvider dataProvider) {
                    return mLineChart.getAxisLeft().getAxisMinimum();
                }
            });

            Drawable drawable = ContextCompat.getDrawable(mListenerView.getCurrentActivity(), R.color.color_blue6_opacity20);
            set1.setFillDrawable(drawable);

            ArrayList<ILineDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);

            LineData lineData = new LineData(dataSets);
            mLineChart.setData(lineData);
        }
    }
}

