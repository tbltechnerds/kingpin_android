package com.kingpin.ui.adapter.holder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kingpin.R;
import com.kingpin.mvp.model.AffiliateCriedit;
import com.kingpin.mvp.model.Withdraw;
import com.kingpin.mvp.view.BaseView;
import com.kingpin.mvp.view.PaymentHistoryView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AffiliateHistoryHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.txt_username) TextView mClientName;
    @BindView(R.id.txt_purchase_date) TextView mPurchaseDate;
    @BindView(R.id.txt_membership_type) TextView mMembershipType;
    @BindView(R.id.txt_commission) TextView mCommission;
    @BindView(R.id.txt_date_available_withdrawal) TextView mAvailableWithdrawDate;

    private BaseView mListenerView;
    private AffiliateCriedit mCriedit;

    public AffiliateHistoryHolder(@NonNull View itemView, BaseView listenerView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        mListenerView = listenerView;
    }

    public void bind(AffiliateCriedit criedit) {
        this.mCriedit = criedit;
        mClientName.setText(criedit.getClientName());
        mPurchaseDate.setText(criedit.getPurchasedMembershipDate());
        mMembershipType.setText(criedit.getClientMembershipType());
        mCommission.setText(String.format("$%.02f", criedit.getCommission()));
        mAvailableWithdrawDate.setText(criedit.getWithdrawAvailableDate());
    }
}
