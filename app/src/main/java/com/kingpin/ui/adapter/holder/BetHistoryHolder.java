package com.kingpin.ui.adapter.holder;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.kingpin.R;
import com.kingpin.common.base.Constant;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.manager.TempDataManager;
import com.kingpin.mvp.model.Bet;
import com.kingpin.mvp.model.Kingpin;
import com.kingpin.mvp.model.Profile;
import com.kingpin.mvp.view.BaseView;
import com.kingpin.mvp.view.UpcomingGameDetailView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BetHistoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.txt_game) TextView mGameView;
    @BindView(R.id.txt_pick) TextView mPickView;
    @BindView(R.id.txt_amount) TextView mAmountView;
    @BindView(R.id.txt_result) TextView mResultView;
    @BindView(R.id.txt_amount_won) TextView mAmountWonView;
    @BindView(R.id.btn_share) ImageView btnShare;

    private BaseView mListenerView;
    private Bet mBet;

    public BetHistoryHolder(@NonNull View itemView, BaseView listenerView) {
        super(itemView);
        mListenerView = listenerView;
        ButterKnife.bind(this, itemView);
        this.btnShare.setOnClickListener(this);
    }

    public void bind(Bet bet) {
        mBet = bet;

        if (bet.isParlayBet()) {
            mPickView.setText("Parlay");
            String game = bet.getParlayBetTeamString() + "\n" + bet.getBetDateOnLine();
            mGameView.setText(game);
        } else {
            if (bet.getPeriodCode().equals("")) {
                mPickView.setText(bet.getTeamBet() + " (" + bet.getLine() + ")");
            } else {
                mPickView.setText(bet.getTeamBet() + "(" + bet.getPeriodCode() + ")" + " (" + bet.getLine() + ")");
            }
            String game = bet.getTeams() + "\n" + bet.getBetDateOnLine();
            mGameView.setText(game);
        }

        mAmountView.setText(String.valueOf(bet.getBetAmount()));
        mResultView.setText(bet.getResult().toUpperCase());
        mAmountWonView.setText(String.valueOf(bet.getWinLossAmount()));

        if(bet.getResult().toLowerCase().equals("loss")) {
            mAmountWonView.setTextColor(Color.RED);
            btnShare.setVisibility(View.GONE);
        } else {
            mAmountWonView.setTextColor(Color.GREEN);
            btnShare.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_share:
                onShare();
                break;
        }
    }

    private void onShare() {
        Profile me = DataManager.getInstance().getAccount();
        Kingpin kingpin = TempDataManager.getInstance().getSelectedKingpin();
        if (me != null && kingpin != null) {
            String team = "";
            if (mBet.isParlayBet()) {
                team = mBet.getParlayBetTeamString();
            } else {
                team = mBet.getTeams() + " " + mBet.getTeamBet() + " (" + mBet.getLine() + ")";
            }

            String result = "";
            if (mBet.getResult().toLowerCase().equals("loss")) {
                result = "LOSER!";
            } else {
                result = "WINNER!";
            }

            String text = result + " " + kingpin.getName() + " cashed $" + mBet.getBetAmount() + " on the " + team + " " + Constant.APP_SHARE_LINK + " Get 10% off with promo code: " + me.getReferralAffiliateCode();
            mListenerView.onShareText(text);
        }
    }

}
