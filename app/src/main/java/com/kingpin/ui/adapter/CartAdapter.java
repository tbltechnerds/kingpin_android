package com.kingpin.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kingpin.R;
import com.kingpin.common.manager.DataManager;
import com.kingpin.mvp.model.PlaceBetCart;
import com.kingpin.mvp.view.BaseView;
import com.kingpin.ui.adapter.holder.CartHolder;

import java.util.ArrayList;

public class CartAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<PlaceBetCart> mCarts;

    private BaseView mListenerView;

    public CartAdapter(BaseView listenerView) {
        super();
        this.mListenerView = listenerView;
        this.mCarts = DataManager.getInstance().getCarts();
    }

    @NonNull
    @Override
    public CartHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_cart, parent, false);
        return new CartHolder(v, mListenerView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        CartHolder cartHolder = (CartHolder) holder;
        cartHolder.bind(this.mCarts.get(position));
    }

    @Override
    public int getItemCount() {
        return this.mCarts.size();
    }

    public void updateCarts() {
        this.mCarts = DataManager.getInstance().getCarts();
        notifyDataSetChanged();
    }
}
