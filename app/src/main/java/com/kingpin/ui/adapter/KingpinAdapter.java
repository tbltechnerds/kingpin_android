package com.kingpin.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kingpin.R;
import com.kingpin.common.manager.TempDataManager;
import com.kingpin.mvp.model.Kingpin;
import com.kingpin.mvp.view.BaseView;
import com.kingpin.ui.adapter.holder.KingpinHolder;
import com.kingpin.ui.fragment.MyKingpinFragment;
import com.kingpin.ui.fragment.TopKingpinFragment;

import java.util.ArrayList;

public class KingpinAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Kingpin> mKingpins;
    private BaseView mListenerView;
    private boolean mIsFollower;

    public KingpinAdapter(boolean isFollower, BaseView listenerView) {
        super();
        this.mListenerView = listenerView;
        this.mIsFollower = isFollower;
    }

    @NonNull
    @Override
    public KingpinHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_kingpin, parent, false);
        return new KingpinHolder(v, mListenerView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        KingpinHolder kingpinHolder = (KingpinHolder) holder;
        kingpinHolder.bind(this.mKingpins.get(position), this.mIsFollower);
    }

    @Override
    public int getItemCount() {
        return this.mKingpins.size();
    }

    public void updateKingpins() {
        if (mListenerView instanceof TopKingpinFragment) {
            this.mKingpins = TempDataManager.getInstance().getKingpins();
        } else if (mListenerView instanceof MyKingpinFragment) {
            this.mKingpins = TempDataManager.getInstance().getMyKingpins();
        }
        notifyDataSetChanged();
    }
}
