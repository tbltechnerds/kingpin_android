package com.kingpin.ui.adapter.holder;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.kingpin.R;
import com.kingpin.common.manager.DataManager;
import com.kingpin.mvp.model.BovadaGame;
import com.kingpin.mvp.model.PlaceBetCart;
import com.kingpin.mvp.view.BaseView;
import com.kingpin.mvp.view.CartView;
import com.kingpin.mvp.view.PlacePickView;
import com.kingpin.ui.fragment.CartFragment;

import java.nio.DoubleBuffer;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CartHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.text_title) TextView mTxtTitle;
    @BindView(R.id.text_team) TextView mTxtTeam;
    @BindView(R.id.text_value) TextView mTxtValue;
    @BindView(R.id.text_amount) TextView mTxtAmount;
    @BindView(R.id.text_result) TextView mTxtResult;
    @BindView(R.id.btn_close) ImageView mBtnClose;

    private BaseView mListenerView;
    private PlaceBetCart mCart;

    public CartHolder(@NonNull View itemView, BaseView listenerView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.mCart = null;
        this.mListenerView = listenerView;
        mBtnClose.setOnClickListener(this);

        mTxtAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = mTxtAmount.getText().toString();
                if (!text.equals("")) {
                    try {
                        int value = Integer.parseInt(text);
                        ((CartView)mListenerView).updateCartWithAmount(value, mCart);
                    } catch (Exception e) {

                    }
                }
            }
        });
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    public void bind(PlaceBetCart cart) {
        if (cart == null) {
            return;
        }

        mCart = cart;
        mTxtTitle.setText(mCart.teams);

        String periodCode = cart.getPeriodCode();
        if (periodCode.equals("")) {
            mTxtTeam.setText(mCart.team_bet);
        } else {
            mTxtTeam.setText(mCart.team_bet + "(" + periodCode + ")");
        }

        mTxtValue.setText(mCart.line);
        mTxtAmount.setText(String.format("%d", mCart.bet_amount));

        try {
            double rate = Double.parseDouble(mCart.odd);
            double winRate = rate - 1.0;
            double result = winRate * Double.valueOf(cart.bet_amount);
            mTxtResult.setText(String.format("$%.2f", result));
        } catch (Exception e) {
            mTxtResult.setText("$0.00");
        }

        if (((CartView)mListenerView).isParlay()) {
            mTxtAmount.setEnabled(false);
        } else {
            mTxtAmount.setEnabled(true);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_close:
                ((CartView)mListenerView).onRemoveCart(mCart);
                break;
        }
    }
}
