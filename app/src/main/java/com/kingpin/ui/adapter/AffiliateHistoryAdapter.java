package com.kingpin.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kingpin.R;
import com.kingpin.mvp.model.AffiliateCriedit;
import com.kingpin.mvp.model.Withdraw;
import com.kingpin.mvp.view.BaseView;
import com.kingpin.ui.adapter.holder.AffiliateHistoryHolder;
import com.kingpin.ui.adapter.holder.PaymentHistoryHolder;

import java.util.ArrayList;

public class AffiliateHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<AffiliateCriedit> mCriedits;
    private BaseView mListenerView;

    public AffiliateHistoryAdapter(BaseView listenerView) {
        super();
        mListenerView = listenerView;
        mCriedits = new ArrayList<>();
    }

    @NonNull
    @Override
    public AffiliateHistoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_affiliate_history, parent, false);
        return new AffiliateHistoryHolder(v, mListenerView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        AffiliateHistoryHolder paymentHistoryHolder = (AffiliateHistoryHolder) holder;
        paymentHistoryHolder.bind(this.mCriedits.get(position));
    }

    @Override
    public int getItemCount() {
        return this.mCriedits.size();
    }

    public void updateCriedits(ArrayList<AffiliateCriedit> criedits) {
        this.mCriedits = criedits;
        notifyDataSetChanged();
    }

    public ArrayList<AffiliateCriedit> getCriedits() {
        return this.mCriedits;
    }
}