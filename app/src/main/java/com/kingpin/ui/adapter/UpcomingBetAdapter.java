package com.kingpin.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kingpin.R;
import com.kingpin.common.utility.Comparator;
import com.kingpin.mvp.model.Bet;
import com.kingpin.mvp.view.BaseView;
import com.kingpin.ui.adapter.holder.UpcomingBetHolder;

import java.util.ArrayList;
import java.util.Collections;

public class UpcomingBetAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private boolean isFollowed;
    private boolean isMine;
    private ArrayList<Bet> upcomingBetList;
    private BaseView mListenerView;

    public UpcomingBetAdapter(BaseView listenerView) {
        super();
        this.isFollowed = false;
        this.isMine = false;
        upcomingBetList = new ArrayList<>();
        this.mListenerView = listenerView;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_upcoming, parent, false);
        return new UpcomingBetHolder(v, this.mListenerView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        UpcomingBetHolder upcomingBetHolder = (UpcomingBetHolder) holder;
        upcomingBetHolder.bind(upcomingBetList.get(position), this.isFollowed, this.isMine);
    }

    @Override
    public int getItemCount() {
        return upcomingBetList.size();
    }

    public void updateUpcomingBets(ArrayList<Bet> upcomingBetList, boolean isFollowed, boolean isMine) {
        this.upcomingBetList = upcomingBetList;
        this.isFollowed = isFollowed;
        this.isMine = isMine;
    }

    public ArrayList<Bet> getUpcomingBetList() {
        return this.upcomingBetList;
    }

    public void sort() {
        Collections.sort(upcomingBetList, new Comparator.DateComparator2());
        this.notifyDataSetChanged();
    }
}