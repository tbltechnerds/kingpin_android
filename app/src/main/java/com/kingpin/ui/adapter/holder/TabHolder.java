package com.kingpin.ui.adapter.holder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.kingpin.R;
import com.kingpin.common.base.Constant;
import com.kingpin.mvp.view.BaseView;
import com.kingpin.mvp.view.UpcomingGameView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TabHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.txt_name) TextView mNameView;

    private BaseView mListenerView;
    private Constant.FILTER_SPORT mTabType;

    public TabHolder(@NonNull View itemView, BaseView listenerView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.mListenerView = listenerView;
        mNameView.setOnClickListener(this);
    }

    public void bind(Constant.FILTER_SPORT type, boolean bChoose) {
        this.mTabType = type;
        String tabName = "";
        switch (type) {
            case FOOTBALLNFL:
                tabName = mListenerView.getCurrentActivity().getResources().getString(R.string.tab_upcome_football_nfl);
                break;
            case FOOTBALLNCAAF:
                tabName = mListenerView.getCurrentActivity().getResources().getString(R.string.tab_upcome_football_ncaaf);
                break;
            case BASKETBALLNBA:
                tabName = mListenerView.getCurrentActivity().getResources().getString(R.string.tab_upcome_basketball_nba);
                break;
            case BASKETBALLNCAAB:
                tabName = mListenerView.getCurrentActivity().getResources().getString(R.string.tab_upcome_basketball_ncaab);
                break;
            case BASEBALL:
                tabName = mListenerView.getCurrentActivity().getResources().getString(R.string.tab_upcome_baseball);
                break;
            case HOCKEY:
                tabName = mListenerView.getCurrentActivity().getResources().getString(R.string.tab_upcome_hockey);
                break;
            case SOCCER:
                tabName = mListenerView.getCurrentActivity().getResources().getString(R.string.tab_upcome_soccer);
                break;
            case TENNIS:
                tabName = mListenerView.getCurrentActivity().getResources().getString(R.string.tab_upcome_tennis);
                break;
            case BOXING:
                tabName = mListenerView.getCurrentActivity().getResources().getString(R.string.tab_upcome_boxing);
                break;
            case MMA:
                tabName = mListenerView.getCurrentActivity().getResources().getString(R.string.tab_upcome_mma);
                break;

        }
        this.mNameView.setText(tabName);
        this.mNameView.setSelected(bChoose);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_name:
                if(this.mNameView.isSelected())
                    return;

                if(mListenerView instanceof UpcomingGameView)
                    ((UpcomingGameView) mListenerView).onTab(this.mTabType);
                break;
        }
    }
}