package com.kingpin.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kingpin.R;
import com.kingpin.common.base.Constant;
import com.kingpin.common.utility.Comparator;
import com.kingpin.mvp.model.UpcomingGame;
import com.kingpin.mvp.model.UpcomingGameDetail;
import com.kingpin.mvp.view.BaseView;
import com.kingpin.ui.adapter.holder.UpcomeGameHolder;

import java.util.ArrayList;
import java.util.Collections;

public class UpcomeGameAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<String> mStaticSports;
    private ArrayList<UpcomingGameDetail> mMatchs;
    private ArrayList<UpcomingGame> mGames;
    private BaseView mListenerView;
    private Constant.FILTER_SPORT mChooseSport;

    public UpcomeGameAdapter(BaseView listenerView) {
        super();
        this.mListenerView = listenerView;
        this.mStaticSports = new ArrayList<>();
        this.mMatchs = new ArrayList<>();
        this.mGames = new ArrayList<>();
        mChooseSport = Constant.FILTER_SPORT.FOOTBALL;

        update();
    }

    @NonNull
    @Override
    public UpcomeGameHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_game, parent, false);
        return new UpcomeGameHolder(v, mListenerView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        UpcomeGameHolder upcomeGameholder = (UpcomeGameHolder) holder;
        upcomeGameholder.bind(this.mMatchs.get(position));
    }

    @Override
    public int getItemCount() {
        return this.mMatchs.size();
    }

    public void updateGames(ArrayList<UpcomingGame> games) {
        this.mGames = games;
        update();
    }

    public void chooseTab(Constant.FILTER_SPORT sport) {
        this.mChooseSport = sport;
        update();
    }

    private void update() {
        updateStaticSports();
        mMatchs.clear();
        for(UpcomingGame game : mGames) {
            if(mStaticSports.contains(game.getName().toLowerCase()))
                mMatchs.addAll(game.getGames());
        }
        Collections.sort(mMatchs, new Comparator.UpcomingGameDateComparator());
        this.notifyDataSetChanged();
    }

    private void updateStaticSports() {

        mStaticSports.clear();
        switch (mChooseSport) {
            case FOOTBALLNFL:
                mStaticSports.add("NFL".toLowerCase());
                mStaticSports.add("NFL 1st Half".toLowerCase());
                mStaticSports.add("NFL 1st Quarter".toLowerCase());
                break;
            case FOOTBALLNCAAF:
                mStaticSports.add("NCF".toLowerCase());
                mStaticSports.add("NCF 1st Half".toLowerCase());
                break;
            case BASKETBALLNBA:
                mStaticSports.add("NBA".toLowerCase());
                mStaticSports.add("NBA 1st Half".toLowerCase());
                break;
            case BASKETBALLNCAAB:
                mStaticSports.add("NCB".toLowerCase());
                mStaticSports.add("NCB 1st Half".toLowerCase());
                break;
            case BASEBALL:
                mStaticSports.add("MLB".toLowerCase());
                break;
            case HOCKEY:
                mStaticSports.add("nhl".toLowerCase());
                break;
            case SOCCER:
                mStaticSports.add("Soccer".toLowerCase());
                mStaticSports.add("English Premier League".toLowerCase());
                mStaticSports.add("English Championship".toLowerCase());
                mStaticSports.add("English FA Cup".toLowerCase());
                mStaticSports.add("Spanish La Liga".toLowerCase());
                mStaticSports.add("German Bundesliga 1".toLowerCase());
                mStaticSports.add("French Ligue 1".toLowerCase());
                mStaticSports.add("Italian Serie A".toLowerCase());
                mStaticSports.add("UEFA Europa League".toLowerCase());
                mStaticSports.add("UEFA Champions League".toLowerCase());
                mStaticSports.add("MLS".toLowerCase());
                mStaticSports.add("FIFA World Cup 2018".toLowerCase());
                break;
            case TENNIS:
                mStaticSports.add("Tennis".toLowerCase());
                break;
            case BOXING:
                mStaticSports.add("Boxing".toLowerCase());
                break;
            case MMA:
                mStaticSports.add("MMA".toLowerCase());
                break;
        }
    }
}
