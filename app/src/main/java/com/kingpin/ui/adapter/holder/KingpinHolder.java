package com.kingpin.ui.adapter.holder;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IFillFormatter;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.kingpin.R;
import com.kingpin.common.base.Constant;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.manager.FileManager;
import com.kingpin.common.task.DownloadAvatarTask;
import com.kingpin.common.utility.CommonUtils;
import com.kingpin.listener.DownloadAvatarListener;
import com.kingpin.mvp.model.BaseStatistics;
import com.kingpin.mvp.model.FilterOptions;
import com.kingpin.mvp.model.Kingpin;
import com.kingpin.mvp.model.Profile;
import com.kingpin.mvp.model.Statistics;
import com.kingpin.mvp.view.BaseView;
import com.kingpin.mvp.view.MyKingpinView;
import com.kingpin.mvp.view.TopKingpinView;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class KingpinHolder extends RecyclerView.ViewHolder implements View.OnClickListener, DownloadAvatarListener {

    @BindView(R.id.img_profile) RoundedImageView mProfileView;
    @BindView(R.id.txt_name) TextView mNameView;
    @BindView(R.id.txt_city) TextView mCityView;
    @BindView(R.id.txt_rank) TextView mRankView;
    @BindView(R.id.txt_winnings) TextView mWinningsView;
    @BindView(R.id.txt_days_betting_key) TextView mDaysBetKeyView;
    @BindView(R.id.txt_days_betting) TextView mDaysBetView;
    @BindView(R.id.txt_top_sports_key) TextView mTopSportsKeyView;
    @BindView(R.id.txt_top_sports) TextView mTopSportsView;
    @BindView(R.id.txt_bets) TextView mBetCountView;
    @BindView(R.id.txt_win_loss) TextView mWinLoseView;
    @BindView(R.id.txt_last_winnings) TextView mLastWinningsView;
    @BindView(R.id.txt_win_percent) TextView mWinPercentView;
    @BindView(R.id.txt_yesterday_winnings) TextView mYesterdayWinnings;
    @BindView(R.id.txt_followers) TextView mFollowers;
    @BindView(R.id.txt_follow) TextView mFollowView;
    @BindView(R.id.line_chart) LineChart lineChart;
    @BindView(R.id.btn_share)
    ImageView btnShare;

    private BaseView mListenerView;
    private Kingpin mKingpin;

    public KingpinHolder(@NonNull View itemView, BaseView listenerView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.mKingpin = null;
        this.mListenerView = listenerView;
        this.mProfileView.setOnClickListener(this);
        this.mFollowView.setOnClickListener(this);
        this.btnShare.setOnClickListener(this);
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    public void bind(Kingpin kingpin, boolean isFollower) {

        boolean isSameData = false;
        if(this.mKingpin != null) {
            if(this.mKingpin.getId() == kingpin.getId())
                isSameData = true;
        }

        this.mKingpin = kingpin;

        mNameView.setText(kingpin.getName());
        mCityView.setText(kingpin.getCity());
        mRankView.setText((kingpin.getRank() != 1000000) ? String.valueOf(kingpin.getRank()) : "NR");

        if(isFollower) {
            mDaysBetKeyView.setText("Days\nfollowing");
            mDaysBetView.setText(String.valueOf(kingpin.getDaysFollowing()));
            mTopSportsKeyView.setText("Total money paid");
            mTopSportsKeyView.setTextSize(mListenerView.getCurrentActivity().getResources().getDimension(R.dimen.dimen_10sp) / mListenerView.getCurrentActivity().getResources().getDisplayMetrics().density);
            mTopSportsView.setText("$" + kingpin.getTotalMoneyPaid());
        }
        else {
            mDaysBetKeyView.setText("Days\nbetting");
            mDaysBetView.setText(String.valueOf(kingpin.getDaysBetting()));
            mTopSportsKeyView.setText("Top sports");
            mTopSportsKeyView.setTextSize(mListenerView.getCurrentActivity().getResources().getDimension(R.dimen.dimen_12sp) / mListenerView.getCurrentActivity().getResources().getDisplayMetrics().density);
            mTopSportsView.setText(FilterOptions.getMainFilterOptionString(kingpin.getTopSport()));
        }

        mBetCountView.setText(String.valueOf(kingpin.getCountBets()));
        mWinningsView.setText("$" + kingpin.getWinningLosses());
        mWinningsView.setTextColor((kingpin.getWinningLosses() >= 0) ? Color.GREEN : Color.WHITE);
        mLastWinningsView.setText("$" + kingpin.getLast30daysWinnings());
        mLastWinningsView.setTextColor((kingpin.getLast30daysWinnings() >= 0 ? Color.GREEN : Color.WHITE));
        mWinLoseView.setText(String.format("%d-%d-%d", kingpin.getCountWin(), kingpin.getCountLoss(), kingpin.getCountDraw()));
        mWinPercentView.setText(kingpin.getPercentWin() + "%");
        mYesterdayWinnings.setText("$" + kingpin.getLast1daysWinnings());
        mYesterdayWinnings.setTextColor((kingpin.getLast1daysWinnings() >= 0) ? Color.GREEN : Color.WHITE);
        mFollowers.setText(String.format("%d", kingpin.getCountFollowers()));

        Profile me = DataManager.getInstance().getAccount();
        if(me == null) {
            mFollowView.setVisibility(View.VISIBLE);
            mFollowView.setText("View\nPicks");
        }
        else {
            if(me.getId() == kingpin.getId()) {
                mFollowView.setVisibility(View.GONE);
            }
            else {
                if(me.getMembership() == 0) {
                    mFollowView.setVisibility(View.VISIBLE);
                    mFollowView.setText("View\nPicks");
                }
                else {
                    if(kingpin.isUpcoming()) {
                        mFollowView.setVisibility(View.VISIBLE);
                        mFollowView.setText("Upcoming\nBets");
                    }
                    else {
                        mFollowView.setVisibility(View.GONE);
                    }
                }
            }
        }

        if(!isSameData) {
            mProfileView.setImageResource(R.mipmap.img_detail_avatar);
            Bitmap bitmap = downloadImage(kingpin.getAvatar());
            if(bitmap != null || mProfileView != null)
                mProfileView.setImageBitmap(bitmap);

            loadChart(kingpin.getValuesForUpperGraph());
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_profile:
                if(mListenerView instanceof TopKingpinView) {
                    ((TopKingpinView) mListenerView).onViewUserProfile(this.mKingpin);
                } else if (mListenerView instanceof MyKingpinView) {
                    ((MyKingpinView) mListenerView).onViewUserProfile(this.mKingpin);
                }
                break;
            case R.id.txt_follow:
                if(mListenerView instanceof TopKingpinView) {
                    ((TopKingpinView) mListenerView).onUpcomingBets(this.mKingpin);
                } else if (mListenerView instanceof MyKingpinView) {
                    ((MyKingpinView) mListenerView).onUpcomingBets(this.mKingpin);
                }
                break;
            case R.id.btn_share:
                onShare();
                break;
        }
    }

    @Override
    public void DownloadAvatarCompleted(Bitmap bitmap) {
        if(bitmap != null && mProfileView != null)
            mProfileView.setImageBitmap(bitmap);
    }

    private Bitmap downloadImage(String image_url) {
        if(image_url == null || image_url.isEmpty())
            return null;

        if(mListenerView == null)
            return null;

        String url = Constant.SERVER_HOST + "/" + image_url;
        String[] url_peaces = url.split("/");
        String file_name = url_peaces[url_peaces.length - 1];
        String file_path = CommonUtils.getFilePath(mListenerView.getCurrentActivity());

        Bitmap bitmap = FileManager.getBitmap(file_path + "/" + file_name);
        if(bitmap == null)
            new DownloadAvatarTask(CommonUtils.getFilePath(mListenerView.getCurrentActivity()), this).execute(url);
        return bitmap;
    }

    private void loadChart(ArrayList<Float> values) {

        lineChart.setBackgroundColor(Color.TRANSPARENT);
        lineChart.getDescription().setEnabled(false);
        lineChart.setTouchEnabled(false);
        lineChart.setDrawGridBackground(false);
        lineChart.setDragEnabled(false);
        lineChart.setScaleEnabled(false);
        lineChart.setPinchZoom(false);

        lineChart.getAxisLeft().setEnabled(false);
        lineChart.getAxisLeft().setSpaceTop(0);
        lineChart.getAxisLeft().setSpaceBottom(0);
        lineChart.getAxisRight().setEnabled(false);
        lineChart.getXAxis().setEnabled(false);
        lineChart.setViewPortOffsets(mListenerView.getCurrentActivity().getResources().getDimensionPixelSize(R.dimen.dimen_2dp) / mListenerView.getCurrentActivity().getResources().getDisplayMetrics().density,
                0, 0, mListenerView.getCurrentActivity().getResources().getDimensionPixelSize(R.dimen.dimen_2dp) / mListenerView.getCurrentActivity().getResources().getDisplayMetrics().density);

        setData(values);

        lineChart.animateX(1500);

        Legend legend = lineChart.getLegend();
        legend.setEnabled(false);
    }

    private void setData(ArrayList<Float> data) {

        ArrayList<Entry> values = new ArrayList<>();
        for(int index = 0; index < data.size(); index++)
            values.add(new Entry(index, data.get(index)));

        if(data.size() == 1)
            values.add(new Entry(1, data.get(0)));

        LineDataSet set1;

        if (lineChart.getData() != null && lineChart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) lineChart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            set1.notifyDataSetChanged();
            lineChart.getData().notifyDataChanged();
            lineChart.notifyDataSetChanged();
        }
        else {
            set1 = new LineDataSet(values, null);

            set1.setDrawIcons(false);
            set1.setDrawCircles(false);
            set1.setDrawValues(false);
            set1.setHighlightEnabled(false);

            set1.setColor(mListenerView.getCurrentActivity().getResources().getColor(R.color.color_blue6));
            set1.setLineWidth(mListenerView.getCurrentActivity().getResources().getDimensionPixelSize(R.dimen.dimen_2dp) / mListenerView.getCurrentActivity().getResources().getDisplayMetrics().density);

            set1.setDrawFilled(true);
            set1.setFillFormatter(new IFillFormatter() {
                @Override
                public float getFillLinePosition(ILineDataSet dataSet, LineDataProvider dataProvider) {
                    return lineChart.getAxisLeft().getAxisMinimum();
                }
            });

            Drawable drawable = ContextCompat.getDrawable(mListenerView.getCurrentActivity(), R.color.color_blue6_opacity20);
            set1.setFillDrawable(drawable);

            ArrayList<ILineDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);

            LineData lineData = new LineData(dataSets);
            lineChart.setData(lineData);
        }
    }

    private void onShare() {
        Profile me = DataManager.getInstance().getAccount();
        if (me != null) {
            String text =  mKingpin.getName() + " has won $" + mKingpin.getWinningLosses() + " at sports betting "  + Constant.APP_SHARE_LINK + " Get 10% off with promo code: " + me.getReferralAffiliateCode();
            mListenerView.onShareText(text);
        }
    }
}
