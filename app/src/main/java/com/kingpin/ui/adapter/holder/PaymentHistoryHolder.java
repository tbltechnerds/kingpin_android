package com.kingpin.ui.adapter.holder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kingpin.R;
import com.kingpin.mvp.model.Withdraw;
import com.kingpin.mvp.view.BaseView;
import com.kingpin.mvp.view.PaymentHistoryView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentHistoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.txt_date) TextView mDateView;
    @BindView(R.id.txt_amount) TextView mAmountView;
    @BindView(R.id.txt_total_pot) TextView mTotalPotView;
    @BindView(R.id.txt_month) TextView mMonthView;
    @BindView(R.id.txt_rank) TextView mRankView;
    @BindView(R.id.txt_description) TextView mDescriptionView;
    @BindView(R.id.lyt_history) LinearLayout mLayoutHistory;
    @BindView(R.id.lyt_description) LinearLayout mLayoutDescription;

    private BaseView mListenerView;
    private Withdraw mWithdraw;

    public PaymentHistoryHolder(@NonNull View itemView, BaseView listenerView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        mListenerView = listenerView;
        mLayoutHistory.setOnClickListener(this);
        mLayoutDescription.setOnClickListener(this);
    }

    public void bind(Withdraw withdraw) {
        this.mWithdraw = withdraw;
        mDateView.setText((withdraw.getCreatedDate() == null) ? "" : withdraw.getCreatedDate());
        mAmountView.setText(String.valueOf(withdraw.getAmount()));
        mTotalPotView.setText(String.valueOf(withdraw.getTotalPot()));
        mMonthView.setText((withdraw.getMonth() == null) ? "" : withdraw.getMonth());
        mRankView.setText((withdraw.getRanking() == 0) ? "" : String.valueOf(withdraw.getRanking()));
        mDescriptionView.setText((withdraw.getDescription() == null) ? "" : withdraw.getDescription());
        updateViews();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lyt_history:
            case R.id.lyt_description:
                if(mListenerView instanceof PaymentHistoryView)
                    ((PaymentHistoryView) mListenerView).OnChooseHistory(mWithdraw);
                break;
        }
    }

    private void updateViews() {
        mLayoutDescription.setVisibility(mWithdraw.isExpanded() ? View.VISIBLE : View.GONE);
    }
}
