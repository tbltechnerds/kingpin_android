package com.kingpin.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kingpin.R;
import com.kingpin.common.base.Constant;
import com.kingpin.mvp.view.BaseView;
import com.kingpin.ui.adapter.holder.TabHolder;

import java.util.ArrayList;

public class TabAdapter extends RecyclerView.Adapter<TabHolder> {

    private ArrayList<Constant.FILTER_SPORT> mTabList;
    private BaseView mListenerView;
    private int mChooseIndex;

    public TabAdapter(ArrayList<Constant.FILTER_SPORT> tabList, BaseView listenerView) {
        super();
        this.mListenerView = listenerView;
        mTabList = tabList;
        mChooseIndex = 0;
    }

    @NonNull
    @Override
    public TabHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_tab, parent, false);
        return new TabHolder(v, mListenerView);
    }

    @Override
    public void onBindViewHolder(@NonNull TabHolder holder, int position) {
        holder.bind(this.mTabList.get(position), (position == mChooseIndex));
    }

    @Override
    public int getItemCount() {
        return this.mTabList.size();
    }

    public void updateTab(Constant.FILTER_SPORT tabType) {
        this.mChooseIndex = this.mTabList.indexOf(tabType);
        notifyDataSetChanged();
    }
}