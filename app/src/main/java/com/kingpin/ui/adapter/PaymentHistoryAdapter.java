package com.kingpin.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kingpin.R;
import com.kingpin.mvp.model.Withdraw;
import com.kingpin.mvp.view.BaseView;
import com.kingpin.ui.adapter.holder.PaymentHistoryHolder;

import java.util.ArrayList;

public class PaymentHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Withdraw> mWithdraws;
    private BaseView mListenerView;

    public PaymentHistoryAdapter(BaseView listenerView) {
        super();
        mListenerView = listenerView;
        mWithdraws = new ArrayList<>();
    }

    @NonNull
    @Override
    public PaymentHistoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_payment_history, parent, false);
        return new PaymentHistoryHolder(v, mListenerView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        PaymentHistoryHolder paymentHistoryHolder = (PaymentHistoryHolder) holder;
        paymentHistoryHolder.bind(this.mWithdraws.get(position));
    }

    @Override
    public int getItemCount() {
        return this.mWithdraws.size();
    }

    public void updateWithdraws(ArrayList<Withdraw> withdraws) {
        this.mWithdraws = withdraws;
        notifyDataSetChanged();
    }

    public ArrayList<Withdraw> getWithdraws() {
        return this.mWithdraws;
    }
}