package com.kingpin.ui.adapter.holder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.kingpin.R;
import com.kingpin.mvp.model.Statistics;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StatisticHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.txt_label) TextView mLabelView;
    @BindView(R.id.txt_value) TextView mValueView;

    public StatisticHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind(Statistics statistics) {
        mLabelView.setText(statistics.getLabel());
        mValueView.setText(statistics.getValue());
    }
}
