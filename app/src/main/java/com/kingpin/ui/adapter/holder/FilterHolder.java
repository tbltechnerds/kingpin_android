package com.kingpin.ui.adapter.holder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.kingpin.R;
import com.kingpin.mvp.view.BaseView;
import com.kingpin.mvp.view.MyKingpinView;
import com.kingpin.mvp.view.TopKingpinView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FilterHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.txt_name) TextView mNameView;

    private BaseView mListenerView;
    private int mFilterType;

    public FilterHolder(@NonNull View itemView, BaseView listenerView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.mListenerView = listenerView;
        mNameView.setOnClickListener(this);
    }

    public void bind(int type, boolean bChoose) {
        this.mFilterType = type;
        String tabName = "";
        switch (type) {
            case 0:
                tabName = mListenerView.getCurrentActivity().getResources().getString(R.string.filter_ranking);
                break;
            case 1:
                tabName = mListenerView.getCurrentActivity().getResources().getString(R.string.filter_winning);
                break;
            case 2:
                tabName = mListenerView.getCurrentActivity().getResources().getString(R.string.filter_bets);
                break;
            case 3:
                tabName = mListenerView.getCurrentActivity().getResources().getString(R.string.filter_last_winning);
                break;
            case 4:
                tabName = mListenerView.getCurrentActivity().getResources().getString(R.string.filter_total_win);
                break;
            case 5:
                tabName = mListenerView.getCurrentActivity().getResources().getString(R.string.filter_win);
                break;
        }
        this.mNameView.setText(tabName);
        this.mNameView.setSelected(bChoose);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_name:
                if(mListenerView instanceof TopKingpinView) {
                    ((TopKingpinView) mListenerView).onSort(this.mFilterType);
                } else if (mListenerView instanceof MyKingpinView) {
                    ((MyKingpinView) mListenerView).onSort(this.mFilterType);
                }
                break;
        }
    }
}
