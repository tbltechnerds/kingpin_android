package com.kingpin.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kingpin.R;
import com.kingpin.common.utility.Comparator;
import com.kingpin.mvp.model.Bet;
import com.kingpin.mvp.view.BaseView;
import com.kingpin.ui.adapter.holder.BetHistoryHolder;
import com.kingpin.ui.fragment.UserProfileFragment;

import java.util.ArrayList;
import java.util.Collections;

public class BetHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Bet> betHistoryList;
    private BaseView mListenerView;

    public BetHistoryAdapter(ArrayList<Bet> bets, BaseView listenerView) {
        super();
        mListenerView = listenerView;
        betHistoryList = bets;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_betting_history, parent, false);
        return new BetHistoryHolder(v, mListenerView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        BetHistoryHolder betHistoryHolder = (BetHistoryHolder) holder;
        betHistoryHolder.bind(betHistoryList.get(position));
    }

    @Override
    public int getItemCount() {
        return betHistoryList.size();
    }

    public void updateBetHistories(ArrayList<Bet> betHistoryList) {
        this.betHistoryList = betHistoryList;
    }

    public ArrayList<Bet> getBetHistoryList() {
        return this.betHistoryList;
    }

    public void sort(int sortIndex, boolean revert) {
        switch (sortIndex) {
            case UserProfileFragment.SORT_DATE:
                if(revert)
                    Collections.sort(betHistoryList, new Comparator.DateComparator2());
                else
                    Collections.sort(betHistoryList, new Comparator.DateComparator1());
                break;
            case UserProfileFragment.SORT_AMOUNT_BET:
                if(revert)
                    Collections.sort(betHistoryList, new Comparator.BetAmountComparator2());
                else
                    Collections.sort(betHistoryList, new Comparator.BetAmountComparator1());
                break;
            case UserProfileFragment.SORT_AMOUNT_WON:
                if(revert)
                    Collections.sort(betHistoryList, new Comparator.WonAmountComparator2());
                else
                    Collections.sort(betHistoryList, new Comparator.WonAmountComparator1());
                break;
        }
        this.notifyDataSetChanged();
    }
}
