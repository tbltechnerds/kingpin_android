package com.kingpin.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kingpin.R;
import com.kingpin.mvp.view.BaseView;
import com.kingpin.ui.adapter.holder.FilterHolder;

import java.util.ArrayList;

public class FilterAdapter extends RecyclerView.Adapter<FilterHolder> {

    private ArrayList<Integer> mFilterList;
    private BaseView mListenerView;
    private int mChooseIndex;

    public FilterAdapter(ArrayList<Integer> filterList, BaseView listenerView) {
        super();
        this.mListenerView = listenerView;
        mFilterList = filterList;
        mChooseIndex = 0;
    }

    @NonNull
    @Override
    public FilterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_filter, parent, false);
        return new FilterHolder(v, mListenerView);
    }

    @Override
    public void onBindViewHolder(@NonNull FilterHolder holder, int position) {
        holder.bind(this.mFilterList.get(position), (position == mChooseIndex));
    }

    @Override
    public int getItemCount() {
        return this.mFilterList.size();
    }

    public void updateFilter(int filterType) {
        this.mChooseIndex = this.mFilterList.indexOf(filterType);
        notifyDataSetChanged();
    }

    public int getCurrentIndex() {
        return mChooseIndex;
    }
}