package com.kingpin.ui.adapter.holder;

import android.annotation.SuppressLint;
import android.provider.SyncStateContract;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kingpin.R;
import com.kingpin.common.base.Constant;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.utility.DateUtils;
import com.kingpin.mvp.model.Profile;
import com.kingpin.mvp.model.UpcomingGameDetail;
import com.kingpin.mvp.view.BaseView;
import com.kingpin.mvp.view.UpcomingGameView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UpcomeGameHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.txt_date) TextView mDateView;
    @BindView(R.id.txt_title) TextView mTitleView;
    @BindView(R.id.txt_pick) TextView mPickView;
    @BindView(R.id.lyt_total) LinearLayout mTotalLayout;

    private BaseView mListenerView;
    private UpcomingGameDetail mMatch;

    public UpcomeGameHolder(@NonNull View itemView, BaseView listenerView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.mListenerView = listenerView;
        this.mTotalLayout.setOnClickListener(this);
    }

    @SuppressLint("SetTextI18n")
    public void bind(UpcomingGameDetail gameDetail) {
        this.mMatch = gameDetail;

        String date = DateUtils.convertToLocalDate(gameDetail.getMatchDate());
        String time = DateUtils.convertToLocalTime(gameDetail.getMatchDate());
        mDateView.setText(date + "\n" + time);
        mTitleView.setText(gameDetail.getName());
        if(gameDetail.getBets().size() == 1)
            mPickView.setText("1 Pick");
        else
            mPickView.setText(gameDetail.getBets().size() + " Picks");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lyt_total:
                if(mListenerView instanceof UpcomingGameView)
                    ((UpcomingGameView) mListenerView).onChooseGame(this.mMatch);
                break;
            case R.id.btn_share:
                shareGame();
                break;
        }
    }

    private void shareGame() {
        Profile me = DataManager.getInstance().getAccount();
        if (me != null) {
            String text = "Expert " + this.mMatch.getName() + " Picks " + Constant.APP_SHARE_LINK + " Get 10% off with promo code: " + me.getReferralAffiliateCode();
            mListenerView.onShareText(text);
        }
    }
}
