package com.kingpin.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kingpin.R;
import com.kingpin.mvp.model.BaseStatistics;
import com.kingpin.mvp.model.GraphStatistics;
import com.kingpin.mvp.model.Statistics;
import com.kingpin.mvp.view.BaseView;
import com.kingpin.ui.adapter.holder.GraphHolder;
import com.kingpin.ui.adapter.holder.StatisticHolder;

import java.util.ArrayList;

public class StatisticAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<BaseStatistics> statisticList;
    private BaseView mListenerView;

    public StatisticAdapter(BaseView listenerView) {
        super();
        statisticList = new ArrayList<>();
        this.mListenerView = listenerView;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case BaseStatistics.TYPE_NORMAL: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_statistics, parent, false);
                return new StatisticHolder(v);
            }
            case BaseStatistics.TYPE_GRAPH: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_graph, parent, false);
                return new GraphHolder(v, mListenerView);
            }
            default: {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_statistics, parent, false);
                return new StatisticHolder(v);
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case BaseStatistics.TYPE_NORMAL:
                StatisticHolder statisticHolder = (StatisticHolder) holder;
                statisticHolder.bind((Statistics)statisticList.get(position));
                break;
            case BaseStatistics.TYPE_GRAPH:
                GraphHolder graphHolder = (GraphHolder) holder;
                graphHolder.bind((GraphStatistics) statisticList.get(position));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return statisticList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return this.statisticList.get(position).getType();
    }

    public void updateStatistics(ArrayList<BaseStatistics> statisticList) {
        this.statisticList = statisticList;
    }
}