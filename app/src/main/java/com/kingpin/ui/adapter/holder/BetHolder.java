package com.kingpin.ui.adapter.holder;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kingpin.R;
import com.kingpin.common.base.Constant;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.manager.FileManager;
import com.kingpin.common.task.DownloadAvatarTask;
import com.kingpin.common.utility.CommonUtils;
import com.kingpin.listener.DownloadAvatarListener;
import com.kingpin.mvp.model.Profile;
import com.kingpin.mvp.model.UpcomingGamePicker;
import com.kingpin.mvp.view.BaseView;
import com.kingpin.mvp.view.UpcomingGameDetailView;
import com.makeramen.roundedimageview.RoundedImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BetHolder extends RecyclerView.ViewHolder implements View.OnClickListener, DownloadAvatarListener {

    @BindView(R.id.img_profile) RoundedImageView mProfileView;
    @BindView(R.id.txt_username) TextView mNameView;
    @BindView(R.id.txt_rank) TextView mRankView;
    @BindView(R.id.txt_winnings) TextView mWiningsView;
    @BindView(R.id.txt_pick) TextView mPickView;
    @BindView(R.id.txt_amount) TextView mAmountView;
    @BindView(R.id.lyt_value) LinearLayout mValueLayout;
    @BindView(R.id.btn_share) ImageView btnShare;

    private BaseView mListenerView;
    private UpcomingGamePicker mBet;

    public BetHolder(@NonNull View itemView, BaseView listenerView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.mListenerView = listenerView;
        this.mProfileView.setOnClickListener(this);
        this.mValueLayout.setOnClickListener(this);
        this.btnShare.setOnClickListener(this);
    }

    @SuppressLint({"DefaultLocale", "SetTextI18n"})
    public void bind(UpcomingGamePicker bet) {
        this.mBet = bet;

        Bitmap bitmap = downloadImage(bet.getAvatar());
        if(bitmap != null || mProfileView != null)
            mProfileView.setImageBitmap(bitmap);

        mNameView.setText(bet.getName());
        mRankView.setText((bet.getKinpinRankString().isEmpty()) ? "" : "#" + bet.getKinpinRankString());
        mWiningsView.setText(String.format("$%.0f", bet.getWinloss()));

        if(DataManager.getInstance().isSubscribed()) {
            mPickView.setText(String.format("%s (%s)", bet.getTeamBet(), bet.getLine()));
            mAmountView.setText(String.format("$%.0f", bet.getBetAmount()));
        }
        else {
            mPickView.setText("Subscribe to see");
            mAmountView.setText("");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lyt_value:
                if(mListenerView instanceof UpcomingGameDetailView)
                    ((UpcomingGameDetailView) mListenerView).onSubscribe();
                break;
            case R.id.img_profile:
                if(mListenerView instanceof UpcomingGameDetailView)
                    ((UpcomingGameDetailView) mListenerView).onKingpinProfile(mBet.getUserId());
                break;
            case R.id.btn_share:
                OnShare();
                break;
        }
    }

    @Override
    public void DownloadAvatarCompleted(Bitmap bitmap) {
        if(bitmap != null && mProfileView != null)
            mProfileView.setImageBitmap(bitmap);
    }

    private Bitmap downloadImage(String image_url) {
        if(image_url == null || image_url.isEmpty())
            return null;

        if(mListenerView == null)
            return null;

        String url = Constant.SERVER_HOST + "/" + image_url;
        String[] url_peaces = url.split("/");
        String file_name = url_peaces[url_peaces.length - 1];
        String file_path = CommonUtils.getFilePath(mListenerView.getCurrentActivity());

        Bitmap bitmap = FileManager.getBitmap(file_path + "/" + file_name);
        if(bitmap == null)
            new DownloadAvatarTask(CommonUtils.getFilePath(mListenerView.getCurrentActivity()), this).execute(url);
        return bitmap;
    }

    private void OnShare() {
        Profile me = DataManager.getInstance().getAccount();
        if (me != null) {
            String text = "Top Expert, " + mBet.getName() + "’s " + mBet.getTeamBet() + " Pick " + Constant.APP_SHARE_LINK + " Get 10% off with promo code: " + me.getReferralAffiliateCode();
            mListenerView.onShareText(text);
        }
    }
}
