package com.kingpin.ui.activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;

import com.kingpin.R;

import butterknife.BindView;

public class RuleActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.btn_back) ImageView btnBack;
    @BindView(R.id.view_web) WebView webView;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_rule;
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        btnBack.setOnClickListener(this);
        webView.loadUrl("file:///android_asset/rules.html");
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                finish();
                break;
        }
    }
}

