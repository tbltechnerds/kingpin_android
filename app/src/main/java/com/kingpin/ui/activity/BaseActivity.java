package com.kingpin.ui.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.kingpin.R;
import com.kingpin.common.base.Constant;
import com.kingpin.common.manager.DataManager;
import com.kingpin.listener.UIListener;
import com.kingpin.mvp.model.Profile;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseActivity extends AppCompatActivity implements UIListener.BaseActivityListener {

    private Unbinder unbinder;
    private int activeFragment;
    private int modalFragment;
    private Toast toast;
    private KProgressHUD loadingDialog;
    private boolean isPaused = false;

    @LayoutRes
    protected abstract int getLayoutRes();

    @CallSuper
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutRes());
        unbinder = ButterKnife.bind(this);
        toast = null;
        loadingDialog = null;
        activeFragment = UIListener.BaseActivityListener.FRAGMENT_NULL;
        modalFragment = UIListener.BaseActivityListener.FRAGMENT_NULL;
    }

    @CallSuper
    @Override
    protected void onResume() {
        super.onResume();
        isPaused = false;
    }

    @CallSuper
    @Override
    protected void onPause() {
        super.onPause();
        isPaused = true;
    }

    @CallSuper
    @Override
    protected void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }

    @Override
    public void updateFragment(int fragment) {
        this.activeFragment = fragment;
    }

    @Override
    public void addFragment(int fragment) {
        this.modalFragment = fragment;
    }

    @Override
    public void removeFragment(int fragment) {
        this.modalFragment = UIListener.BaseActivityListener.FRAGMENT_NULL;
    }

    @Override
    public void showLoading() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideLoading();

             //   loadingDialog = ProgressDialog.show(BaseActivity.this, null, "please wait", true);
             //   loadingDialog.setCancelable(false);
                loadingDialog = KProgressHUD.create(BaseActivity.this)
                                            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                                            .setLabel(getResources().getString(R.string.text_wait))
                                            .setCancellable(false)
                                            .setAnimationSpeed(2)
                                            .setDimAmount(0.5f)
                                            .show();

            }
        });
    }

    @Override
    public void hideLoading() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(loadingDialog != null && loadingDialog.isShowing())
                    loadingDialog.dismiss();
            }
        });
    }

    @Override
    public void showToast(@StringRes final int messageId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(toast != null)
                    toast.cancel();

                if(!isPaused) {
                    toast = Toast.makeText(BaseActivity.this, messageId, Toast.LENGTH_LONG);
                    toast.show();
                }
            }
        });
    }

    @Override
    public void showToast(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(toast != null)
                    toast.cancel();

                if(!isPaused) {
                    toast = Toast.makeText(BaseActivity.this, message, Toast.LENGTH_LONG);
                    toast.show();
                }
            }
        });
    }

    @Override
    public void hideToast() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(toast != null)
                    toast.cancel();
            }
        });
    }

    @Override
    public void startIntent(final Class<?> activity, final int fragment, final boolean bFinish) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(BaseActivity.this, activity);
                intent.putExtra(Constant.INTENT_EXTRA_FRAGMENT, fragment);
                startActivity(intent);
                if(bFinish)
                    finish();
            }
        });
    }

    @Override
    public void startIntent(final Intent intent, final int fragment, final boolean bFinish) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                intent.putExtra(Constant.INTENT_EXTRA_FRAGMENT, fragment);
                startActivity(intent);
                if(bFinish)
                    finish();
            }
        });
    }

    @Override
    public BaseActivity getActivity() {
        return this;
    }

    protected int getActiveFragment() {
        return this.activeFragment;
    }

    protected int getModalFragment() {
        return this.modalFragment;
    }

    public void upgradeProfileWithInAppPurchase() {
        Profile me = DataManager.getInstance().getAccount();
        if(me == null || me.getMembership() != 0)
            return;

        if (getModalFragment() == UIListener.HomeActivityListener.FRAGMENT_INAPP_PURCHASE) {
            return;
        }

        if(this instanceof HomeActivity)
            addFragment(UIListener.HomeActivityListener.FRAGMENT_INAPP_PURCHASE);
        else if(this instanceof ProfileActivity)
            addFragment(UIListener.ProfileActivityListener.FRAGMENT_INAPP_PURCHASE);
    }

    public void openLandingScreen() {
        startIntent(StartActivity.class, UIListener.StartActivityListener.FRAGMENT_LAND, true);
    }

    public void rateApp() {
        Uri uri = Uri.parse("market://details?id=" + getActivity().getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + getActivity().getPackageName())));
        }
    }
}
