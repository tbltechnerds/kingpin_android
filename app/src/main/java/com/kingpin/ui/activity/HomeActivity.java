package com.kingpin.ui.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kingpin.R;
import com.kingpin.common.base.Constant;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.manager.InAppManager;
import com.kingpin.listener.UIListener;
import com.kingpin.mvp.model.Profile;
import com.kingpin.ui.fragment.AskRateFragment;
import com.kingpin.ui.fragment.BetHelpFragment;
import com.kingpin.ui.fragment.FilterFragment;
import com.kingpin.ui.fragment.InAppPurchaseFragment;
import com.kingpin.ui.fragment.MyKingpinFragment;
import com.kingpin.ui.fragment.MyStaticsFragment;
import com.kingpin.ui.fragment.PlacePickFilterFragment;
import com.kingpin.ui.fragment.PlacePickFragment;
import com.kingpin.ui.fragment.SettingFragment;
import com.kingpin.ui.fragment.SubmitFeedbackFragment;
import com.kingpin.ui.fragment.TopKingpinFragment;
import com.kingpin.ui.fragment.UpcomingGameDetailFragment;
import com.kingpin.ui.fragment.UpcomingGameFragment;
import com.kingpin.ui.fragment.UserProfileFragment;

import butterknife.BindView;

public class HomeActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.tab_upcoming_game) LinearLayout tabUpcomingGame;
    @BindView(R.id.tab_top_kingpiin) LinearLayout tabTopKingpin;
    @BindView(R.id.tab_my_statics) LinearLayout tabMyStatics;
    @BindView(R.id.tab_place_pick) LinearLayout tabPlacePick;
    @BindView(R.id.tab_free_trial) LinearLayout tabFreeTrial;
    @BindView(R.id.tab_my_kingpin) LinearLayout tabMyKingpin;
    @BindView(R.id.lyt_bottom_menu) LinearLayout bottomMenu;
    @BindView(R.id.text_freetrial) TextView tabTextFreetrial;

    private UpcomingGameFragment mUpcomingGameFragment;
    private TopKingpinFragment mTopKingpinFragment;
    private MyStaticsFragment mMyStaticsFragment;
    private PlacePickFragment mPlacePickFragment;
    private MyKingpinFragment mMyKingpinFragment;
    private SettingFragment mSettingFragment;
    private UpcomingGameDetailFragment mUpcomingGameDetailFragment;
    private InAppPurchaseFragment mInAppPurchaseFragment;
    private FilterFragment mFilterFragment;
    private UserProfileFragment mUserProfileFragment;
    private PlacePickFilterFragment mPlacePickFilterFragment;
    private BetHelpFragment mBetHelpFragment;
    private AskRateFragment mAskRateFragment;
    private SubmitFeedbackFragment mSubmitFeedbackFragment;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_home;
    }

    @Override
    public void onAttachFragment(Fragment fragment) {

        if(fragment instanceof UpcomingGameFragment) {
            mUpcomingGameFragment = (UpcomingGameFragment) fragment;
            mUpcomingGameFragment.setUIListener(this);
        }
        else if(fragment instanceof TopKingpinFragment) {
            mTopKingpinFragment = (TopKingpinFragment) fragment;
            mTopKingpinFragment.setUIListener(this);
        }
        else if(fragment instanceof MyStaticsFragment) {
            mMyStaticsFragment = (MyStaticsFragment) fragment;
            mMyStaticsFragment.setUIListener(this);
        }
        else if(fragment instanceof PlacePickFragment) {
            mPlacePickFragment = (PlacePickFragment) fragment;
            mPlacePickFragment.setUIListener(this);
        }
        else if(fragment instanceof MyKingpinFragment) {
            mMyKingpinFragment = (MyKingpinFragment) fragment;
            mMyKingpinFragment.setUIListener(this);
        }
        else if(fragment instanceof SettingFragment) {
            mSettingFragment = (SettingFragment) fragment;
            mSettingFragment.setUIListener(this);
        }
        else if(fragment instanceof  UpcomingGameDetailFragment) {
            mUpcomingGameDetailFragment = (UpcomingGameDetailFragment) fragment;
            mUpcomingGameDetailFragment.setUIListener(this);
        }
        else if(fragment instanceof InAppPurchaseFragment) {
            mInAppPurchaseFragment = (InAppPurchaseFragment) fragment;
            mInAppPurchaseFragment.setUIListener(this);
        }
        else if(fragment instanceof FilterFragment) {
            mFilterFragment = (FilterFragment) fragment;
            mFilterFragment.setUIListener(this);
        }
        else if(fragment instanceof UserProfileFragment) {
            mUserProfileFragment = (UserProfileFragment) fragment;
            mUserProfileFragment.setUIListener(this);
        }
        else if(fragment instanceof PlacePickFilterFragment) {
            mPlacePickFilterFragment = (PlacePickFilterFragment) fragment;
            mPlacePickFilterFragment.setUIListener(this);
        }
        else if(fragment instanceof BetHelpFragment) {
            mBetHelpFragment = (BetHelpFragment) fragment;
            mBetHelpFragment.setUIListener(this);
        }
        else if(fragment instanceof AskRateFragment) {
            mAskRateFragment = (AskRateFragment) fragment;
            mAskRateFragment.setUIListener(this);
        }
        else if(fragment instanceof SubmitFeedbackFragment) {
            mSubmitFeedbackFragment = (SubmitFeedbackFragment) fragment;
            mSubmitFeedbackFragment.setUIListener(this);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        InAppManager.getInstance().startBillingClient(this);

        tabUpcomingGame.setOnClickListener(this);
        tabTopKingpin.setOnClickListener(this);
        tabMyStatics.setOnClickListener(this);
        tabPlacePick.setOnClickListener(this);
        tabFreeTrial.setOnClickListener(this);
        tabMyKingpin.setOnClickListener(this);

        initialUI();
    }

    public void initialUI() {
        if (DataManager.getInstance().isTrialAvailable()) {
            tabTextFreetrial.setText(R.string.tab_freetrial);
        } else {
            tabTextFreetrial.setText(R.string.tab_upgrade);
        }

        updateFragment(getIntent().getIntExtra(Constant.INTENT_EXTRA_FRAGMENT, UIListener.HomeActivityListener.FRAGMENT_UPCOMING_GAME));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tab_upcoming_game:
                updateFragment(UIListener.HomeActivityListener.FRAGMENT_UPCOMING_GAME);
                break;
            case R.id.tab_top_kingpiin:
                updateFragment(UIListener.HomeActivityListener.FRAGMENT_TOP_KINGPIN);
                break;
            case R.id.tab_my_statics:
                if(DataManager.getInstance().isLoggedInStatus())
                    updateFragment(UIListener.HomeActivityListener.FRAGMENT_MY_STATICS);
                else
                    openLandingScreen();
                break;
            case R.id.tab_place_pick:
                updateFragment(UIListener.HomeActivityListener.FRAGMENT_PLACE_PICK);
                break;
            case R.id.tab_free_trial:
                if(DataManager.getInstance().isLoggedInStatus())
                    upgradeProfileWithInAppPurchase();
                else
                    openLandingScreen();
                break;
            case R.id.tab_my_kingpin:
                updateFragment(UIListener.HomeActivityListener.FRAGMENT_MY_KINGPIN);
                break;
        }
    }

    @Override
    public void updateFragment(int fragment) {
        super.updateFragment(fragment);

        if(mUpcomingGameFragment == null) {
            mUpcomingGameFragment = new UpcomingGameFragment();
            mUpcomingGameFragment.setUIListener(this);
        }

        if(mTopKingpinFragment == null) {
            mTopKingpinFragment = new TopKingpinFragment();
            mTopKingpinFragment.setUIListener(this);
        }

        if(mMyStaticsFragment == null) {
            mMyStaticsFragment = new MyStaticsFragment();
            mMyStaticsFragment.setUIListener(this);
        }

        if(mPlacePickFragment == null) {
            mPlacePickFragment = new PlacePickFragment();
            mPlacePickFragment.setUIListener(this);
        }

        if(mMyKingpinFragment == null) {
            mMyKingpinFragment = new MyKingpinFragment();
            mMyKingpinFragment.setUIListener(this);
        }

        if(getModalFragment() != UIListener.BaseActivityListener.FRAGMENT_NULL)
            removeFragment(getModalFragment());

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        switch (getActiveFragment()) {
            case UIListener.HomeActivityListener.FRAGMENT_UPCOMING_GAME:
                transaction.replace(R.id.fragment_container, mUpcomingGameFragment);
                break;
            case UIListener.HomeActivityListener.FRAGMENT_TOP_KINGPIN:
                transaction.replace(R.id.fragment_container, mTopKingpinFragment);
                break;
            case UIListener.HomeActivityListener.FRAGMENT_MY_STATICS:
                transaction.replace(R.id.fragment_container, mMyStaticsFragment);
                break;
            case UIListener.HomeActivityListener.FRAGMENT_PLACE_PICK:
                transaction.replace(R.id.fragment_container, mPlacePickFragment);
                break;
            case UIListener.HomeActivityListener.FRAGMENT_MY_KINGPIN:
                transaction.replace(R.id.fragment_container, mMyKingpinFragment);
                break;
        }
        transaction.commitAllowingStateLoss();
        updateTab();
    }

    @Override
    public void addFragment(int fragment) {
        super.addFragment(fragment);

        if(mSettingFragment == null) {
            mSettingFragment = new SettingFragment();
            mSettingFragment.setUIListener(this);
        }

        if(mUpcomingGameDetailFragment == null) {
            mUpcomingGameDetailFragment = new UpcomingGameDetailFragment();
            mUpcomingGameDetailFragment.setUIListener(this);
        }

        if(mInAppPurchaseFragment == null) {
            mInAppPurchaseFragment = new InAppPurchaseFragment();
            mInAppPurchaseFragment.setUIListener(this);
        }

        if(mFilterFragment == null) {
            mFilterFragment = new FilterFragment();
            mFilterFragment.setUIListener(this);
        }

        if(mUserProfileFragment == null) {
            mUserProfileFragment = new UserProfileFragment();
            mUserProfileFragment.setUIListener(this);
        }

        if(mPlacePickFilterFragment == null) {
            mPlacePickFilterFragment = new PlacePickFilterFragment();
            mPlacePickFilterFragment.setUIListener(this);
        }

        if(mBetHelpFragment == null) {
            mBetHelpFragment = new BetHelpFragment();
            mBetHelpFragment.setUIListener(this);
        }

        if(mAskRateFragment == null) {
            mAskRateFragment = new AskRateFragment();
            mAskRateFragment.setUIListener(this);
        }

        if(mSubmitFeedbackFragment == null) {
            mSubmitFeedbackFragment = new SubmitFeedbackFragment();
            mSubmitFeedbackFragment.setUIListener(this);
        }

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        switch (getModalFragment()) {
            case UIListener.HomeActivityListener.FRAGMENT_SETTING:
                transaction.add(R.id.fragment_container, mSettingFragment);
                break;
            case UIListener.HomeActivityListener.FRAGMENT_UPCOMING_DETAIL:
                transaction.add(R.id.fragment_container, mUpcomingGameDetailFragment);
                bottomMenu.setVisibility(View.GONE);
                break;
            case UIListener.HomeActivityListener.FRAGMENT_INAPP_PURCHASE:
                transaction.add(R.id.fragment_container, mInAppPurchaseFragment);
                break;
            case UIListener.HomeActivityListener.FRAGMENT_FILTER:
                transaction.add(R.id.fragment_container, mFilterFragment);
                break;
            case UIListener.HomeActivityListener.FRAGMENT_USER_PROFILE:
                transaction.add(R.id.fragment_container, mUserProfileFragment);
                bottomMenu.setVisibility(View.GONE);
                break;
            case UIListener.HomeActivityListener.FRAGMENT_PLACEPICK_FILTER:
                transaction.add(R.id.fragment_container, mPlacePickFilterFragment);
                break;
            case UIListener.HomeActivityListener.FRAGMENT_BETHELP:
                transaction.add(R.id.fragment_container, mBetHelpFragment);
                break;
            case UIListener.HomeActivityListener.FRAGMENT_ASK_RATE:
                transaction.add(R.id.fragment_container, mAskRateFragment);
                break;
            case UIListener.HomeActivityListener.FRAGMENT_SUBMIT_FEEDBACK:
                transaction.add(R.id.fragment_container, mSubmitFeedbackFragment);
                break;
        }

        transaction.commitAllowingStateLoss();
    }

    @Override
    public void removeFragment(int fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        switch (fragment) {
            case UIListener.HomeActivityListener.FRAGMENT_SETTING:
                if(mSettingFragment == null)
                    break;

                transaction.remove(mSettingFragment);
                break;
            case UIListener.HomeActivityListener.FRAGMENT_UPCOMING_DETAIL:
                if(mUpcomingGameDetailFragment == null)
                    break;

                transaction.remove(mUpcomingGameDetailFragment);
                bottomMenu.setVisibility(View.VISIBLE);
                break;
            case UIListener.HomeActivityListener.FRAGMENT_INAPP_PURCHASE:
                if(mInAppPurchaseFragment == null)
                    break;

                transaction.remove(mInAppPurchaseFragment);
                break;
            case UIListener.HomeActivityListener.FRAGMENT_FILTER:
                if(mFilterFragment == null)
                    break;

                transaction.remove(mFilterFragment);
                break;
            case UIListener.HomeActivityListener.FRAGMENT_USER_PROFILE:
                if(mUserProfileFragment == null)
                    break;

                transaction.remove(mUserProfileFragment);
                bottomMenu.setVisibility(View.VISIBLE);
                break;
            case UIListener.HomeActivityListener.FRAGMENT_PLACEPICK_FILTER:
                if(mPlacePickFilterFragment == null)
                    break;

                transaction.remove(mPlacePickFilterFragment);
                break;
            case UIListener.HomeActivityListener.FRAGMENT_BETHELP:
                if(mBetHelpFragment == null)
                    break;

                transaction.remove(mBetHelpFragment);
                break;
            case UIListener.HomeActivityListener.FRAGMENT_ASK_RATE:
                if(mAskRateFragment == null)
                    break;

                transaction.remove(mAskRateFragment);
                break;
            case UIListener.HomeActivityListener.FRAGMENT_SUBMIT_FEEDBACK:
                if(mSubmitFeedbackFragment == null)
                    break;

                transaction.remove(mSubmitFeedbackFragment);
                break;
        }
        transaction.commitAllowingStateLoss();
        super.removeFragment(fragment);
    }

    public void updateTab() {

        bottomMenu.setVisibility(View.VISIBLE);

        Profile me = DataManager.getInstance().getAccount();
        if(me != null && me.getMembership() == 1) {
            tabFreeTrial.setVisibility(View.GONE);
            tabMyKingpin.setVisibility(View.VISIBLE);
        } else {
            tabFreeTrial.setVisibility(View.VISIBLE);
            tabMyKingpin.setVisibility(View.GONE);
        }

        tabUpcomingGame.setSelected(false);
        tabTopKingpin.setSelected(false);
        tabMyStatics.setSelected(false);
        tabPlacePick.setSelected(false);
        tabFreeTrial.setSelected(false);
        tabMyKingpin.setSelected(false);

        switch (getActiveFragment()) {
            case UIListener.HomeActivityListener.FRAGMENT_UPCOMING_GAME:
                tabUpcomingGame.setSelected(true);
                break;
            case UIListener.HomeActivityListener.FRAGMENT_TOP_KINGPIN:
                tabTopKingpin.setSelected(true);
                break;
            case UIListener.HomeActivityListener.FRAGMENT_MY_STATICS:
                tabMyStatics.setSelected(true);
                break;
            case UIListener.HomeActivityListener.FRAGMENT_PLACE_PICK:
                tabPlacePick.setSelected(true);
                break;
            case UIListener.HomeActivityListener.FRAGMENT_FREE_TRIAL:
                tabFreeTrial.setSelected(true);
                break;
            case UIListener.HomeActivityListener.FRAGMENT_MY_KINGPIN:
                tabMyKingpin.setSelected(true);
                break;
        }
    }

    public void openMyStaticsTab() {
        updateFragment(UIListener.HomeActivityListener.FRAGMENT_MY_STATICS);
    }

    public void openUpcomingGamesTab() {
        updateFragment(UIListener.HomeActivityListener.FRAGMENT_UPCOMING_GAME);
    }
}
