package com.kingpin.ui.activity;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.kingpin.R;
import com.kingpin.common.base.Constant;
import com.kingpin.listener.UIListener;
import com.kingpin.ui.fragment.CartFragment;

public class CartActivity extends BaseActivity {

    private CartFragment mCartFragment;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_cart;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        updateFragment(getIntent().getIntExtra(Constant.INTENT_EXTRA_FRAGMENT, UIListener.CartActivityListener.FRAGMENT_CART));
    }

    @Override
    public void updateFragment(int fragment) {
        super.updateFragment(fragment);

        if(mCartFragment == null) {
            mCartFragment = new CartFragment();
            mCartFragment.setUIListener(this);
        }

        if(getModalFragment() != UIListener.BaseActivityListener.FRAGMENT_NULL)
            removeFragment(getModalFragment());

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        switch (getActiveFragment()) {
            case UIListener.CartActivityListener.FRAGMENT_CART:
                transaction.replace(R.id.fragment_container, mCartFragment);
                break;
        }
        transaction.commitAllowingStateLoss();
    }

    @Override
    public void removeFragment(int fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.commitAllowingStateLoss();
        super.removeFragment(fragment);
    }
}
