package com.kingpin.ui.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.kingpin.R;
import com.kingpin.common.base.Constant;
import com.kingpin.listener.UIListener;
import com.kingpin.ui.adapter.AffiliateHistoryAdapter;
import com.kingpin.ui.fragment.AffiliateHistoryFragment;
import com.kingpin.ui.fragment.EditProfileFragment;
import com.kingpin.ui.fragment.InAppPurchaseFragment;
import com.kingpin.ui.fragment.PaymentHistoryFragment;
import com.kingpin.ui.fragment.ProfileFragment;
import com.kingpin.ui.fragment.RequestPayoutFragment;

public class ProfileActivity extends BaseActivity {

    private ProfileFragment mProfileFragment;
    private InAppPurchaseFragment mPurchaseFragment;
    private RequestPayoutFragment mRequestPayoutFragment;
    private PaymentHistoryFragment mPaymentHistoryFragment;
    private EditProfileFragment mEditProfileFragment;
    private AffiliateHistoryFragment mAffiliateHistoryFragment;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_profile;
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        if(fragment instanceof ProfileFragment) {
            mProfileFragment = (ProfileFragment) fragment;
            mProfileFragment.setUIListener(this);
        }
        else if(fragment instanceof InAppPurchaseFragment) {
            mPurchaseFragment = (InAppPurchaseFragment) fragment;
            mPurchaseFragment.setUIListener(this);
        }
        else if(fragment instanceof RequestPayoutFragment) {
            mRequestPayoutFragment = (RequestPayoutFragment) fragment;
            mRequestPayoutFragment.setUIListener(this);
        }
        else if(fragment instanceof PaymentHistoryFragment) {
            mPaymentHistoryFragment = (PaymentHistoryFragment) fragment;
            mPaymentHistoryFragment.setUIListener(this);
        }
        else if(fragment instanceof EditProfileFragment) {
            mEditProfileFragment = (EditProfileFragment) fragment;
            mEditProfileFragment.setUIListener(this);
        }
        else if(fragment instanceof AffiliateHistoryFragment) {
            mAffiliateHistoryFragment = (AffiliateHistoryFragment) fragment;
            mAffiliateHistoryFragment.setUIListener(this);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        updateFragment(getIntent().getIntExtra(Constant.INTENT_EXTRA_FRAGMENT, UIListener.ProfileActivityListener.FRAGMENT_PROFILE));
    }

    @Override
    public void updateFragment(int fragment) {
        super.updateFragment(fragment);

        if(mProfileFragment == null) {
            mProfileFragment = new ProfileFragment();
            mProfileFragment.setUIListener(this);
        }

        if(mRequestPayoutFragment == null) {
            mRequestPayoutFragment = new RequestPayoutFragment();
            mRequestPayoutFragment.setUIListener(this);
        }

        if(mPaymentHistoryFragment == null) {
            mPaymentHistoryFragment = new PaymentHistoryFragment();
            mPaymentHistoryFragment.setUIListener(this);
        }

        if(mEditProfileFragment == null) {
            mEditProfileFragment = new EditProfileFragment();
            mEditProfileFragment.setUIListener(this);
        }

        if(mAffiliateHistoryFragment == null) {
            mAffiliateHistoryFragment = new AffiliateHistoryFragment();
            mAffiliateHistoryFragment.setUIListener(this);
        }

        if(getModalFragment() != UIListener.BaseActivityListener.FRAGMENT_NULL)
            removeFragment(getModalFragment());

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        switch (getActiveFragment()) {
            case UIListener.ProfileActivityListener.FRAGMENT_PROFILE:
                transaction.replace(R.id.fragment_container, mProfileFragment);
                break;
            case UIListener.ProfileActivityListener.FRAGMENT_REQUEST_PAYOUT:
                transaction.replace(R.id.fragment_container, mRequestPayoutFragment);
                break;
            case UIListener.ProfileActivityListener.FRAGMENT_PAYMENT_HISTORY:
                transaction.replace(R.id.fragment_container, mPaymentHistoryFragment);
                break;
            case UIListener.ProfileActivityListener.FRAGMENT_EDIT_PROFILE:
                transaction.replace(R.id.fragment_container, mEditProfileFragment);
                break;
            case UIListener.ProfileActivityListener.FRAGMENT_AFFILIATE_HISTORY:
                transaction.replace(R.id.fragment_container, mAffiliateHistoryFragment);
                break;
        }
        transaction.commitAllowingStateLoss();
    }

    @Override
    public void addFragment(int fragment) {
        super.addFragment(fragment);

        if(mPurchaseFragment == null) {
            mPurchaseFragment = new InAppPurchaseFragment();
            mPurchaseFragment.setUIListener(this);
        }

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        switch (getModalFragment()) {
            case UIListener.ProfileActivityListener.FRAGMENT_INAPP_PURCHASE:
                transaction.add(R.id.fragment_container, mPurchaseFragment);
                break;
        }

        transaction.commitAllowingStateLoss();
    }

    @Override
    public void removeFragment(int fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        switch (fragment) {
            case UIListener.ProfileActivityListener.FRAGMENT_INAPP_PURCHASE:
                if(mPurchaseFragment == null)
                    break;

                transaction.remove(mPurchaseFragment);
                break;
        }
        transaction.commitAllowingStateLoss();
        super.removeFragment(fragment);
    }
}

