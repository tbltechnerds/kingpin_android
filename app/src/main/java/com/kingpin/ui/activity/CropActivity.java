package com.kingpin.ui.activity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.kingpin.R;
import com.kingpin.common.manager.TempDataManager;
import com.theartofdev.edmodo.cropper.CropImageView;

import butterknife.BindView;

public class CropActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.ivCrop) CropImageView cropView;
    @BindView(R.id.btn_done) TextView btnDone;
    @BindView(R.id.btn_back) ImageView btnBack;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_crop;
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        cropView.setImageBitmap(TempDataManager.getInstance().getTempBitmap());
        btnDone.setOnClickListener(this);
        btnBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_done:
                Bitmap original = cropView.getCroppedImage();
                Bitmap scaled = Bitmap.createScaledBitmap(original, 200, 200, false);
                TempDataManager.getInstance().setTempBitmap(scaled);
                setResult(Activity.RESULT_OK);
                finish();
                break;
            case R.id.btn_back:
                break;
        }
    }
}
