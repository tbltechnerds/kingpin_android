package com.kingpin.ui.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.kingpin.R;
import com.kingpin.common.base.Constant;
import com.kingpin.listener.UIListener;
import com.kingpin.ui.fragment.BovadaGameFragment;
import com.kingpin.ui.fragment.EditProfileFragment;
import com.kingpin.ui.fragment.PaymentHistoryFragment;
import com.kingpin.ui.fragment.ProfileFragment;
import com.kingpin.ui.fragment.RapidGameFragment;
import com.kingpin.ui.fragment.RequestPayoutFragment;

public class GameDetailActivity extends BaseActivity {

    private RapidGameFragment mRapidGameFragment;
    private BovadaGameFragment mBovadaGameFragment;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_game_detail;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        updateFragment(getIntent().getIntExtra(Constant.INTENT_EXTRA_FRAGMENT, UIListener.GameDetailActivityListener.FRAGMENT_RAPID));
    }

    @Override
    public void updateFragment(int fragment) {
        super.updateFragment(fragment);

        if(mRapidGameFragment == null) {
            mRapidGameFragment = new RapidGameFragment();
            mRapidGameFragment.setUIListener(this);
        }

        if(mBovadaGameFragment == null) {
            mBovadaGameFragment = new BovadaGameFragment();
            mBovadaGameFragment.setUIListener(this);
        }

        if(getModalFragment() != UIListener.BaseActivityListener.FRAGMENT_NULL)
            removeFragment(getModalFragment());

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        switch (getActiveFragment()) {
            case UIListener.GameDetailActivityListener.FRAGMENT_RAPID:
                transaction.replace(R.id.fragment_container, mRapidGameFragment);
                break;
            case UIListener.GameDetailActivityListener.FRAGMENT_BOVADA:
                transaction.replace(R.id.fragment_container, mBovadaGameFragment);
                break;
        }
        transaction.commitAllowingStateLoss();
    }

    @Override
    public void removeFragment(int fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.commitAllowingStateLoss();
        super.removeFragment(fragment);
    }
}
