package com.kingpin.ui.activity;

import android.app.Activity;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.kingpin.R;
import com.kingpin.common.utility.FingerPrintUtils;
import com.kingpin.listener.AuthListener;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import butterknife.BindView;

@SuppressWarnings("ALL")
@RequiresApi(api = Build.VERSION_CODES.M)
public class AuthActivity extends BaseActivity implements View.OnClickListener, AuthListener {

    private static final String DEFAULT_KEY_NAME = "default_key";

    @BindView(R.id.btn_back) ImageView btnBack;
    @BindView(R.id.img_finger) ImageView imageFinger;
    @BindView(R.id.txt_finger) TextView textFinger;

    private KeyStore mKeyStore;
    private KeyGenerator mKeyGenerator;
    private Cipher defaultCipher;
    private FingerprintManager mFingerprintManager;
    private FingerPrintUtils mFingerPrintUtil;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_auth;
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        btnBack.setOnClickListener(this);

        initCrypto();
        createFingerPrintUtil();
    }

    @Override
    public void onResume() {
        super.onResume();

        if(initCipher()) {
            updateToListen();
            startListening();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        stopListening();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopListening();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                setResult(Activity.RESULT_CANCELED);
                finish();
                break;
        }
    }

    @Override
    public void onAuthenticated(String strSuccess) {
        imageFinger.removeCallbacks(mResetRunnable);
        imageFinger.setImageResource(R.drawable.ic_fingerprint_success);
        textFinger.setText(strSuccess);
        textFinger.setTextColor(ContextCompat.getColor(this, R.color.color_green1));
        imageFinger.postDelayed(new Runnable() {
            @Override
            public void run() {
                completedAuth();
            }
        }, 1500);
    }

    @Override
    public void onAuthError(String strError) {
        imageFinger.removeCallbacks(mResetRunnable);
        imageFinger.setImageResource(R.drawable.ic_fingerprint_error);
        textFinger.setText(strError);
        textFinger.setTextColor(ContextCompat.getColor(this, R.color.color_red1));
    }

    @Override
    public void onAuthFailed(String strError) {
        imageFinger.removeCallbacks(mResetRunnable);
        imageFinger.setImageResource(R.drawable.ic_fingerprint_error);
        textFinger.setText(strError);
        textFinger.setTextColor(ContextCompat.getColor(this, R.color.color_red1));
        imageFinger.postDelayed(mResetRunnable, 1500);
    }

    private void initCrypto() {

        try {
            mKeyStore = KeyStore.getInstance("AndroidKeyStore");
        }
        catch (KeyStoreException e) {
            throw new RuntimeException("Failed to get an instance of KeyStore", e);
        }

        try {
            mKeyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
            defaultCipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        }
        catch (NoSuchAlgorithmException | NoSuchProviderException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get an instance of KeyGenerator", e);
        }

        mFingerprintManager = getSystemService(FingerprintManager.class);

        createKey(DEFAULT_KEY_NAME);
    }

    private void createFingerPrintUtil() {
        mFingerPrintUtil = new FingerPrintUtils(mFingerprintManager, this);
    }

    private boolean initCipher() {
        try {
            mKeyStore.load(null);
            SecretKey key = (SecretKey) mKeyStore.getKey(DEFAULT_KEY_NAME, null);
            defaultCipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        }
        catch (KeyPermanentlyInvalidatedException e) {
            return false;
        }
        catch (KeyStoreException | CertificateException | UnrecoverableKeyException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }

    private void startListening() {

        FingerprintManager.CryptoObject cryptoObject = new FingerprintManager.CryptoObject(defaultCipher);
        mFingerPrintUtil.startListening(cryptoObject);
    }

    private void stopListening() {
        mFingerPrintUtil.stopListening();
    }

    private void createKey(String keyName) {

        try {
            mKeyStore.load(null);

            KeyGenParameterSpec.Builder builder = new KeyGenParameterSpec.Builder(keyName, KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                builder.setInvalidatedByBiometricEnrollment(true);

            mKeyGenerator.init(builder.build());
            mKeyGenerator.generateKey();
        }
        catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException | CertificateException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void updateToListen() {
        imageFinger.setImageResource(R.mipmap.icon_fingerprint);
        textFinger.setText(R.string.text_touch_sensor);
        textFinger.setTextColor(ContextCompat.getColor(this, R.color.color_blue2));
    }

    private void completedAuth() {
        setResult(Activity.RESULT_OK);
        finish();
    }

    private Runnable mResetRunnable = new Runnable() {
        @Override
        public void run() {
            updateToListen();
        }
    };
}
