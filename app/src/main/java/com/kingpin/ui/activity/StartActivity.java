package com.kingpin.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.kingpin.R;
import com.kingpin.common.base.Constant;
import com.kingpin.common.manager.DataManager;
import com.kingpin.listener.UIListener;
import com.kingpin.ui.fragment.ForgotPwdFragment;
import com.kingpin.ui.fragment.LandFragment;
import com.kingpin.ui.fragment.SplashFragment;

public class StartActivity extends BaseActivity {

    private SplashFragment      mSplashFragment;
    private LandFragment        mLandFragment;
    private ForgotPwdFragment   mForgotPwdFragment;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_start;
    }

    @Override
    public void onAttachFragment(Fragment fragment) {

        if(fragment instanceof SplashFragment) {
            mSplashFragment = (SplashFragment) fragment;
            mSplashFragment.setUIListener(this);
        }
        else if(fragment instanceof LandFragment) {
            mLandFragment = (LandFragment) fragment;
            mLandFragment.setUIListener(this);
        }
        else if(fragment instanceof ForgotPwdFragment) {
            mForgotPwdFragment = (ForgotPwdFragment) fragment;
            mForgotPwdFragment.setUIListener(this);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        updateFragment(getIntent().getIntExtra(Constant.INTENT_EXTRA_FRAGMENT, UIListener.StartActivityListener.FRAGMENT_SPLASH));

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }

                        String token = task.getResult().getToken();
                        DataManager.getInstance().setDeviceToken(token);

                        String msg = getString(R.string.fcm_token, token);
                        Log.d("", msg);
                    }
                });

    /*    try {
            PackageInfo info = getPackageManager().getPackageInfo("com.kingpin", PackageManager.GET_SIGNATURES);
            for(Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String test =  Base64.encodeToString(md.digest(), Base64.DEFAULT);
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        }
        catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }*/
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        for(Fragment fragment : getSupportFragmentManager().getFragments())
            fragment.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void updateFragment(int fragment) {
        super.updateFragment(fragment);

        if(mSplashFragment == null) {
            mSplashFragment = new SplashFragment();
            mSplashFragment.setUIListener(this);
        }

        if(mLandFragment == null) {
            mLandFragment = new LandFragment();
            mLandFragment.setUIListener(this);
        }

        if(mForgotPwdFragment == null) {
            mForgotPwdFragment = new ForgotPwdFragment();
            mForgotPwdFragment.setUIListener(this);
        }

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        switch (getActiveFragment()) {
            case UIListener.StartActivityListener.FRAGMENT_SPLASH:
                transaction.replace(R.id.fragment_container, mSplashFragment);
                break;
            case UIListener.StartActivityListener.FRAGMENT_LAND:
                transaction.replace(R.id.fragment_container, mLandFragment);
                break;
            case UIListener.StartActivityListener.FRAGMENT_FORGOTPWD:
                transaction.replace(R.id.fragment_container, mForgotPwdFragment);
                break;
        }
        transaction.commitAllowingStateLoss();
    }
}
