package com.kingpin.ui.fragment;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chanven.lib.cptr.PtrDefaultHandler;
import com.chanven.lib.cptr.PtrFrameLayout;
import com.chanven.lib.cptr.loadmore.OnLoadMoreListener;
import com.chanven.lib.cptr.recyclerview.RecyclerAdapterWithHF;
import com.kingpin.R;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.manager.TempDataManager;
import com.kingpin.listener.SpanListener;
import com.kingpin.mvp.model.Kingpin;
import com.kingpin.mvp.model.Profile;
import com.kingpin.mvp.presenter.MyKingpinPresenter;
import com.kingpin.mvp.presenter.TopKingpinPresenter;
import com.kingpin.mvp.view.MyKingpinView;
import com.kingpin.ui.activity.HomeActivity;
import com.kingpin.ui.adapter.FilterAdapter;
import com.kingpin.ui.adapter.KingpinAdapter;
import com.kingpin.ui.widget.ActionSpan;
import com.kingpin.ui.widget.RefreshLayout;
import com.kingpin.ui.widget.TabDividerItemDecorator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;

public class MyKingpinFragment extends BaseFragment<MyKingpinView, MyKingpinPresenter> implements MyKingpinView, View.OnClickListener, SpanListener {

    @BindView(R.id.btn_filter) ImageView mBtnFilter;
    @BindView(R.id.btn_setting) ImageView mBtnSetting;
    @BindView(R.id.btn_cart) ImageView mBtnCart;
    @BindView(R.id.bubble_cart) TextView mBubbleView;
    @BindView(R.id.list_filter) RecyclerView mFilterView;
    @BindView(R.id.lyt_pull) RefreshLayout mRefreshView;
    @BindView(R.id.list_kingpin) RecyclerView mKingpinView;
    @BindView(R.id.txt_hint) TextView mHintView;
    @BindView(R.id.text_no_kingpin) TextView mNoKingpin;

    private FilterAdapter mFilterAdapter;
    private KingpinAdapter mKingpinAdapter;

    private int sortIndex;
    private boolean revert;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_my_kingpin;
    }

    @NonNull
    @Override
    public MyKingpinPresenter createPresenter() {
        return new MyKingpinPresenter();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        sortIndex = 0;
        revert = false;

        mBtnFilter.setOnClickListener(this);
        mBtnSetting.setOnClickListener(this);
        mBtnCart.setOnClickListener(this);

        makeSpanHint();

        ArrayList<Integer> filterList = new ArrayList<>();
        for(int index = 0; index < 6; index++)
            filterList.add(index);

        mFilterView.setLayoutManager(new LinearLayoutManager(getCurrentActivity(), LinearLayoutManager.HORIZONTAL, false));
        mFilterAdapter = new FilterAdapter(filterList, this);
        mFilterView.setAdapter(mFilterAdapter);

        Drawable drawable = ContextCompat.getDrawable(getCurrentActivity(), R.drawable.drawable_tab_decorator);
        if(drawable != null)
            mFilterView.addItemDecoration(new TabDividerItemDecorator(drawable, TabDividerItemDecorator.HORIZONTAL));


        mKingpinView.setLayoutManager(new LinearLayoutManager(getCurrentActivity(), LinearLayout.VERTICAL, false));
        mKingpinAdapter = new KingpinAdapter(false, this);
        mKingpinView.setAdapter(new RecyclerAdapterWithHF(mKingpinAdapter));
        mKingpinAdapter.updateKingpins();

        mRefreshView.setPtrHandler(new PtrDefaultHandler() {
            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                getPresenter().getMyKingpins();
            }
        });

        mRefreshView.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void loadMore() {
                //getPresenter().getMyKingpins();
            }
        });
        mRefreshView.setLoadMoreEnable(false);
        mRefreshView.post(new Runnable() {
            @Override
            public void run() {
                getMyKingpins();
            }
        });

        onLoadKingpins(TempDataManager.getInstance().getMyKingpins());
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_filter:
                OnFilter(this);
                break;
            case R.id.btn_setting:
                OnSetting();
                break;
            case R.id.btn_cart:
                gotoCartScreen();
                break;
        }
    }

    @Override
    public void onClicekedSpan(String spanString) {
        String strHint2 = getCurrentActivity().getResources().getString(R.string.hint_kingpin2);

        if(spanString.equals(strHint2)) {
            gotoRuleScreen();
        }
    }

    @Override
    public void onSort(int filter_type) {
        if (sortIndex == filter_type) {
            revert = !revert;
        } else {
            sortIndex = filter_type;
            revert = false;
        }
        mFilterAdapter.updateFilter(sortIndex);
        onLoadKingpins(TempDataManager.getInstance().getMyKingpins());
    }

    @Override
    public void onViewUserProfile(Kingpin kingpin) {
        Profile me = DataManager.getInstance().getAccount();
        if(me != null && DataManager.getInstance().isLoggedInStatus() && kingpin.getId() == me.getId()) {
            ((HomeActivity)getCurrentActivity()).openMyStaticsTab();
        } else {
            gotoKingpinProfileScreen(kingpin, null, false);
        }
    }

    @Override
    public void onUpcomingBets(Kingpin kingpin) {
        gotoKingpinProfileScreen(kingpin, null, true);
    }

    @Override
    public void onLoadKingpins(ArrayList<Kingpin> kingpins) {
        if (sortIndex == 0) { // rank
            Collections.sort(kingpins, new Comparator<Kingpin>() {
                @Override
                public int compare(Kingpin kingpin1, Kingpin kingpin2) {
                    if (revert) {
                        return Integer.compare(kingpin2.getRank(), kingpin1.getRank());
                    } else {
                        return Integer.compare(kingpin1.getRank(), kingpin2.getRank());
                    }
                }
            });
        } else if (sortIndex == 1) { // winnings
            Collections.sort(kingpins, new Comparator<Kingpin>() {
                @Override
                public int compare(Kingpin kingpin1, Kingpin kingpin2) {
                    if (revert) {
                        return Integer.compare(kingpin1.getWinningLosses(), kingpin2.getWinningLosses());
                    } else {
                        return Integer.compare(kingpin2.getWinningLosses(), kingpin1.getWinningLosses());
                    }
                }
            });
        } else if (sortIndex == 2) { // number of bets
            Collections.sort(kingpins, new Comparator<Kingpin>() {
                @Override
                public int compare(Kingpin kingpin1, Kingpin kingpin2) {
                    if (revert) {
                        return Integer.compare(kingpin1.getCountBets(), kingpin2.getCountBets());
                    } else {
                        return Integer.compare(kingpin2.getCountBets(), kingpin1.getCountBets());
                    }
                }
            });
        } else if (sortIndex == 3) { // last 30 day's winning
            Collections.sort(kingpins, new Comparator<Kingpin>() {
                @Override
                public int compare(Kingpin kingpin1, Kingpin kingpin2) {
                    if (revert) {
                        return Integer.compare(kingpin1.getLast30daysWinnings(), kingpin2.getLast30daysWinnings());
                    } else {
                        return Integer.compare(kingpin2.getLast30daysWinnings(), kingpin1.getLast30daysWinnings());
                    }
                }
            });
        } else if (sortIndex == 4) { // total wins
            Collections.sort(kingpins, new Comparator<Kingpin>() {
                @Override
                public int compare(Kingpin kingpin1, Kingpin kingpin2) {
                    if (revert) {
                        return Integer.compare(kingpin1.getCountWin(), kingpin2.getCountWin());
                    } else {
                        return Integer.compare(kingpin2.getCountWin(), kingpin1.getCountWin());
                    }
                }
            });
        } else if (sortIndex == 5) { // win percent
            Collections.sort(kingpins, new Comparator<Kingpin>() {
                @Override
                public int compare(Kingpin kingpin1, Kingpin kingpin2) {
                    if (revert) {
                        return Integer.compare(kingpin1.getPercentWin(), kingpin2.getPercentWin());
                    } else {
                        return Integer.compare(kingpin2.getPercentWin(), kingpin1.getPercentWin());
                    }
                }
            });
        }

        showCartBadge(mBubbleView);

        if (kingpins.size() > 0) {
            mNoKingpin.setVisibility(View.GONE);
        } else {
            mNoKingpin.setVisibility(View.VISIBLE);
        }

        Profile me = DataManager.getInstance().getAccount();
        if (me != null && me.getMembership() == 1) {
            mNoKingpin.setText("You aren’t following any Kingpins. To follow a Kingpin: \n\n1) go to Top Kingpins \n2) click on their profile \n3) click “Follow” \n4) get a notification every time they place a pick (if notifications are on)");
        } else {
            mNoKingpin.setText("You aren’t following any Kingpins. To follow a Kingpin: \n\n1) upgrade to a premium membership \n2) go to Top Kingpins \n3) click on their profile \n4) click “Follow” \n5) get a notification every time they place a pick (if notifications are on)");
        }

        mKingpinAdapter.updateKingpins();
        mRefreshView.refreshComplete();
    }

    @Override
    public void onLoadFailedKingpins() {
        mRefreshView.refreshComplete();
    }

    @Override
    public void OnChangedFilterOption() {
        onSort(TempDataManager.getInstance().getSelectedFilter());
    }

    private void makeSpanHint() {
        SpannableStringBuilder builder = new SpannableStringBuilder();

        String strHint1 = String.format(getCurrentActivity().getResources().getString(R.string.hint_kingpin1), DataManager.getInstance().getMonthlyPot());
        String strHint2 = getCurrentActivity().getResources().getString(R.string.hint_kingpin2);

        SpannableString spanHint1 = new SpannableString(strHint1);
        spanHint1.setSpan(new ForegroundColorSpan(getCurrentActivity().getResources().getColor(R.color.color_white)), 0, strHint1.length(), 0);
        builder.append(spanHint1);
        builder.append(" ");

        SpannableString spanHint2 = new SpannableString(strHint2);
        spanHint2.setSpan(new ActionSpan(strHint2, getCurrentActivity().getResources().getColor(R.color.color_white), this), 0, strHint2.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append(spanHint2);

        mHintView.setText(builder, TextView.BufferType.SPANNABLE);
        mHintView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void getMyKingpins() {
        mRefreshView.autoRefresh(true);
    }
}
