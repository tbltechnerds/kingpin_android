package com.kingpin.ui.fragment;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.billingclient.api.SkuDetails;
import com.chanven.lib.cptr.PtrDefaultHandler;
import com.chanven.lib.cptr.PtrFrameLayout;
import com.chanven.lib.cptr.recyclerview.RecyclerAdapterWithHF;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.kingpin.R;
import com.kingpin.billingmodule.billing.BillingConstants;
import com.kingpin.billingmodule.billing.BillingProvider;
import com.kingpin.common.base.Constant;
import com.kingpin.common.base.Constant.FILTER_SPORT;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.manager.InAppManager;
import com.kingpin.common.manager.TempDataManager;
import com.kingpin.common.utility.DateUtils;
import com.kingpin.common.utility.PreferenceUtils;
import com.kingpin.listener.SpanListener;
import com.kingpin.listener.UIListener;
import com.kingpin.mvp.model.Kingpin;
import com.kingpin.mvp.model.Profile;
import com.kingpin.mvp.model.TopUsers;
import com.kingpin.mvp.model.UpcomingGame;
import com.kingpin.mvp.model.UpcomingGameDetail;
import com.kingpin.mvp.model.response.UpcomeGameResponse;
import com.kingpin.mvp.presenter.InAppPurchasePresenter;
import com.kingpin.mvp.presenter.UpcomingGamePresenter;
import com.kingpin.mvp.view.UpcomingGameView;
import com.kingpin.ui.activity.HomeActivity;
import com.kingpin.ui.adapter.TabAdapter;
import com.kingpin.ui.adapter.UpcomeGameAdapter;
import com.kingpin.ui.widget.ActionSpan;
import com.kingpin.ui.widget.RefreshLayout;
import com.kingpin.ui.widget.TabDividerItemDecorator;
import com.makeramen.roundedimageview.RoundedImageView;
import com.revenuecat.purchases.Entitlement;
import com.revenuecat.purchases.EntitlementInfo;
import com.revenuecat.purchases.PeriodType;
import com.revenuecat.purchases.PurchaserInfo;
import com.revenuecat.purchases.Purchases;
import com.revenuecat.purchases.PurchasesError;
import com.revenuecat.purchases.interfaces.ReceivePurchaserInfoListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.PicassoProvider;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;

public class UpcomingGameFragment extends BaseFragment<UpcomingGameView, UpcomingGamePresenter> implements UpcomingGameView, View.OnClickListener, SpanListener {

    @BindView(R.id.btn_setting) ImageView btnSetting;
    @BindView(R.id.btn_cart) ImageView btnCart;
    @BindView(R.id.bubble_cart) TextView textBubble;
    @BindView(R.id.list_tab) RecyclerView mTabView;
    @BindView(R.id.lyt_pull) RefreshLayout mRefreshView;
    @BindView(R.id.list_game) RecyclerView mGameView;
    @BindView(R.id.txt_monthly_pot) TextView mMonthlyPotView;

    @BindView(R.id.ly_topusers) LinearLayout lyTopUsers;
    @BindView(R.id.txt_top_cappers) TextView txtTopCappers;

    @BindView(R.id.ly_topuser1) LinearLayout lyTopUser1;
    @BindView(R.id.img_topuser1_profile) RoundedImageView imgTopUser1;
    @BindView(R.id.txt_topuser1_username) TextView txtTopUserName1;
    @BindView(R.id.txt_topuser1_rank) TextView txtTopUserRank1;
    @BindView(R.id.txt_topuser1_wins) TextView txtTopUserWin1;
    @BindView(R.id.text_topuser1_winnings_label) TextView txtTopUserWinLabel1;

    @BindView(R.id.ly_topuser2) LinearLayout lyTopUser2;
    @BindView(R.id.img_topuser2_profile) RoundedImageView imgTopUser2;
    @BindView(R.id.txt_topuser2_username) TextView txtTopUserName2;
    @BindView(R.id.txt_topuser2_rank) TextView txtTopUserRank2;
    @BindView(R.id.txt_topuser2_wins) TextView txtTopUserWin2;
    @BindView(R.id.ly_topuser2_border) FrameLayout lyTopUser2Border;
    @BindView(R.id.text_topuser2_winnings_label) TextView txtTopUserWinLabel2;

    @BindView(R.id.ly_topuser3) LinearLayout lyTopUser3;
    @BindView(R.id.img_topuser3_profile) RoundedImageView imgTopUser3;
    @BindView(R.id.txt_topuser3_username) TextView txtTopUserName3;
    @BindView(R.id.txt_topuser3_rank) TextView txtTopUserRank3;
    @BindView(R.id.txt_topuser3_wins) TextView txtTopUserWin3;
    @BindView(R.id.text_topuser3_winnings_label) TextView txtTopUserWinLabel3;

    private TabAdapter mTabAdapter;
    private UpcomeGameAdapter mGameAdapter;

    private JsonArray allTopUsers;
    private ArrayList<Kingpin> topKingpins;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_upcoming_game;
    }

    @NonNull
    @Override
    public UpcomingGamePresenter createPresenter() {
        return new UpcomingGamePresenter();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnSetting.setOnClickListener(this);
        btnCart.setOnClickListener(this);
        lyTopUser1.setOnClickListener(this);
        lyTopUser2.setOnClickListener(this);
        lyTopUser3.setOnClickListener(this);

        makeSpanHint();

        ArrayList<FILTER_SPORT> tabList = new ArrayList<>();
        tabList.add(FILTER_SPORT.FOOTBALLNFL);
        tabList.add(FILTER_SPORT.FOOTBALLNCAAF);
        tabList.add(FILTER_SPORT.BASKETBALLNBA);
        tabList.add(FILTER_SPORT.BASKETBALLNCAAB);
        tabList.add(FILTER_SPORT.BASEBALL);
        tabList.add(FILTER_SPORT.HOCKEY);
        tabList.add(FILTER_SPORT.SOCCER);
        tabList.add(FILTER_SPORT.TENNIS);
        tabList.add(FILTER_SPORT.BOXING);
        tabList.add(FILTER_SPORT.MMA);

        mTabView.setLayoutManager(new LinearLayoutManager(getCurrentActivity(), LinearLayoutManager.HORIZONTAL, false));
        mTabAdapter = new TabAdapter(tabList, this);
        mTabView.setAdapter(mTabAdapter);

        Drawable drawable = ContextCompat.getDrawable(getCurrentActivity(), R.drawable.drawable_tab_decorator);
        if(drawable != null)
            mTabView.addItemDecoration(new TabDividerItemDecorator(drawable, TabDividerItemDecorator.HORIZONTAL));

        mGameView.setLayoutManager(new LinearLayoutManager(getCurrentActivity(), LinearLayout.VERTICAL, false));
        mGameAdapter = new UpcomeGameAdapter(this);
        mGameView.setAdapter(new RecyclerAdapterWithHF(mGameAdapter));
        mGameAdapter.updateGames(TempDataManager.getInstance().getUpcomingGames());

        mRefreshView.setPtrHandler(new PtrDefaultHandler() {
            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                getPresenter().getUpcomeGames();
            }
        });

        onTab(TempDataManager.getInstance().getSelectedSport());
    }

    @Override
    public void onResume() {
        super.onResume();

        showCartBadge(textBubble);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_setting:
                OnSetting();
                break;
            case R.id.btn_cart:
                gotoCartScreen();
                break;
            case R.id.ly_topuser1:
                onTapTopUser(0);
                break;
            case R.id.ly_topuser2:
                onTapTopUser(1);
                break;
            case R.id.ly_topuser3:
                onTapTopUser(2);
                break;
        }
    }

    @Override
    public void onTab(FILTER_SPORT tabType) {
        mTabAdapter.updateTab(tabType);
        mGameAdapter.chooseTab(tabType);

        TempDataManager.getInstance().setSelectedSport(tabType);
        loadTopUsers();

        getPresenter().getUpcomeGames();
    }

    @Override
    public void onLoadGames(ArrayList<UpcomingGame> games, JsonArray allTopUsers) {
        this.allTopUsers = allTopUsers;
        loadTopUsers();

        mGameAdapter.updateGames(games);
        mRefreshView.refreshComplete();

        if (TempDataManager.getInstance().isFirstLoading) {
            TempDataManager.getInstance().isFirstLoading = false;
            Profile me = DataManager.getInstance().getAccount();
            if (me != null) {
                // ******************
                // only check inapp when membership = 0, in other case, it will be updated on server side
                // ******************
                if (me.getMembership() == 0) {
                    if (DataManager.getInstance().isAvailableNonRenewSubscribeInApp(getContext())) {
                        getPresenter().updateProfileWithSubscribedStatus(true, false, true);
                    } else {
                        checkSubscribeStatus();
                    }
                } else {
                    getPresenter().saveDeviceToken();
                }
            }
        }
    }

    @Override
    public void onChooseGame(UpcomingGameDetail match) {
        TempDataManager.getInstance().setSelectedMatch(match);
        addFragment(UIListener.HomeActivityListener.FRAGMENT_UPCOMING_DETAIL);
    }

    @Override
    public void OnUpdatedProfile() {
        ((HomeActivity)getCurrentActivity()).updateTab();
        Profile me = DataManager.getInstance().getAccount();
        if (me != null) {
            if (me.getMembership() == 0) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getCurrentActivity().upgradeProfileWithInAppPurchase();
                    }
                }, 3000);
            } else {
                if (me.isRanked()) {
                    SimpleDateFormat local_format = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                    Date now = new Date();
                    String askedDate = PreferenceUtils.getStringPreference(getCurrentActivity(), Constant.PREF_ASKRATE_DATE);
                    try {
                        Date date = local_format.parse(askedDate);
                        if (date != null) {
                            long miliseconds = now.getTime() - date.getTime();
                            if (miliseconds > 2 * 7 * 24 * 3600 * 1000) {
                                addFragment(UIListener.HomeActivityListener.FRAGMENT_ASK_RATE);
                            } else {
                                return;
                            }
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    String today = local_format.format(now);
                    PreferenceUtils.putPreference(getCurrentActivity(), Constant.PREF_ASKRATE_DATE, today);
                }
            }
        }
    }

    @Override
    public void onClicekedSpan(String spanString) {
        String strHint2 = getCurrentActivity().getResources().getString(R.string.hint_kingpin2);
        if(spanString.equals(strHint2)) {
            gotoRuleScreen();
        }
    }

    private void makeSpanHint() {
        SpannableStringBuilder builder = new SpannableStringBuilder();

        String strHint1 = String.format(getCurrentActivity().getResources().getString(R.string.hint_kingpin1), DataManager.getInstance().getMonthlyPot());
        String strHint2 = getCurrentActivity().getResources().getString(R.string.hint_kingpin2);

        SpannableString spanHint1 = new SpannableString(strHint1);
        spanHint1.setSpan(new ForegroundColorSpan(getCurrentActivity().getResources().getColor(R.color.color_white)), 0, strHint1.length(), 0);
        builder.append(spanHint1);
        builder.append(" ");

        SpannableString spanHint2 = new SpannableString(strHint2);
        spanHint2.setSpan(new ActionSpan(strHint2, getCurrentActivity().getResources().getColor(R.color.color_white), this), 0, strHint2.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append(spanHint2);

        mMonthlyPotView.setText(builder, TextView.BufferType.SPANNABLE);
        mMonthlyPotView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void checkSubscribeStatus() {
        Purchases.getSharedInstance().getPurchaserInfo(new ReceivePurchaserInfoListener() {
            @Override
            public void onReceived(@NonNull PurchaserInfo purchaserInfo) {
                EntitlementInfo pro = purchaserInfo.getEntitlements().get(BillingConstants.ENTITLEMENT_PRO);
                if (pro != null  && pro.isActive()) {
                    // Grant user "pro" access
                    if (pro.getPeriodType() == PeriodType.TRIAL) {
                        getPresenter().updateProfileWithSubscribedStatus(true, true, true);
                    } else {
                        String skuId = pro.getProductIdentifier();
                        if (skuId != null
                                && (skuId.equals(BillingConstants.SKU_FREETRIAL_MONTHLY_PROMO) || skuId.equals(BillingConstants.SKU_FREETRIAL_MONTHLY_NOPROMO))) {
                            if (hasValidPromoCode()) {
                                getPresenter().purchasedSubscribe("month", 0, 31.99);
                            } else {
                                getPresenter().updateProfileWithSubscribedStatus(true, false, true);
                            }
                        } else if (skuId != null && (skuId.equals(BillingConstants.SKU_DAY_PROMO) || skuId.equals(BillingConstants.SKU_DAY_NOPROMO))) {
                            Date latestPurchaseDate = pro.getLatestPurchaseDate();
                            if (latestPurchaseDate != null) {
                                Calendar calendar = Calendar.getInstance(); // gets a calendar using the default time zone and locale.
                                calendar.setTime(latestPurchaseDate);
                                calendar.add(Calendar.SECOND, Constant.NonRenewableSubscribeDuration);
                                Date expire = calendar.getTime();
                                Date now = new Date();
                                if (expire.after(now)) {
                                    getPresenter().updateProfileWithSubscribedStatus(true, false, true);
                                } else {
                                    noHaveAvailableSubscribe();
                                }
                            } else {
                                noHaveAvailableSubscribe();
                            }
                        } else {
                            getPresenter().updateProfileWithSubscribedStatus(true, false, true);
                        }
                    }
                } else {
                    noHaveAvailableSubscribe();
                }
            }

            @Override
            public void onError(@NonNull PurchasesError error) {
                noHaveAvailableSubscribe();
            }
        });
    }

    private void noHaveAvailableSubscribe() {
        // *************
        // disable to downgrade membership
        // *************
        //getPresenter().updateProfileWithSubscribedStatus(false, false, true);

        // register device token
        getPresenter().saveDeviceToken();
    }

    private boolean hasValidPromoCode() {
        Profile me = DataManager.getInstance().getAccount();
        if (me != null && me.isVaildAffiliateCode()) { // has valid promo code
            return true;
        } else {
            return false;
        }
    }

    private void loadTopUsers() {
        FILTER_SPORT sport = TempDataManager.getInstance().getSelectedSport();
        String sportName = "";
        switch (sport) {
            case FOOTBALLNFL:
                sportName = "NFL";
                break;
            case FOOTBALLNCAAF:
                sportName = "NCF";
                break;
            case BASKETBALLNBA:
                sportName = "NBA";
                break;
            case BASKETBALLNCAAB:
                sportName = "NCB";
                break;
            case BASEBALL:
                sportName = "MLB";
                break;
            case HOCKEY:
                sportName = "NHL";
                break;
            case SOCCER:
                sportName = "Soccer";
                break;
            case TENNIS:
                sportName = "Tennis";
                break;
            case BOXING:
                sportName = "Boxing";
                break;
            case MMA:
                sportName = "MMA";
                break;
            default:
                break;
        }

        txtTopCappers.setText("Top " + sportName + " Cappers");

        topKingpins = new ArrayList<>();

        if (!sportName.equals("") && allTopUsers != null) {
            for (JsonElement element : allTopUsers) {
                if (element.isJsonObject()) {
                    JsonObject subTopUsersObj = element.getAsJsonObject();
                    if(subTopUsersObj.has("sport") && !subTopUsersObj.get("sport").isJsonNull()) {
                        String sportName1 = subTopUsersObj.get("sport").getAsString();
                        if (sportName.equals(sportName1)) {
                            JsonArray subTopUsers = subTopUsersObj.get("user_info").getAsJsonArray();
                            for (JsonElement kingpinElement : subTopUsers) {
                                if (kingpinElement.isJsonObject()) {
                                    JsonObject kingpinObj = kingpinElement.getAsJsonObject();
                                    GsonBuilder gsonBuilder = new GsonBuilder();
                                    gsonBuilder.registerTypeAdapter(Kingpin.class, Kingpin.deserializer);
                                    Gson gson = gsonBuilder.create();
                                    final Kingpin kingpin = gson.fromJson(kingpinObj, Kingpin.class);
                                    topKingpins.add(kingpin);
                                }
                            }
                        }
                    }
                }
            }
        }

        Collections.sort(topKingpins, new Comparator<Kingpin>() {
            @Override
            public int compare(Kingpin kingpin1, Kingpin kingpin2) {
                return Integer.compare(kingpin1.getRank(), kingpin2.getRank());
            }
        });

        if (topKingpins.size() > 0) {
            lyTopUsers.setVisibility(View.VISIBLE);

            lyTopUser1.setVisibility(View.VISIBLE);
            Kingpin topUser1 = topKingpins.get(0);
            String url1 = Constant.SERVER_HOST + "/" + topUser1.getAvatar();
            Picasso.get()
                    .load(url1)
                    .placeholder(R.mipmap.img_detail_avatar)
                    .into(imgTopUser1);
            txtTopUserName1.setText(topUser1.getName());
            //txtTopUserRank1.setText(topUser1.getRankLabel(sportName));
            txtTopUserRank1.setText("#1 " + sportName);
            txtTopUserWin1.setText("$" + topUser1.getWinningLosses());
            txtTopUserWinLabel1.setText(String.format("%s Winnings", sportName));

            if (topKingpins.size() > 1) {
                lyTopUser2.setVisibility(View.VISIBLE);
                Kingpin topUser2 = topKingpins.get(1);
                String url2 = Constant.SERVER_HOST + "/" + topUser2.getAvatar();
                Picasso.get()
                        .load(url2)
                        .placeholder(R.mipmap.img_detail_avatar)
                        .into(imgTopUser2);
                txtTopUserName2.setText(topUser2.getName());
                //txtTopUserRank2.setText(topUser2.getRankLabel(sportName));
                txtTopUserRank2.setText("#2 " + sportName);
                txtTopUserWin2.setText("$" + topUser2.getWinningLosses());
                txtTopUserWinLabel2.setText(String.format("%s Winnings", sportName));

                if (topKingpins.size() > 2) {
                    lyTopUser2Border.setVisibility(View.VISIBLE);
                    lyTopUser3.setVisibility(View.VISIBLE);
                    Kingpin topUser3 = topKingpins.get(2);
                    String url3 = Constant.SERVER_HOST + "/" + topUser3.getAvatar();
                    Picasso.get()
                            .load(url3)
                            .placeholder(R.mipmap.img_detail_avatar)
                            .into(imgTopUser3);
                    txtTopUserName3.setText(topUser3.getName());
                    //txtTopUserRank3.setText(topUser3.getRankLabel(sportName));
                    txtTopUserRank3.setText("#3 " + sportName);
                    txtTopUserWin3.setText("$" + topUser3.getWinningLosses());
                    txtTopUserWinLabel3.setText(String.format("%s Winnings", sportName));
                } else {
                    lyTopUser2Border.setVisibility(View.GONE);
                    lyTopUser3.setVisibility(View.GONE);
                }
            } else {
                lyTopUser2.setVisibility(View.GONE);
            }
        } else {
            lyTopUsers.setVisibility(View.GONE);
        }
    }

    private void onTapTopUser(int index) {
        if (topKingpins.size() > index) {
            Kingpin kingpin = topKingpins.get(index);
            Profile me = DataManager.getInstance().getAccount();
            if(me != null && kingpin.getId() == me.getId()) {
                ((HomeActivity)getCurrentActivity()).openMyStaticsTab();
            } else {
                gotoKingpinProfileScreen(kingpin, null, false);
            }
        }
    }
}