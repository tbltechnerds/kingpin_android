package com.kingpin.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kingpin.R;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.manager.TempDataManager;
import com.kingpin.common.utility.DateUtils;
import com.kingpin.mvp.model.BovadaGame;
import com.kingpin.mvp.model.BovadaGameOutcome;
import com.kingpin.mvp.model.PlaceBetCart;
import com.kingpin.mvp.presenter.BovadaGamePresenter;
import com.kingpin.mvp.presenter.RapidGamePresenter;
import com.kingpin.mvp.view.BovadaGameView;
import com.kingpin.mvp.view.RapidGameView;

import java.util.Date;

import butterknife.BindView;

public class BovadaGameFragment extends BaseFragment<BovadaGameView, BovadaGamePresenter> implements View.OnClickListener {
    @BindView(R.id.txt_title) TextView mTxtTitle;
    @BindView(R.id.btn_back) ImageView mBtnBack;
    @BindView(R.id.btn_cart) ImageView mBtnCart;
    @BindView(R.id.bubble_cart) TextView mBubbleView;
    @BindView(R.id.txt_moneyline_value1) TextView mTxtMoneylineValue1;
    @BindView(R.id.lyt_moneyline_x) LinearLayout mLytMoneylineX;
    @BindView(R.id.txt_moneyline_valuex) TextView mTxtMoneylineValueX;
    @BindView(R.id.txt_moneyline_value2) TextView mTxtMoneylineValue2;
    @BindView(R.id.txt_spread_balance1) TextView mTxtSpreadBalance1;
    @BindView(R.id.txt_spread_balance2) TextView mTxtSpreadBalance2;
    @BindView(R.id.txt_spread_value1) TextView mTxtSpreadValue1;
    @BindView(R.id.txt_spread_value2) TextView mTxtSpreadValue2;
    @BindView(R.id.txt_total_balance1) TextView mTxtTotalBalance1;
    @BindView(R.id.txt_total_balance2) TextView mTxtTotalBalance2;
    @BindView(R.id.txt_total_value1) TextView mTxtTotalValue1;
    @BindView(R.id.txt_total_value2) TextView mTxtTotalValue2;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_bovada_game;
    }

    @Override
    public BovadaGamePresenter createPresenter() {
        return new BovadaGamePresenter();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mBtnBack.setOnClickListener(this);
        mBtnCart.setOnClickListener(this);

        mTxtMoneylineValue1.setOnClickListener(this);
        mTxtMoneylineValueX.setOnClickListener(this);
        mTxtMoneylineValue2.setOnClickListener(this);
        mTxtSpreadValue1.setOnClickListener(this);
        mTxtSpreadValue2.setOnClickListener(this);
        mTxtTotalValue1.setOnClickListener(this);
        mTxtTotalValue2.setOnClickListener(this);

        loadValues();
    }

    @Override
    public void onResume() {
        super.onResume();
        showCartBadge(mBubbleView);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_back:
                getCurrentActivity().finish();
                break;
            case R.id.btn_cart:
                gotoCartScreen();
                break;
            case R.id.txt_moneyline_value1:
            case R.id.txt_moneyline_valuex:
            case R.id.txt_moneyline_value2:
            case R.id.txt_spread_value1:
            case R.id.txt_spread_value2:
            case R.id.txt_total_value1:
            case R.id.txt_total_value2:
                onAddBetToCart(view.getId());
                break;
        }
    }

    private void onAddBetToCart(int buttonid) {
        if (!DataManager.getInstance().isLoggedInStatus()) {
            getCurrentActivity().openLandingScreen();
            return;
        }

        BovadaGame game = (BovadaGame)TempDataManager.getInstance().getSelectedGame();
        if (game == null) {
            showToast("Something went wrong. Please try again.");
            return;
        }

        PlaceBetCart cart = new PlaceBetCart();
        cart.teams = game.getGameTitle();
        cart.sport = game.getSport();

        if (game.isPassedGame()) {
            showToast("The match has already started.");
            return;
        }

        Date startTime = game.getGameStartDate();
        cart.match_date = DateUtils.convertLocalDateToServerDateString(startTime);

        String opposite_lines = "";
        String opposite_market_ids = "";

        BovadaGameOutcome outcome = new BovadaGameOutcome();
        if (buttonid == R.id.txt_moneyline_value1 || buttonid == R.id.txt_moneyline_valuex || buttonid == R.id.txt_moneyline_value2) {
            if (game.isSoccer()) {
                if (buttonid == R.id.txt_moneyline_value1) {
                    outcome = game.getMoneyline1();
                    cart.code = -1;
                    opposite_lines = String.format("%s,%s", game.getMoneylineX().getPrice().getAmerican(), game.getMoneyline2().getPrice().getAmerican());
                    opposite_market_ids = String.format("%s,%s", game.getMoneylineX().getPrice().getId(), game.getMoneyline2().getPrice().getId());
                } else if (buttonid == R.id.txt_moneyline_valuex) {
                    outcome = game.getMoneylineX();
                    cart.code = 0;
                    opposite_lines = String.format("%s,%s", game.getMoneyline1().getPrice().getAmerican(), game.getMoneyline2().getPrice().getAmerican());
                    opposite_market_ids = String.format("%s,%s", game.getMoneyline1().getPrice().getId(), game.getMoneyline2().getPrice().getId());
                } else if (buttonid == R.id.txt_moneyline_value2) {
                    outcome = game.getMoneyline2();
                    cart.code = -2;
                    opposite_lines = String.format("%s,%s", game.getMoneylineX().getPrice().getAmerican(), game.getMoneyline1().getPrice().getAmerican());
                    opposite_market_ids = String.format("%s,%s", game.getMoneylineX().getPrice().getId(), game.getMoneyline1().getPrice().getId());
                }
            } else {
                if (buttonid == R.id.txt_moneyline_value1) {
                    outcome = game.getMoneyline1();
                    cart.code = 1;
                    opposite_lines = game.getMoneyline2().getPrice().getAmerican();
                    opposite_market_ids = game.getMoneyline2().getPrice().getId();
                } else if (buttonid == R.id.txt_moneyline_value2) {
                    outcome = game.getMoneyline2();
                    cart.code = 2;
                    opposite_lines = game.getMoneyline1().getPrice().getAmerican();
                    opposite_market_ids = game.getMoneyline1().getPrice().getId();
                }
            }
            cart.team_bet = outcome.getDesc();
            cart.bet_type = "moneyline";
            cart.bet_param = "1";
        } else if (buttonid == R.id.txt_spread_value1 || buttonid == R.id.txt_spread_value2) {
            if (buttonid == R.id.txt_spread_value1) {
                outcome = game.getSpread1();
                cart.code = 3;
                opposite_lines = game.getSpread2().getPrice().getAmerican();
                opposite_market_ids = game.getSpread2().getPrice().getId();
            } else {
                outcome = game.getSpread2();
                cart.code = 4;
                opposite_lines = game.getSpread1().getPrice().getAmerican();
                opposite_market_ids = game.getSpread1().getPrice().getId();
            }
            cart.team_bet = outcome.getDesc() + " " + outcome.getPrice().getHandicap();
            cart.bet_type = "handicap";
            cart.bet_param = outcome.getPrice().getHandicap();
        } else if (buttonid == R.id.txt_total_value1 || buttonid == R.id.txt_total_value2) {
            if (buttonid == R.id.txt_total_value1) {
                outcome = game.getTotal1();
                cart.code = 5;
                opposite_lines = game.getTotal2().getPrice().getAmerican();
                opposite_market_ids = game.getTotal2().getPrice().getId();
            } else {
                outcome = game.getTotal2();
                cart.code = 6;
                opposite_lines = game.getTotal1().getPrice().getAmerican();
                opposite_market_ids = game.getTotal1().getPrice().getId();
            }
            cart.team_bet = outcome.getDesc() + " " + outcome.getPrice().getHandicap();
            cart.bet_type = "total";
            cart.bet_param = outcome.getPrice().getHandicap();
        } else {
            showToast("Something went wrong. Please try again.");
            return;
        }

        cart.odd = outcome.getPrice().getFractional();
        cart.line = outcome.getPrice().getAmerican();
        cart.marketid = outcome.getPrice().getId();
        cart.opposite_lines = opposite_lines;
        cart.opposite_market_ids = opposite_market_ids;

        if (!DataManager.getInstance().addCart(cart)) {
            showToast("Already in the cart.");
            return;
        }

        showToast("Added to bet slip.");

        showCartBadge(mBubbleView);
    }

    private void loadValues() {
        BovadaGame game = (BovadaGame) TempDataManager.getInstance().getSelectedGame();
        if (game != null) {
            mTxtTitle.setText(game.getGameTitle());
            if (game.isSoccer()) {
                mTxtMoneylineValue1.setText(game.getMoneyline1().getPrice().getAmerican());
                mLytMoneylineX.setVisibility(View.VISIBLE);
                mTxtMoneylineValueX.setText(game.getMoneylineX().getPrice().getAmerican());
                mTxtMoneylineValue2.setText(game.getMoneyline2().getPrice().getAmerican());
            } else {
                mTxtMoneylineValue1.setText(game.getMoneyline1().getPrice().getAmerican());
                mLytMoneylineX.setVisibility(View.GONE);
                mTxtMoneylineValueX.setText("");
                mTxtMoneylineValue2.setText(game.getMoneyline2().getPrice().getAmerican());
            }

            mTxtSpreadBalance1.setText(game.getSpread1().getPrice().getHandicap());
            mTxtSpreadBalance2.setText(game.getSpread2().getPrice().getHandicap());
            mTxtSpreadValue1.setText(game.getSpread1().getPrice().getAmerican());
            mTxtSpreadValue2.setText(game.getSpread2().getPrice().getAmerican());

            mTxtTotalBalance1.setText(game.getTotal1().getPrice().getHandicap());
            mTxtTotalBalance2.setText(game.getTotal2().getPrice().getHandicap());
            mTxtTotalValue1.setText(game.getTotal1().getPrice().getAmerican());
            mTxtTotalValue2.setText(game.getTotal2().getPrice().getAmerican());
        } else {
            mTxtTitle.setText("");

            mTxtMoneylineValue1.setText("");
            mLytMoneylineX.setVisibility(View.GONE);
            mTxtMoneylineValueX.setText("");
            mTxtMoneylineValue2.setText("");

            mTxtSpreadBalance1.setText("");
            mTxtSpreadBalance2.setText("");
            mTxtSpreadValue1.setText("");
            mTxtSpreadValue2.setText("");

            mTxtTotalBalance1.setText("");
            mTxtTotalBalance2.setText("");
            mTxtTotalValue1.setText("");
            mTxtTotalValue2.setText("");
        }

        showCartBadge(mBubbleView);
    }
}
