package com.kingpin.ui.fragment;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chanven.lib.cptr.PtrDefaultHandler;
import com.chanven.lib.cptr.PtrFrameLayout;
import com.chanven.lib.cptr.loadmore.OnLoadMoreListener;
import com.chanven.lib.cptr.recyclerview.RecyclerAdapterWithHF;
import com.kingpin.R;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.manager.TempDataManager;
import com.kingpin.listener.SpanListener;
import com.kingpin.listener.UIListener;
import com.kingpin.mvp.model.Kingpin;
import com.kingpin.mvp.model.Profile;
import com.kingpin.mvp.presenter.TopKingpinPresenter;
import com.kingpin.mvp.view.TopKingpinView;
import com.kingpin.ui.activity.BaseActivity;
import com.kingpin.ui.activity.HomeActivity;
import com.kingpin.ui.activity.StartActivity;
import com.kingpin.ui.adapter.FilterAdapter;
import com.kingpin.ui.adapter.KingpinAdapter;
import com.kingpin.ui.widget.ActionSpan;
import com.kingpin.ui.widget.RefreshLayout;
import com.kingpin.ui.widget.TabDividerItemDecorator;

import java.util.ArrayList;

import butterknife.BindView;

public class TopKingpinFragment extends BaseFragment<TopKingpinView, TopKingpinPresenter> implements TopKingpinView, View.OnClickListener, SpanListener {

    @BindView(R.id.btn_filter) ImageView mBtnFilter;
    @BindView(R.id.btn_setting) ImageView mBtnSetting;
    @BindView(R.id.btn_cart) ImageView mBtnCart;
    @BindView(R.id.bubble_cart) TextView mBubbleView;
    @BindView(R.id.list_filter) RecyclerView mFilterView;
    @BindView(R.id.lyt_pull) RefreshLayout mRefreshView;
    @BindView(R.id.list_kingpin) RecyclerView mKingpinView;
    @BindView(R.id.txt_hint) TextView mHintView;

    private FilterAdapter mFilterAdapter;
    private KingpinAdapter mKingpinAdapter;
    private int mCurrentPage;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_top_kingpin;
    }

    @NonNull
    @Override
    public TopKingpinPresenter createPresenter() {
        return new TopKingpinPresenter();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        DataManager.getInstance().getFilterOptions().setInitializeFilterOption();

        mBtnFilter.setOnClickListener(this);
        mBtnSetting.setOnClickListener(this);
        mBtnCart.setOnClickListener(this);

        mCurrentPage = 0;

        makeSpanHint();

        ArrayList<Integer> filterList = new ArrayList<>();
        for(int index = 0; index < 6; index++)
            filterList.add(index);

        mFilterView.setLayoutManager(new LinearLayoutManager(getCurrentActivity(), LinearLayoutManager.HORIZONTAL, false));
        mFilterAdapter = new FilterAdapter(filterList, this);
        mFilterView.setAdapter(mFilterAdapter);

        Drawable drawable = ContextCompat.getDrawable(getCurrentActivity(), R.drawable.drawable_tab_decorator);
        if(drawable != null)
            mFilterView.addItemDecoration(new TabDividerItemDecorator(drawable, TabDividerItemDecorator.HORIZONTAL));


        mKingpinView.setLayoutManager(new LinearLayoutManager(getCurrentActivity(), LinearLayout.VERTICAL, false));
        mKingpinAdapter = new KingpinAdapter(false, this);
        mKingpinView.setAdapter(new RecyclerAdapterWithHF(mKingpinAdapter));
        mKingpinAdapter.updateKingpins();

        mRefreshView.setPtrHandler(new PtrDefaultHandler() {
            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                mCurrentPage = 0;
                getPresenter().getTopKingpins(mCurrentPage);
            }
        });

        mRefreshView.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void loadMore() {
                getPresenter().getTopKingpins(mCurrentPage);
            }
        });
        mRefreshView.setLoadMoreEnable(true);

        mRefreshView.post(new Runnable() {
            @Override
            public void run() {
                refreshKingpins(0);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        showCartBadge(mBubbleView);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_filter:
                OnFilter(this);
                break;
            case R.id.btn_setting:
                OnSetting();
                break;
            case R.id.btn_cart:
                gotoCartScreen();
                break;
        }
    }

    @Override
    public void onClicekedSpan(String spanString) {
        String strHint2 = getCurrentActivity().getResources().getString(R.string.hint_kingpin2);
        String strHint4 = getCurrentActivity().getResources().getString(R.string.hint_kingpin4);

        if(spanString.equals(strHint2)) {
            gotoRuleScreen();
        }
        else if(spanString.equals(strHint4)) {
            if(DataManager.getInstance().isLoggedInStatus())
                updateProfileWithInAppPurchase();
            else
                getCurrentActivity().openLandingScreen();
        }
    }

    @Override
    public void onSort(int filter_type) {
        if(TempDataManager.getInstance().getSelectedFilter() == filter_type) {
            TempDataManager.getInstance().setFilterRevert(!TempDataManager.getInstance().isFilterRevert());
        }
        else {
            TempDataManager.getInstance().setFilterRevert(false);
            TempDataManager.getInstance().setSelectedFilter(filter_type);
        }

        refreshKingpins(filter_type);
    }

    private void refreshKingpins(int filter_type) {
        mFilterAdapter.updateFilter(TempDataManager.getInstance().getSelectedFilter());
        TempDataManager.getInstance().setKingpins(new ArrayList<Kingpin>());
        mKingpinAdapter.updateKingpins();
        mCurrentPage = 0;
        mRefreshView.refreshComplete();
        mRefreshView.loadMoreComplete(true);
        mRefreshView.autoRefresh(true);
    }

    @Override
    public void onViewUserProfile(Kingpin kingpin) {
        Profile me = DataManager.getInstance().getAccount();
        if(me != null && kingpin.getId() == me.getId()) {
            ((HomeActivity)getCurrentActivity()).openMyStaticsTab();
        } else {
            gotoKingpinProfileScreen(kingpin, null, false);
        }
    }

    @Override
    public void onUpcomingBets(Kingpin kingpin) {
        gotoKingpinProfileScreen(kingpin, null, true);
    }

    @Override
    public void onLoadKingpins(ArrayList<Kingpin> kingpins, int current_page) {
        mCurrentPage = current_page;

        mKingpinAdapter.updateKingpins();
        mRefreshView.refreshComplete();
        mRefreshView.loadMoreComplete(true);
    }

    @Override
    public void onLoadFailedKingpins() {
        mRefreshView.refreshComplete();
        mRefreshView.loadMoreComplete(true);
    }

    @Override
    public void OnChangedFilterOption() {
        refreshKingpins(TempDataManager.getInstance().getSelectedFilter());
    }

    private void makeSpanHint() {
        SpannableStringBuilder builder = new SpannableStringBuilder();

        String strHint1 = String.format(getCurrentActivity().getResources().getString(R.string.hint_kingpin1), DataManager.getInstance().getMonthlyPot());
        String strHint2 = getCurrentActivity().getResources().getString(R.string.hint_kingpin2);

        SpannableString spanHint1 = new SpannableString(strHint1);
        spanHint1.setSpan(new ForegroundColorSpan(getCurrentActivity().getResources().getColor(R.color.color_white)), 0, strHint1.length(), 0);
        builder.append(spanHint1);
        builder.append(" ");

        SpannableString spanHint2 = new SpannableString(strHint2);
        spanHint2.setSpan(new ActionSpan(strHint2, getCurrentActivity().getResources().getColor(R.color.color_white), this), 0, strHint2.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append(spanHint2);

        if (DataManager.getInstance().isTrialAvailable() && !DataManager.getInstance().isSubscribed()) {
            String strHint3 = "";
            if (hasValidPromoCode()) {
                strHint3 = getCurrentActivity().getResources().getString(R.string.hint_kingpin3_with_promo) + " ";
            } else {
                strHint3 = getCurrentActivity().getResources().getString(R.string.hint_kingpin3) + " ";
            }
            String strHint4 = getCurrentActivity().getResources().getString(R.string.hint_kingpin4);

            SpannableString spanHint3 = new SpannableString(strHint3);
            spanHint3.setSpan(new ForegroundColorSpan(getCurrentActivity().getResources().getColor(R.color.color_white)), 0, strHint3.length(), 0);
            builder.append(spanHint3);
            builder.append(" ");

            SpannableString spanHint4 = new SpannableString(strHint4);
            spanHint4.setSpan(new ActionSpan(strHint4, getCurrentActivity().getResources().getColor(R.color.color_white), this), 0, strHint4.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            builder.append(spanHint4);
        }

        mHintView.setText(builder, TextView.BufferType.SPANNABLE);
        mHintView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private boolean hasValidPromoCode() {
        Profile me = DataManager.getInstance().getAccount();
        if (me != null && me.isVaildAffiliateCode()) { // has valid promo code
            return true;
        } else {
            return false;
        }
    }
}
