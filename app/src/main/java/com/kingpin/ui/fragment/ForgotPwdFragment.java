package com.kingpin.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.kingpin.R;
import com.kingpin.common.utility.CommonUtils;
import com.kingpin.listener.UIListener;
import com.kingpin.mvp.presenter.ForgotPwdPresenter;
import com.kingpin.mvp.view.ForgotPwdView;

import butterknife.BindView;

public class ForgotPwdFragment extends BaseFragment<ForgotPwdView, ForgotPwdPresenter> implements ForgotPwdView, View.OnClickListener {

    @BindView(R.id.btn_back) TextView btnBack;
    @BindView(R.id.btn_send) TextView btnSend;
    @BindView(R.id.edit_email) EditText editEmail;
    @BindView(R.id.edit_code) EditText editCode;
    @BindView(R.id.edit_password) EditText editPassword;
    @BindView(R.id.edit_confirm_password) EditText editPasswordConfirm;
    @BindView(R.id.btn_change) TextView btnChange;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_forgot_pwd;
    }

    @NonNull
    @Override
    public ForgotPwdPresenter createPresenter() {
        return new ForgotPwdPresenter();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnBack.setOnClickListener(this);
        btnSend.setOnClickListener(this);
        btnChange.setOnClickListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                updateFragment(UIListener.StartActivityListener.FRAGMENT_LAND);
                break;
            case R.id.btn_send:
                sendEmail();
                break;
            case R.id.btn_change:
                changePassword();
                break;
        }
    }

    private void sendEmail() {
        String email = editEmail.getText().toString().trim();
        if(!CommonUtils.isValidEmail(email)) {
            showToast(R.string.toast_valid_email);
            return;
        }

        hideKeyboard();
        getPresenter().requestCode(email);
    }

    private void changePassword() {
        String email = editEmail.getText().toString().trim();
        String code = editCode.getText().toString().trim();
        String password = editPassword.getText().toString().trim();
        String confirm = editPasswordConfirm.getText().toString().trim();

        if(!CommonUtils.isValidEmail(email)) {
            showToast(R.string.toast_valid_email);
            return;
        }

        if(code.length() == 0) {
            showToast(R.string.toast_input_code);
            return;
        }

        if(password.length() == 0) {
            showToast(R.string.toast_input_password);
            return;
        }

        if(!password.equals(confirm)) {
            showToast(R.string.toast_confirm_password);
            return;
        }

        hideKeyboard();
        getPresenter().changePassword(email, code, password);
    }
}