package com.kingpin.ui.fragment;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.kingpin.R;
import com.kingpin.common.manager.DataManager;
import com.kingpin.listener.UIListener;
import com.kingpin.mvp.model.Profile;
import com.kingpin.mvp.presenter.SettingPresenter;
import com.kingpin.mvp.view.SettingView;
import com.kingpin.ui.activity.HomeActivity;
import com.makeramen.roundedimageview.RoundedImageView;

import butterknife.BindView;

public class SettingFragment extends BaseFragment<SettingView, SettingPresenter> implements SettingView, View.OnClickListener, Animation.AnimationListener {

    private static final int ANIM_NONE = 0;
    private static final int ANIM_SHOW = 1;
    private static final int ANIM_HIDE = 2;

    @BindView(R.id.lyt_back) FrameLayout layoutBack;
    @BindView(R.id.lyt_main) FrameLayout layoutMain;
    @BindView(R.id.img_profile) RoundedImageView profileView;
    @BindView(R.id.txt_name) TextView nameView;
    @BindView(R.id.txt_balance) TextView balanceView;
    @BindView(R.id.menu_profile) TextView menuProfile;
    @BindView(R.id.menu_top_kingpin) TextView menuTopKingpin;
    @BindView(R.id.menu_share) TextView menuShare;
    @BindView(R.id.menu_rules) TextView menuRules;
    @BindView(R.id.menu_contact) TextView menuContact;
    @BindView(R.id.menu_logout) TextView menuLogout;

    private int anim_state;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_setting;
    }

    @NonNull
    @Override
    public SettingPresenter createPresenter() {
        return new SettingPresenter();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        layoutBack.setOnClickListener(this);
        layoutMain.setOnClickListener(this);
        menuProfile.setOnClickListener(this);
        menuTopKingpin.setOnClickListener(this);
        menuShare.setOnClickListener(this);
        menuRules.setOnClickListener(this);
        menuContact.setOnClickListener(this);
        menuLogout.setOnClickListener(this);

        init();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lyt_back:
                hide();
                break;
            case R.id.lyt_main:
                break;
            case R.id.menu_profile:
                hide();
                gotoProfileScreen();
                break;
            case R.id.menu_top_kingpin:
                hide();
                updateFragment(UIListener.HomeActivityListener.FRAGMENT_TOP_KINGPIN);
                break;
            case R.id.menu_share:
                hide();
                shareApp();
                break;
            case R.id.menu_rules:
                hide();
                gotoRuleScreen();
                break;
            case R.id.menu_contact:
                hide();
                openUrl("https://kingpin.pro/contact-us");
                break;
            case R.id.menu_logout:
                getPresenter().signout();
                break;
        }
    }

    @Override
    public void DownloadAvatarCompleted(Bitmap bitmap) {
        if(bitmap != null && profileView != null)
            profileView.setImageBitmap(bitmap);
    }

    @Override
    public void onAnimationStart(Animation animation) {
        switch (anim_state) {
            case ANIM_SHOW:
                layoutBack.setVisibility(View.VISIBLE);
                layoutMain.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        switch (anim_state) {
            case ANIM_HIDE:
                layoutBack.setVisibility(View.INVISIBLE);
                layoutMain.setVisibility(View.INVISIBLE);
                removeFragment(UIListener.HomeActivityListener.FRAGMENT_SETTING);
                break;
        }

        layoutBack.clearAnimation();
        layoutMain.clearAnimation();
        anim_state = ANIM_NONE;
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    private void init() {
        layoutBack.setVisibility(View.INVISIBLE);
        layoutMain.setVisibility(View.INVISIBLE);

        Profile me = DataManager.getInstance().getAccount();
        if (me == null) {
            return;
        }

        nameView.setText(me.getName());
        balanceView.setText(String.format(getCurrentActivity().getResources().getString(R.string.text_balance), (int)DataManager.getInstance().getBalance()));

        Bitmap bitmap = downloadImage(me.getAvatar());
        if(bitmap != null || profileView != null)
            profileView.setImageBitmap(bitmap);

        anim_state = ANIM_NONE;
        layoutBack.post(new Runnable() {
            @Override
            public void run() {
                show();
            }
        });
    }

    private void show() {
        if(anim_state != ANIM_NONE)
            return;

        anim_state = ANIM_SHOW;
        animate();
    }

    @Override
    public void hide() {
        if(anim_state != ANIM_NONE)
            return;

        anim_state = ANIM_HIDE;
        animate();
    }

    private void animate() {
        int anim_width = layoutMain.getMeasuredWidth();

        TranslateAnimation slideAnim;
        AlphaAnimation alphaAnim;
        switch (anim_state) {
            case ANIM_SHOW:
                slideAnim = new TranslateAnimation(anim_width, 0, 0, 0);
                alphaAnim = new AlphaAnimation(0.0f, 1.0f);
                break;
            case ANIM_HIDE:
                slideAnim = new TranslateAnimation(0, anim_width, 0, 0);
                alphaAnim = new AlphaAnimation(1.0f, 0.0f);
                break;
            default:
                return;
        }

        slideAnim.setDuration(300);
        slideAnim.setFillAfter(true);
        slideAnim.setAnimationListener(this);

        alphaAnim.setDuration(300);
        alphaAnim.setFillAfter(true);

        layoutBack.startAnimation(alphaAnim);
        layoutMain.startAnimation(slideAnim);
    }

    @Override
    public void onLoggedOut() {
        DataManager.getInstance().signout(getCurrentActivity());
        hide();
        ((HomeActivity)getCurrentActivity()).openUpcomingGamesTab();
    }
}

