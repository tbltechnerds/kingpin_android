package com.kingpin.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kingpin.R;
import com.kingpin.common.base.Constant;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.manager.TempDataManager;
import com.kingpin.listener.FilterListener;
import com.kingpin.listener.UIListener;
import com.kingpin.mvp.presenter.FilterPresenter;
import com.kingpin.mvp.view.FilterView;

import butterknife.BindView;

public class FilterFragment extends BaseFragment<FilterView, FilterPresenter> implements FilterView, View.OnClickListener, Animation.AnimationListener {

    private static final int ANIM_NONE = 0;
    private static final int ANIM_SHOW = 1;
    private static final int ANIM_HIDE = 2;

    @BindView(R.id.lyt_back) FrameLayout layoutBack;
    @BindView(R.id.lyt_main) LinearLayout layoutMain;
    @BindView(R.id.lyt_active_kingpin) LinearLayout layoutActiveKingpin;
    @BindView(R.id.lyt_search) LinearLayout layoutSearch;
    @BindView(R.id.lyt_time_period) LinearLayout layoutTimePeriod;
    @BindView(R.id.lyt_bettype) LinearLayout layoutBetType;
    @BindView(R.id.btn_back) ImageView btnBack;
    @BindView(R.id.btn_filter) TextView btnFilter;
    @BindView(R.id.btn_close_search) ImageView btnCloseSearch;
    @BindView(R.id.edit_search) EditText editSearch;

    @BindView(R.id.check_active_kingpin) CheckBox checkActiveKingpin;

    @BindView(R.id.check_all) CheckBox checkTimeAll;
    @BindView(R.id.check_yesterday) CheckBox checkTimeYesterday;
    @BindView(R.id.check_one_week) CheckBox checkTimeOneWeek;
    @BindView(R.id.check_one_month) CheckBox checkTimeOneMonth;
    @BindView(R.id.check_three_month) CheckBox checkTimeThreeMonth;
    @BindView(R.id.check_six_month) CheckBox checkTimeSixMonth;
    @BindView(R.id.check_one_year) CheckBox checkTimeOneYear;

    // sport
    @BindView(R.id.check_sport_all) CheckBox checkSportAll;

    // football
    @BindView(R.id.check_football) CheckBox checkFootball;
    @BindView(R.id.check_football_nfl) CheckBox checkFootballNFL;
    @BindView(R.id.check_football_nfl_1st_half) CheckBox checkFootballNFL1stHalf;
    @BindView(R.id.check_football_nfl_1st_quarter) CheckBox checkFootballNFL1stQuarter;
    @BindView(R.id.check_football_ncaaf) CheckBox checkFootballNCAAF;
    @BindView(R.id.check_football_ncaaf_1st_half) CheckBox checkFootballNCAAF1stHalf;
    @BindView(R.id.check_football_cfl) CheckBox checkFootballCFL;

    // basketball
    @BindView(R.id.check_basketball) CheckBox checkBasketball;
    @BindView(R.id.check_basketball_nba) CheckBox checkBasketballNBA;
    @BindView(R.id.check_basketball_nba_1st_half) CheckBox checkBasketballNBA1stHalf;
    @BindView(R.id.check_basketball_ncaa_full_game) CheckBox checkBasketballNCAAB;
    @BindView(R.id.check_basketball_ncaa_1st_half) CheckBox checkBasketballNCAAB1stHalf;
    @BindView(R.id.check_basketball_wnba) CheckBox checkBasketballWNBA;

    // baseball
    @BindView(R.id.check_baseball) CheckBox checkBaseball;
    @BindView(R.id.check_baseball_mlb) CheckBox checkBaseballMLB;
    @BindView(R.id.check_baseball_mlb_1st_five) CheckBox checkBaseballMLB1stFive;

    // hockey
    @BindView(R.id.check_hockey) CheckBox checkHockey;
    @BindView(R.id.check_hockey_nhl) CheckBox checkHockeyNHL;
    @BindView(R.id.check_hockey_nhl_1st_period) CheckBox checkHockeyNHL1stPeriod;

    // soccer
    @BindView(R.id.check_soccer) CheckBox checkSoccer;
    @BindView(R.id.check_soccer_english_premier_league) CheckBox checkSoccerEnglishPremierLeague;
    @BindView(R.id.check_soccer_english_championship) CheckBox checkSoccerEnglishChampionship;
    @BindView(R.id.check_soccer_english_facup) CheckBox checkSoccerEnglishFACup;
    @BindView(R.id.check_soccer_spanish_la_liga) CheckBox checkSoccerSpanishLaLiga;
    @BindView(R.id.check_soccer_german_bundesliga1) CheckBox checkSoccerGermanBundesliga1;
    @BindView(R.id.check_soccer_french_ligue1) CheckBox checkSoccerFrenchLigue;
    @BindView(R.id.check_soccer_italian_serie_a) CheckBox checkSoccerItalianSerieA;
    @BindView(R.id.check_soccer_uefa_europa_league) CheckBox checkSoccerUEFAEuropaLeague;
    @BindView(R.id.check_soccer_uefa_champions_league) CheckBox checkSoccerUEFAChampionLeague;
    @BindView(R.id.check_soccer_mls) CheckBox checkSoccerMLS;
    @BindView(R.id.check_soccer_fifa_worldcup) CheckBox checkSoccerFIFAWorldcup;

    @BindView(R.id.check_tennis) CheckBox checkTennis;
    @BindView(R.id.check_boxing) CheckBox checkBoxing;
    @BindView(R.id.check_mma) CheckBox checkMMA;

    @BindView(R.id.check_bettype_all) CheckBox checkBetTypeAll;
    @BindView(R.id.check_moneyline) CheckBox checkMoneyline;
    @BindView(R.id.check_spread) CheckBox checkSpread;
    @BindView(R.id.check_overunder) CheckBox checkOverunder;
    @BindView(R.id.check_parlay) CheckBox checkParlay;

    private int anim_state;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_filter;
    }

    @NonNull
    @Override
    public FilterPresenter createPresenter() {
        return new FilterPresenter();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        layoutBack.setOnClickListener(this);
        btnBack.setOnClickListener(this);
        btnFilter.setOnClickListener(this);
        btnCloseSearch.setOnClickListener(this);

        editSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus)
                    getCurrentActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
            }
        });

        checkActiveKingpin.setOnClickListener(this);
        checkTimeAll.setOnClickListener(this);
        checkTimeYesterday.setOnClickListener(this);
        checkTimeOneWeek.setOnClickListener(this);
        checkTimeOneMonth.setOnClickListener(this);
        checkTimeOneYear.setOnClickListener(this);
        checkTimeThreeMonth.setOnClickListener(this);
        checkTimeSixMonth.setOnClickListener(this);

        checkSportAll.setOnClickListener(this);

        checkFootball.setOnClickListener(this);
        checkFootballNFL.setOnClickListener(this);
        checkFootballNFL1stHalf.setOnClickListener(this);
        checkFootballNFL1stQuarter.setOnClickListener(this);
        checkFootballNCAAF.setOnClickListener(this);
        checkFootballNCAAF1stHalf.setOnClickListener(this);
        checkFootballCFL.setOnClickListener(this);

        checkBasketball.setOnClickListener(this);
        checkBasketballNBA.setOnClickListener(this);
        checkBasketballNBA1stHalf.setOnClickListener(this);
        checkBasketballNCAAB.setOnClickListener(this);
        checkBasketballNCAAB1stHalf.setOnClickListener(this);
        checkBasketballWNBA.setOnClickListener(this);

        checkBaseball.setOnClickListener(this);
        checkBaseballMLB.setOnClickListener(this);
        checkBaseballMLB1stFive.setOnClickListener(this);

        checkHockey.setOnClickListener(this);
        checkHockeyNHL.setOnClickListener(this);
        checkHockeyNHL1stPeriod.setOnClickListener(this);

        checkSoccer.setOnClickListener(this);
        checkSoccerEnglishPremierLeague.setOnClickListener(this);
        checkSoccerEnglishChampionship.setOnClickListener(this);
        checkSoccerEnglishFACup.setOnClickListener(this);
        checkSoccerSpanishLaLiga.setOnClickListener(this);
        checkSoccerGermanBundesliga1.setOnClickListener(this);
        checkSoccerFrenchLigue.setOnClickListener(this);
        checkSoccerItalianSerieA.setOnClickListener(this);
        checkSoccerUEFAEuropaLeague.setOnClickListener(this);
        checkSoccerUEFAChampionLeague.setOnClickListener(this);
        checkSoccerMLS.setOnClickListener(this);
        checkSoccerFIFAWorldcup.setOnClickListener(this);

        checkTennis.setOnClickListener(this);
        checkBoxing.setOnClickListener(this);
        checkMMA.setOnClickListener(this);

        checkBetTypeAll.setOnClickListener(this);
        checkMoneyline.setOnClickListener(this);
        checkSpread.setOnClickListener(this);
        checkOverunder.setOnClickListener(this);
        checkParlay.setOnClickListener(this);

        init();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lyt_back:
            case R.id.btn_back:
                hide();
                return;
            case R.id.btn_filter:
                save();
                return;
            case R.id.btn_close_search:
                hideKeyboard();
                editSearch.setText("");
                return;
            case R.id.check_active_kingpin:
                DataManager.getInstance().getFilterOptions().setOnlyActiveKingpin(checkActiveKingpin.isChecked());
                break;
            case R.id.check_all:
                DataManager.getInstance().getFilterOptions().setTimePeriod(Constant.TIME_PERIOD.ALL);
                break;
            case R.id.check_yesterday:
                DataManager.getInstance().getFilterOptions().setTimePeriod(Constant.TIME_PERIOD.YESTERDAY);
                break;
            case R.id.check_one_week:
                DataManager.getInstance().getFilterOptions().setTimePeriod(Constant.TIME_PERIOD.ONEWEEK);
                break;
            case R.id.check_one_month:
                DataManager.getInstance().getFilterOptions().setTimePeriod(Constant.TIME_PERIOD.ONEMONTH);
                break;
            case R.id.check_one_year:
                DataManager.getInstance().getFilterOptions().setTimePeriod(Constant.TIME_PERIOD.ONEYEAR);
                break;
            case R.id.check_three_month:
                DataManager.getInstance().getFilterOptions().setTimePeriod(Constant.TIME_PERIOD.THREEMONTH);
                break;
            case R.id.check_six_month:
                DataManager.getInstance().getFilterOptions().setTimePeriod(Constant.TIME_PERIOD.SIXMONTH);
                break;

            case R.id.check_sport_all:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.ALL);
                break;

            case R.id.check_football:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.FOOTBALL);
                break;
            case R.id.check_football_nfl:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.FOOTBALLNFL);
                break;
            case R.id.check_football_nfl_1st_half:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.FOOTBALLNFL1stHalf);
                break;
            case R.id.check_football_nfl_1st_quarter:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.FOOTBALLNFL1stQuarter);
                break;
            case R.id.check_football_ncaaf:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.FOOTBALLNCAAF);
                break;
            case R.id.check_football_ncaaf_1st_half:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.FOOTBALLNCAAF1stHalf);
                break;
            case R.id.check_football_cfl:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.FOOTBALLCFL);
                break;

            case R.id.check_basketball:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.BASKETBALL);
                break;
            case R.id.check_basketball_nba:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.BASKETBALLNBA);
                break;
            case R.id.check_basketball_nba_1st_half:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.BASKETBALLNBA1stHalf);
                break;
            case R.id.check_basketball_ncaa_full_game:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.BASKETBALLNCAAB);
                break;
            case R.id.check_basketball_ncaa_1st_half:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.BASKETBALLNCAAB1stHalf);
                break;
            case R.id.check_basketball_wnba:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.BASKETBALLWNBA);
                break;

            case R.id.check_baseball:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.BASEBALL);
                break;
            case R.id.check_baseball_mlb:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.BASEBALLMLB);
                break;
            case R.id.check_baseball_mlb_1st_five:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.BASEBALLMLB1stFive);
                break;

            case R.id.check_hockey:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.HOCKEY);
                break;
            case R.id.check_hockey_nhl:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.HOCKEYNHL);
                break;
            case R.id.check_hockey_nhl_1st_period:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.HOCKEYNHL1stPeriod);
                break;

            case R.id.check_soccer:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.SOCCER);
                break;
            case R.id.check_soccer_english_premier_league:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.SOCCEREnglishPremierLeague);
                break;
            case R.id.check_soccer_english_championship:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.SOCCEREnglishChampionship);
                break;
            case R.id.check_soccer_english_facup:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.SOCCEREnglishFACup);
                break;
            case R.id.check_soccer_spanish_la_liga:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.SOCCERSpanishLaLiga);
                break;
            case R.id.check_soccer_german_bundesliga1:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.SOCCERGermanBundesliga1);
                break;
            case R.id.check_soccer_french_ligue1:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.SOCCERFrenchLigue);
                break;
            case R.id.check_soccer_italian_serie_a:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.SOCCERItalianSerieA);
                break;
            case R.id.check_soccer_uefa_europa_league:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.SOCCERUEFAEuropaLeague);
                break;
            case R.id.check_soccer_uefa_champions_league:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.SOCCERUEFAChampionLeague);
                break;
            case R.id.check_soccer_mls:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.SOCCERMLS);
                break;
            case R.id.check_soccer_fifa_worldcup:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.SOCCERFIFAWorldcup);
                break;

            case R.id.check_tennis:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.TENNIS);
                break;
            case R.id.check_boxing:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.BOXING);
                break;
            case R.id.check_mma:
                DataManager.getInstance().getFilterOptions().setSport(Constant.FILTER_SPORT.MMA);
                break;

            case R.id.check_bettype_all:
                DataManager.getInstance().getFilterOptions().setBetType(Constant.FILTER_BETTYPE.ALL);
                break;
            case R.id.check_moneyline:
                DataManager.getInstance().getFilterOptions().setBetType(Constant.FILTER_BETTYPE.MONEYLINE);
                break;
            case R.id.check_spread:
                DataManager.getInstance().getFilterOptions().setBetType(Constant.FILTER_BETTYPE.SPREAD);
                break;
            case R.id.check_overunder:
                DataManager.getInstance().getFilterOptions().setBetType(Constant.FILTER_BETTYPE.OVER_UNDER);
                break;
            case R.id.check_parlay:
                DataManager.getInstance().getFilterOptions().setBetType(Constant.FILTER_BETTYPE.PARLAY);
                break;
        }

        editSearch.clearFocus();
        editSearch.requestLayout();
        updateUI();
    }

    @Override
    public void onAnimationStart(Animation animation) {
        switch (anim_state) {
            case ANIM_SHOW:
                layoutBack.setVisibility(View.VISIBLE);
                layoutMain.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        switch (anim_state) {
            case ANIM_HIDE:
                layoutBack.setVisibility(View.INVISIBLE);
                layoutMain.setVisibility(View.INVISIBLE);
                removeFragment(UIListener.HomeActivityListener.FRAGMENT_FILTER);
                break;
            case ANIM_SHOW:
                editSearch.clearFocus();
                editSearch.requestLayout();
                break;
        }

        layoutBack.clearAnimation();
        layoutMain.clearAnimation();
        anim_state = ANIM_NONE;
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    private void init() {

        layoutBack.setVisibility(View.INVISIBLE);
        layoutMain.setVisibility(View.INVISIBLE);

        anim_state = ANIM_NONE;
        updateUI();

        layoutBack.post(new Runnable() {
            @Override
            public void run() {
                show();
            }
        });
    }

    private void updateUI() {
        editSearch.setText(DataManager.getInstance().getFilterOptions().getKeyword());

        checkActiveKingpin.setChecked(DataManager.getInstance().getFilterOptions().isOnlyActiveKingpin());

        checkTimeAll.setChecked(DataManager.getInstance().getFilterOptions().getTimePeriod() == Constant.TIME_PERIOD.ALL);
        checkTimeYesterday.setChecked(DataManager.getInstance().getFilterOptions().getTimePeriod() == Constant.TIME_PERIOD.YESTERDAY);
        checkTimeOneWeek.setChecked(DataManager.getInstance().getFilterOptions().getTimePeriod() == Constant.TIME_PERIOD.ONEWEEK);
        checkTimeOneMonth.setChecked(DataManager.getInstance().getFilterOptions().getTimePeriod() == Constant.TIME_PERIOD.ONEMONTH);
        checkTimeThreeMonth.setChecked(DataManager.getInstance().getFilterOptions().getTimePeriod() == Constant.TIME_PERIOD.THREEMONTH);
        checkTimeSixMonth.setChecked(DataManager.getInstance().getFilterOptions().getTimePeriod() == Constant.TIME_PERIOD.SIXMONTH);
        checkTimeOneYear.setChecked(DataManager.getInstance().getFilterOptions().getTimePeriod() == Constant.TIME_PERIOD.ONEYEAR);

        checkSportAll.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.ALL));

        checkFootball.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.FOOTBALL));
        checkFootballNFL.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.FOOTBALLNFL));
        checkFootballNFL1stHalf.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.FOOTBALLNFL1stHalf));
        checkFootballNFL1stQuarter.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.FOOTBALLNFL1stQuarter));
        checkFootballNCAAF.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.FOOTBALLNCAAF));
        checkFootballNCAAF1stHalf.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.FOOTBALLNCAAF1stHalf));
        checkFootballCFL.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.FOOTBALLCFL));

        checkBasketball.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.BASKETBALL));
        checkBasketballNBA.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.BASKETBALLNBA));
        checkBasketballNBA1stHalf.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.BASKETBALLNBA1stHalf));
        checkBasketballNCAAB.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.BASKETBALLNCAAB));
        checkBasketballNCAAB1stHalf.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.BASKETBALLNCAAB1stHalf));
        checkBasketballWNBA.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.BASKETBALLWNBA));

        checkBaseball.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.BASEBALL));
        checkBaseballMLB.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.BASEBALLMLB));
        checkBaseballMLB1stFive.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.BASEBALLMLB1stFive));

        checkHockey.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.HOCKEY));
        checkHockeyNHL.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.HOCKEYNHL));
        checkHockeyNHL1stPeriod.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.HOCKEYNHL1stPeriod));

        checkSoccer.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.SOCCER));
        checkSoccerEnglishPremierLeague.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.SOCCEREnglishPremierLeague));
        checkSoccerEnglishChampionship.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.SOCCEREnglishChampionship));
        checkSoccerEnglishFACup.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.SOCCEREnglishFACup));
        checkSoccerSpanishLaLiga.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.SOCCERSpanishLaLiga));
        checkSoccerGermanBundesliga1.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.SOCCERGermanBundesliga1));
        checkSoccerFrenchLigue.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.SOCCERFrenchLigue));
        checkSoccerItalianSerieA.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.SOCCERItalianSerieA));
        checkSoccerUEFAEuropaLeague.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.SOCCERUEFAEuropaLeague));
        checkSoccerUEFAChampionLeague.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.SOCCERUEFAChampionLeague));
        checkSoccerMLS.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.SOCCERMLS));
        checkSoccerFIFAWorldcup.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.SOCCERFIFAWorldcup));

        checkTennis.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.TENNIS));
        checkBoxing.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.BOXING));
        checkMMA.setChecked(DataManager.getInstance().isSelectedSport(Constant.FILTER_SPORT.MMA));

        checkBetTypeAll.setChecked(DataManager.getInstance().isSelectedBetType(Constant.FILTER_BETTYPE.ALL));
        checkMoneyline.setChecked(DataManager.getInstance().isSelectedBetType(Constant.FILTER_BETTYPE.MONEYLINE));
        checkSpread.setChecked(DataManager.getInstance().isSelectedBetType(Constant.FILTER_BETTYPE.SPREAD));
        checkOverunder.setChecked(DataManager.getInstance().isSelectedBetType(Constant.FILTER_BETTYPE.OVER_UNDER));
        checkParlay.setChecked(DataManager.getInstance().isSelectedBetType(Constant.FILTER_BETTYPE.PARLAY));

        layoutActiveKingpin.setVisibility(TempDataManager.getInstance().isDisableActiveFilter() ? View.GONE : View.VISIBLE);
        layoutSearch.setVisibility(TempDataManager.getInstance().isDisableSearchFilter() ? View.GONE : View.VISIBLE);
        layoutTimePeriod.setVisibility(TempDataManager.getInstance().isDisableTimePeriodFilter() ? View.GONE : View.VISIBLE);
    }

    private void show() {
        if(anim_state != ANIM_NONE)
            return;

        anim_state = ANIM_SHOW;
        animate();
    }

    private void hide() {
        if(anim_state != ANIM_NONE)
            return;

        anim_state = ANIM_HIDE;
        animate();
    }

    private void animate() {

        AlphaAnimation alphaAnim;
        switch (anim_state) {
            case ANIM_SHOW:
                alphaAnim = new AlphaAnimation(0.0f, 1.0f);
                break;
            case ANIM_HIDE:
                alphaAnim = new AlphaAnimation(1.0f, 0.0f);
                break;
            default:
                return;
        }

        alphaAnim.setDuration(300);
        alphaAnim.setFillAfter(true);
        alphaAnim.setAnimationListener(this);

        layoutBack.startAnimation(alphaAnim);
        layoutMain.startAnimation(alphaAnim);
    }

    private void save() {
        String keyword = editSearch.getText().toString().trim();
        if(!keyword.isEmpty() && keyword.length() < 4) {
            showToast("Username should contain more than 3 characters");
            return;
        }

        DataManager.getInstance().getFilterOptions().setKeyword(keyword);
        DataManager.getInstance().getFilterOptions().saveFilterOption(getCurrentActivity());
        FilterListener listener = TempDataManager.getInstance().getmFilterListener();
        if(listener != null)
            listener.OnChangedFilterOption();
        hide();
    }
}