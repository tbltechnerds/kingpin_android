package com.kingpin.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.hannesdorfmann.mosby.mvp.MvpFragment;
import com.kcode.bottomlib.BottomDialog;
import com.kingpin.R;
import com.kingpin.common.base.Constant;
import com.kingpin.common.manager.APICallback;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.manager.FileManager;
import com.kingpin.common.manager.TempDataManager;
import com.kingpin.common.task.DownloadAvatarTask;
import com.kingpin.common.utility.CommonUtils;
import com.kingpin.listener.DownloadAvatarListener;
import com.kingpin.listener.FilterListener;
import com.kingpin.listener.UIListener;
import com.kingpin.mvp.model.BovadaGame;
import com.kingpin.mvp.model.Game;
import com.kingpin.mvp.model.Kingpin;
import com.kingpin.mvp.model.Profile;
import com.kingpin.mvp.model.RapidGame;
import com.kingpin.mvp.presenter.BasePresenter;
import com.kingpin.mvp.view.BaseView;
import com.kingpin.ui.activity.BaseActivity;
import com.kingpin.ui.activity.CartActivity;
import com.kingpin.ui.activity.ProfileActivity;
import com.kingpin.ui.activity.GameDetailActivity;
import com.kingpin.ui.activity.RuleActivity;
import com.kingpin.ui.activity.StartActivity;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseFragment<V extends BaseView, P extends BasePresenter<V>> extends MvpFragment<V, P> implements BaseView, DownloadAvatarListener, FilterListener {

    private Unbinder unbinder;
    private UIListener.BaseActivityListener uiListener;

    protected boolean isDisableTimeperiodFilter = false;
    protected boolean isDisableActiveFilter = false;
    protected boolean isDisableSearchUserNameFilter = false;
    protected boolean isDisableBetTypeFilter = false;

    @LayoutRes
    protected abstract int getLayoutRes();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getLayoutRes(), container, false);
    }

    @CallSuper
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        unbinder = ButterKnife.bind(this, view);
        super.onViewCreated(view, savedInstanceState);
        APICallback.getInstance().registerPresenter(getPresenter());
    }

    @CallSuper
    @Override
    public void onDestroyView() {
        unbinder.unbind();
        APICallback.getInstance().unregisterPresenter(getPresenter());
        super.onDestroyView();
    }

    @CallSuper
    @Override
    public void showLoading() {
        if(this.uiListener != null)
            this.uiListener.showLoading();
    }

    @CallSuper
    @Override
    public void hideLoading() {
        if(this.uiListener != null)
            this.uiListener.hideLoading();
    }

    @Override
    public void showToast(@StringRes int messageId) {
        if(this.uiListener != null)
            this.uiListener.showToast(messageId);
    }

    @Override
    public void showToast(String message) {
        if(this.uiListener != null)
            this.uiListener.showToast(message);
    }

    @Override
    public void hideToast() {
        if(this.uiListener != null)
            this.uiListener.hideToast();
    }

    @Override
    public void updateFragment(int fragment_id) {
        if(this.uiListener != null)
            this.uiListener.updateFragment(fragment_id);
    }

    @Override
    public void onShareText(String text) {
        shareText(text);
    }

    @Override
    public void addFragment(int fragment_id) {
        if(this.uiListener != null)
            this.uiListener.addFragment(fragment_id);
    }

    @Override
    public void removeFragment(int fragment_id) {
        if(this.uiListener != null)
            this.uiListener.removeFragment(fragment_id);
    }

    @Override
    public void startIntent(Class<?> activity, int fragment, boolean bFinish) {
        if(this.uiListener != null)
            this.uiListener.startIntent(activity, fragment, bFinish);
    }

    @Override
    public void startIntent(Intent intent, int fragment, boolean bFinish) {
        if(this.uiListener != null)
            this.uiListener.startIntent(intent, fragment, bFinish);
    }

    @Override
    public BaseActivity getCurrentActivity() {
        if(this.uiListener != null)
            return this.uiListener.getActivity();
        else
            return null;
    }

    @Override
    public void DownloadAvatarCompleted(Bitmap bitmap) {
    }

    @Override
    public void OnChangedFilterOption() {

    }

    public void setUIListener(UIListener.BaseActivityListener listener) {
        this.uiListener = listener;
    }

    public void openUrl(String url) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

    public void hideKeyboard() {
        View view = getCurrentActivity().getCurrentFocus();
        if(view != null) {
            InputMethodManager imm = (InputMethodManager) getCurrentActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public Bitmap downloadImage(String image_url) {
        if(image_url == null || image_url.isEmpty())
            return null;

        String url = Constant.SERVER_HOST + "/" + image_url;
        String[] url_peaces = url.split("/");
        String file_name = url_peaces[url_peaces.length - 1];
        String file_path = CommonUtils.getFilePath(getCurrentActivity());

        Bitmap bitmap = FileManager.getBitmap(file_path + "/" + file_name);
        if(bitmap == null)
            new DownloadAvatarTask(CommonUtils.getFilePath(getCurrentActivity()), this).execute(url);
        return bitmap;
    }

    public void OnSetting() {
        if(DataManager.getInstance().isLoggedInStatus())
            getPresenter().getBalance();
        else
            showConfirm();
    }

    public void OnFilter(FilterListener listener) {
        TempDataManager.getInstance().setDisableActiveFilter(isDisableActiveFilter);
        TempDataManager.getInstance().setDisableSearchFilter(isDisableSearchUserNameFilter);
        TempDataManager.getInstance().setDisableTimePeriodFilter(isDisableTimeperiodFilter);
        TempDataManager.getInstance().setDisableBetTypeFilter(isDisableBetTypeFilter);
        TempDataManager.getInstance().setFilterListener(listener);
        addFragment(UIListener.HomeActivityListener.FRAGMENT_FILTER);
    }

    public void updateProfileWithInAppPurchase() {
        Profile me = DataManager.getInstance().getAccount();
        if(me == null || me.getMembership() != 0)
            return;

        addFragment(UIListener.HomeActivityListener.FRAGMENT_INAPP_PURCHASE);
    }

    public void gotoRuleScreen() {
        startActivity(new Intent(getCurrentActivity(), RuleActivity.class));
    }

    public void gotoCartScreen() {
        if (DataManager.getInstance().isLoggedInStatus()) {
            startIntent(CartActivity.class, UIListener.CartActivityListener.FRAGMENT_CART, false);
        } else {
            getCurrentActivity().openLandingScreen();
        }
    }

    public void gotoGameScreen(Game game) {
        TempDataManager.getInstance().setSelectedGame(game);
        if (game.isBovada()) {
            startIntent(GameDetailActivity.class, UIListener.GameDetailActivityListener.FRAGMENT_BOVADA, false);
        } else {
            startIntent(GameDetailActivity.class, UIListener.GameDetailActivityListener.FRAGMENT_RAPID, false);
        }
    }

    public void gotoKingpinProfileScreen(Kingpin kingpin, Integer kingpinId, boolean bShowUpcoming) { // default -> id : null, bshow : false
        TempDataManager.getInstance().setSelectedKingpin(kingpin);
        TempDataManager.getInstance().setSelectedKingpinId(kingpinId);
        TempDataManager.getInstance().setShowUpcoming(bShowUpcoming);
        addFragment(UIListener.HomeActivityListener.FRAGMENT_USER_PROFILE);
    }

    public void gotoProfileScreen() {
        startIntent(ProfileActivity.class, UIListener.ProfileActivityListener.FRAGMENT_PROFILE, false);
    }

    public void shareApp() {
        String shareBody = "https://itunes.apple.com/us/app/kingpin-pro-sports-picks/id1390339038?mt=8";
        shareText(shareBody);
    }

    public void shareText(String text) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, getCurrentActivity().getResources().getString(R.string.app_name));
        shareIntent.putExtra(Intent.EXTRA_TEXT, text);
        startActivity(Intent.createChooser(shareIntent, getCurrentActivity().getResources().getString(R.string.app_name)));
    }

    private void showConfirm() {
        String[] menuArray = {
                getCurrentActivity().getResources().getString(R.string.menu_login_or_register),
                getCurrentActivity().getResources().getString(R.string.menu_share1),
                getCurrentActivity().getResources().getString(R.string.menu_rules1)
        };

        BottomDialog dialog = BottomDialog.newInstance(getCurrentActivity().getResources().getString(R.string.app_name), getCurrentActivity().getResources().getString(R.string.menu_Cancel), menuArray);
        dialog.show(getChildFragmentManager(), "dialog");
        dialog.setListener(new BottomDialog.OnClickListener() {
            @Override
            public void click(int position) {
                switch (position) {
                    case 0:
                        getCurrentActivity().openLandingScreen();
                        break;
                    case 1:
                        shareApp();
                        break;
                    case 2:
                        gotoRuleScreen();
                        break;
                }
            }
        });
    }

    public void showCartBadge(TextView badgeView) {
        int numberOfBets = DataManager.getInstance().getCartBadge();
        if (numberOfBets > 0) {
            badgeView.setVisibility(View.VISIBLE);
            badgeView.setText(String.format("%d", numberOfBets));
        } else {
            badgeView.setVisibility(View.GONE);
        }
    }
}
