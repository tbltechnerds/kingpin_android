package com.kingpin.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.kingpin.R;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.manager.TempDataManager;
import com.kingpin.common.utility.DateUtils;
import com.kingpin.mvp.model.PlaceBetCart;
import com.kingpin.mvp.model.RapidGame;
import com.kingpin.mvp.presenter.RapidGamePresenter;
import com.kingpin.mvp.view.RapidGameView;

import java.util.Date;

import butterknife.BindView;

public class RapidGameFragment extends BaseFragment<RapidGameView, RapidGamePresenter> implements View.OnClickListener {

    @BindView(R.id.txt_title) TextView mTxtTitle;
    @BindView(R.id.btn_back) ImageView mBtnBack;
    @BindView(R.id.btn_cart) ImageView mBtnCart;
    @BindView(R.id.bubble_cart) TextView mBubbleView;
    @BindView(R.id.txt_moneyline_value1) TextView mTxtMoneylineValue1;
    @BindView(R.id.lyt_moneyline_x) LinearLayout mLytMoneylineX;
    @BindView(R.id.txt_moneyline_valuex) TextView mTxtMoneylineValueX;
    @BindView(R.id.txt_moneyline_value2) TextView mTxtMoneylineValue2;
    @BindView(R.id.txt_spread_balance1) TextView mTxtSpreadBalance1;
    @BindView(R.id.txt_spread_balance2) TextView mTxtSpreadBalance2;
    @BindView(R.id.txt_spread_value1) TextView mTxtSpreadValue1;
    @BindView(R.id.txt_spread_value2) TextView mTxtSpreadValue2;
    @BindView(R.id.txt_total_balance1) TextView mTxtTotalBalance1;
    @BindView(R.id.txt_total_balance2) TextView mTxtTotalBalance2;
    @BindView(R.id.txt_total_value1) TextView mTxtTotalValue1;
    @BindView(R.id.txt_total_value2) TextView mTxtTotalValue2;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_rapid_game;
    }

    @Override
    public RapidGamePresenter createPresenter() {
        return new RapidGamePresenter();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mBtnBack.setOnClickListener(this);
        mBtnCart.setOnClickListener(this);
        mBubbleView.setVisibility(View.GONE);

        mTxtMoneylineValue1.setOnClickListener(this);
        mLytMoneylineX.setVisibility(View.GONE);
        mTxtMoneylineValueX.setOnClickListener(this);
        mTxtMoneylineValue2.setOnClickListener(this);
        mTxtSpreadValue1.setOnClickListener(this);
        mTxtSpreadValue2.setOnClickListener(this);
        mTxtTotalValue1.setOnClickListener(this);
        mTxtTotalValue2.setOnClickListener(this);

        loadValues();
    }

    @Override
    public void onResume() {
        super.onResume();
        showCartBadge(mBubbleView);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_back:
                getCurrentActivity().finish();
                break;
            case R.id.btn_cart:
                gotoCartScreen();
                break;
            case R.id.txt_moneyline_value1:
            case R.id.txt_moneyline_valuex:
            case R.id.txt_moneyline_value2:
            case R.id.txt_spread_value1:
            case R.id.txt_spread_value2:
            case R.id.txt_total_value1:
            case R.id.txt_total_value2:
                onAddBetToCart(view.getId());
                break;
        }
    }

    private void onAddBetToCart(int buttonid) {
        if (!DataManager.getInstance().isLoggedInStatus()) {
            getCurrentActivity().openLandingScreen();
            return;
        }

        RapidGame game = (RapidGame) TempDataManager.getInstance().getSelectedGame();
        if (game == null) {
            showToast("Something went wrong. Please try again.");
            return;
        }

        PlaceBetCart cart = new PlaceBetCart();
        cart.teams = game.getGameName();
        cart.sport = game.getSport();
        cart.event_id = game.getEventId();

        if (game.isPassedGame()) {
            showToast("The match has already started.");
            return;
        }

        Date startTime = game.getGameDate();
        cart.match_date = DateUtils.convertLocalDateToServerDateString(startTime);

        JsonObject details = game.getGameDetails();
        if (details == null) {
            showToast("Something went wrong. Please try again.");
            return;
        }

        int line_id = 0;
        if (details.has("line_id") && !details.get("line_id").isJsonNull()) {
            line_id = details.get("line_id").getAsInt();
        }

        if (buttonid == R.id.txt_moneyline_value1 || buttonid == R.id.txt_moneyline_value2) {
            cart.bet_type = "moneyline";
            cart.bet_param = "1";
            JsonObject moneyline = game.getMoneylineData();
            if (moneyline == null) {
                showToast("Something went wrong. Please try again.");
                return;
            }

            if (DataManager.hasValidValue(moneyline, "moneyline_home") &&
                DataManager.hasValidValue(moneyline, "moneyline_away")) {

                double value1 = moneyline.get("moneyline_home").getAsDouble();
                double value2 = moneyline.get("moneyline_away").getAsDouble();

                if (buttonid == R.id.txt_moneyline_value1) {
                    cart.team_bet = game.getHomeTeamName();
                    cart.odd = String.format("%.1f", atod(value1));
                    cart.code = 1;
                    cart.line = String.format("%.1f", value1);
                    cart.marketid = String.format("%d", line_id * 10 + 1);
                    cart.opposite_lines = String.format("%.1f", value2);
                    cart.opposite_market_ids = String.format("%d", line_id * 10 + 2);
                } else {
                    cart.team_bet = game.getAwayTeamName();
                    cart.odd = String.format("%.1f", atod(value2));
                    cart.code = 2;
                    cart.line = String.format("%.1f", value2);
                    cart.marketid = String.format("%d", line_id * 10 + 2);
                    cart.opposite_lines = String.format("%.1f", value1);
                    cart.opposite_market_ids = String.format("%d", line_id * 10 + 1);
                }
            } else {
                showToast("Something went wrong. Please try again.");
                return;
            }
        } else if (buttonid == R.id.txt_spread_value1 || buttonid == R.id.txt_spread_value2) {
            cart.bet_type = "handicap";
            JsonObject spread = game.getSpreadData();
            if (spread == null) {
                showToast("Something went wrong. Please try again.");
                return;
            }

            if (DataManager.hasValidValue(spread, "point_spread_home_money") &&
                    DataManager.hasValidValue(spread, "point_spread_home") &&
                    DataManager.hasValidValue(spread, "point_spread_away_money") &&
                    DataManager.hasValidValue(spread, "point_spread_away")) {

                double balance1 = spread.get("point_spread_home").getAsDouble();
                double value1 = spread.get("point_spread_home_money").getAsDouble();
                double balance2 = spread.get("point_spread_away").getAsDouble();
                double value2 = spread.get("point_spread_away_money").getAsDouble();

                if (buttonid == R.id.txt_spread_value1) {
                    cart.bet_param = String.format("%.1f", balance1);
                    cart.team_bet = String.format("%s %.1f", game.getHomeTeamName(), balance1);
                    cart.odd = String.format("%.1f", atod(value1));
                    cart.code = 3;
                    cart.line = String.format("%.1f", value1);
                    cart.marketid = String.format("%d", line_id * 10 + 3);
                    cart.opposite_lines = String.format("%.1f", value2);
                    cart.opposite_market_ids = String.format("%d", line_id * 10 + 4);
                } else {
                    cart.bet_param = String.format("%.1f", balance2);
                    cart.team_bet = String.format("%s %.1f", game.getAwayTeamName(), balance2);
                    cart.odd = String.format("%.1f", atod(value2));
                    cart.code = 4;
                    cart.line = String.format("%.1f", value2);
                    cart.marketid = String.format("%d", line_id * 10 + 4);
                    cart.opposite_lines = String.format("%.1f", value1);
                    cart.opposite_market_ids = String.format("%d", line_id * 10 + 3);
                }
            } else {
                showToast("Something went wrong. Please try again.");
                return;
            }
        } else if (buttonid == R.id.txt_total_value1 || buttonid == R.id.txt_total_value2) {
            cart.bet_type = "total";
            JsonObject total = game.getTotalData();
            if (total == null) {
                showToast("Something went wrong. Please try again.");
                return;
            }

            if (DataManager.hasValidValue(total, "total_over_money") &&
                    DataManager.hasValidValue(total, "total_over") &&
                    DataManager.hasValidValue(total, "total_under_money") &&
                    DataManager.hasValidValue(total, "total_under")) {

                double balance1 = total.get("total_over").getAsDouble();
                double value1 = total.get("total_over_money").getAsDouble();
                double balance2 = total.get("total_under").getAsDouble();
                double value2 = total.get("total_under_money").getAsDouble();

                if (buttonid == R.id.txt_total_value1) {
                    cart.bet_param = String.format("%.1f", balance1);
                    cart.team_bet = String.format("Over %.1f", balance1);
                    cart.odd = String.format("%.1f", atod(value1));
                    cart.code = 5;
                    cart.line = String.format("%.1f", value1);
                    cart.marketid = String.format("%d", line_id * 10 + 5);
                    cart.opposite_lines = String.format("%.1f", value2);
                    cart.opposite_market_ids = String.format("%d", line_id * 10 + 6);
                } else {
                    cart.bet_param = String.format("%.1f", balance2);
                    cart.team_bet = String.format("Under %.1f", balance2);
                    cart.odd = String.format("%.1f", atod(value2));
                    cart.code = 6;
                    cart.line = String.format("%.1f", value2);
                    cart.marketid = String.format("%d", line_id * 10 + 6);
                    cart.opposite_lines = String.format("%.1f", value1);
                    cart.opposite_market_ids = String.format("%d", line_id * 10 + 5);
                }
            } else {
                showToast("Something went wrong. Please try again.");
                return;
            }
        } else {
            showToast("Something went wrong. Please try again.");
            return;
        }

        if (!DataManager.getInstance().addCart(cart)) {
            showToast("Already in the cart.");
            return;
        }

        showToast("Added to bet slip.");

        showCartBadge(mBubbleView);
    }

    private String getFormattedIntValue(double value) {
        if (value > 0) {
            return String.format("+%d", (int)value);
        } else if (value < 0) {
            return String.format("%d", (int)value);
        } else {
            return "0";
        }
    }

    private double atod(Double value) {
        double result = 0.0;
        if (value > 0) {
            result = value / 100.0 + 1.0;
        } else {
            result = (-100.0 / value) + 1.0;
        }
        return result;
    }

    private void loadValues() {
        RapidGame game = (RapidGame) TempDataManager.getInstance().getSelectedGame();
        if (game != null) {
            mTxtTitle.setText(game.getGameName());

            JsonObject moneyline = game.getMoneylineData();

            if (moneyline != null) {
                if (moneyline.has("moneyline_home") && !moneyline.get("moneyline_home").isJsonNull()) {
                    double moneyline_home = moneyline.get("moneyline_home").getAsDouble();
                    mTxtMoneylineValue1.setText(getFormattedIntValue(moneyline_home));
                } else {
                    mTxtMoneylineValue1.setText("");
                }
                if (moneyline.has("moneyline_away") && !moneyline.get("moneyline_away").isJsonNull()) {
                    double moneyline_away = moneyline.get("moneyline_away").getAsDouble();
                    mTxtMoneylineValue2.setText(getFormattedIntValue(moneyline_away));
                } else {
                    mTxtMoneylineValue2.setText("");
                }
            } else {
                mTxtMoneylineValue1.setText("");
                mTxtMoneylineValue2.setText("");
            }

            JsonObject spread = game.getSpreadData();
            if (spread != null) {
                if (spread.has("point_spread_home") && !spread.get("point_spread_home").isJsonNull()) {
                    double point_spread_home = spread.get("point_spread_home").getAsDouble();
                    mTxtSpreadBalance1.setText(String.format("%.1f", point_spread_home));
                } else {
                    mTxtSpreadBalance1.setText("");
                }
                if (spread.has("point_spread_home_money") && !spread.get("point_spread_home_money").isJsonNull()) {
                    double point_spread_home_money = spread.get("point_spread_home_money").getAsDouble();
                    mTxtSpreadValue1.setText(getFormattedIntValue(point_spread_home_money));
                } else {
                    mTxtSpreadValue1.setText("");
                }

                if (spread.has("point_spread_away") && !spread.get("point_spread_away").isJsonNull()) {
                    double point_spread_away = spread.get("point_spread_away").getAsDouble();
                    mTxtSpreadBalance2.setText(String.format("%.1f", point_spread_away));
                } else {
                    mTxtSpreadBalance2.setText("");
                }
                if (spread.has("point_spread_away_money") && !spread.get("point_spread_away_money").isJsonNull()) {
                    double point_spread_away_money = spread.get("point_spread_away_money").getAsDouble();
                    mTxtSpreadValue2.setText(getFormattedIntValue(point_spread_away_money));
                } else {
                    mTxtSpreadValue2.setText("");
                }
            } else {
                mTxtSpreadBalance1.setText("");
                mTxtSpreadValue1.setText("");
                mTxtSpreadBalance2.setText("");
                mTxtSpreadValue2.setText("");
            }

            JsonObject total = game.getTotalData();
            if (total != null) {
                if (total.has("total_over") && !total.get("total_over").isJsonNull()) {
                    double total_over = total.get("total_over").getAsDouble();
                    mTxtTotalBalance1.setText(String.format("%.1f", total_over));
                } else {
                    mTxtTotalBalance1.setText("");
                }
                if (total.has("total_over_money") && !total.get("total_over_money").isJsonNull()) {
                    double total_over_money = total.get("total_over_money").getAsDouble();
                    mTxtTotalValue1.setText(getFormattedIntValue(total_over_money));
                } else {
                    mTxtTotalValue1.setText("");
                }

                if (total.has("total_under") && !total.get("total_under").isJsonNull()) {
                    double total_under = total.get("total_under").getAsDouble();
                    mTxtTotalBalance2.setText(String.format("%.1f", total_under));
                } else {
                    mTxtTotalBalance2.setText("");
                }
                if (total.has("total_under_money") && !total.get("total_under_money").isJsonNull()) {
                    double total_under_money = total.get("total_under_money").getAsDouble();
                    mTxtTotalValue2.setText(getFormattedIntValue(total_under_money));
                } else {
                    mTxtTotalValue2.setText("");
                }
            } else {
                mTxtTotalBalance1.setText("");
                mTxtTotalValue1.setText("");
                mTxtTotalBalance2.setText("");
                mTxtTotalValue2.setText("");
            }
        } else {
            mTxtTitle.setText("");

            mTxtMoneylineValue1.setText("");
            mTxtMoneylineValue2.setText("");

            mTxtSpreadBalance1.setText("");
            mTxtSpreadValue1.setText("");
            mTxtSpreadBalance2.setText("");
            mTxtSpreadValue2.setText("");

            mTxtTotalBalance1.setText("");
            mTxtTotalValue1.setText("");
            mTxtTotalBalance2.setText("");
            mTxtTotalValue2.setText("");
        }

        showCartBadge(mBubbleView);
    }
}
