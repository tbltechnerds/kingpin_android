package com.kingpin.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.chanven.lib.cptr.recyclerview.RecyclerAdapterWithHF;
import com.kingpin.R;
import com.kingpin.common.manager.DataManager;
import com.kingpin.mvp.model.PlaceBetCart;
import com.kingpin.mvp.presenter.CartPresenter;
import com.kingpin.mvp.view.CartView;
import com.kingpin.ui.adapter.CartAdapter;

import butterknife.BindView;

public class CartFragment extends BaseFragment<CartView, CartPresenter> implements CartView, View.OnClickListener {

    @BindView(R.id.btn_back) ImageView mBtnBack;
    @BindView(R.id.bubble_cart) TextView mCartBubble;
    @BindView(R.id.btn_removeall) TextView mBtnRemoveAll;
    @BindView(R.id.btn_placebets) TextView mBtnPlaceBets;
    @BindView(R.id.swt_parlay) Switch mSwtParlay;
    @BindView(R.id.swt_confirm) Switch mSwtConfirm;
    @BindView(R.id.text_straight_picks) EditText mTxtStraightPicks;
    @BindView(R.id.text_parlay_amount) EditText mTxtParlayAmount;
    @BindView(R.id.text_total_stake) TextView mTxtTotalStake;
    @BindView(R.id.text_to_win) TextView mTxtToWin;
    @BindView(R.id.list_picks) RecyclerView mCartsView;

    private CartAdapter adapter;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_cart;
    }

    @NonNull
    @Override
    public CartPresenter createPresenter() {
        return new CartPresenter();
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mBtnBack.setOnClickListener(this);
        mBtnRemoveAll.setOnClickListener(this);
        mBtnPlaceBets.setOnClickListener(this);

        mSwtParlay.setChecked(false);
        mSwtParlay.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                loadCarts();
            }
        });

        mSwtConfirm.setChecked(false);

        mTxtStraightPicks.setText("0");
        mTxtStraightPicks.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = mTxtStraightPicks.getText().toString();
                if (!text.equals("")) {
                    try {
                        int value = Integer.parseInt(text);
                        DataManager.getInstance().updateAllCartWithAmount(value);
                        loadCarts();
                    } catch (Exception e) {

                    }
                } else {
                    mTxtStraightPicks.setText("0");
                }
            }
        });

        mTxtParlayAmount.setText("0");
        mTxtParlayAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = mTxtParlayAmount.getText().toString();
                if (!text.equals("")) {
                    loadCarts();
                } else {
                    mTxtParlayAmount.setText("0");
                }
            }
        });

        mCartsView.setLayoutManager(new LinearLayoutManager(getCurrentActivity(), LinearLayout.VERTICAL, false));
        adapter = new CartAdapter(this);
        mCartsView.setAdapter(new RecyclerAdapterWithHF(adapter));
        loadCarts();
    }

    @Override
    public void onResume() {
        super.onResume();

        showCartBadge(mCartBubble);
    }

    @Override
    public  void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_back:
                getCurrentActivity().finish();
                break;
            case R.id.btn_removeall:
                onRemoveAll();
                break;
            case R.id.btn_placebets:
                onPlaceBets();
                break;
        }
    }

    private void onRemoveAll() {
        DataManager.getInstance().clearCart();
        loadCarts();
    }

    private void onPlaceBets() {
        if (DataManager.getInstance().getCarts().size() == 0) {
            return;
        }

        if (!mSwtConfirm.isChecked()) {
            showToast("Please confirm your bets.");
            return;
        }

        if (mSwtParlay.isChecked()) {
            int stake = getTotalStake();
            if (stake < 50) {
                showToast("The minimum bet is $50");
                return;
            } else if (stake > 100) {
                showToast("The maximum bet is $100");
                return;
            }

//            The logic for placing a bet is not correct, right now there is the 1) Money Line; 2) Spread; and 3) Over Under.  It does not allow you to put a parlay bet on the same game, it SHOULD, with the following restriction:
//            1)    If a parlay bet involves the Money Line, they cannot also add the Spread for that same match to the parlay
//            2)   If a parlay bet involves the Spread, they cannot also add the Money Line for the same match to the parlay
//            3)   If they try to do this, an error message should say “You cannot parlay the money line and spread on a single game”
            for (int index1 = 0 ; index1 < DataManager.getInstance().getCarts().size() ; index1 ++) {
                PlaceBetCart cart1 = DataManager.getInstance().getCarts().get(index1);
                for (int index2 = 0 ; index2 < DataManager.getInstance().getCarts().size() ; index2 ++) {
                    PlaceBetCart cart2 = DataManager.getInstance().getCarts().get(index2);
                    if (cart1.marketid != cart2.marketid && cart1.sport == cart2.sport && cart1.teams == cart2.teams) {
                        if (cart1.bet_type.equals("moneyline") && cart2.bet_type.equals("handicap")) {
                            showToast("You cannot parlay the money line and spread on a single game");
                            return;
                        }
                        if (cart2.bet_type.equals("moneyline") && cart1.bet_type.equals("handicap")) {
                            showToast("You cannot parlay the money line and spread on a single game");
                            return;
                        }
                    }
                }
            }
        } else {
            for (int index = 0 ; index < DataManager.getInstance().getCarts().size() ; index ++) {
                PlaceBetCart cart = DataManager.getInstance().getCarts().get(index);
                if (cart.bet_amount < 50) {
                    showToast("The minimum bet is $50");
                    return;
                } else if (cart.bet_amount > 1000) {
                    showToast("The maximum bet is $1000");
                    return;
                }
            }
        }

        getPresenter().saveBet(mSwtParlay.isChecked(), getTotalStake(), getTotalWin());
    }

    public void loadCarts() {
        showCartBadge(mCartBubble);
        adapter.updateCarts();

        mTxtTotalStake.setText(String.format("$%d", getTotalStake()));
        mTxtToWin.setText(String.format("$%.2f", getTotalWin()));
    }

    public int getTotalStake() {
        try {
            int totalStake = 0;
            if (mSwtParlay.isChecked()) {
                totalStake = Integer.parseInt(mTxtParlayAmount.getText().toString());
            } else {
                for (int index = 0 ; index < DataManager.getInstance().getCarts().size() ; index ++ ) {
                    PlaceBetCart cart = DataManager.getInstance().getCarts().get(index);
                    totalStake += cart.bet_amount;
                }
            }
            return totalStake;
        } catch (Exception e) {
            return 0;
        }
    }

    public double getTotalWin() {
        try {
            double totalWin = 0.0;
            if (mSwtParlay.isChecked()) {
                double winRate = 1.0;
                for (int index = 0 ; index < DataManager.getInstance().getCarts().size() ; index ++ ) {
                    PlaceBetCart cart = DataManager.getInstance().getCarts().get(index);
                    double rate = Double.parseDouble(cart.odd);
                    winRate *= rate;
                }
                winRate -= 1.0;
                totalWin = Double.valueOf(getTotalStake()) * winRate;
            } else {
                for (int index = 0 ; index < DataManager.getInstance().getCarts().size() ; index ++ ) {
                    PlaceBetCart cart = DataManager.getInstance().getCarts().get(index);
                    double rate = Double.parseDouble(cart.odd);
                    double winRate = rate - 1.0;
                    totalWin += Double.valueOf(cart.bet_amount) * winRate;
                }
            }
            return totalWin;
        } catch (Exception e) {
            return 0.0;
        }
    }

    @Override
    public boolean isParlay() {
        return mSwtParlay.isChecked();
    }

    @Override
    public void onRemoveCart(PlaceBetCart cart) {
        DataManager.getInstance().removeCart(cart);
        loadCarts();
    }

    @Override
    public void updateCartWithAmount(int amount, PlaceBetCart cart) {
        DataManager.getInstance().updateCartWithAmount(amount, cart);
        loadCarts();
    }

    @Override
    public void completedSaveBets() {
        DataManager.getInstance().clearCart();
        loadCarts();
        showToast("Bets placed.");
    }

    @Override
    public void failedSaveBets() {
        showToast("Failed");
    }
}
