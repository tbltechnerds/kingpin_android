package com.kingpin.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kingpin.R;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.utility.CommonUtils;
import com.kingpin.listener.UIListener;
import com.kingpin.mvp.presenter.RequestPayoutPresenter;
import com.kingpin.mvp.view.RequestPayoutView;

import butterknife.BindView;

public class RequestPayoutFragment extends BaseFragment<RequestPayoutView, RequestPayoutPresenter> implements RequestPayoutView, View.OnClickListener {

    private static final int TAB_NONE = 0;
    private static final int TAB_PAYPAL = 1;
    private static final int TAB_BANK = 2;

    @BindView(R.id.btn_back) ImageView btnBack;
    @BindView(R.id.tab_paypal) LinearLayout tabPaypal;
    @BindView(R.id.tab_bank) LinearLayout tabBank;
    @BindView(R.id.indicator_paypal) FrameLayout indicatorPaypal;
    @BindView(R.id.indicator_bank) FrameLayout indicatorBank;
    @BindView(R.id.edit_paypal_amount) EditText editPaypalAmount;
    @BindView(R.id.edit_paypal_email) EditText editPaypalEmail;
    @BindView(R.id.edit_bank_amount) EditText editBankAmount;
    @BindView(R.id.edit_bank_account_name) EditText editBankAccountName;
    @BindView(R.id.edit_bank_account_iban) EditText editBankAccountIBan;
    @BindView(R.id.edit_swift_code) EditText editBankSwiftCode;
    @BindView(R.id.edit_bank_name) EditText editBankName;
    @BindView(R.id.edit_bank_branch_city) EditText editBankBranchCity;
    @BindView(R.id.edit_bank_address) EditText editBankAddress;
    @BindView(R.id.btn_request_payout) TextView btnRequestPayout;
    @BindView(R.id.lyt_paypal) LinearLayout layoutPaypal;
    @BindView(R.id.lyt_bank) LinearLayout layoutBank;

    private int mTabIndex = TAB_NONE;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_request_payout;
    }

    @NonNull
    @Override
    public RequestPayoutPresenter createPresenter() {
        return new RequestPayoutPresenter();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnBack.setOnClickListener(this);
        btnRequestPayout.setOnClickListener(this);
        tabBank.setOnClickListener(this);
        tabPaypal.setOnClickListener(this);

        initUI();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                updateFragment(UIListener.ProfileActivityListener.FRAGMENT_PROFILE);
                break;
            case R.id.btn_request_payout:
                if(mTabIndex == TAB_PAYPAL)
                    reqPayoutWithPaypal();
                else if(mTabIndex == TAB_BANK)
                    reqPayoutWithBank();
                break;
            case R.id.tab_paypal:
                updateUI(TAB_PAYPAL);
                break;
            case R.id.tab_bank:
                updateUI(TAB_BANK);
                break;
        }
    }

    private void initUI() {
        editPaypalAmount.setText("");
        editPaypalEmail.setText("");

        editBankAmount.setText("");
        editBankAccountName.setText("");
        editBankAccountIBan.setText("");
        editBankSwiftCode.setText("");
        editBankName.setText("");
        editBankBranchCity.setText("");
        editBankAddress.setText("");

        updateUI(TAB_PAYPAL);
    }

    private void updateUI(int tabIndex) {

        if(mTabIndex == tabIndex)
            return;

        mTabIndex = tabIndex;
        switch (mTabIndex) {
            case TAB_PAYPAL:
                indicatorPaypal.setSelected(true);
                indicatorBank.setSelected(false);
                layoutPaypal.setVisibility(View.VISIBLE);
                layoutBank.setVisibility(View.GONE);
                break;
            case TAB_BANK:
                indicatorPaypal.setSelected(false);
                indicatorBank.setSelected(true);
                layoutPaypal.setVisibility(View.GONE);
                layoutBank.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void reqPayoutWithPaypal() {
        String amount = editPaypalAmount.getText().toString().trim();
        String email = editPaypalEmail.getText().toString().trim();

        if(amount.isEmpty()) {
            showToast(R.string.toast_amount);
            return;
        }

        double amountValue = Double.valueOf(amount);
        if(amountValue > DataManager.getInstance().getRealBalance()) {
            showToast(R.string.toast_big_amount);
            return;
        }

        if(amountValue < 1) {
            showToast(R.string.toast_amount_min1);
            return;
        }

        if(email.isEmpty()) {
            showToast(R.string.toast_paypal_email);
            return;
        }

        if(!CommonUtils.isValidEmail(email)) {
            showToast(R.string.toast_paypal_email);
            return;
        }

        getPresenter().requestPayoutWithPaypal(amountValue, email);
    }

    private void reqPayoutWithBank() {
        String amount = editBankAmount.getText().toString().trim();
        String holderName = editBankAccountName.getText().toString().trim();
        String ibanName = editBankAccountIBan.getText().toString().trim();
        String swiftCode = editBankSwiftCode.getText().toString().trim();
        String bankName = editBankName.getText().toString().trim();
        String bankBranch = editBankBranchCity.getText().toString().trim();
        String bankAddress = editBankAddress.getText().toString().trim();

        if(amount.isEmpty()) {
            showToast(R.string.toast_amount);
            return;
        }

        double amountValue = Double.valueOf(amount);
        if(amountValue > DataManager.getInstance().getRealBalance()) {
            showToast(R.string.toast_big_amount);
            return;
        }

        if(amountValue < 1000) {
            showToast(R.string.toast_amount_min2);
            return;
        }

        if(holderName.isEmpty()) {
            showToast(R.string.toast_bank_holder);
            return;
        }

        if(ibanName.isEmpty()) {
            showToast(R.string.toast_bank_number);
            return;
        }

        if(swiftCode.isEmpty()) {
            showToast(R.string.toast_bank_swift);
            return;
        }

        if(bankName.isEmpty()) {
            showToast(R.string.toast_bank_name);
            return;
        }

        if(bankBranch.isEmpty()) {
            showToast(R.string.toast_bank_city);
            return;
        }

        if(bankAddress.isEmpty()) {
            showToast(R.string.toast_bank_address);
            return;
        }

        getPresenter().requestPayoutWithBank(amountValue, holderName, ibanName, swiftCode, bankName, bankBranch, bankAddress);
    }
}
