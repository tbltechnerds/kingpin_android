package com.kingpin.ui.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.kingpin.R;
import com.kingpin.common.base.Constant;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.utility.CommonUtils;
import com.kingpin.common.utility.PreferenceUtils;
import com.kingpin.listener.SpanListener;
import com.kingpin.listener.UIListener;
import com.kingpin.mvp.presenter.LandPresenter;
import com.kingpin.mvp.view.LandView;
import com.kingpin.ui.activity.AuthActivity;
import com.kingpin.ui.activity.HomeActivity;
import com.kingpin.ui.widget.ActionSpan;

import org.json.JSONObject;

import butterknife.BindView;

@SuppressWarnings("deprecation")
public class LandFragment extends BaseFragment<LandView, LandPresenter> implements LandView, View.OnClickListener, SpanListener {

    private static final int TAB_NONE = 0;
    private static final int TAB_LOGIN = 1;
    private static final int TAB_REGISTER = 2;

    private static final int REQUEST_AUTH = 1000;

    @BindView(R.id.tab_login) LinearLayout tabLogin;
    @BindView(R.id.tab_register) LinearLayout tabRegister;
    @BindView(R.id.indicator_login) FrameLayout indicatorLogin;
    @BindView(R.id.indicator_register) FrameLayout indicatorRegister;
    @BindView(R.id.scroll_login) ScrollView scrollLogin;
    @BindView(R.id.scroll_register) ScrollView scrollRegister;
    @BindView(R.id.txt_policy) TextView textPolicy;
    @BindView(R.id.edit_register_name) EditText editRegisterName;
    @BindView(R.id.edit_register_city) EditText editRegisterCity;
    @BindView(R.id.edit_register_email) EditText editRegisterEmail;
    @BindView(R.id.edit_register_password) EditText editRegisterPassword;
    @BindView(R.id.edit_register_confirm_password) EditText editRegisterPasswordConfirm;
    @BindView(R.id.edit_register_promo) EditText editRegisterPromo;
    @BindView(R.id.edit_login_email) EditText editLoginEmail;
    @BindView(R.id.edit_login_password) EditText editLoginPassword;
    @BindView(R.id.btn_finger) ImageButton btnFingerPrint;
    @BindView(R.id.btn_register) TextView btnRegister;
    @BindView(R.id.btn_login) TextView btnLogin;
    @BindView(R.id.check_remember) CheckBox checkRemember;
    @BindView(R.id.btn_skip) TextView btnSkip;
    @BindView(R.id.view_login_facebook) FrameLayout viewLoginFacebook;
    @BindView(R.id.btn_login_facebook) LoginButton btnLoginFacebook;
    @BindView(R.id.view_register_facebook) FrameLayout viewRegisterFacebook;
    @BindView(R.id.btn_register_facebook) LoginButton btnRegisterFacebook;
    @BindView(R.id.btn_forgot) TextView btnForgot;

    private int mTabIndex = TAB_NONE;
    private CallbackManager mFacebookCallbackManager;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_land;
    }

    @NonNull
    @Override
    public LandPresenter createPresenter() {
        return new LandPresenter();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        FacebookSdk.sdkInitialize(getCurrentActivity().getApplicationContext());

        tabLogin.setOnClickListener(this);
        tabRegister.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        btnSkip.setOnClickListener(this);
        btnForgot.setOnClickListener(this);
        btnFingerPrint.setOnClickListener(this);

        mFacebookCallbackManager = CallbackManager.Factory.create();
        btnRegisterFacebook.setReadPermissions("email", "public_profile");
        btnLoginFacebook.setReadPermissions("email", "public_profile");

        btnRegisterFacebook.registerCallback(mFacebookCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        handleFacebookSigninResult(response);
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, name, first_name, last_name, email, gender");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException error) {
            }
        });

        btnLoginFacebook.registerCallback(mFacebookCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        handleFacebookSigninResult(response);
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, name, first_name, last_name, email, gender");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException error) {
            }
        });

        initUI();
        getPresenter().getMonthlyPot();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_AUTH) {
            if(resultCode == Activity.RESULT_OK) {
                String userId = PreferenceUtils.getStringPreference(getCurrentActivity(), Constant.PREF_FINGERPRINT_USERID);
                String password = PreferenceUtils.getStringPreference(getCurrentActivity(), Constant.PREF_FINGERPRINT_PASSWORD);
                getPresenter().signin(userId, password);
            }
            return;
        }

        if(FacebookSdk.isFacebookRequestCode(requestCode))
            mFacebookCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tab_login:
                updateUI(TAB_LOGIN);
                break;
            case R.id.tab_register:
                updateUI(TAB_REGISTER);
                break;
            case R.id.btn_register:
                signup();
                break;
            case R.id.btn_login:
                signin();
                break;
            case R.id.btn_skip:
                gotoHome();
                break;
            case R.id.btn_forgot:
                updateFragment(UIListener.StartActivityListener.FRAGMENT_FORGOTPWD);
                break;
            case R.id.btn_finger:
                Intent intent = new Intent(getContext(), AuthActivity.class);
                startActivityForResult(intent, REQUEST_AUTH);
                break;
        }
    }

    @Override
    public void onClicekedSpan(String spanString) {
        String strTerm = getCurrentActivity().getResources().getString(R.string.text_terms);
        String strPolicy = getCurrentActivity().getResources().getString(R.string.text_policy);

        if(spanString.equals(strTerm))
            openUrl("https://kingpin.pro/terms");
        else if(spanString.equals(strPolicy))
            openUrl("https://kingpin.pro/privacy");
    }

    @Override
    public void completedToGetBalance() {
        if (DataManager.getInstance().getShowPromoCodeOnRegister()) {
            editRegisterPromo.setVisibility(View.VISIBLE);
        } else {
            editRegisterPromo.setVisibility(View.GONE);
        }

        if (DataManager.getInstance().getShowFacebookLogin()) {
            viewLoginFacebook.setVisibility(View.VISIBLE);
            viewRegisterFacebook.setVisibility(View.VISIBLE);
        } else {
            viewLoginFacebook.setVisibility(View.GONE);
            viewRegisterFacebook.setVisibility(View.GONE);
        }
    }

    @Override
    public void gotoHome() {
        startIntent(HomeActivity.class, UIListener.HomeActivityListener.FRAGMENT_UPCOMING_GAME, true);
    }

    @Override
    public boolean isChoosenRemember() {
        return checkRemember.isChecked();
    }

    @Override
    public void askSupportFingerprint(String userId, String password) {
        if(DataManager.getInstance().enableFingerprintID(getCurrentActivity())) {
            DataManager.getInstance().saveFingerprintID(getCurrentActivity(), userId, password);
            gotoHome();
        }
        else {
            if(CommonUtils.enabledFingerPrint(getCurrentActivity())) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getCurrentActivity());
                AlertDialog dialog = builder.setTitle(R.string.app_name)
                        .setMessage(R.string.alert_fingerprint)
                        .setCancelable(false)
                        .setPositiveButton(R.string.text_yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                DataManager.getInstance().saveFingerprintID(getCurrentActivity(), editLoginEmail.getText().toString().trim(), editLoginPassword.getText().toString().trim());
                                gotoHome();

                            }
                        })
                        .setNegativeButton(R.string.text_no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                DataManager.getInstance().clearFingerprintID(getCurrentActivity());
                                gotoHome();
                            }
                        }).create();
                dialog.show();
            }
            else {
                gotoHome();
            }
        }
    }

    private void initUI() {

        editLoginEmail.setText("");
        editLoginPassword.setText("");

        editRegisterName.setText("");
        editRegisterCity.setText("");
        editRegisterEmail.setText("");
        editRegisterPassword.setText("");
        editRegisterPasswordConfirm.setText("");
        editRegisterPromo.setText("");

        if (DataManager.getInstance().getShowPromoCodeOnRegister()) {
            editRegisterPromo.setVisibility(View.VISIBLE);
        } else {
            editRegisterPromo.setVisibility(View.GONE);
        }

        if (DataManager.getInstance().getShowFacebookLogin()) {
            viewLoginFacebook.setVisibility(View.VISIBLE);
            viewRegisterFacebook.setVisibility(View.VISIBLE);
        } else {
            viewLoginFacebook.setVisibility(View.GONE);
            viewRegisterFacebook.setVisibility(View.GONE);
        }

        makeSpanPolicy();
        updateUI(TAB_LOGIN);
    }

    private void updateUI(int tab) {

        if(DataManager.getInstance().enableFingerprintID(getCurrentActivity())) {
            btnFingerPrint.setEnabled(true);
            btnFingerPrint.setAlpha(1.0f);
        }
        else {
            btnFingerPrint.setEnabled(false);
            btnFingerPrint.setAlpha(0.5f);
        }

        if(mTabIndex == tab)
            return;

        mTabIndex = tab;
        switch (mTabIndex) {
            case TAB_LOGIN:
                indicatorLogin.setSelected(true);
                indicatorRegister.setSelected(false);
                scrollLogin.setVisibility(View.VISIBLE);
                scrollRegister.setVisibility(View.GONE);
                break;
            case TAB_REGISTER:
                indicatorLogin.setSelected(false);
                indicatorRegister.setSelected(true);
                scrollLogin.setVisibility(View.GONE);
                scrollRegister.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void makeSpanPolicy() {
        SpannableStringBuilder builder = new SpannableStringBuilder();

        String strHint1 = getCurrentActivity().getResources().getString(R.string.hint_policy1);
        String strHint2 = getCurrentActivity().getResources().getString(R.string.hint_policy2);
        String strTerm = getCurrentActivity().getResources().getString(R.string.text_terms);
        String strPolicy = getCurrentActivity().getResources().getString(R.string.text_policy);

        SpannableString spanHint1 = new SpannableString(strHint1);
        spanHint1.setSpan(new ForegroundColorSpan(getCurrentActivity().getResources().getColor(R.color.color_gray1)), 0, strHint1.length(), 0);
        builder.append(spanHint1);

        SpannableString spanTerm = new SpannableString(strTerm);
        spanTerm.setSpan(new ActionSpan(strTerm, getCurrentActivity().getResources().getColor(R.color.color_blue3), this), 0, strTerm.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append(spanTerm);
        builder.append(" ");

        SpannableString spanHint2 = new SpannableString(strHint2);
        spanHint2.setSpan(new ForegroundColorSpan(getCurrentActivity().getResources().getColor(R.color.color_gray1)), 0, strHint2.length(), 0);
        builder.append(spanHint2);

        SpannableString spanPolicy = new SpannableString(strPolicy);
        spanPolicy.setSpan(new ActionSpan(strPolicy, getCurrentActivity().getResources().getColor(R.color.color_blue3), this), 0, strPolicy.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append(spanPolicy);

        textPolicy.setText(builder, TextView.BufferType.SPANNABLE);
        textPolicy.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void signup() {
        String name = editRegisterName.getText().toString().trim();
        String city = editRegisterCity.getText().toString().trim();
        String email = editRegisterEmail.getText().toString().trim();
        String password = editRegisterPassword.getText().toString().trim();
        String passwordConfirm = editRegisterPasswordConfirm.getText().toString().trim();
        String promoCode = editRegisterPromo.getText().toString().trim();

        if(name.length() == 0 || city.length() == 0 || password.length() == 0) {
            showToast(R.string.toast_input_error);
            return;
        }

        if(!CommonUtils.isValidEmail(email)) {
            showToast(R.string.toast_valid_email);
            return;
        }

        if(!password.equals(passwordConfirm)) {
            showToast(R.string.toast_confirm_password);
            return;
        }

        hideKeyboard();
        getPresenter().signup(name, city, email, password, promoCode);
    }

    private void signin() {
        String email = editLoginEmail.getText().toString().trim();
        String password = editLoginPassword.getText().toString().trim();

        if(!CommonUtils.isValidEmail(email)) {
            showToast(R.string.toast_valid_email);
            return;
        }

        if(password.length() == 0) {
            showToast(R.string.toast_confirm_password);
            return;
        }

        hideKeyboard();
        getPresenter().signin(email, password);
    }

    private void handleFacebookSigninResult(GraphResponse response) {
        JSONObject jsonResponse = response.getJSONObject();

        String fb_id = jsonResponse.optString("id", "");
        String first_name = jsonResponse.optString("first_name", "");
        String last_name = jsonResponse.optString("last_name", "");
        String email = jsonResponse.optString("email", "");

        getPresenter().signin_fb(fb_id, email, first_name, last_name);
    }
}