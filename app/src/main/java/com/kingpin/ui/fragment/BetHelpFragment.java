package com.kingpin.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.android.billingclient.api.SkuDetails;
import com.kingpin.R;
import com.kingpin.billingmodule.billing.BillingConstants;
import com.kingpin.billingmodule.billing.BillingProvider;
import com.kingpin.common.base.Constant;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.manager.InAppManager;
import com.kingpin.common.utility.PreferenceUtils;
import com.kingpin.listener.SpanListener;
import com.kingpin.listener.UIListener;
import com.kingpin.mvp.presenter.BetHelpPresenter;
import com.kingpin.mvp.presenter.InAppPurchasePresenter;
import com.kingpin.mvp.view.BetHelpView;
import com.kingpin.mvp.view.InAppPurchaseView;
import com.kingpin.ui.activity.HomeActivity;
import com.kingpin.ui.activity.ProfileActivity;
import com.kingpin.ui.widget.ActionSpan;

import java.util.List;

import butterknife.BindView;

public class BetHelpFragment extends BaseFragment<BetHelpView, BetHelpPresenter> implements BetHelpView, View.OnClickListener, SpanListener {

    private static final String TAG = "BetHelpFragment";

    @BindView(R.id.txt_close) TextView mBtnClose;
    @BindView(R.id.txt_help) TextView mTxtBody;
    @BindView(R.id.txt_do_not_show) TextView mBtnDoNotShow;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_bethelp;
    }

    @NonNull
    @Override
    public BetHelpPresenter createPresenter() {
        return new BetHelpPresenter();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mBtnClose.setOnClickListener(this);
        mBtnDoNotShow.setOnClickListener(this);

        makeSpanHint();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_close:
                onClose();
                break;
            case R.id.txt_do_not_show:
                onDoNotShow();
                break;
        }
    }

    private void makeSpanHint() {
        SpannableStringBuilder builder = new SpannableStringBuilder();

        String strHint1 = getCurrentActivity().getResources().getString(R.string.hint_bethelp_text1);
        String strHint2 = getCurrentActivity().getResources().getString(R.string.hint_bethelp_text2);
        String strHint3 = getCurrentActivity().getResources().getString(R.string.hint_bethelp_text3);
        String strHint4 = getCurrentActivity().getResources().getString(R.string.hint_bethelp_text4);
        String strHint5 = getCurrentActivity().getResources().getString(R.string.hint_bethelp_text5);
        String strHint6 = getCurrentActivity().getResources().getString(R.string.hint_bethelp_text6);

        SpannableString spanHint1 = new SpannableString(strHint1);
        spanHint1.setSpan(new ForegroundColorSpan(getCurrentActivity().getResources().getColor(R.color.color_white)), 0, strHint1.length(), 0);
        builder.append(spanHint1);
        builder.append("\n\n");

        SpannableString spanHint2 = new SpannableString(strHint2);
        spanHint2.setSpan(new ActionSpan(strHint2, getCurrentActivity().getResources().getColor(R.color.color_white), this), 0, strHint2.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append(spanHint2);
        builder.append(" ");

        SpannableString spanHint3 = new SpannableString(strHint3);
        spanHint3.setSpan(new ForegroundColorSpan(getCurrentActivity().getResources().getColor(R.color.color_white)), 0, strHint3.length(), 0);
        builder.append(spanHint3);
        builder.append("\n\n");

        SpannableString spanHint4 = new SpannableString(strHint4);
        spanHint4.setSpan(new ForegroundColorSpan(getCurrentActivity().getResources().getColor(R.color.color_white)), 0, strHint4.length(), 0);
        builder.append(spanHint4);
        builder.append("\n\n");

        SpannableString spanHint5 = new SpannableString(strHint5);
        spanHint5.setSpan(new ForegroundColorSpan(getCurrentActivity().getResources().getColor(R.color.color_white)), 0, strHint5.length(), 0);
        builder.append(spanHint5);
        builder.append(" ");

        SpannableString spanHint6 = new SpannableString(strHint6);
        spanHint6.setSpan(new ActionSpan(strHint6, getCurrentActivity().getResources().getColor(R.color.color_white), this), 0, strHint6.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append(spanHint6);

        mTxtBody.setText(builder, TextView.BufferType.SPANNABLE);
        mTxtBody.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    public void onClicekedSpan(String spanString) {
        String strHint2 = getCurrentActivity().getResources().getString(R.string.hint_bethelp_text2);
        String strHint6 = getCurrentActivity().getResources().getString(R.string.hint_bethelp_text6);

        if(spanString.equals(strHint2)) {
            onSubscribe();
        } else if (spanString.equals(strHint6)) {
            onRule();
        }
    }

    private void onClose() {
        removeFragment(UIListener.HomeActivityListener.FRAGMENT_BETHELP);
    }

    private void onSubscribe() {
        onClose();
        updateProfileWithInAppPurchase();
    }

    private void onRule() {
        onClose();
        gotoRuleScreen();
    }

    private void onDoNotShow() {
        PreferenceUtils.putPreference(getCurrentActivity(), Constant.PREF_DO_NOT_SHOW_BET_HELP, true);
        onClose();
    }
}
