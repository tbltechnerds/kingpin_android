package com.kingpin.ui.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chanven.lib.cptr.PtrDefaultHandler;
import com.chanven.lib.cptr.PtrFrameLayout;
import com.chanven.lib.cptr.recyclerview.RecyclerAdapterWithHF;
import com.kingpin.R;
import com.kingpin.common.manager.DataManager;
import com.kingpin.mvp.model.BaseStatistics;
import com.kingpin.mvp.model.Bet;
import com.kingpin.mvp.model.GraphStatistics;
import com.kingpin.mvp.model.Kingpin;
import com.kingpin.mvp.model.Statistics;
import com.kingpin.mvp.presenter.MyStaticsPresenter;
import com.kingpin.mvp.view.MyStaticsView;
import com.kingpin.ui.adapter.BetHistoryAdapter;
import com.kingpin.ui.adapter.StatisticAdapter;
import com.kingpin.ui.adapter.UpcomingBetAdapter;
import com.kingpin.ui.widget.RefreshLayout;

import java.util.ArrayList;

import butterknife.BindView;

public class MyStaticsFragment extends BaseFragment<MyStaticsView, MyStaticsPresenter> implements MyStaticsView, View.OnClickListener {

    private static final int TAB_STATISTICS = 0;
    private static final int TAB_BET_HISTORY = 1;
    private static final int TAB_UPCOMING_BET = 2;

    public static final int SORT_DATE = 0;
    public static final int SORT_AMOUNT_BET = 1;
    public static final int SORT_AMOUNT_WON = 2;

    @BindView(R.id.btn_filter) ImageView btnFilter;
    @BindView(R.id.btn_setting) ImageView btnSetting;
    @BindView(R.id.btn_cart) ImageView btnCart;
    @BindView(R.id.bubble_cart) TextView textBubble;
    @BindView(R.id.tab_statics) TextView tabStatistics;
    @BindView(R.id.tab_bet_history) TextView tabBetHistory;
    @BindView(R.id.tab_upcoming) TextView tabUpcomingBet;
    @BindView(R.id.lyt_pull_statistic) RefreshLayout mRefreshStatisticView;
    @BindView(R.id.lyt_pull_bet_history) RefreshLayout mRefreshBetHistoryView;
    @BindView(R.id.lyt_pull_upcoming_bet) RefreshLayout mRefreshUpcomingBetView;
    @BindView(R.id.sort_date) TextView sortDateView;
    @BindView(R.id.sort_amount_bet) TextView sortAmountBetView;
    @BindView(R.id.sort_amount_won) TextView sortAmountWonView;
    @BindView(R.id.lyt_sort) LinearLayout sortLayout;
    @BindView(R.id.lyt_table_header) FrameLayout headerLayout;
    @BindView(R.id.lyt_header_history) LinearLayout historyHeaderLayout;
    @BindView(R.id.lyt_header_upcoming) LinearLayout upcomoingHeaderLayout;
    @BindView(R.id.list_statistics) RecyclerView mStatisticsView;
    @BindView(R.id.list_bet_history) RecyclerView mBetHistoryView;
    @BindView(R.id.list_upcoming_bet) RecyclerView mUpcomingBetsView;

    private int mTabIndex;
    private int mSortIndex;
    private boolean mRevert;
    private StatisticAdapter mStatisticAdapter;
    private BetHistoryAdapter mBetHistoryAdapter;
    private UpcomingBetAdapter mUpcomingBetAdapter;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_my_statics;
    }

    @NonNull
    @Override
    public MyStaticsPresenter createPresenter() {
        return new MyStaticsPresenter();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        DataManager.getInstance().getFilterOptions().setInitializeFilterOption();

        mTabIndex = TAB_STATISTICS;
        mSortIndex = SORT_DATE;
        mRevert = false;
        isDisableActiveFilter = true;
        isDisableSearchUserNameFilter = true;


        btnFilter.setOnClickListener(this);
        btnSetting.setOnClickListener(this);
        btnCart.setOnClickListener(this);
        tabStatistics.setOnClickListener(this);
        tabBetHistory.setOnClickListener(this);
        tabUpcomingBet.setOnClickListener(this);
        sortDateView.setOnClickListener(this);
        sortAmountBetView.setOnClickListener(this);
        sortAmountWonView.setOnClickListener(this);

        mStatisticsView.setLayoutManager(new LinearLayoutManager(getCurrentActivity(), LinearLayout.VERTICAL, false));
        mStatisticAdapter = new StatisticAdapter(this);
        mStatisticsView.setAdapter(new RecyclerAdapterWithHF(mStatisticAdapter));

        mBetHistoryView.setLayoutManager(new LinearLayoutManager(getCurrentActivity(), LinearLayout.VERTICAL, false));
        mBetHistoryAdapter = new BetHistoryAdapter(new ArrayList<Bet>(), this);
        mBetHistoryView.setAdapter(new RecyclerAdapterWithHF(mBetHistoryAdapter));

        mUpcomingBetsView.setLayoutManager(new LinearLayoutManager(getCurrentActivity(), LinearLayout.VERTICAL, false));
        mUpcomingBetAdapter = new UpcomingBetAdapter(this);
        mUpcomingBetsView.setAdapter(new RecyclerAdapterWithHF(mUpcomingBetAdapter));

        mRefreshStatisticView.setPtrHandler(new PtrDefaultHandler() {
            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                getPresenter().getKingpinDetail();
            }
        });

        mRefreshBetHistoryView.setPtrHandler(new PtrDefaultHandler() {
            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                getPresenter().getBettingHistory();
            }
        });

        mRefreshUpcomingBetView.setPtrHandler(new PtrDefaultHandler() {
            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                getPresenter().getUpcomingBets();
            }
        });

        updateUI();
        mRefreshStatisticView.autoRefresh(true);
    }

    @Override
    public void onResume() {
        super.onResume();

        showCartBadge(textBubble);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_filter:
                OnFilter(this);
                break;
            case R.id.btn_setting:
                OnSetting();
                break;
            case R.id.btn_cart:
                gotoCartScreen();
                break;
            case R.id.tab_statics:
                onTab(TAB_STATISTICS);
                break;
            case R.id.tab_bet_history:
                onTab(TAB_BET_HISTORY);
                break;
            case R.id.tab_upcoming:
                onTab(TAB_UPCOMING_BET);
                break;
            case R.id.sort_date:
                onSort(SORT_DATE);
                break;
            case R.id.sort_amount_bet:
                onSort(SORT_AMOUNT_BET);
                break;
            case R.id.sort_amount_won:
                onSort(SORT_AMOUNT_WON);
                break;
        }
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void OnLoadKingpinDetail(Kingpin kingpin) {
        ArrayList<BaseStatistics> statisticList = new ArrayList<>();
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Days betting", String.valueOf(kingpin.getDaysBetting())));
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "# of bets", String.valueOf(kingpin.getCountBets())));
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Winnings / (losses)", "$" + kingpin.getWinningLosses()));
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "W-L-D", String.format("%d-%d-%d", kingpin.getCountWin(), kingpin.getCountLoss(), kingpin.getCountDraw())));
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Win %", kingpin.getPercentWin() + "%"));
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Avg. bet size", "$" + kingpin.getAvgBetSize()));
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Avg. bet risk amount", kingpin.getAvgBetRisk() + "%"));
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Avg. win size", "$" + kingpin.getAvgWinSize()));
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "# of bets per 30 days", String.valueOf(kingpin.getBetsPer30Days())));

        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Last 30 days' # of bets", String.valueOf(kingpin.getCount30daysBets())));
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Last 30 days' winnings", "$" + kingpin.getLast30daysWinnings()));
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Last 30 days' W-L-D", String.format("%d-%d-%d", kingpin.getCount30daysWin(), kingpin.getCount30daysLoss(), kingpin.getCount30daysDraw())));
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Last 30 days' win %", kingpin.getLast30daysPercentWin() + "%"));

        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Last 7 days' # of bets", String.valueOf(kingpin.getCount7daysBets())));
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Last 7 days' winnings", "$" + kingpin.getLast7daysWinnings()));
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Last 7 days' W-L-D", String.format("%d-%d-%d", kingpin.getCount7daysWin(), kingpin.getCount7daysLoss(), kingpin.getCount7daysDraw())));
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Last 7 days' win %", kingpin.getLast7daysPercentWin() + "%"));

        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Yesterday' # of bets", String.valueOf(kingpin.getCount1daysBets())));
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Yesterday' winnings", "$" + kingpin.getLast1daysWinnings()));
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Yesterday' W-L-D", String.format("%d-%d-%d", kingpin.getCount1daysWin(), kingpin.getCount1daysLoss(), kingpin.getCount1daysDraw())));
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Yesterday' win %", kingpin.getLast1daysPercentWin() + "%"));

        statisticList.add(new GraphStatistics(BaseStatistics.TYPE_GRAPH, "Winnings / losses over time", "$ " + kingpin.getWinningLosses(), kingpin.getDatesForUpperGraph(), kingpin.getValuesForUpperGraph()));
        mStatisticAdapter.updateStatistics(statisticList);

        loadInfos();
        mRefreshStatisticView.refreshComplete();
    }

    @Override
    public void OnLoadBetHistories(ArrayList<Bet> bets) {
        mBetHistoryAdapter.updateBetHistories(bets);
        loadInfos();
        mRefreshBetHistoryView.refreshComplete();
    }

    @Override
    public void OnLoadUpcomingBets(ArrayList<Bet> bets) {
        mUpcomingBetAdapter.updateUpcomingBets(bets, true, true);
        loadInfos();
        mRefreshUpcomingBetView.refreshComplete();
    }

    @Override
    public void OnChangedFilterOption() {
        switch (mTabIndex) {
            case TAB_STATISTICS:
                mStatisticAdapter.updateStatistics(new ArrayList<BaseStatistics>());
                mRefreshStatisticView.autoRefresh(true);
                break;
            case TAB_BET_HISTORY:
                mBetHistoryAdapter.updateBetHistories(new ArrayList<Bet>());
                mRefreshBetHistoryView.autoRefresh(true);
                break;
            case TAB_UPCOMING_BET:
                mUpcomingBetAdapter.updateUpcomingBets(new ArrayList<Bet>(), true, true);
                mRefreshUpcomingBetView.autoRefresh(true);
                break;
        }
    }

    private void updateUI() {
        updateSort();
        updateTab();
        loadInfos();
    }

    private void updateTab() {
        tabStatistics.setSelected(false);
        tabBetHistory.setSelected(false);
        tabUpcomingBet.setSelected(false);

        switch (mTabIndex) {
            case TAB_STATISTICS:
                tabStatistics.setSelected(true);
                sortLayout.setVisibility(View.GONE);
                headerLayout.setVisibility(View.GONE);
                break;
            case TAB_BET_HISTORY:
                tabBetHistory.setSelected(true);
                sortLayout.setVisibility(View.VISIBLE);
                headerLayout.setVisibility(View.VISIBLE);
                historyHeaderLayout.setVisibility(View.VISIBLE);
                upcomoingHeaderLayout.setVisibility(View.GONE);
                break;
            case TAB_UPCOMING_BET:
                tabUpcomingBet.setSelected(true);
                sortLayout.setVisibility(View.GONE);
                headerLayout.setVisibility(View.VISIBLE);
                historyHeaderLayout.setVisibility(View.GONE);
                upcomoingHeaderLayout.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void updateSort() {
        sortDateView.setSelected(false);
        sortAmountBetView.setSelected(false);
        sortAmountWonView.setSelected(false);

        switch (mSortIndex) {
            case SORT_DATE:
                sortDateView.setSelected(true);
                break;
            case SORT_AMOUNT_BET:
                sortAmountBetView.setSelected(true);
                break;
            case SORT_AMOUNT_WON:
                sortAmountWonView.setSelected(true);
                break;
        }
    }

    private void onTab(int tabIndex) {
        mTabIndex = tabIndex;
        updateUI();

        if(mTabIndex == TAB_BET_HISTORY) {
            if(mBetHistoryAdapter.getBetHistoryList().size() == 0)
                getPresenter().getBettingHistory();
        }
        else if(mTabIndex == TAB_UPCOMING_BET) {
            if(mUpcomingBetAdapter.getUpcomingBetList().size() == 0)
                getPresenter().getUpcomingBets();
        }

        isDisableTimeperiodFilter = (mTabIndex == TAB_UPCOMING_BET);
    }

    private void onSort(int sortIndex) {
        if(mSortIndex == sortIndex) {
            mRevert = !mRevert;
        }
        else {
            mRevert = false;
            mSortIndex = sortIndex;
        }
        updateUI();
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    private void loadInfos() {
        switch (mTabIndex) {
            case TAB_STATISTICS:
                mStatisticAdapter.notifyDataSetChanged();
                mRefreshStatisticView.setVisibility(View.VISIBLE);
                mRefreshBetHistoryView.setVisibility(View.GONE);
                mRefreshUpcomingBetView.setVisibility(View.GONE);
                break;
            case TAB_BET_HISTORY:
                mBetHistoryAdapter.sort(mSortIndex, mRevert);
                mRefreshStatisticView.setVisibility(View.GONE);
                mRefreshBetHistoryView.setVisibility(View.VISIBLE);
                mRefreshUpcomingBetView.setVisibility(View.GONE);
                break;
            case TAB_UPCOMING_BET:
                mUpcomingBetAdapter.sort();
                mRefreshStatisticView.setVisibility(View.GONE);
                mRefreshBetHistoryView.setVisibility(View.GONE);
                mRefreshUpcomingBetView.setVisibility(View.VISIBLE);
                break;
        }
    }
}
