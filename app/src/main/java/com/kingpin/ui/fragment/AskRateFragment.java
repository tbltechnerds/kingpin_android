package com.kingpin.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.TextView;

import com.kingpin.R;
import com.kingpin.common.base.Constant;
import com.kingpin.common.utility.PreferenceUtils;
import com.kingpin.listener.SpanListener;
import com.kingpin.listener.UIListener;
import com.kingpin.mvp.presenter.AskRatePresenter;
import com.kingpin.mvp.presenter.BetHelpPresenter;
import com.kingpin.mvp.view.AskRateView;
import com.kingpin.mvp.view.BetHelpView;
import com.kingpin.ui.widget.ActionSpan;

import butterknife.BindView;

public class AskRateFragment extends BaseFragment<AskRateView, AskRatePresenter> implements AskRateView, View.OnClickListener {

    private static final String TAG = "AskRateFragment";

    @BindView(R.id.text_yes) TextView mBtnYes;
    @BindView(R.id.text_no) TextView mBtnNo;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_askrate;
    }

    @NonNull
    @Override
    public AskRatePresenter createPresenter() {
        return new AskRatePresenter();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mBtnYes.setOnClickListener(this);
        mBtnNo.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.text_yes:
                onRate();
                break;
            case R.id.text_no:
                onNo();
                break;

        }
    }

    private void onClose() {
        removeFragment(UIListener.HomeActivityListener.FRAGMENT_ASK_RATE);
    }

    private void onRate() {
        onClose();
        getCurrentActivity().rateApp();
    }

    private void onNo() {
        onClose();
        addFragment(UIListener.HomeActivityListener.FRAGMENT_SUBMIT_FEEDBACK);
    }

}
