package com.kingpin.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.kingpin.R;
import com.kingpin.listener.UIListener;
import com.kingpin.mvp.presenter.AskRatePresenter;
import com.kingpin.mvp.presenter.SubmitFeedbackPresenter;
import com.kingpin.mvp.view.AskRateView;
import com.kingpin.mvp.view.SubmitFeedbackView;

import butterknife.BindView;

public class SubmitFeedbackFragment extends BaseFragment<SubmitFeedbackView, SubmitFeedbackPresenter> implements SubmitFeedbackView, View.OnClickListener {

    private static final String TAG = "SubmitFeedbackFragment";

    @BindView(R.id.text_submit) TextView mBtnSubmit;
    @BindView(R.id.text_notnow) TextView mBtnNotNow;
    @BindView(R.id.text_feedback) EditText mTextFeedback;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_submit_feedback;
    }

    @NonNull
    @Override
    public SubmitFeedbackPresenter createPresenter() {
        return new SubmitFeedbackPresenter();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mBtnSubmit.setOnClickListener(this);
        mBtnNotNow.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.text_submit:
                onSubmit();
                break;
            case R.id.text_notnow:
                onClose();
                break;
        }
    }

    private void onClose() {
        removeFragment(UIListener.HomeActivityListener.FRAGMENT_SUBMIT_FEEDBACK);
    }

    private void onSubmit() {
        String feedback = mTextFeedback.getText().toString().trim();
        getPresenter().sendFeedback(feedback);
    }

    @Override
    public void onSuccessToSubmit() {
        showToast("Your feedback sent successfully.");
        onClose();
    }

    @Override
    public void onFailedToSubmit() {
        showToast("Failed to submit the feedback. Please try again.");
    }
}
