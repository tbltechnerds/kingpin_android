package com.kingpin.ui.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bigkoo.pickerview.builder.OptionsPickerBuilder;
import com.bigkoo.pickerview.listener.OnOptionsSelectListener;
import com.bigkoo.pickerview.view.OptionsPickerView;
import com.kingpin.R;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.utility.CommonUtils;
import com.kingpin.listener.UIListener;
import com.kingpin.mvp.model.Profile;
import com.kingpin.mvp.presenter.ProfilePresenter;
import com.kingpin.mvp.view.ProfileView;
import com.makeramen.roundedimageview.RoundedImageView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import butterknife.BindView;

public class ProfileFragment extends BaseFragment<ProfileView, ProfilePresenter> implements ProfileView, View.OnClickListener {

    @BindView(R.id.btn_back) ImageView btnBack;
    @BindView(R.id.btn_edit) ImageView btnEdit;
    @BindView(R.id.img_profile) RoundedImageView profileView;
    @BindView(R.id.txt_name) TextView nameView;
    @BindView(R.id.btn_real_balance) TextView btnRealBalance;
    @BindView(R.id.txt_location) TextView locationView;
    @BindView(R.id.txt_email) TextView emailView;
    @BindView(R.id.txt_twitter) TextView twitterView;
    @BindView(R.id.txt_instagram) TextView instagramView;
    @BindView(R.id.lyt_email_notification) FrameLayout layoutEmailNotification;
    @BindView(R.id.lyt_touchid) FrameLayout layoutTouchID;
    @BindView(R.id.txt_open_notification) TextView btnOpenNotificationSetting;
    @BindView(R.id.check_email) CheckBox checkEmailNotification;
    @BindView(R.id.check_touchid) CheckBox checkTouchID;
    @BindView(R.id.btn_request_payout) TextView btnRequestPayout;
    @BindView(R.id.btn_affiliate_history) TextView btnAffiliateHistory;
    @BindView(R.id.btn_payment_history) TextView btnPaymentHistory;
    @BindView(R.id.btn_clear_bet) TextView btnClearBetHistory;
    @BindView(R.id.btn_upgrade_account) TextView btnUpgradeAccount;
    @BindView(R.id.txt_membership) TextView membershipView;
    @BindView(R.id.txt_betting_strategy) EditText mBettingStrategy;
    @BindView(R.id.btn_save_strategy) TextView btnSaveStrategy;
    @BindView(R.id.txt_affiliate_terms) TextView txtAffiliateTerms;
    @BindView(R.id.txt_promo_code) TextView txtPromoCode;
    @BindView(R.id.txt_affiliate_earnings) TextView txtAffiliateEarnings;
    @BindView(R.id.txt_available_withdraw) TextView txtAvailableWithdraw;


    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_profile;
    }

    @NonNull
    @Override
    public ProfilePresenter createPresenter() {
        return new ProfilePresenter();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnBack.setOnClickListener(this);
        btnEdit.setOnClickListener(this);
        btnRealBalance.setOnClickListener(this);
        btnOpenNotificationSetting.setOnClickListener(this);
        btnRequestPayout.setOnClickListener(this);
        btnAffiliateHistory.setOnClickListener(this);
        btnPaymentHistory.setOnClickListener(this);
        btnClearBetHistory.setOnClickListener(this);
        btnUpgradeAccount.setOnClickListener(this);
        layoutEmailNotification.setOnClickListener(this);
        layoutTouchID.setOnClickListener(this);
        btnSaveStrategy.setOnClickListener(this);

        checkEmailNotification.setChecked(false);
        checkTouchID.setChecked(DataManager.getInstance().enableFingerprintID(getCurrentActivity()));

        loadUserInfo();
        getPresenter().getAvailableBalance();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                getCurrentActivity().finish();
                break;
            case R.id.btn_edit:
                updateFragment(UIListener.ProfileActivityListener.FRAGMENT_EDIT_PROFILE);
                break;
            case R.id.btn_balance:
                break;
            case R.id.btn_real_balance:
                break;
            case R.id.txt_open_notification:
                Intent intent = new Intent();
                intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    intent.putExtra("android.provider.extra.APP_PACKAGE", getCurrentActivity().getPackageName());
                }
                else {
                    intent.putExtra("app_package", getCurrentActivity().getPackageName());
                    intent.putExtra("app_uid", getCurrentActivity().getApplicationInfo().uid);
                }
                startActivity(intent);
                break;
            case R.id.lyt_email_notification:
                checkEmailNotification.setChecked(!checkEmailNotification.isChecked());
                getPresenter().updateNotificationSetting(checkEmailNotification.isChecked() ? 1 : 0);
                break;
            case R.id.lyt_touchid:
                touchDisable();
                break;
            case R.id.btn_request_payout:
                updateFragment(UIListener.ProfileActivityListener.FRAGMENT_REQUEST_PAYOUT);
                break;
            case R.id.btn_affiliate_history:
                updateFragment(UIListener.ProfileActivityListener.FRAGMENT_AFFILIATE_HISTORY);
                break;
            case R.id.btn_payment_history:
                updateFragment(UIListener.ProfileActivityListener.FRAGMENT_PAYMENT_HISTORY);
                break;
            case R.id.btn_clear_bet:
                showAlertForClear();
                break;
            case R.id.btn_upgrade_account:
                upgradeAccount();
                break;
            case R.id.btn_save_strategy:
                saveBettingStrategy();
                break;
        }
    }

    private void saveBettingStrategy() {
        String strategy = mBettingStrategy.getText().toString().trim();
        getPresenter().updateBettingStrategy(strategy);
    }

    @Override
    public void DownloadAvatarCompleted(Bitmap bitmap) {
        if(bitmap != null && profileView != null)
            profileView.setImageBitmap(bitmap);
    }

    @Override
    public void OnLoadRealBalance() {
        loadUserInfo();
    }

    @Override
    public void OnUpdatedProfile() {
        loadUserInfo();
    }

    @SuppressLint("DefaultLocale")
    private void loadUserInfo() {
        Profile me = DataManager.getInstance().getAccount();
        if(me == null)
            return;

        Bitmap bitmap = downloadImage(me.getAvatar());
        if(bitmap != null || profileView != null)
            profileView.setImageBitmap(bitmap);

        nameView.setText(me.getName());
        locationView.setText(me.getFullAddress());
        emailView.setText(me.getEmail());
        twitterView.setText(me.getTwitter());
        instagramView.setText(me.getInstagram());

        btnRealBalance.setText(String.format("Real Account Balance: $%d", (int)DataManager.getInstance().getRealBalance()));
        checkEmailNotification.setChecked(me.getNotifications() == 1);
        checkTouchID.setChecked(me.getPushNotification() == 1);
        mBettingStrategy.setText(me.getBettingStrategy());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            txtAffiliateTerms.setText(Html.fromHtml(DataManager.getInstance().getAffiliateTerms(), Html.FROM_HTML_MODE_COMPACT));
        } else {
            txtAffiliateTerms.setText(Html.fromHtml(DataManager.getInstance().getAffiliateTerms()));
        }
        txtPromoCode.setText(me.getReferralAffiliateCode());
        txtAffiliateEarnings.setText(String.format("$%d", (int)DataManager.getInstance().getAffiliateEarnings()));
        txtAvailableWithdraw.setText(String.format("$%d", (int)DataManager.getInstance().getAffiliateAvailable()));

        if(DataManager.getInstance().isSubscribed()) {
            btnUpgradeAccount.setText(getCurrentActivity().getResources().getString(R.string.text_cancel_account));
            btnUpgradeAccount.setVisibility(View.GONE);
            membershipView.setText(getCurrentActivity().getResources().getString(R.string.hint_member1));
        }
        else {
            btnUpgradeAccount.setText(getCurrentActivity().getResources().getString(R.string.text_upgrade_account));
            btnUpgradeAccount.setVisibility(View.VISIBLE);
            membershipView.setText(getCurrentActivity().getResources().getString(R.string.hint_member));
        }
    }

    private void showAlertForClear() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getCurrentActivity());
        builder.setCancelable(false);
        builder.setTitle(getCurrentActivity().getResources().getString(R.string.app_name));
        builder.setMessage(getCurrentActivity().getResources().getString(R.string.toast_clear_bet));
        builder.setPositiveButton(getCurrentActivity().getResources().getString(R.string.text_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectSportToClear();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(getCurrentActivity().getResources().getString(R.string.text_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert=builder.create();
        alert.show();
    }

    private void selectSportToClear() {

    }

    private void upgradeAccount() {
        Profile me = DataManager.getInstance().getAccount();
        if(me != null && DataManager.getInstance().isSubscribed()) {
            String paymentType = me.getPaymentType();
            if (paymentType.equals("inapp")) {
                openUrl("https://support.apple.com/en-us/HT202039#subscriptions");
            } else if (paymentType.equals("inapp_android")) {
                openUrl("https://support.google.com/googleplay/answer/7018481?co=GENIE.Platform%3DAndroid&hl=en");
            } else if (paymentType.equals("paypal")) {
                openUrl("https://www.paypal.com/us/smarthelp/article/how-do-i-cancel-a-subscription-faq57");
            } else if (paymentType.equals("stripe")) {
                showToast("Please visit www.KingPin.pro, and visit “My Profile” to cancel.");
            }
        } else {
            getCurrentActivity().upgradeProfileWithInAppPurchase();
        }
    }

    private void touchDisable() {
        if(checkTouchID.isChecked()) {
            DataManager.getInstance().clearFingerprintID(getCurrentActivity());
            checkTouchID.setChecked(false);
        }
        else {
            if(CommonUtils.enabledFingerPrint(getCurrentActivity())) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getCurrentActivity());
                builder.setCancelable(false);
                builder.setTitle(getCurrentActivity().getResources().getString(R.string.app_name));
                builder.setMessage(getCurrentActivity().getResources().getString(R.string.toast_fingerprint));
                builder.setPositiveButton(getCurrentActivity().getResources().getString(R.string.text_gotit), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog alert=builder.create();
                alert.show();
            }
            else {
                AlertDialog.Builder builder = new AlertDialog.Builder(getCurrentActivity());
                builder.setCancelable(false);
                builder.setTitle(getCurrentActivity().getResources().getString(R.string.app_name));
                builder.setMessage(getCurrentActivity().getResources().getString(R.string.toast_fingerprint1));
                builder.setPositiveButton(getCurrentActivity().getResources().getString(R.string.text_gotit), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog alert=builder.create();
                alert.show();
            }
        }
    }
}

