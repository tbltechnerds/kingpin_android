package com.kingpin.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.chanven.lib.cptr.PtrDefaultHandler;
import com.chanven.lib.cptr.PtrFrameLayout;
import com.chanven.lib.cptr.recyclerview.RecyclerAdapterWithHF;
import com.kingpin.R;
import com.kingpin.listener.UIListener;
import com.kingpin.mvp.model.AffiliateCriedit;
import com.kingpin.mvp.model.Withdraw;
import com.kingpin.mvp.presenter.AffiliateHistoryPresenter;
import com.kingpin.mvp.presenter.PaymentHistoryPresenter;
import com.kingpin.mvp.view.AffiliateHistoryView;
import com.kingpin.mvp.view.PaymentHistoryView;
import com.kingpin.ui.adapter.AffiliateHistoryAdapter;
import com.kingpin.ui.adapter.PaymentHistoryAdapter;
import com.kingpin.ui.widget.RefreshLayout1;

import java.util.ArrayList;

import butterknife.BindView;

public class AffiliateHistoryFragment extends BaseFragment<AffiliateHistoryView, AffiliateHistoryPresenter> implements AffiliateHistoryView, View.OnClickListener {

    @BindView(R.id.btn_back) ImageView btnBack;
    @BindView(R.id.lyt_pull) RefreshLayout1 mRefreshView;
    @BindView(R.id.list_history) RecyclerView mHistoryView;

    private AffiliateHistoryAdapter mHistoryAdapter;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_affiliate_history;
    }

    @NonNull
    @Override
    public AffiliateHistoryPresenter createPresenter() {
        return new AffiliateHistoryPresenter();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnBack.setOnClickListener(this);

        mHistoryView.setLayoutManager(new LinearLayoutManager(getCurrentActivity(), LinearLayout.VERTICAL, false));
        mHistoryAdapter = new AffiliateHistoryAdapter(this);
        mHistoryView.setAdapter(new RecyclerAdapterWithHF(mHistoryAdapter));

        mRefreshView.setPtrHandler(new PtrDefaultHandler() {
            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                getPresenter().getAffiliateHistory();
            }
        });

        mRefreshView.post(new Runnable() {
            @Override
            public void run() {
                mRefreshView.autoRefresh(true);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                updateFragment(UIListener.ProfileActivityListener.FRAGMENT_PROFILE);
                break;
        }
    }

    @Override
    public void OnLoadAffiliateHistories(ArrayList<AffiliateCriedit> criedits) {
        mRefreshView.refreshComplete();
        mHistoryAdapter.updateCriedits(criedits);
    }
}

