package com.kingpin.ui.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.kingpin.R;
import com.kingpin.listener.UIListener;
import com.kingpin.mvp.presenter.SplashPresenter;
import com.kingpin.mvp.view.SplashView;

public class SplashFragment extends BaseFragment<SplashView, SplashPresenter> implements SplashView {

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_splash;
    }

    @NonNull
    @Override
    public SplashPresenter createPresenter() {
        return new SplashPresenter();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                updateFragment(UIListener.StartActivityListener.FRAGMENT_LAND);
            }
        }, 2000);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
