package com.kingpin.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kingpin.R;
import com.kingpin.common.base.Constant;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.manager.TempDataManager;
import com.kingpin.common.utility.DateUtils;
import com.kingpin.listener.UIListener;
import com.kingpin.mvp.model.Profile;
import com.kingpin.mvp.model.UpcomingGameDetail;
import com.kingpin.mvp.presenter.UpcomingGameDetailPresenter;
import com.kingpin.mvp.view.UpcomingGameDetailView;
import com.kingpin.ui.activity.HomeActivity;
import com.kingpin.ui.activity.StartActivity;
import com.kingpin.ui.adapter.BetAdapter;

import butterknife.BindView;

public class UpcomingGameDetailFragment extends BaseFragment<UpcomingGameDetailView, UpcomingGameDetailPresenter> implements UpcomingGameDetailView, View.OnClickListener {

    @BindView(R.id.btn_back) ImageView btnBack;
    @BindView(R.id.btn_setting) ImageView btnSetting;
    @BindView(R.id.btn_cart) ImageView btnCart;
    @BindView(R.id.bubble_cart) TextView textBubble;
    @BindView(R.id.txt_title) TextView textTitle;
    @BindView(R.id.txt_time) TextView textTime;
    @BindView(R.id.btn_share) ImageView btnShare;
    @BindView(R.id.list_bets) RecyclerView betList;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_upcoming_game_detail;
    }

    @NonNull
    @Override
    public UpcomingGameDetailPresenter createPresenter() {
        return new UpcomingGameDetailPresenter();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        UpcomingGameDetail match = TempDataManager.getInstance().getSelectedMatch();
        if(match == null) {
            removeFragment(UIListener.HomeActivityListener.FRAGMENT_UPCOMING_DETAIL);
            return;
        }

        btnBack.setOnClickListener(this);
        btnSetting.setOnClickListener(this);
        btnCart.setOnClickListener(this);
        btnShare.setOnClickListener(this);

        init(match);
    }

    @Override
    public void onResume() {
        super.onResume();

        showCartBadge(textBubble);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onKingpinProfile(int user_id) {
        Profile me = DataManager.getInstance().getAccount();
        if(me != null && user_id == me.getId()) {
            ((HomeActivity)getCurrentActivity()).openMyStaticsTab();
        } else {
            gotoKingpinProfileScreen(null, user_id, false);
        }
    }

    @Override
    public void onSubscribe() {
        if(DataManager.getInstance().isLoggedInStatus()) {
            if(!DataManager.getInstance().isSubscribed())
                updateProfileWithInAppPurchase();
        }
        else {
            getCurrentActivity().openLandingScreen();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                removeFragment(UIListener.HomeActivityListener.FRAGMENT_UPCOMING_DETAIL);
                break;
            case R.id.btn_setting:
                OnSetting();
                break;
            case R.id.btn_cart:
                gotoCartScreen();
                break;
            case R.id.btn_share:
                shareGame();
                break;
        }
    }

    private void init(UpcomingGameDetail match){
        textTime.setText(DateUtils.convertToLocalDateAndTime(match.getMatchDate()));
        textTitle.setText(match.getName());

        betList.setLayoutManager(new LinearLayoutManager(getCurrentActivity(), LinearLayout.VERTICAL, false));
        BetAdapter betAdapter = new BetAdapter(match.getBets(), this);
        betList.setAdapter(betAdapter);
    }

    private void shareGame() {
        Profile me = DataManager.getInstance().getAccount();
        UpcomingGameDetail match = TempDataManager.getInstance().getSelectedMatch();
        if (me != null && match != null) {
            String text = "Expert " + match.getName() + " Picks " + Constant.APP_SHARE_LINK + " Get 10% off with promo code: " + me.getReferralAffiliateCode();
            shareText(text);
        }
    }
}