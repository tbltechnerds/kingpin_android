package com.kingpin.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.kingpin.R;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.manager.TempDataManager;
import com.kingpin.listener.UIListener;
import com.kingpin.mvp.model.Profile;
import com.kingpin.mvp.presenter.EditProfilePresenter;
import com.kingpin.mvp.view.EditProfileView;
import com.kingpin.ui.activity.CropActivity;
import com.kingpin.ui.widget.ImagePicker.ImagePicker;
import com.makeramen.roundedimageview.RoundedImageView;

import butterknife.BindView;

public class EditProfileFragment extends BaseFragment<EditProfileView, EditProfilePresenter> implements EditProfileView, View.OnClickListener {

    private final static int REQUEST_PICK_IMAGE = 1;
    private final static int REQUEST_CROP_IMAGE = 2;

    @BindView(R.id.btn_back) ImageView btnBack;
    @BindView(R.id.btn_done) ImageView btnDone;
    @BindView(R.id.img_profile) RoundedImageView profileView;
    @BindView(R.id.txt_name) TextView nameView;
    @BindView(R.id.btn_balance) TextView btnBalance;
    @BindView(R.id.edit_city) EditText editCity;
    @BindView(R.id.edit_state) EditText editState;
    @BindView(R.id.edit_country) EditText editCountry;
    @BindView(R.id.txt_email) TextView emailView;
    @BindView(R.id.txt_twitter) EditText editTwitter;
    @BindView(R.id.txt_instagram) EditText editInstagram;
    @BindView(R.id.check_password) CheckBox checkPassword;
    @BindView(R.id.edit_old_password) EditText editOldPassword;
    @BindView(R.id.edit_new_password) EditText editNewPassword;
    @BindView(R.id.edit_confirm_password) EditText editConfirmPassword;
    @BindView(R.id.btn_save) TextView btnSave;
    @BindView(R.id.btn_camera) ImageView btnCamera;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_edit_profile;
    }

    @NonNull
    @Override
    public EditProfilePresenter createPresenter() {
        return new EditProfilePresenter();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnBack.setOnClickListener(this);
        btnDone.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        btnCamera.setOnClickListener(this);
        emailView.setOnClickListener(this);

        TempDataManager.getInstance().setTempBitmap(null);

        checkPassword.setChecked(false);

        loadUserInfo();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_PICK_IMAGE:
                if(resultCode == Activity.RESULT_OK) {
                    Bitmap bitmap = ImagePicker.getImageFromResult(getContext(), requestCode, resultCode, data);
                    if(bitmap != null) {
                        profileView.setImageBitmap(bitmap);
                        TempDataManager.getInstance().setTempBitmap(bitmap);

                        Intent intent = new Intent(getContext(), CropActivity.class);
                        startActivityForResult(intent, REQUEST_CROP_IMAGE);
                    }
                }
                break;
            case REQUEST_CROP_IMAGE:
                if(resultCode == Activity.RESULT_OK) {
                    profileView.setImageBitmap(TempDataManager.getInstance().getTempBitmap());
                }
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                hideKeyboard();
                updateFragment(UIListener.ProfileActivityListener.FRAGMENT_PROFILE);
                break;
            case R.id.btn_done:
            case R.id.btn_save:
                hideKeyboard();
                saveProfile();
                break;
            case R.id.btn_camera:
                ImagePicker.pickImage(this, getCurrentActivity().getResources().getString(R.string.pick_title), REQUEST_PICK_IMAGE, false);
                break;
            case R.id.txt_email:
                alertEmail();
                break;
        }
    }

    @Override
    public void DownloadAvatarCompleted(Bitmap bitmap) {
        if(bitmap != null && profileView != null)
            profileView.setImageBitmap(bitmap);
    }

    @Override
    public void OnUpdatedProfile() {
        loadUserInfo();
    }

    @SuppressLint("DefaultLocale")
    private void loadUserInfo() {
        Profile me = DataManager.getInstance().getAccount();
        if(me == null)
            return;

        Bitmap bitmap = downloadImage(me.getAvatar());
        if(bitmap != null || profileView != null)
            profileView.setImageBitmap(bitmap);

        nameView.setText(me.getName());
        btnBalance.setText(String.format("Account Balance: $%d", (int) DataManager.getInstance().getBalance()));
        editCity.setText((me.getCity() != null) ? me.getCity() : "");
        editState.setText((me.getState() != null) ? me.getState() : "");
        editCountry.setText((me.getCountry() != null) ? me.getCountry() : "");
        emailView.setText((me.getEmail() != null) ? me.getEmail() : "");
        editTwitter.setText(me.getTwitter());
        editInstagram.setText(me.getInstagram());
        editOldPassword.setText("");
        editNewPassword.setText("");
        editConfirmPassword.setText("");
    }

    private void alertEmail() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getCurrentActivity());
        builder.setCancelable(false);
        builder.setTitle(getCurrentActivity().getResources().getString(R.string.app_name));
        builder.setMessage(getCurrentActivity().getResources().getString(R.string.alert_change_email));
        builder.setPositiveButton(getCurrentActivity().getResources().getString(R.string.text_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert=builder.create();
        alert.show();
    }

    private void saveProfile() {
        String city = editCity.getText().toString().trim();
        String state = editState.getText().toString().trim();
        String country = editCountry.getText().toString().trim();
        String twitter = editTwitter.getText().toString().trim();
        String instagram = editInstagram.getText().toString().trim();
        String oldPassword = "";
        String newPassword = "";

        if(checkPassword.isChecked()) {
            oldPassword = editOldPassword.getText().toString().trim();
            newPassword = editNewPassword.getText().toString().trim();
            String confirmPassword = editConfirmPassword.getText().toString().trim();

            if(oldPassword.isEmpty()) {
                showToast(R.string.toast_old_password);
                return;
            }

            if(newPassword.isEmpty()) {
                showToast(R.string.toast_new_password);
                return;
            }

            if(!newPassword.equals(confirmPassword)) {
                showToast(R.string.toast_confirm_password1);
                return;
            }
        }

        getPresenter().saveProfile(city, state, country, twitter, instagram, checkPassword.isChecked(), oldPassword, newPassword);
    }
}
