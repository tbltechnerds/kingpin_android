package com.kingpin.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.google.gson.JsonObject;
import com.kingpin.R;
import com.kingpin.billingmodule.billing.BillingConstants;
import com.kingpin.billingmodule.billing.BillingManager;
import com.kingpin.billingmodule.billing.BillingProvider;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.manager.InAppManager;
import com.kingpin.listener.UIListener;
import com.kingpin.mvp.model.Profile;
import com.kingpin.mvp.presenter.InAppPurchasePresenter;
import com.kingpin.mvp.view.InAppPurchaseView;
import com.kingpin.ui.activity.HomeActivity;
import com.kingpin.ui.activity.ProfileActivity;
import com.revenuecat.purchases.Entitlement;
import com.revenuecat.purchases.Offering;
import com.revenuecat.purchases.PurchaserInfo;
import com.revenuecat.purchases.Purchases;
import com.revenuecat.purchases.PurchasesError;
import com.revenuecat.purchases.interfaces.MakePurchaseListener;
import com.revenuecat.purchases.interfaces.ReceiveEntitlementsListener;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

public class InAppPurchaseFragment extends BaseFragment<InAppPurchaseView, InAppPurchasePresenter> implements InAppPurchaseView, View.OnClickListener {

    private static final String TAG = "InAppPurchaseFragment";

    @BindView(R.id.txt_close) TextView btnClose;
    @BindView(R.id.lyt_total) FrameLayout layoutTotal;
    @BindView(R.id.btn_trial) TextView btnTrial;
    @BindView(R.id.txt_cal_month) TextView textTrial;
    @BindView(R.id.txt_privacy) TextView btnPrivacy;
    @BindView(R.id.txt_service) TextView btnTermService;
    @BindView(R.id.btn_purchase_day) TextView btnPurchaseDay;
    @BindView(R.id.txt_purchase_day_desc) TextView txtPurchaseDay;
    @BindView(R.id.btn_purchase_month) TextView btnPurchaseMonth;
    @BindView(R.id.btn_purchase_quarterly) TextView btnPurchaseQuarterly;
    @BindView(R.id.btn_purchase_yearly) TextView btnPurchaseYearly;
    @BindView(R.id.txt_details) TextView txtDetails;

    private Map<String, Offering> offerings;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_inapp_purchase;
    }

    @NonNull
    @Override
    public InAppPurchasePresenter createPresenter() {
        return new InAppPurchasePresenter();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        offerings = null;

        btnClose.setOnClickListener(this);
        layoutTotal.setOnClickListener(this);
        btnTrial.setOnClickListener(this);
        btnPrivacy.setOnClickListener(this);
        btnTermService.setOnClickListener(this);
        btnPurchaseDay.setOnClickListener(this);
        btnPurchaseMonth.setOnClickListener(this);
        btnPurchaseQuarterly.setOnClickListener(this);
        btnPurchaseYearly.setOnClickListener(this);

        if(DataManager.getInstance().isTrialAvailable()) {
            btnTrial.setVisibility(View.VISIBLE);
            textTrial.setVisibility(View.VISIBLE);
            btnPurchaseDay.setVisibility(View.GONE);
            txtPurchaseDay.setVisibility(View.GONE);
            txtDetails.setText("Monthly, quarterly, and yearly memberships are all recurring.  You will be charged each period until you cancel.");
        }
        else {
            btnTrial.setVisibility(View.GONE);
            textTrial.setVisibility(View.GONE);
            btnPurchaseDay.setVisibility(View.VISIBLE);
            txtPurchaseDay.setVisibility(View.VISIBLE);
            txtDetails.setText("Monthly, quarterly, and yearly memberships are all recurring.  You will be charged each period until you cancel.  Daily passes do not renew automatically.");
        }

        showDefaultPrices();
        getAllInAppItems();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_close:
                onClose();
                break;
            case R.id.txt_privacy:
                openUrl("https://kingpin.pro/privacy");
                break;
            case R.id.txt_service:
                openUrl("https://kingpin.pro/terms");
                break;
            case R.id.btn_trial:
                if (hasValidPromoCode()) {
                    onPurchase(BillingConstants.OFFERING_TRIAL_MONTHLY_PROMO);
                } else {
                    onPurchase(BillingConstants.OFFERING_TRIAL_MONTHLY);
                }
                break;
            case R.id.btn_purchase_day:
                if (hasValidPromoCode()) {
                    onPurchase(BillingConstants.OFFERING_DAY_PROMO);
                } else {
                    onPurchase(BillingConstants.OFFERING_DAY);
                }
                break;
            case R.id.btn_purchase_month:
                if (hasValidPromoCode()) {
                    onPurchase(BillingConstants.OFFERING_MONTHLY_PROMO);
                } else {
                    onPurchase(BillingConstants.OFFERING_MONTHLY);
                }
                break;
            case R.id.btn_purchase_quarterly:
                if (hasValidPromoCode()) {
                    onPurchase(BillingConstants.OFFERING_QUARTERLY_PROMO);
                } else {
                    onPurchase(BillingConstants.OFFERING_QUARTERLY);
                }
                break;
            case R.id.btn_purchase_yearly:
                if (hasValidPromoCode()) {
                    onPurchase(BillingConstants.OFFERING_YEARLY_PROMO);
                } else {
                    onPurchase(BillingConstants.OFFERING_YEARLY);
                }
                break;
        }
    }

    private void onClose() {
        if(getCurrentActivity() instanceof HomeActivity)
            removeFragment(UIListener.HomeActivityListener.FRAGMENT_INAPP_PURCHASE);
        else if(getCurrentActivity() instanceof ProfileActivity)
            removeFragment(UIListener.ProfileActivityListener.FRAGMENT_INAPP_PURCHASE);
    }

    private void getAllInAppItems() {
        showLoading();
        Purchases.getSharedInstance().getEntitlements(new ReceiveEntitlementsListener() {
            @Override
            public void onReceived(@Nullable Map<String, Entitlement> entitlements) {
                hideLoading();
                Entitlement pro = entitlements.get(BillingConstants.ENTITLEMENT_PRO);
                if (pro != null) {
                    offerings = pro.getOfferings();
                    showInAppItems();
                } else {
                    failedToGetInAppItems();
                }
            }

            @Override
            public void onError(@NonNull PurchasesError error) {
                /* Optional error handling */
                hideLoading();
                failedToGetInAppItems();
            }
        });
    }

    private void showInAppItems() {
        if (offerings == null) {
            return;
        }

        if (hasValidPromoCode()) { // has valid promo code
            if (DataManager.getInstance().isTrialAvailable()) { // free trial turn on
                for (Offering offering : offerings.values()) {
                    String sku = offering.getSkuDetails().getSku();
                    String price = offering.getSkuDetails().getPrice();
                    if (sku != null && price != null) {
                        switch (sku) {
                            case BillingConstants.SKU_FREETRIAL_MONTHLY_PROMO:
                                btnTrial.setText(String.format("Start My 7-Day Free Trial(then %s/month)", price));
                                textTrial.setText(String.format("Then %s per month.  You will be charged monthly until you cancel.", price));
                                break;
                            case BillingConstants.SKU_MONTHLY_PROMO:
                                btnPurchaseMonth.setText(String.format("Monthly %s / Month (21%s Discount)", price, "%"));
                                break;
                            case BillingConstants.SKU_QUARTERLY_PROMO:
                                btnPurchaseQuarterly.setText(String.format("Quarterly %s / Quarter (Save 48%s)", price, "%"));
                                break;
                            case BillingConstants.SKU_YEARLY_PROMO:
                                btnPurchaseYearly.setText(String.format("Yearly %s / Year (Save 84%s)", price, "%"));
                                break;
                            default:
                                break;
                        }
                    }
                }
            } else { // free trial turn off
                for (Offering offering : offerings.values()) {
                    String sku = offering.getSkuDetails().getSku();
                    String price = offering.getSkuDetails().getPrice();
                    if (sku != null && price != null) {
                        switch (sku) {
                            case BillingConstants.SKU_DAY_PROMO:
                                btnPurchaseDay.setText(String.format("Day Pass %s 1-Day Pass (20%s Discount)", price, "%"));
                                break;
                            case BillingConstants.SKU_MONTHLY_PROMO:
                                btnPurchaseMonth.setText(String.format("Monthly %s / Month (21%s Discount)", price, "%"));
                                break;
                            case BillingConstants.SKU_QUARTERLY_PROMO:
                                btnPurchaseQuarterly.setText(String.format("Quarterly %s / Quarter (Save 48%s)", price, "%"));
                                break;
                            case BillingConstants.SKU_YEARLY_PROMO:
                                btnPurchaseYearly.setText(String.format("Yearly %s / Year (Save 84%s)", price, "%"));
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        } else { // no valid promo code
            if (DataManager.getInstance().isTrialAvailable()) { // free trial turn on
                for (Offering offering : offerings.values()) {
                    String sku = offering.getSkuDetails().getSku();
                    String price = offering.getSkuDetails().getPrice();
                    if (sku != null && price != null) {
                        switch (sku) {
                            case BillingConstants.SKU_FREETRIAL_MONTHLY_NOPROMO:
                                btnTrial.setText(String.format("Start My 7-Day Free Trial(then %s/month)", price));
                                textTrial.setText(String.format("Then %s per month.  You will be charged monthly until you cancel.", price));
                                break;
                            case BillingConstants.SKU_MONTHLY_NOPROMO:
                                btnPurchaseMonth.setText(String.format("Monthly %s / Month", price));
                                break;
                            case BillingConstants.SKU_QUARTERLY_NOPROMO:
                                btnPurchaseQuarterly.setText(String.format("Quarterly %s / Quarter (Save 42%s)", price, "%"));
                                break;
                            case BillingConstants.SKU_YEARLY_NOPROMO:
                                btnPurchaseYearly.setText(String.format("Yearly %s / Year (Save 79%s)", price, "%"));
                                break;
                            default:
                                break;
                        }
                    }
                }
            } else { // free trial turn off
                for (Offering offering : offerings.values()) {
                    String sku = offering.getSkuDetails().getSku();
                    String price = offering.getSkuDetails().getPrice();
                    if (sku != null && price != null) {
                        switch (sku) {
                            case BillingConstants.SKU_DAY_NOPROMO:
                                btnPurchaseDay.setText(String.format("Day Pass %s 1-Day Pass", price));
                                break;
                            case BillingConstants.SKU_MONTHLY_NOPROMO:
                                btnPurchaseMonth.setText(String.format("Monthly %s / Month", price));
                                break;
                            case BillingConstants.SKU_QUARTERLY_NOPROMO:
                                btnPurchaseQuarterly.setText(String.format("Quarterly %s / Quarter (Save 42%s)", price, "%"));
                                break;
                            case BillingConstants.SKU_YEARLY_NOPROMO:
                                btnPurchaseYearly.setText(String.format("Yearly %s / Year (Save 79%s)", price, "%"));
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }

    private void failedToGetInAppItems() {
        showToast("Failed to get purchase items. Try again");
        onClose();;
    }

    private void onPurchase(String offeringId) {
        final SkuDetails skuDetails = getSkuDetails(offeringId);
        if (skuDetails == null) {
            showToast("Failed to purchase. Try later.");
            return;
        }

        Purchases.getSharedInstance().makePurchase(
                getCurrentActivity(),
                skuDetails,
                new MakePurchaseListener() {
                    @Override
                    public void onCompleted(@NonNull Purchase purchase, @NonNull PurchaserInfo purchaserInfo) {
                        if (purchaserInfo.getEntitlements().get(BillingConstants.ENTITLEMENT_PRO).isActive()) {
                            // Unlock that great "pro" content
                            completedPurchaseItem(skuDetails.getSku());
                        } else {
                            showToast("Failed to purchase. Try later.");
                        }
                    }

                    @Override
                    public void onError(@NonNull PurchasesError error, Boolean userCancelled) {
                        // No purchase
                        showToast("Failed to purchase. Try later.");
                    }
                }
        );
    }

    private SkuDetails getSkuDetails(String offeringId) {
        if (offerings == null) {
            return null;
        }
        return offerings.get(offeringId).getSkuDetails();
    }

    private boolean hasValidPromoCode() {
        Profile me = DataManager.getInstance().getAccount();
        if (me != null && me.isVaildAffiliateCode()) { // has valid promo code
            return true;
        } else {
            return false;
        }
    }

    private void showDefaultPrices() {
        if (hasValidPromoCode()) { // has valid promo code
            btnTrial.setText(String.format("Start My 7-Day Free Trial(then %s/month)", "$31.99"));
            textTrial.setText(String.format("Then %s%.2f per month.  You will be charged monthly until you cancel.", "$",  31.99f));
            btnPurchaseDay.setText(String.format("Day Pass %s%.2f 1-Day Pass (20%s Discount)", "$",  7.99f, "%"));
            btnPurchaseMonth.setText(String.format("Monthly %s%.2f / Month (21%s Discount)", "$",  31.99f, "%"));
            btnPurchaseQuarterly.setText(String.format("Quarterly %s%.2f / Quarter (Save 48%s)", "$",  55.99f, "%"));
            btnPurchaseYearly.setText(String.format("Yearly %s%.2f / Year (Save 84%s)", "$",  79.99f, "%"));
        } else { // no valid promo code
            btnTrial.setText(String.format("Start My 7-Day Free Trial(then %s/month)", "$39.99"));
            textTrial.setText(String.format("Then %s%.2f per month.  You will be charged monthly until you cancel.", "$",  39.99f));
            btnPurchaseDay.setText(String.format("Day Pass %s%.2f 1-Day Pass", "$",  9.99f));
            btnPurchaseMonth.setText(String.format("Monthly %s%.2f / Month", "$",  39.99f));
            btnPurchaseQuarterly.setText(String.format("Quarterly %s%.2f / Quarter (Save 42%s)", "$",  69.99f, "%"));
            btnPurchaseYearly.setText(String.format("Yearly %s%.2f / Year (Save 79%s)", "$",  99.99f, "%"));
        }
    }

    public void completedPurchaseItem(String skuId) {
        switch (skuId) {
            case BillingConstants.SKU_DAY_PROMO:
            case BillingConstants.SKU_DAY_NOPROMO:
                DataManager.getInstance().purchasedNonRenewSubscribeInApp(this.getContext());
                if (hasValidPromoCode()) {
                    getPresenter().purchasedSubscribe("day", 0, 7.99);
                } else {
                    getPresenter().updateProfileWithSubscribedStatus(true, false, false);
                }
                break;
            case BillingConstants.SKU_FREETRIAL_MONTHLY_PROMO:
            case BillingConstants.SKU_FREETRIAL_MONTHLY_NOPROMO:
                if (hasValidPromoCode()) {
                    getPresenter().purchasedSubscribe("month", 1, 31.99);
                } else {
                    getPresenter().updateProfileWithSubscribedStatus(true, true, false);
                }
                break;
            case BillingConstants.SKU_MONTHLY_PROMO:
            case BillingConstants.SKU_MONTHLY_NOPROMO:
                if (hasValidPromoCode()) {
                    getPresenter().purchasedSubscribe("month", 0, 31.99);
                } else {
                    getPresenter().updateProfileWithSubscribedStatus(true, false, false);
                }
                break;
            case BillingConstants.SKU_QUARTERLY_PROMO:
            case BillingConstants.SKU_QUARTERLY_NOPROMO:
                if (hasValidPromoCode()) {
                    getPresenter().purchasedSubscribe("quarter", 0, 55.99);
                } else {
                    getPresenter().updateProfileWithSubscribedStatus(true, false, false);
                }
                break;
            case BillingConstants.SKU_YEARLY_PROMO:
            case BillingConstants.SKU_YEARLY_NOPROMO:
                if (hasValidPromoCode()) {
                    getPresenter().purchasedSubscribe("year", 0, 79.99);
                } else {
                    getPresenter().updateProfileWithSubscribedStatus(true, false, false);
                }
                break;
        }
    }

    @Override
    public void OnUpdatedProfile() {
        ((HomeActivity)getCurrentActivity()).initialUI();
    }
}
