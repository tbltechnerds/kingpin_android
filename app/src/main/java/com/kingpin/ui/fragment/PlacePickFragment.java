package com.kingpin.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chanven.lib.cptr.PtrDefaultHandler;
import com.chanven.lib.cptr.PtrFrameLayout;
import com.chanven.lib.cptr.loadmore.OnLoadMoreListener;
import com.chanven.lib.cptr.recyclerview.RecyclerAdapterWithHF;
import com.kingpin.R;
import com.kingpin.common.base.Constant;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.manager.TempDataManager;
import com.kingpin.common.utility.PreferenceUtils;
import com.kingpin.listener.SpanListener;
import com.kingpin.listener.UIListener;
import com.kingpin.mvp.model.BovadaGame;
import com.kingpin.mvp.model.Game;
import com.kingpin.mvp.model.RapidGame;
import com.kingpin.mvp.presenter.PlacePickPresenter;
import com.kingpin.mvp.view.PlacePickView;
import com.kingpin.ui.adapter.GameAdapter;
import com.kingpin.ui.widget.ActionSpan;
import com.kingpin.ui.widget.RefreshLayout;

import butterknife.BindView;

public class PlacePickFragment extends BaseFragment<PlacePickView, PlacePickPresenter> implements PlacePickView, View.OnClickListener, SpanListener {

    @BindView(R.id.btn_filter) ImageView mBtnFilter;
    @BindView(R.id.btn_setting) ImageView mBtnSetting;
    @BindView(R.id.btn_cart) ImageView mBtnCart;
    @BindView(R.id.bubble_cart) TextView mBubbleView;
    @BindView(R.id.lyt_pull) RefreshLayout mRefreshView;
    @BindView(R.id.list_kingpin) RecyclerView mGamesView;
    @BindView(R.id.txt_hint) TextView mHintView;
    @BindView(R.id.txt_nogames) TextView mNoGamesView;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_place_pick;
    }

    @NonNull
    @Override
    public PlacePickPresenter createPresenter() {
        return new PlacePickPresenter();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        isDisableActiveFilter = true;
        isDisableSearchUserNameFilter = true;
        isDisableTimeperiodFilter = true;

        mNoGamesView.setText("");

        mBtnFilter.setOnClickListener(this);
        mBtnSetting.setOnClickListener(this);
        mBtnCart.setOnClickListener(this);

        mBubbleView.setVisibility(View.GONE);

        makeSpanHint();

        mGamesView.setLayoutManager(new LinearLayoutManager(getCurrentActivity(), LinearLayout.VERTICAL, false));
        GameAdapter adapter = new GameAdapter(this);
        mGamesView.setAdapter(new RecyclerAdapterWithHF(adapter));
        adapter.updateGames();

        mRefreshView.setPtrHandler(new PtrDefaultHandler() {
            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                getPresenter().getGames();
            }
        });

        mRefreshView.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void loadMore() {
                getPresenter().getGames();
            }
        });
        mRefreshView.setLoadMoreEnable(true);

        mRefreshView.post(new Runnable() {
            @Override
            public void run() {
                loadGames();
            }
        });

        Boolean doNotShow = PreferenceUtils.getBooleanPreference(getCurrentActivity(), Constant.PREF_DO_NOT_SHOW_BET_HELP);
        if (!doNotShow) {
            addFragment(UIListener.HomeActivityListener.FRAGMENT_BETHELP);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        showCartBadge(mBubbleView);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onLoadGames() {
        GameAdapter adapter = new GameAdapter(this);
        mGamesView.setAdapter(new RecyclerAdapterWithHF(adapter));
        adapter.updateGames();

        if (TempDataManager.getInstance().hasGames()) {
            mNoGamesView.setVisibility(View.GONE);
        } else {
            mNoGamesView.setVisibility(View.VISIBLE);
        }

        mRefreshView.refreshComplete();
        mRefreshView.loadMoreComplete(true);
    }

    @Override
    public void onLoadFailedToGetGames() {
        mRefreshView.refreshComplete();
        mRefreshView.loadMoreComplete(true);
    }

    @Override
    public void onViewGame(Game game) {
        gotoGameScreen(game);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_filter:
                OnPlacePickFilter();
                break;
            case R.id.btn_setting:
                OnSetting();
                break;
            case R.id.btn_cart:
                gotoCartScreen();
                break;
        }
    }

    @Override
    public void OnChangedFilterOption() {
        TempDataManager.getInstance().clearGames();
        onLoadGames();
        loadGames();
    }

    private void OnPlacePickFilter() {
        TempDataManager.getInstance().setFilterListener(this);
        addFragment(UIListener.HomeActivityListener.FRAGMENT_PLACEPICK_FILTER);
    }

    private void makeSpanHint() {
        SpannableStringBuilder builder = new SpannableStringBuilder();

        String strHint1 = String.format(getCurrentActivity().getResources().getString(R.string.hint_kingpin1), DataManager.getInstance().getMonthlyPot());
        String strHint2 = getCurrentActivity().getResources().getString(R.string.hint_kingpin2);

        SpannableString spanHint1 = new SpannableString(strHint1);
        spanHint1.setSpan(new ForegroundColorSpan(getCurrentActivity().getResources().getColor(R.color.color_white)), 0, strHint1.length(), 0);
        builder.append(spanHint1);
        builder.append(" ");

        SpannableString spanHint2 = new SpannableString(strHint2);
        spanHint2.setSpan(new ActionSpan(strHint2, getCurrentActivity().getResources().getColor(R.color.color_white), this), 0, strHint2.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append(spanHint2);

        mHintView.setText(builder, TextView.BufferType.SPANNABLE);
        mHintView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    public void onClicekedSpan(String spanString) {
        String strHint2 = getCurrentActivity().getResources().getString(R.string.hint_kingpin2);

        if(spanString.equals(strHint2)) {
            gotoRuleScreen();
        }
    }

    private void loadGames() {
        mNoGamesView.setText(String.format("There are no %s games available, filter on the top left to find upcoming games to pick", DataManager.getInstance().getCurrentPlacebetFilterSportName()));
        mRefreshView.autoRefresh(true);
        showCartBadge(mBubbleView);
    }
}

