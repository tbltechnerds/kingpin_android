package com.kingpin.ui.fragment;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chanven.lib.cptr.PtrDefaultHandler;
import com.chanven.lib.cptr.PtrFrameLayout;
import com.chanven.lib.cptr.recyclerview.RecyclerAdapterWithHF;
import com.kingpin.R;
import com.kingpin.common.manager.DataManager;
import com.kingpin.common.manager.TempDataManager;
import com.kingpin.listener.UIListener;
import com.kingpin.mvp.model.BaseStatistics;
import com.kingpin.mvp.model.Bet;
import com.kingpin.mvp.model.FilterOptions;
import com.kingpin.mvp.model.GraphStatistics;
import com.kingpin.mvp.model.Kingpin;
import com.kingpin.mvp.model.Profile;
import com.kingpin.mvp.model.Statistics;
import com.kingpin.mvp.presenter.UserProfilePresenter;
import com.kingpin.mvp.view.UserProfileView;
import com.kingpin.ui.activity.StartActivity;
import com.kingpin.ui.adapter.BetAdapter;
import com.kingpin.ui.adapter.BetHistoryAdapter;
import com.kingpin.ui.adapter.StatisticAdapter;
import com.kingpin.ui.adapter.UpcomingBetAdapter;
import com.kingpin.ui.widget.RefreshLayout;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;

import butterknife.BindView;

public class UserProfileFragment extends BaseFragment<UserProfileView, UserProfilePresenter> implements UserProfileView, View.OnClickListener {

    private static final int TAB_STATISTICS = 0;
    private static final int TAB_BET_HISTORY = 1;
    private static final int TAB_UPCOMING_BET = 2;
    private static final int TAB_BETTING_STRATEGY = 3;

    public static final int SORT_DATE = 0;
    public static final int SORT_AMOUNT_BET = 1;
    public static final int SORT_AMOUNT_WON = 2;

    @BindView(R.id.btn_back) ImageView btnBack;
    @BindView(R.id.btn_filter) ImageView btnFilter;
    @BindView(R.id.bubble_cart) TextView textBubble;
    @BindView(R.id.tab_statics) TextView tabStatistics;
    @BindView(R.id.tab_bet_history) TextView tabBetHistory;
    @BindView(R.id.tab_upcoming) TextView tabUpcomingBet;
    @BindView(R.id.tab_betting_strategy) TextView tabBettingStrategy;
    @BindView(R.id.lyt_pull_statistic) RefreshLayout mRefreshStatisticView;
    @BindView(R.id.lyt_pull_bet_history) RefreshLayout mRefreshBetHistoryView;
    @BindView(R.id.lyt_pull_upcoming_bet) RefreshLayout mRefreshUpcomingBetView;
    @BindView(R.id.txt_status) TextView mStatusView;
    @BindView(R.id.sort_date) TextView sortDateView;
    @BindView(R.id.sort_amount_bet) TextView sortAmountBetView;
    @BindView(R.id.sort_amount_won) TextView sortAmountWonView;
    @BindView(R.id.lyt_sort) LinearLayout sortLayout;
    @BindView(R.id.lyt_table_header) FrameLayout headerLayout;
    @BindView(R.id.lyt_header_history) LinearLayout historyHeaderLayout;
    @BindView(R.id.lyt_header_upcoming) LinearLayout upcomoingHeaderLayout;
    @BindView(R.id.txt_name) TextView mNameView;
    @BindView(R.id.txt_city) TextView mCityView;
    @BindView(R.id.txt_rank) TextView mRankView;
    @BindView(R.id.txt_days_betting) TextView mDaysBettingView;
    @BindView(R.id.txt_bets) TextView mBetCountView;
    @BindView(R.id.txt_winnings) TextView mWinningsView;
    @BindView(R.id.txt_top_sports) TextView mTopSportView;
    @BindView(R.id.txt_win_loss) TextView mWinLossView;
    @BindView(R.id.txt_yesterday_winnings) TextView mYesterdayWinnings;
    @BindView(R.id.txt_followers) TextView mFollowers;
    @BindView(R.id.img_profile) RoundedImageView mProfileView;
    @BindView(R.id.list_statistics) RecyclerView mStatisticsView;
    @BindView(R.id.list_bet_history) RecyclerView mBetHistoryView;
    @BindView(R.id.list_upcoming_bet) RecyclerView mUpcomingBetsView;
    @BindView(R.id.txt_betting_strategy) TextView mBettingStrategy;
    @BindView(R.id.lyt_social) LinearLayout mSocial;
    @BindView(R.id.lyt_twitter) LinearLayout mTwitter;
    @BindView(R.id.lyt_instagram) LinearLayout mInstagram;
    @BindView(R.id.txt_twitter_username) TextView mTwitterUsername;
    @BindView(R.id.txt_instagram_username) TextView mInstagramUsername;

    private int mTabIndex;
    private int mSortIndex;
    private boolean mRevert;
    private StatisticAdapter mStatisticAdapter;
    private BetHistoryAdapter mBetHistoryAdapter;
    private UpcomingBetAdapter mUpcomingBetAdapter;
    private boolean mFirstLoading;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_user_profile;
    }

    @NonNull
    @Override
    public UserProfilePresenter createPresenter() {
        return new UserProfilePresenter();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        DataManager.getInstance().getFilterOptions().setInitializeFilterOption();

        if(TempDataManager.getInstance().isShowUpcoming())
            mTabIndex = TAB_UPCOMING_BET;
        else
            mTabIndex = TAB_STATISTICS;
        mSortIndex = SORT_DATE;
        mRevert = false;
        mFirstLoading = true;

        btnBack.setOnClickListener(this);
        btnFilter.setOnClickListener(this);
        tabStatistics.setOnClickListener(this);
        tabBetHistory.setOnClickListener(this);
        tabUpcomingBet.setOnClickListener(this);
        tabBettingStrategy.setOnClickListener(this);
        mStatusView.setOnClickListener(this);
        sortDateView.setOnClickListener(this);
        sortAmountBetView.setOnClickListener(this);
        sortAmountWonView.setOnClickListener(this);

        mTwitter.setOnClickListener(this);
        mInstagram.setOnClickListener(this);

        mStatisticsView.setLayoutManager(new LinearLayoutManager(getCurrentActivity(), LinearLayout.VERTICAL, false));
        mStatisticAdapter = new StatisticAdapter(this);
        mStatisticsView.setAdapter(new RecyclerAdapterWithHF(mStatisticAdapter));

        mBetHistoryView.setLayoutManager(new LinearLayoutManager(getCurrentActivity(), LinearLayout.VERTICAL, false));
        mBetHistoryAdapter = new BetHistoryAdapter(new ArrayList<Bet>(), this);
        mBetHistoryView.setAdapter(new RecyclerAdapterWithHF(mBetHistoryAdapter));

        mUpcomingBetsView.setLayoutManager(new LinearLayoutManager(getCurrentActivity(), LinearLayout.VERTICAL, false));
        mUpcomingBetAdapter = new UpcomingBetAdapter(this);
        mUpcomingBetsView.setAdapter(new RecyclerAdapterWithHF(mUpcomingBetAdapter));

        mRefreshStatisticView.setPtrHandler(new PtrDefaultHandler() {
            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                getPresenter().getKingpinDetail();
            }
        });

        mRefreshBetHistoryView.setPtrHandler(new PtrDefaultHandler() {
            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                getPresenter().getBettingHistory();
            }
        });

        mRefreshUpcomingBetView.setPtrHandler(new PtrDefaultHandler() {
            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                getPresenter().getUpcomingBets();
            }
        });

        updateUI();
        getPresenter().getKingpinDetail();
    }

    @Override
    public void onResume() {
        super.onResume();

        showCartBadge(textBubble);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                removeFragment(UIListener.HomeActivityListener.FRAGMENT_USER_PROFILE);
                break;
            case R.id.btn_filter:
                OnFilter(this);
                break;
            case R.id.tab_statics:
                onTab(TAB_STATISTICS);
                break;
            case R.id.tab_bet_history:
                onTab(TAB_BET_HISTORY);
                break;
            case R.id.tab_upcoming:
                onTab(TAB_UPCOMING_BET);
                break;
            case R.id.tab_betting_strategy:
                onTab(TAB_BETTING_STRATEGY);
                break;
            case R.id.txt_status:
                onStatus();
                break;
            case R.id.sort_date:
                onSort(SORT_DATE);
                break;
            case R.id.sort_amount_bet:
                onSort(SORT_AMOUNT_BET);
                break;
            case R.id.sort_amount_won:
                onSort(SORT_AMOUNT_WON);
                break;
            case R.id.lyt_twitter:
                onTwitter();
                break;
            case R.id.lyt_instagram:
                onInstagram();
                break;
        }
    }

    private void onTwitter() {
        Kingpin kingpin = TempDataManager.getInstance().getSelectedKingpin();
        if(kingpin != null) {
            openUrl("https://www.twitter.com/" + kingpin.getTwitter());
        }
    }

    private void onInstagram() {
        Kingpin kingpin = TempDataManager.getInstance().getSelectedKingpin();
        if(kingpin != null) {
            openUrl("https://www.instagram.com/" + kingpin.getInstagram());
        }
    }

    @Override
    public void DownloadAvatarCompleted(Bitmap bitmap) {
        if(bitmap != null && mProfileView != null)
            mProfileView.setImageBitmap(bitmap);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void OnLoadKingpinDetail(Kingpin kingpin) {

        ArrayList<BaseStatistics> statisticList = new ArrayList<>();
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Days betting", String.valueOf(kingpin.getDaysBetting())));
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "# of bets", String.valueOf(kingpin.getCountBets())));
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Winnings / (losses)", "$" + kingpin.getWinningLosses()));
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "W-L-D", String.format("%d-%d-%d", kingpin.getCountWin(), kingpin.getCountLoss(), kingpin.getCountDraw())));
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Win %", kingpin.getPercentWin() + "%"));
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Avg. bet size", "$" + kingpin.getAvgBetSize()));
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Avg. bet risk amount", kingpin.getAvgBetRisk() + "%"));
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Avg. win size", "$" + kingpin.getAvgWinSize()));
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "# of bets per 30 days", String.valueOf(kingpin.getBetsPer30Days())));

        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Last 30 days' # of bets", String.valueOf(kingpin.getCount30daysBets())));
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Last 30 days' winnings", "$" + kingpin.getLast30daysWinnings()));
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Last 30 days' W-L-D", String.format("%d-%d-%d", kingpin.getCount30daysWin(), kingpin.getCount30daysLoss(), kingpin.getCount30daysDraw())));
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Last 30 days' win %", kingpin.getLast30daysPercentWin() + "%"));

        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Last 7 days' # of bets", String.valueOf(kingpin.getCount7daysBets())));
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Last 7 days' winnings", "$" + kingpin.getLast7daysWinnings()));
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Last 7 days' W-L-D", String.format("%d-%d-%d", kingpin.getCount7daysWin(), kingpin.getCount7daysLoss(), kingpin.getCount7daysDraw())));
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Last 7 days' win %", kingpin.getLast7daysPercentWin() + "%"));

        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Yesterday' # of bets", String.valueOf(kingpin.getCount1daysBets())));
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Yesterday' winnings", "$" + kingpin.getLast1daysWinnings()));
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Yesterday' W-L-D", String.format("%d-%d-%d", kingpin.getCount1daysWin(), kingpin.getCount1daysLoss(), kingpin.getCount1daysDraw())));
        statisticList.add(new Statistics(BaseStatistics.TYPE_NORMAL, "Yesterday' win %", kingpin.getLast1daysPercentWin() + "%"));

        statisticList.add(new GraphStatistics(BaseStatistics.TYPE_GRAPH, "Winnings / losses over time", "$ " + kingpin.getWinningLosses(), kingpin.getDatesForUpperGraph(), kingpin.getValuesForUpperGraph()));
        mStatisticAdapter.updateStatistics(statisticList);

        loadInfos();

        if(mTabIndex == TAB_BET_HISTORY)
            getPresenter().getBettingHistory();
        else if(mTabIndex == TAB_UPCOMING_BET)
            getPresenter().getUpcomingBets();
        mRefreshStatisticView.refreshComplete();
    }

    @Override
    public void OnLoadBetHistories(ArrayList<Bet> bets) {
        mBetHistoryAdapter.updateBetHistories(bets);
        loadInfos();
        mRefreshBetHistoryView.refreshComplete();
    }

    @Override
    public void OnLoadUpcomingBets(ArrayList<Bet> bets) {
        Kingpin kingpin = TempDataManager.getInstance().getSelectedKingpin();
        mUpcomingBetAdapter.updateUpcomingBets(bets, !canFollow(kingpin), false);

        if(bets.size() == 0 && mFirstLoading) {
            mTabIndex = TAB_BET_HISTORY;
            updateUI();
            getPresenter().getBettingHistory();
        }
        else {
            loadInfos();
        }

        mFirstLoading = false;
        mRefreshUpcomingBetView.refreshComplete();
    }

    @Override
    public void OnChooseUpcomingBet() {
        Kingpin kingpin = TempDataManager.getInstance().getSelectedKingpin();
        if(kingpin != null && canFollow(kingpin)) {
            if(DataManager.getInstance().isLoggedInStatus()) {
                Profile me = DataManager.getInstance().getAccount();
                if(me != null) {
                    if(me.getMembership() == 0) {
                        updateProfileWithInAppPurchase();
                    }
                    else {
                        if (me.getId() != kingpin.getId()) {
                            if (kingpin.isFollowed()) {
                                getPresenter().unfollowKingping(kingpin);
                            } else {
                                getPresenter().followKingping(kingpin);
                            }
                        }
                    }
                }
            }
            else {
                getCurrentActivity().openLandingScreen();
            }
        }
    }

    @Override
    public void OnChangedFilterOption() {
        switch (mTabIndex) {
            case TAB_STATISTICS:
                mStatisticAdapter.updateStatistics(new ArrayList<BaseStatistics>());
                break;
            case TAB_BET_HISTORY:
                mBetHistoryAdapter.updateBetHistories(new ArrayList<Bet>());
                break;
            case TAB_UPCOMING_BET:
                mUpcomingBetAdapter.updateUpcomingBets(new ArrayList<Bet>(), false, false);
                break;
        }

        loadInfos();
        getPresenter().getKingpinDetail();
    }

    private void updateUI() {
        updateSort();
        updateTab();
        loadInfos();
    }

    private void updateTab() {
        tabStatistics.setSelected(false);
        tabBetHistory.setSelected(false);
        tabUpcomingBet.setSelected(false);
        tabBettingStrategy.setSelected(false);

        switch (mTabIndex) {
            case TAB_STATISTICS:
                tabStatistics.setSelected(true);
                sortLayout.setVisibility(View.GONE);
                headerLayout.setVisibility(View.GONE);
                mBettingStrategy.setVisibility(View.GONE);
                break;
            case TAB_BET_HISTORY:
                tabBetHistory.setSelected(true);
                sortLayout.setVisibility(View.VISIBLE);
                headerLayout.setVisibility(View.VISIBLE);
                historyHeaderLayout.setVisibility(View.VISIBLE);
                upcomoingHeaderLayout.setVisibility(View.GONE);
                mBettingStrategy.setVisibility(View.GONE);
                break;
            case TAB_UPCOMING_BET:
                tabUpcomingBet.setSelected(true);
                sortLayout.setVisibility(View.GONE);
                headerLayout.setVisibility(View.VISIBLE);
                historyHeaderLayout.setVisibility(View.GONE);
                upcomoingHeaderLayout.setVisibility(View.VISIBLE);
                mBettingStrategy.setVisibility(View.GONE);
                break;
            case TAB_BETTING_STRATEGY:
                tabBettingStrategy.setSelected(true);
                sortLayout.setVisibility(View.GONE);
                headerLayout.setVisibility(View.GONE);
                historyHeaderLayout.setVisibility(View.GONE);
                upcomoingHeaderLayout.setVisibility(View.GONE);
                mBettingStrategy.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void updateSort() {
        sortDateView.setSelected(false);
        sortAmountBetView.setSelected(false);
        sortAmountWonView.setSelected(false);

        switch (mSortIndex) {
            case SORT_DATE:
                sortDateView.setSelected(true);
                break;
            case SORT_AMOUNT_BET:
                sortAmountBetView.setSelected(true);
                break;
            case SORT_AMOUNT_WON:
                sortAmountWonView.setSelected(true);
                break;
        }
    }

    private void onTab(int tabIndex) {
        mTabIndex = tabIndex;
        updateUI();

        if(mTabIndex == TAB_BET_HISTORY) {
            if(mBetHistoryAdapter.getBetHistoryList().size() == 0)
                getPresenter().getBettingHistory();
        }
        else if(mTabIndex == TAB_UPCOMING_BET) {
            if(mUpcomingBetAdapter.getUpcomingBetList().size() == 0)
                getPresenter().getUpcomingBets();
        }

        isDisableTimeperiodFilter = (mTabIndex == TAB_UPCOMING_BET);
    }

    private void onSort(int sortIndex) {
        if(mSortIndex == sortIndex) {
            mRevert = !mRevert;
        }
        else {
            mRevert = false;
            mSortIndex = sortIndex;
        }
        updateUI();
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    private void loadInfos() {
        Kingpin kingpin = TempDataManager.getInstance().getSelectedKingpin();
        if(kingpin == null) {
            mNameView.setText("");
            mCityView.setText("");
            mRankView.setText("NR");
            mDaysBettingView.setText("0");
            mBetCountView.setText("0");
            mWinningsView.setText("$0");
            mTopSportView.setText("");
            mWinLossView.setText("0-0-0\n(0%)");
            mYesterdayWinnings.setText("$0");
            mFollowers.setText("0");
            mStatusView.setVisibility(View.GONE);
            mSocial.setVisibility(View.GONE);
        }
        else {
            mNameView.setText(kingpin.getName());
            mCityView.setText(kingpin.getCity());
            mRankView.setText((kingpin.getRank() != 1000000) ? String.valueOf(kingpin.getRank()) : "NR");
            mDaysBettingView.setText(String.valueOf(kingpin.getDaysBetting()));
            mBetCountView.setText(String.valueOf(kingpin.getCountBets()));
            mWinningsView.setText("$" + kingpin.getWinningLosses());
            mTopSportView.setText(FilterOptions.getMainFilterOptionString(kingpin.getTopSport()));
            mWinLossView.setText(String.format("%d-%d-%d\n(%d%s)", kingpin.getCountWin(), kingpin.getCountLoss(), kingpin.getCountDraw(), kingpin.getPercentWin(), "%"));
            mYesterdayWinnings.setText("$" + kingpin.getLast1daysWinnings());
            mFollowers.setText(String.format("%d", kingpin.getCountFollowers()));

            Profile me = DataManager.getInstance().getAccount();
            if(me == null) {
                mStatusView.setVisibility(View.VISIBLE);
                mStatusView.setText("Login");
            }
            else {
                if(me.getId() == kingpin.getId()) {
                    mStatusView.setVisibility(View.GONE);
                }
                else {
                    if(me.getMembership() == 0) {
                        mStatusView.setVisibility(View.VISIBLE);
                        mStatusView.setText("View\nPicks");
                    }
                    else {
                        if(canFollow(kingpin)) {
                            mStatusView.setVisibility(View.VISIBLE);
                            mStatusView.setText("Follow");
                        }
                        else {
                            mStatusView.setVisibility(View.VISIBLE);
                            mStatusView.setText("Unfollow");
                        }
                    }
                }
            }

            Bitmap bitmap = downloadImage(kingpin.getAvatar());
            if(bitmap != null || mProfileView != null)
                mProfileView.setImageBitmap(bitmap);

            if (kingpin.getTwitter().isEmpty()) {
                mTwitter.setVisibility(View.GONE);
            } else {
                mTwitter.setVisibility(View.VISIBLE);
                mTwitterUsername.setText(kingpin.getTwitter());
            }

            if (kingpin.getInstagram().isEmpty()) {
                mInstagram.setVisibility(View.GONE);
            } else {
                mInstagram.setVisibility(View.VISIBLE);
                mInstagramUsername.setText(kingpin.getInstagram());
            }

            if (kingpin.getTwitter().isEmpty() && kingpin.getInstagram().isEmpty()) {
                mSocial.setVisibility(View.GONE);
            } else {
                mSocial.setVisibility(View.VISIBLE);
            }

            if (kingpin.getBettingStrategy().isEmpty()) {
                tabBettingStrategy.setVisibility(View.GONE);
            } else {
                tabBettingStrategy.setVisibility(View.VISIBLE);
            }
        }

        switch (mTabIndex) {
            case TAB_STATISTICS:
                mStatisticAdapter.notifyDataSetChanged();
                mRefreshStatisticView.setVisibility(View.VISIBLE);
                mRefreshBetHistoryView.setVisibility(View.GONE);
                mRefreshUpcomingBetView.setVisibility(View.GONE);
                break;
            case TAB_BET_HISTORY:
                mBetHistoryAdapter.sort(mSortIndex, mRevert);
                mRefreshStatisticView.setVisibility(View.GONE);
                mRefreshBetHistoryView.setVisibility(View.VISIBLE);
                mRefreshUpcomingBetView.setVisibility(View.GONE);
                break;
            case TAB_UPCOMING_BET:
                mUpcomingBetAdapter.sort();
                mRefreshStatisticView.setVisibility(View.GONE);
                mRefreshBetHistoryView.setVisibility(View.GONE);
                mRefreshUpcomingBetView.setVisibility(View.VISIBLE);
                break;
        }
    }

    private boolean canFollow(Kingpin kingpin) {
        Profile me = DataManager.getInstance().getAccount();
        if(me != null && me.getId() == kingpin.getId())
            return false;

        return !kingpin.isFollowed();
    }

    private void onStatus() {
        Profile me = DataManager.getInstance().getAccount();
        if (me == null) {
            getCurrentActivity().openLandingScreen();
        } else {
            if (me.getMembership() == 0) { // upgrade
                getCurrentActivity().upgradeProfileWithInAppPurchase();
            } else {
                Kingpin kingpin = TempDataManager.getInstance().getSelectedKingpin();
                if (kingpin != null) {
                    if (me.getId() != kingpin.getId()) {
                        if (kingpin.isFollowed()) {
                            getPresenter().unfollowKingping(kingpin);
                        } else {
                            getPresenter().followKingping(kingpin);
                        }
                    }
                }
            }
        }
    }
    @Override
    public void OnCompletedFollow() {
        getPresenter().getKingpinDetail();
    }

    @Override
    public void OnCompletedUnfollow() {
        getPresenter().getKingpinDetail();
    }
}
