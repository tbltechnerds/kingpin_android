package com.kingpin.ui.widget;

import android.content.Context;
import android.util.AttributeSet;

import com.chanven.lib.cptr.PtrFrameLayout;
import com.chanven.lib.cptr.loadmore.ILoadMoreViewFactory;

public class RefreshLayout1 extends PtrFrameLayout {

    private RefreshHeader1 mHeader;

    public RefreshLayout1(Context context) {
        super(context);
        initViews();
    }

    public RefreshLayout1(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();
    }

    public RefreshLayout1(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initViews();
    }

    private void initViews() {
        mHeader = new RefreshHeader1(getContext());
        setHeaderView(mHeader);
        addPtrUIHandler(mHeader);

        ILoadMoreViewFactory loadMoreViewFactory = new LoadMoreFooter();
        setFooterView(loadMoreViewFactory);
    }

    public RefreshHeader1 getHeader() {
        return mHeader;
    }
}
