package com.kingpin.ui.widget;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class TabDividerItemDecorator extends RecyclerView.ItemDecoration {

    public static final int HORIZONTAL = 0;
    public static final int VERTICAL = 1;

    private Drawable mDivider;
    private int mOrientation;

    public TabDividerItemDecorator(Drawable divider, int orientation) {
        mDivider = divider;
        mOrientation = orientation;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        if(parent.getChildAdapterPosition(view) == 0)
            return;

        if(mOrientation == HORIZONTAL)
            outRect.left = mDivider.getIntrinsicWidth();
        else if(mOrientation == VERTICAL)
            outRect.top = mDivider.getIntrinsicHeight();
    }

    @Override
    public void onDraw(@NonNull Canvas canvas, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        switch (mOrientation) {
            case HORIZONTAL:
                drawHorizontal(canvas, parent);
                break;
            case VERTICAL:
                drawVeritical(canvas, parent);
                break;
        }
    }

    private void drawHorizontal(Canvas canvas, RecyclerView parent) {
        int dividerTop = parent.getPaddingTop();
        int dividerBottom = parent.getHeight() - parent.getPaddingBottom();

        int childCount = parent.getChildCount();
        for(int i = 0; i < childCount - 2; i++) {
            View child = parent.getChildAt(i);

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();
            int dividerLeft = child.getRight() + params.rightMargin;
            int dividerRight = dividerLeft + mDivider.getIntrinsicWidth();

            mDivider.setBounds(dividerLeft, dividerTop, dividerRight, dividerBottom);
            mDivider.draw(canvas);
        }
    }

    private void drawVeritical(Canvas canvas, RecyclerView parent) {
        int dividerLeft = parent.getPaddingLeft();
        int dividerRight = parent.getWidth() - parent.getPaddingRight();

        int childCount = parent.getChildCount();
        for(int i = 0; i < childCount - 2; i++) {
            View child = parent.getChildAt(i);

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();
            int dividerTop = child.getBottom() + params.bottomMargin;
            int dividerBottom = dividerTop + mDivider.getIntrinsicHeight();

            mDivider.setBounds(dividerLeft, dividerTop, dividerRight, dividerBottom);
            mDivider.draw(canvas);
        }
    }
}
