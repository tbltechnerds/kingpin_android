package com.kingpin.ui.widget;

import android.support.annotation.NonNull;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

import com.kingpin.listener.SpanListener;

public class ActionSpan extends ClickableSpan {

    private String spanString;
    private int spanColor;
    private SpanListener spanListener;

    public ActionSpan(String span, int color, SpanListener listener) {
        super();
        this.spanString = span;
        this.spanColor = color;
        this.spanListener = listener;
    }

    @Override
    public void onClick(@NonNull View widget) {
        if(this.spanListener != null)
            this.spanListener.onClicekedSpan(this.spanString);
    }

    public void updateDrawState(@NonNull TextPaint ds) {
        ds.setUnderlineText(true);
        ds.setColor(spanColor);
    }
}
