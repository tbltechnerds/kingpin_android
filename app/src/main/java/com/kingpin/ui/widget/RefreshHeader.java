package com.kingpin.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.chanven.lib.cptr.PtrFrameLayout;
import com.chanven.lib.cptr.PtrUIHandler;
import com.chanven.lib.cptr.indicator.PtrIndicator;
import com.kingpin.R;

public class RefreshHeader extends FrameLayout implements PtrUIHandler {

    private ProgressBar mProgressbar;
    private ImageView mLoadView;

    public RefreshHeader(Context context) {
        super(context);
        initViews();
    }

    public RefreshHeader(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();
    }

    public RefreshHeader(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initViews();
    }

    protected void initViews() {
        View header = LayoutInflater.from(getContext()).inflate(R.layout.view_loading, this);
        mProgressbar = header.findViewById(R.id.progressbar);
        mLoadView = header.findViewById(R.id.img_load);
    }

    @Override
    public void onUIReset(PtrFrameLayout frame) {
        mLoadView.setVisibility(View.VISIBLE);
        mProgressbar.setVisibility(View.GONE);
    }

    @Override
    public void onUIRefreshPrepare(PtrFrameLayout frame) {
        mLoadView.setVisibility(View.VISIBLE);
        mProgressbar.setVisibility(View.GONE);
    }

    @Override
    public void onUIRefreshBegin(PtrFrameLayout frame) {
        mLoadView.setVisibility(View.GONE);
        mProgressbar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onUIRefreshComplete(PtrFrameLayout frame) {
        mLoadView.setVisibility(View.VISIBLE);
        mProgressbar.setVisibility(View.GONE);
    }

    @Override
    public void onUIPositionChange(PtrFrameLayout frame, boolean isUnderTouch, byte status, PtrIndicator ptrIndicator) {
    }
}
