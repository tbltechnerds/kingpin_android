package com.kingpin.ui.widget;

import android.content.Context;
import android.util.AttributeSet;

import com.chanven.lib.cptr.PtrFrameLayout;
import com.chanven.lib.cptr.loadmore.ILoadMoreViewFactory;

public class RefreshLayout extends PtrFrameLayout {

    private RefreshHeader mHeader;

    public RefreshLayout(Context context) {
        super(context);
        initViews();
    }

    public RefreshLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();
    }

    public RefreshLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initViews();
    }

    private void initViews() {
        mHeader = new RefreshHeader(getContext());
        setHeaderView(mHeader);
        addPtrUIHandler(mHeader);

        ILoadMoreViewFactory loadMoreViewFactory = new LoadMoreFooter();
        setFooterView(loadMoreViewFactory);
    }

    public RefreshHeader getHeader() {
        return mHeader;
    }
}
