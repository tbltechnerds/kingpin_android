package com.kingpin.ui.widget;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.chanven.lib.cptr.loadmore.ILoadMoreViewFactory;
import com.kingpin.R;

public class LoadMoreFooter implements ILoadMoreViewFactory {

    @Override
    public ILoadMoreView madeLoadMoreView() {
        return new LoadMoreHelper();
    }

    private class LoadMoreHelper implements ILoadMoreView {

        protected View mFooterView;
        protected ImageView mLoadView;
        protected ProgressBar mProgressbar;

        protected View.OnClickListener onClickRefreshListener;

        @Override
        public void init(FootViewAdder footViewHolder, View.OnClickListener onClickRefreshListener) {
            mFooterView = footViewHolder.addFootView(R.layout.view_loading);
            mLoadView = mFooterView.findViewById(R.id.img_load);
            mProgressbar = mFooterView.findViewById(R.id.progressbar);
            this.onClickRefreshListener = onClickRefreshListener;
            showNormal();
        }

        @Override
        public void showNormal() {
            mProgressbar.setVisibility(View.GONE);
            mLoadView.setVisibility(View.VISIBLE);
            mFooterView.setOnClickListener(onClickRefreshListener);
        }

        @Override
        public void showLoading() {
            mLoadView.setVisibility(View.GONE);
            mProgressbar.setVisibility(View.VISIBLE);
            mFooterView.setOnClickListener(null);
        }

        @Override
        public void showFail(Exception exception) {
            mProgressbar.setVisibility(View.GONE);
            mLoadView.setVisibility(View.VISIBLE);
            mFooterView.setOnClickListener(onClickRefreshListener);
        }

        @Override
        public void showNomore() {
            mProgressbar.setVisibility(View.GONE);
            mLoadView.setVisibility(View.VISIBLE);
            mFooterView.setOnClickListener(null);
        }

        @Override
        public void setFooterVisibility(boolean isVisible) {
            mFooterView.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        }
    }
}
